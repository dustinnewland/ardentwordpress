<?php 
$post_password = $post->post_password;
$logged_in = post_password_required($post->ID);
$enable_header_pw = $post_password?get_post_meta($post->ID, 'enable_header_pw_only', true):false;
$is_event = \Ardent\Plugins\Eventscalendar\Helper::isCalendar();
get_header(); ?>

<?php if(!($enable_header_pw && !$logged_in)){ nectar_page_header($is_event?get_nectar_theme_options()['the-events-calendar-page']:$post->ID); } ?>

<?php
$fp_options = nectar_get_full_page_options();
extract($fp_options);
?>

<div class="container-wrap">
	
	<div class="<?php if($page_full_screen_rows != 'on') echo 'container'; ?> main-content">
		
		<div class="row">
			
			<?php 

			//breadcrumbs
			if ( function_exists( 'yoast_breadcrumb' ) && !is_home() && !is_front_page() ){ yoast_breadcrumb('<p id="breadcrumbs">','</p>'); } 

			//buddypress
			global $bp; 
			if($bp && !bp_is_blog_page()) echo '<h1>' . get_the_title() . '</h1>';
                         
                        //fullscreen rows
			if($page_full_screen_rows == 'on') echo '<div id="nectar_fullscreen_rows" data-animation="'.$page_full_screen_rows_animation.'" data-row-bg-animation="'.$page_full_screen_rows_bg_img_animation.'" data-animation-speed="'.$page_full_screen_rows_animation_speed.'" data-content-overflow="'.$page_full_screen_rows_content_overflow.'" data-mobile-disable="'.$page_full_screen_rows_mobile_disable.'" data-dot-navigation="'.$page_full_screen_rows_dot_navigation.'" data-footer="'.$page_full_screen_rows_footer.'" data-anchors="'.$page_full_screen_rows_anchors.'">';
                    
			if($is_events){ ?>
                            <div class="wpb_row vc_row-fluid vc_row full-width-section standard_section"><div class="row-bg-wrap instance-0"></div><div class="col span_12 dark left"><div style="padding:0 0 20px 0;" class="vc_col-sm-8 wpb_column column_container col padding-5-percent instance-0" data-using-bg="true" data-bg-cover="" data-padding-pos="all" data-has-bg-color="true" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0"><div class="wpb_wrapper"><div class="wpb_text_column wpb_content_element "><div class="wpb_wrapper">
                        <?php }
                                                    
			if(have_posts()) : while(have_posts()) : the_post();
                            the_content();
                        endwhile; endif;
                                
                        if($is_events){ ?></div></div></div></div></div></div><?php }
                        
                        if($page_full_screen_rows == 'on') echo '</div>'; ?>
	
		</div><!--/row-->
		
	</div><!--/container-->
	
</div><!--/container-wrap-->

<?php get_footer(); ?>