<?php
\Ardent\Salient\Section::getInstance('Logo & General Styling', array(
    'id' => 'header-nav-general',
    'fields' => array(
        array('type' => 'input.wp.media', 'name' => 'mobile-logo', 'after' => 'retina-logo', 'label' => 'Mobile Logo Upload', 'subtitle' => 'Upload your logo here and enter the height of it below'),
        array('type' => 'image_select', 'name' => 'mobile_header_format', 'after' => 'mobile-logo', 'label' => 'Mobile Header Layout', 'subtitle' => 'Please select the layout you desire', 'options' => array('default' => array('title' => 'Left Aligned', 'img' => get_stylesheet_directory_uri() . '/img/mobile-logo-left.png'), 'center-aligned' => array('title' => 'Center Aligned', 'img' => get_stylesheet_directory_uri() . '/img/mobile-logo-centered.png')), 'required' => array( 'use-logo', '=', '1' ))
    )
));