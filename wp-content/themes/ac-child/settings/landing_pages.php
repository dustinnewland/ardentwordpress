<?php

$settings = array(
	'Landing Page' => array(
		array('type' => 'input.checkbox', 'name' => 'hide_nav', 'label' => 'Hide Secondary Nav'),
		array('type' => 'input.checkbox', 'name' => 'hide_header', 'label' => 'Hide Header'),
		array('type' => 'input.checkbox', 'name' => 'hide_footer', 'label' => 'Hide Footer'),
	)
);
foreach ($settings as $mbname => $fields) {
	$mb = \Ardent\Wp\Metabox::getInstance('page', $mbname, 'side');
	foreach ($fields as $field) {
		$mb->add_field($field);
	}
}


add_filter('views_edit-page', function($views) {
	global $post_type_object;
	$post_type = $post_type_object->name;
	// Get count
	$q = new WP_Query(array(
		'post_type' => 'page',
		'post_status' => 'any',
		'meta_query' => array(
			'relation' => 'OR',
			array(
				'key' => 'hide_header',
				'value' => 1
			),
			array(
				'key' => 'hide_nav',
				'value' => 1
			),
			array(
				'key' => 'hide_footer',
				'value' => 1
			)
		)
	));
	$views['lp'] = "<a href='edit.php?post_type={$post_type}&lp=1'>Landing Pages</a> ($q->found_posts)";

	return $views;
});

add_action('pre_get_posts', function($q) {
	if ($q->is_admin && $q->is_main_query() && $q->get('post_type') == 'page' && isset($_REQUEST['lp'])) {
		$q->set('meta_query', array(
			'relation' => 'OR',
			array(
				'key' => 'hide_header',
				'value' => 1
			),
			array(
				'key' => 'hide_footer',
				'value' => 1
			)
		));
	}
	return $q;
});

