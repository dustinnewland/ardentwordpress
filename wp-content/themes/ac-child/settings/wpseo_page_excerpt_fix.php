<?php
add_filter('wpseo_metadesc', function($metadesc){
    global $post;
    if(!$metadesc){
        $metadesc = trim(preg_replace('/\s+/', ' ', \Ardent\Post::Excerpt(155,preg_replace('/\[[^\]]+\]/', '', $post->post_content))));
    }
    return $metadesc;
});