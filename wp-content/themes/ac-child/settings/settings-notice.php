<?php
\Ardent\Salient\Section::getInstance('Notice Settings', array(
    'description' => 'When using notices, all containers must have the base notice class. Classes available are: notice, notice-success, notice-warning, notice-error, notice-box',
    'icon_class' => 'el-warning-sign',
    'fields' => array(
        array('type' => 'input.wp.color', 'name' => 'notice-text-color', 'label' => 'General Notice Text Color', 'subtitle' => 'Default is #31708f. This color is also the border color for the box layout.', 'transparent' => false),
        array('type' => 'input.wp.color', 'name' => 'notice-bg-color', 'label' => 'General Notice Background Color', 'subtitle' => 'Default is #d9edf7.', 'transparent' => false),
        array('type' => 'input.wp.color', 'name' => 'notice-success-text-color', 'label' => 'Success Notice Text Color', 'subtitle' => 'Default is #3c763d. This color is also the border color for the box layout.', 'transparent' => false),
        array('type' => 'input.wp.color', 'name' => 'notice-success-bg-color', 'label' => 'Success Notice Background Color', 'subtitle' => 'Default is #dff0d8.', 'transparent' => false),
        array('type' => 'input.wp.color', 'name' => 'notice-warning-text-color', 'label' => 'Warning Notice Text Color', 'subtitle' => 'Default is #8a6d3b. This color is also the border color for the box layout.', 'transparent' => false),
        array('type' => 'input.wp.color', 'name' => 'notice-warning-bg-color', 'label' => 'Warning Notice Background Color', 'subtitle' => 'Default is #fcf8e3.', 'transparent' => false),
        array('type' => 'input.wp.color', 'name' => 'notice-error-text-color', 'label' => 'Error Notice Text Color', 'subtitle' => 'Default is #a94442. This color is also the border color for the box layout.', 'transparent' => false),
        array('type' => 'input.wp.color', 'name' => 'notice-error-bg-color', 'label' => 'Error Notice Background Color', 'subtitle' => 'Default is #f2dede.', 'transparent' => false),
        array('type' => 'input.text', 'name' => 'notice-box-roundness', 'label' => 'Box Border Roundness', 'subtitle' => 'Default is 4. Do not use px or %.'),
        array('type' => 'input.text', 'name' => 'notice-box-vertical-padding', 'label' => 'Box Vertical Padding', 'subtitle' => 'Default is 15. Do not use px or %.'),
        array('type' => 'input.text', 'name' => 'notice-box-horizontal-padding', 'label' => 'Box Horizontal Padding', 'subtitle' => 'Default is 15. Do not use px or %.')
    )
));