<?php
add_action('wp_head', function() {
    $options = get_nectar_theme_options();
    $notices = array('notice', 'notice-success', 'notice-warning', 'notice-error');
    $nops = array();
    foreach($notices as $notice){
        if($options[$notice.'-text-color']){
            $nops[$notice.'-text-color'] = $options[$notice.'-text-color'];
        }
        if($options[$notice.'-bg-color']){
            $nops[$notice.'-bg-color'] = $options[$notice.'-bg-color'];
        }
    }
    if($options['notice-box-roundness'] || $options['notice-box-roundness'] === '0'){ $nops['notice-box-roundness'] = $options['notice-box-roundness']; }
    if($options['notice-box-vertical-padding'] || $options['notice-box-vertical-padding'] === '0'){ $nops['notice-box-vertical-padding'] = $options['notice-box-vertical-padding']; }
    if($options['notice-box-horizontal-padding'] || $options['notice-box-horizontal-padding'] === '0'){ $nops['notice-box-horizontal-padding'] = $options['notice-box-horizontal-padding']; }
    if(count($nops)){
    ?><style type="text/css"><?php
        foreach($nops as $key => $val){
            if(strpos($key, 'notice-text')){
                echo '.'.str_replace('-text-color','',$key).'{ color:'.$val.'; }';
            }else if(strpos($key, '-text-color')){
                echo '.notice.'.str_replace('-text-color','',$key).'{ color:'.$val.'; }';
                echo '.notice.'.str_replace('-text-color','',$key).'.notice-box{ border-color:'.$val.'; }';
            }else if(strpos($key, '-bg-color')){
                echo '.notice.'.str_replace('-bg-color','',$key).'.notice-box{ background-color:'.$val.'; }';
            }else if(strpos($key, '-roundness')){
                echo '.notice.'.str_replace('-roundness','',$key).'{ border-radius:'.$val.'px; }';
            }else if(strpos($key, '-vertical-padding')){
                echo '.notice.'.str_replace('-vertical-padding','',$key).'{ padding-top:'.$val.'px;padding-bottom:'.$val.'px; }';
            }else if(strpos($key, '-horizontal-padding')){
                echo '.notice.'.str_replace('-horizontal-padding','',$key).'{ padding-left:'.$val.'px;padding-right:'.$val.'px; }';
            }
        }
    ?></style><?php
    }
}, 100, 0);