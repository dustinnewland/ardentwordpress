<?php
\Ardent\Salient\Section::getInstance('Footer', array(
    'fields' => array(
        array('id' => 'footer_columns', 'options' => array('6' => array('title' => '6 Columns', 'img' => get_stylesheet_directory_uri() . '/img/6col.png')))
    )
));