<?php
add_filter('upload_mimes', function($mimes) {
    if (is_admin() && is_super_admin()) {
        $mimes['svg'] = 'image/svg+xml';
    }
    return $mimes;
});
