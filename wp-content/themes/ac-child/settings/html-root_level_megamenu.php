<?php
add_action('wp_head', function() {
    $options = get_nectar_theme_options();
    $headerPadding = (!empty($options['header-padding'])) ? $options['header-padding'] : 28;
    ?>
    <style type="text/css">
        body div#header-outer.small-nav ul.root-level-megamenu{
            margin-top:<?php echo -abs(ceil($headerPadding/1.8)); ?>px !important;
        }
    </style>
    <?php
}, 100, 0);