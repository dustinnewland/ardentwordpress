<?php
add_action("wp",function(){
    if(\Ardent\Plugins\Eventscalendar\Helper::isCalendar()){
        add_filter( 'get_post_metadata', function($metadata, $object_id, $meta_key, $single){
            $ops = get_nectar_theme_options();
            if((int)$ops['the-events-calendar-page'] == $object_id){
                if($ops['the-events-calendar-page-title'] && $meta_key == "_nectar_header_title"){
                    return $ops['the-events-calendar-page-title'];
                }else if($ops['the-events-calendar-page-subtitle'] && $meta_key == "_nectar_header_subtitle"){
                    return $ops['the-events-calendar-page-subtitle'];
                }
            }
        }, 100, 4 );
    }
});