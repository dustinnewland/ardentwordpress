<?php
\Ardent\Salient\Section::getInstance('Search Settings', array(
    'icon_class' => 'el-search',
    'fields' => array(
        array('type' => 'input.wp.media', 'name' => 'search-header-image', 'label' => 'Header Image Upload', 'subtitle' => 'Upload an image for the header of the search page.'),
        array('type' => 'input.text', 'name' => 'search-header-image-height', 'label' => 'Header Image Height',  'subtitle' => 'Don\'t include "px" in the string. e.g. 350', 'default' => '350', 'required' => array( 'search-header-image', '!=', '' )),
        array('type' => 'select','name' => 'enable-search-sidebar', 'label' => 'Sidebar Location', 'subtitle' => 'Enables the "Page Sidebar" that is found in Appearance->Widgets.', 'options' => array('' => 'Disabled', 'left' => 'Left', 'right' => 'Right'))
    )
));