<?php
add_action('pre_get_posts', function($q){
    if (!$q->is_admin && $q->is_main_query() && $q->is_search()){
        $options = get_nectar_theme_options();
        $post__not_in = array();
        if($options['page-not-found-page']){
            $post__not_in[] = array((int)$options['page-not-found-page']);
        }
        $exclude = new WP_Query(array(
            's' => $q->query_vars['s'],
            'posts_per_page' => -1,
            'meta_query' => array(array('key' => 'exclude_from_search', 'value' => '1'))
        ));
        if($exclude->found_posts){
            while($exclude->have_posts()){ $exclude->the_post();
                $post__not_in[] = get_the_ID();
            }
            wp_reset_postdata();
            wp_reset_query();
        }
        if(count($post__not_in)) $q->set('post__not_in', $post__not_in);
    }
    return $q;
});