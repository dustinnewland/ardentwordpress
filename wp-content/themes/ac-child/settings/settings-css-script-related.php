<?php
\Ardent\Salient\Section::getInstance('CSS/Script Related', array(
    'id' => 'general-settings-extra',
    'fields' => array(
        array('type' => 'textarea', 'after' => 'external-dynamic-css', 'name' => 'text_replacement', 'label' => 'Text Replacement', 'subtitle' => 'This allows you to replace the text for certain areas of the site.', 'description' => 'When putting in a text replacement, put new entries on a new line but comma separate the values. The first value is the text you want to replace and the second is the text you want to replace it with. So for example: <pre>Text Replacement,New Text Replacement<br />Google Analytics,New Google Analytics</pre>If you want to insert a comma into the text as well, make sure you use quotes. For example:<pre>Text Replacement,"Text Replacement,The New Version"<br />Google Analytics,"Google Analytics,The New Version"</pre>'),
        array('type' => 'textarea', 'before' => 'google-analytics', 'name' => 'google-analytics-below-head', 'label' => 'Google Analytics Below Head Tag', 'subtitle' => 'Please enter in your google analytics tracking code here.<br />Remember to include the entire script from google, if you just enter your tracking ID it won\'t work.'),
        array('type' => 'textarea', 'after' => 'google-analytics', 'name' => 'google-analytics-below-body', 'label' => 'Google Analytics Below Body Tag', 'subtitle' => 'Please enter in your google analytics tracking code here.<br />Remember to include the entire script from google, if you just enter your tracking ID it won\'t work.')
    )
));