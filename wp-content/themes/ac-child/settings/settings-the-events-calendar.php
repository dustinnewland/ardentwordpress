<?php
if (\Ardent\Plugins\Helper::isActive('the-events-calendar')) {
    \Ardent\Salient\Section::getInstance('The Events Calendar', array(
        'id' => 'the-events-calendar-settings',
        'icon_class' => 'el-calendar',
        'fields' => array(
            array('type' => 'select', 'name' => 'the-events-calendar-page', 'label' => 'Page', 'subtitle' => 'Choose the page that will be used as the to pull header information for "The Events Calendar". <br />The only templates that use this feature are "Page Default", "Sidebar", and "Sidebar - Left."', 'options' => array('' => 'Salient Default')+array_combine(array_values(array_map(function($n){ return $n->value; }, \Ardent\Options::Posts('page'))), array_values(array_map(function($n){ return $n->title; }, \Ardent\Options::Posts('page'))))),
            array('type' => 'input.text', 'name' => 'the-events-calendar-page-title', 'label' => 'Header Title Override', 'subtitle' => 'If this is not filled in, it will use the title from the page selected above.'),
            array('type' => 'input.text', 'name' => 'the-events-calendar-page-subtitle', 'label' => 'Header Subtitle Override', 'subtitle' => 'If this is not filled in, it will use the subtitle from the page selected above.')
        )
    ));
}