<?php 
add_action('admin_head', function() {
    $user_meta = new \Ardent\Wp\User\Meta;
    if(!$user_meta->enable_demo_importer){
    ?>
    <script type="text/javascript">
        jQuery(function($){
            $('ul.redux-group-menu li.redux-group-tab-link-li').each(function(){
                if($(this).find('.group_title').text() == 'Demo Importer' || ($(this).hasClass('empty_section') && $(this).find('.group_title').text() == '')){
                    $(this).remove();
                }
            });
        });
    </script>
    <?php
    }
}, 100, 0);
