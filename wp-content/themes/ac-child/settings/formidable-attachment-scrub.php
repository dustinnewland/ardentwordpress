<?php
add_action('frm_settings_form', function($frm_settings) {
    ?>
    <p>
        <label class="frm_left_label" for="formidable_remove_attachments">Do not store uploaded files</label>
        <input type="checkbox" name="formidable_remove_attachments" value="1" id="formidable_remove_attachments" <?php echo get_option('formidable_remove_attachments') ? 'checked' : '' ?> />
    </p>
    <?php
}, 20);

add_action('frm_store_settings', function() {
    update_option('formidable_remove_attachments', (bool) @$_REQUEST['formidable_remove_attachments'], true);
});

if (get_option('formidable_remove_attachments')) {
    add_action('frm_after_create_entry', function($entry_id, $form_id) {
        global $frm_form, $frm_field;
        $form = $frm_form->getOne($form_id);
        $fields = $frm_field->getAll(array('fi.form_id' => $form->id), 'field_order');
        $upload_fields = wp_list_filter($fields, array('type' => 'file'));
        foreach ($upload_fields as $field) {
            if(is_array($_POST['item_meta'][$field->id])){
                foreach ($_POST['item_meta'][$field->id] as $p){
                    if(is_numeric($p)){
                        wp_delete_attachment($p, true);
                    }
                }
            }else if(is_numeric($_POST['item_meta'][$field->id])){
                wp_delete_attachment($_POST['item_meta'][$field->id], true);
            }
        }
    }, 50, 2); //use 50 to make sure this is done very last
}