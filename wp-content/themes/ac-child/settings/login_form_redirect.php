<?php
add_action( 'wp', function(){
    global $post;
    $redirect = get_post_meta($post->ID, 'password_redirect', true);
    if( is_page($post->ID) && $post->post_password && $redirect && !post_password_required($post->ID)){
    	wp_redirect($redirect);
        exit;
    }
});