<?php
add_filter( 'the_password_form', function($output) {
    global $post;
    $output = str_replace('method="post">', 'method="post"><div class="post_password_form">', $output);
    $output = str_replace('</form>', '</div></form>', $output);
    return $output;
} );