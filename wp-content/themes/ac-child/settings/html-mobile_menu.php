<?php
add_action('wp_head', function() {
    $options = get_nectar_theme_options();
    ?>
    <style type="text/css">
        @media only screen and (min-width:1000px){
            <?php if($options['header-slide-out-widget-area'] && $options['off_canvas_menu_mobile_only']){ ?>
                body #header-outer header#top nav > ul.buttons > .slide-out-widget-area-toggle{
                    display:none !important;
                }
            <?php } ?>
        }
        @media only screen and (max-width:1000px){
            <?php if($options['header-slide-out-widget-area'] && $options['off_canvas_menu_mobile_only']){ ?>
                body #header-outer header#top a#toggle-nav{
                    display:none !important;
                }
            <?php } ?>
            <?php if($options['header-slide-out-widget-area']){ ?>
                body.split-menu #header-outer header#top > .container > .row > .col.span_9.col_last .slide-out-widget-area-toggle{
                    position:absolute;
                    top:0;
                    right:0;
                }
            <?php } ?>
        }
    </style>
    <?php
}, 100, 0);
