<?php
\Ardent\Salient\Section::getInstance('404 Settings', array(
    'id' => 'page-not-found-settings',
    'icon_class' => 'el-error',
    'fields' => array(
        array('type' => 'select', 'name' => 'page-not-found-page', 'label' => 'Page', 'subtitle' => 'Choose the page that will be used as the "404 Page Not Found" page.', 'options' => array('' => 'Salient Default')+array_combine(array_values(array_map(function($n){ return $n->value; }, \Ardent\Options::Posts('page'))), array_values(array_map(function($n){ return $n->title; }, \Ardent\Options::Posts('page')))))
    )
));