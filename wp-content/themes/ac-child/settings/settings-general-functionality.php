<?php
global $wp_registered_sidebars;
$sidebars = array();
foreach($wp_registered_sidebars as $location => $data){
    $sidebars[$location] = $data['name'];
}

\Ardent\Salient\Section::getInstance('Functionality', array(
    'id' => 'blog-functionality',
    'fields' => array(
        array('type' => 'switch', 'name' => 'disable_comments', 'before' => 'blog_social', 'title' => 'Disable Comments In Theme Files', 'subtitle' => 'This option, when enabled, will turn off comments in the theme files so that nothing about comments will show up in the blog area.')
    )
));