<?php
add_action('vc_before_init', function () {
    if(defined( 'SALIENT_VC_ACTIVE')) {
        vc_set_shortcodes_templates_dir(get_stylesheet_directory().'/nectar/nectar-vc-shortcodes');
        if (class_exists('WPBakeryVisualComposerAbstract')) {
            \Ardent\Salient\Nectar\Vcmaps::vc_row();
            \Ardent\Salient\Nectar\Vcmaps::vc_column();
            \Ardent\Salient\Nectar\Vcmaps::vc_column_inner();
        }
    }
}, 11);