<?php
add_action('wp_head', function() {
    $options = get_nectar_theme_options();
    $mobileHeight = (!empty($options['mobile-logo-height'])) ? $options['mobile-logo-height'] : null;
    ?>
    <style type="text/css">
        @media only screen and (max-width:1000px){
            body div#header-outer,
            body div#header-outer:not([data-permanent-transparent="1"]){
                <?php echo ($mobileHeight ? 'height:' . (intval($mobileHeight) + 40) . 'px !important;' : ''); ?>
            }
            body.split-menu header#top .mobile-height{
                <?php echo ($mobileHeight ? 'height:' . $mobileHeight . 'px !important;' : ''); ?>
            }
        }
    </style>
    <?php
}, 100, 0);
