<?php 

add_filter('embed_oembed_html', function($markup) {

    if (strpos($markup, 'www.youtube.com') !== FALSE) {
        $markup = str_replace('?feature=oembed', '?feature=oembed&enablejsapi=1&rel=0', $markup);
    }
    if (strpos($markup, '<iframe') !== FALSE) {   // Contains an iframe, wrap it!
        $markup = "<div class='embed-container'>{$markup}</div>";
    }
    return $markup;
});

