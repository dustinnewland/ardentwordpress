<?php
global $options;
if(function_exists('unregister_sidebar')) {
    unregister_sidebar('slide-out-widget-area');
}
if(function_exists('register_sidebar')) {
    $footerColumns = (!empty($options['footer_columns'])) ? $options['footer_columns'] : '';
    $sideWidgetArea = (!empty($options['header-slide-out-widget-area'])) ? $options['header-slide-out-widget-area'] : 'off';
    if($footerColumns == '6'){
            register_sidebar(array('name' => 'Footer Area 3', 'id' => 'footer-area-3', 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget'  => '</div>', 'before_title'  => '<h4>', 'after_title'   => '</h4>'));
            register_sidebar(array('name' => 'Footer Area 4', 'id' => 'footer-area-4', 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget'  => '</div>', 'before_title'  => '<h4>', 'after_title'   => '</h4>'));
            register_sidebar(array('name' => 'Footer Area 5', 'id' => 'footer-area-5', 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget'  => '</div>', 'before_title'  => '<h4>', 'after_title'   => '</h4>'));
            register_sidebar(array('name' => 'Footer Area 6', 'id' => 'footer-area-6', 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget'  => '</div>', 'before_title'  => '<h4>', 'after_title'   => '</h4>'));
    }
    if($sideWidgetArea == '1') {
            register_sidebar(array('name' => 'Off Canvas Menu', 'id' => 'slide-out-widget-area', 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget'  => '</div>', 'before_title'  => '<h4>', 'after_title'   => '</h4>'));
    }
    register_sidebar(array(
        'name'          => __( 'Secondary Nav Widget Sidebar', NECTAR_THEME_NAME ),
        'id'            => 'secondary-nav-widget-sidebar',
        'description'            => 'This is only used when "Salient->Header Navigation->Layout Related->Header Secondary Nav" is set to "Header with Secondary Widgets"'
    ));
    register_sidebar(array(
        'name'          => __( 'Secondary Nav Widget Sidebar For Mobile', NECTAR_THEME_NAME ),
        'id'            => 'secondary-nav-widget-sidebar-mobile',
        'description'            => 'This is only used when "Salient->Header Navigation->Layout Related->Header Secondary Nav Sidebar For Mobile" is set to "Header with Secondary Widgets For Mobile"'
    ));
}