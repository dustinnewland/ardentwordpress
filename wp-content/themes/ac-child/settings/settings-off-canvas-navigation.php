<?php
\Ardent\Salient\Section::getInstance('Off Canvas Navigation', array(
    'id' => 'header-nav-off-canvas-navigation',
    'fields' => array(
        array('type' => 'input.checkbox', 'name' => 'off_canvas_menu_mobile_only', 'after' => 'header-slide-out-widget-area', 'label' => 'Off Canvas Menu on Mobile Only', 'subtitle' => 'Only show the "Off Canvas Menu" on mobile only. This option will also disable the default menu on mobile.', 'default' => '0', 'required' => array( 'header-slide-out-widget-area', '=', '1' ))
    )
));