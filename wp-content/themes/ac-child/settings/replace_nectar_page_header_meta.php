<?php

add_action('init', function() {
    remove_action('add_meta_boxes', 'nectar_metabox_page');
    \Ardent\Salient\Nectar\Header::setFields('page');
    add_filter("get_post_metadata", function($null, $object_id, $meta_key, $single){
        if($meta_key == '_nectar_header_bg' && !is_admin()){
            $post = get_post($object_id);
            $meta = new \Ardent\Wp\Meta($post->ID);
            $bgs = $meta->_nectar_header_bg;
            if(strpos($bgs, 'http') === false){
                $bgary = explode(',', $meta->_nectar_header_bg);
                $bg = wp_get_attachment_image_src($bgary[rand(0,count($bgary)-1)], 'full');
                $bgs = $bg[0];
            }
            return $bgs;
        }
        return null;
    }, 10, 4);
}, 100);

