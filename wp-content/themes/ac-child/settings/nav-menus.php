<?php
add_filter('init', function() {
    $options = get_nectar_theme_options();
    $sideWidgetArea = (!empty($options['header-slide-out-widget-area'])) ? $options['header-slide-out-widget-area'] : 'off';
    unregister_nav_menu('secondary_nav');
    unregister_nav_menu('off_canvas_nav');
    $nectar_menu_arr = array();
    $nectar_menu_arr['secondary_nav'] = 'Secondary Navigation Menu <br /> <small>Will only display if applicable header layout is selected.</small>';
    $nectar_menu_arr['secondary_nav_mobile'] = 'Secondary Navigation Mobile Menu <br /> <small>Will only display if applicable header layout and mobile option is selected.</small>';
    if ($sideWidgetArea == '1') { $nectar_menu_arr['off_canvas_nav'] = 'Off Canvas Navigation Menu'; }
    register_nav_menus($nectar_menu_arr);
});
