<?php
add_action('wp', function() {
    global $post;
    $options = get_nectar_theme_options();
    if (http_response_code() === 404 && $options['page-not-found-page']) {
        $post = get_post((int) $options['page-not-found-page']);
    }
    setup_postdata($post);
});