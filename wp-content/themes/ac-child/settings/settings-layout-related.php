<?php
global $wp_registered_sidebars;
$sidebars = array();
foreach($wp_registered_sidebars as $location => $data){
    $sidebars[$location] = $data['name'];
}

\Ardent\Salient\Section::getInstance('Layout Related', array(
    'id' => 'header-nav-layout',
    'fields' => array(
        array('name' => 'header_layout', 'options' => array('header_with_secondary_widgets' => 'Header with Secondary Widgets', 'header_with_secondary_wysiwyg' => 'Header with Secondary Wysiwyg')),
        array('type' => 'select', 'name' => 'header_secondary_nav_sidebar', 'after' => 'header_layout', 'label' => 'Header Secondary Nav Sidebar', 'subtitle' => 'Please select your secondary nav sidebar here.', 'options' => $sidebars, 'required' => array( 'header_layout', '=', 'header_with_secondary_widgets' )),
        array('type' => 'input.wp.editor', 'name' => 'header_secondary_nav_wysiwyg', 'after' => 'header_secondary_nav_sidebar', 'label' => 'Header Secondary Nav Wysiwyg', 'subtitle' => 'Please input what you would like to show up in the secondary nav area. Feel free to add shortcodes.', 'required' => array( 'header_layout', '=', 'header_with_secondary_wysiwyg' )),
        array('type' => 'input.checkbox', 'name' => 'header_secondary_nav_custom_mobile', 'after' => 'header_secondary_nav_wysiwyg', 'label' => 'Different Header Secondary Nav For Mobile', 'subtitle' => 'Please select whether you want a different area for mobile or not.', 'default' => '0', 'required' => array( 'header_layout', '!=', 'standard' )),
        array('type' => 'select', 'name' => 'header_secondary_nav_sidebar_mobile', 'after' => 'header_secondary_nav_custom_mobile', 'label' => 'Header Secondary Nav Sidebar For Mobile', 'subtitle' => 'Please select your secondary mobile nav sidebar here.', 'options' => $sidebars, 'required' => array(array( 'header_layout', '=', 'header_with_secondary_widgets' ), array( 'header_secondary_nav_custom_mobile', '=', '1' ))),
        array('type' => 'input.wp.editor', 'name' => 'header_secondary_nav_wysiwyg_mobile', 'after' => 'header_secondary_nav_sidebar_mobile', 'label' => 'Header Secondary Nav Wysiwyg For Mobile', 'subtitle' => 'Please input what you would like to show up in the secondary mobile nav area. Feel free to add shortcodes.', 'required' => array(array( 'header_layout', '=', 'header_with_secondary_wysiwyg' ), array( 'header_secondary_nav_custom_mobile', '=', '1' ))),
        array('type' => 'input.checkbox', 'name' => 'header_root_level_megamenu', 'after' => 'header_secondary_nav_wysiwyg_mobile', 'label' => 'Root Level Mega Menu', 'subtitle' => 'This is used to force the mega menu to run at the root level. When enabled it will only use dropdowns on everything below the second level of menu items.', 'default' => '0', 'required' => array( 'header_format', '!=', 'split-menu' )),
        array('type' => 'select', 'name' => 'header_root_level_megamenu_columns', 'after' => 'header_root_level_megamenu', 'title' => 'Mega Menu Columns', 'subtitle' => 'This is used to select the amount of columns within the system to be used. Make sure that you set the columns correctly on the menu items using the class "columns-3" where the number is the amount of columns that was chosen here to ensure proper widths on the columns.', 'options' => array('columns-3' => '3 Columns','columns-4' => '4 Columns','columns-5' => '5 Columns','columns-6' => '6 Columns'), 'required' => array( 'header_root_level_megamenu', '=', '1' ))
    )
));