<?php
add_filter("mce_buttons_2", function($buttons){
    $buttons[]='superscript';
    $buttons[]='subscript';
    return $buttons;
});