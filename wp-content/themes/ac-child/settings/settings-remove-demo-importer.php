<?php
$user_meta = new \Ardent\Wp\User\Meta;
if(!$user_meta->enable_demo_importer){
    add_action('redux/extensions/salient_redux',function($parent){
        $key = \Ardent\Helper::search_keys($parent->sections, 'id', 'wbc_importer_section');
        $parent->sections[$key] = null;
    },1000,1);
}