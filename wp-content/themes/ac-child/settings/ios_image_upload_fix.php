<?php

add_filter('wp_handle_upload_prefilter', 'custom_upload_filter');

function custom_upload_filter($file) {
	if (// Exit conditions
			!is_array($file) ||
			!isset($file['type']) ||
			$file['type'] !== 'image/jpeg' ||
			!isset($file['tmp_name']) ||
			!is_file($file['tmp_name']) ||
			!is_readable($file['tmp_name'])
	) {
		return $file;
	}
	$exif_omap = array(
		3 => 180,
		6 => -90,
		8 => 90
	);   // Exit Orientation value => Rotation mapping
	$exif_data = @exif_read_data($file['tmp_name']);

	if (// Exif-related exit conditions
			!$exif_data ||
			!is_array($exif_data) ||
			!isset($exif_data['Orientation']) ||
			!isset($exif_omap[$exif_data['Orientation']]) ||
			!isset($exif_data['Make']) ||
			$exif_data['Make'] !== 'Apple'
	) {
		return $file;
	}
	list($width, $height) = getimagesize($file['tmp_name']);
	$ip = imagecreatetruecolor($width, $height);
	$im = imagecreatefromjpeg($file['tmp_name']);
	imagecopyresampled($ip, $im, 0, 0, 0, 0, $width, $height, $width, $height);
	$ni = imagerotate($ip, $exif_omap[$exif_data['Orientation']], 0);
	imagejpeg($ni, $file['tmp_name'], 100);
	return $file;
}
