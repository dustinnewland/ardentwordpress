<?php

add_action('init', function() {
	if (defined('SALIENT_VC_ACTIVE')) {
		remove_action('wp_head', array(visual_composer(), 'addMetaData'));
		add_action('wp_head', function() {
			echo '<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="' . vc_asset_url( 'css/vc_lte_ie9.css' ) . '" media="screen"><![endif]-->';
			echo '<!--[if IE  8]><link rel="stylesheet" type="text/css" href="' . vc_asset_url( 'css/vc-ie8.css' ) . '" media="screen"><![endif]-->';
		});
	}
}, 100);

