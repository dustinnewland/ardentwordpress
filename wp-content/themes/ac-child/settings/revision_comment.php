<?php
add_action('wp_loaded', function(){
    if(is_admin()){
        foreach ( get_post_types() as $post_type ) {
            if(post_type_supports($post_type, 'revisions')){
                if($_REQUEST['action'] === 'edit'){
                    $meta = new \Ardent\Wp\Meta((int)$_REQUEST['post']);
                    if($meta->revision_comment !== null){
                        update_post_meta((int)$_REQUEST['post'], 'revision_comment', '');
                    }
                }
                $mb = \Ardent\Wp\Metabox::getInstance($post_type, 'Revision Comment', 'side');
                $mb->add_field(array('type' => 'textarea', 'name' => 'revision_comment', 'style' => 'min-width:100%'));
                $mb->onSave(function($post_id) {
                    update_post_meta($post_id, 'revision_comment', '');
                });
            }
        }
        add_filter('wp_post_revision_title_expanded', function($rev){
            preg_match("/revision=(.*?)'>/", $rev, $ary);
            $meta = new \Ardent\Wp\Meta($ary[1]);
            return $rev.($meta->revision_comment?' - '.$meta->revision_comment:'');
        });
    }
});