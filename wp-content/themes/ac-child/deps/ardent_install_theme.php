<?php
global $ardent_install_theme_cache;

if (!function_exists('ardent_install_theme')) {

    function ardent_install_theme($theme, $url, $version = null, $update_method = 'install') {
        if (defined('WP_INSTALLING') || (!$version && $update_method == 'ensure')) {
            return;
        }
        if (isset($ardent_install_theme_cache[$theme])) { // already installed this theme
            return true;
        }
        $ardent_install_theme_cache[$theme] = true;

        if (!function_exists('wp_get_themes')) {
            require_once(ABSPATH . 'wp-admin/theme.php');
        }
        $redirect = false;
        $themes = array_keys(wp_get_themes());
        if (!in_array($theme, $themes) || $update_method != 'install') { // Theme not installed
            $current_version = get_theme_data(ABSPATH.'wp-content/themes/'.$theme.'/style.css');
            if($current_version['Version'] == $version && $version != null){
                return true;
            }
            $latest_check = json_decode(file_get_contents('http://dev.ardentcreative.com/updates/theme/'.$theme.'/check/'.$current_version['Version']));
            if($update_method == 'install' || ($version != $current_version['Version'] && $update_method == 'ensure') || ($latest_check->new_version != $current_version['Version'] && $update_method == 'latest')){
                // we need to install our dependency
                $output_dir = ABSPATH . 'wp-content/themes/';
                $delete_dir = $output_dir.'/'.$theme;
                if($update_method != 'install' && file_exists($delete_dir)){
                    $skip_dots = new RecursiveDirectoryIterator($delete_dir, RecursiveDirectoryIterator::SKIP_DOTS);
                    $delete_files = new RecursiveIteratorIterator($skip_dots, RecursiveIteratorIterator::CHILD_FIRST);
                    foreach($delete_files as $file) {
                        if ($file->isDir()){
                            rmdir($file->getRealPath());
                        } else {
                            unlink($file->getRealPath());
                        }
                    }
                    rmdir($delete_dir);
                }
                $contents = file_get_contents($url);
                if (!$contents) {
                    return;
                }
                $z = new ZipArchive();
                $n = tempnam(sys_get_temp_dir(), 'archive');
                file_put_contents($n, $contents);
                $z->open($n, ZIPARCHIVE::CHECKCONS);
                $z->extractTo($output_dir);
                unlink($n);
                wp_cache_delete('themes', 'themes');
                $redirect = true;
            }
        }
        if($update_method != 'install'){
            add_filter('site_transient_update_themes', function($value) use ($theme) {
                unset($value->response[$theme]);
                return $value;
            }, 10, 1);
        }
        if($redirect){
            $proto = 'http' . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 's' : '') . '://';
            wp_redirect($proto . @$_SERVER["HTTP_HOST"] . @$_SERVER["REQUEST_URI"]);
            exit;
        }
    }

}