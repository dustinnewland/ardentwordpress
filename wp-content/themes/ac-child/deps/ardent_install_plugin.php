<?php
global $ardent_install_plugin_cache;

if (!function_exists('ardent_install_plugin')) {

    function ardent_install_plugin($plugin, $url, $version = null, $update_method = 'install') {
        if (defined('WP_INSTALLING') || (!$version && $update_method == 'ensure')) {
            return;
        }
        if (isset($ardent_install_plugin_cache[$plugin])) { // already installed this plugin
            return true;
        }
        $ardent_install_plugin_cache[$plugin] = true;

        if (!function_exists('get_plugins')) {
            require_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }
        $plugin_files = array_keys(get_plugins());
        $plugins = array_map(function($a) {
            $i = strpos($a, '/');
            if ($i === false) {
                return $a;
            }
            return substr($a, 0, $i);
        }, $plugin_files);
        
        if (!in_array($plugin, $plugins) || $update_method != 'install') { // Plugin not installed
            $current_version = get_plugin_data(ABSPATH.PLUGINDIR.'/'.$plugin.'/'.$plugin.'.php');
            if($current_version['Version'] == $version && $version != null){
                return true;
            }
            $latest_check = json_decode(file_get_contents('http://dev.ardentcreative.com/updates/plugin/'.$plugin.'/check/'.$current_version['Version']));
            if($update_method == 'install' || ($version != $current_version['Version'] && $update_method == 'ensure') || ($latest_check->new_version != $current_version['Version'] && $update_method == 'latest')){
                // we need to install our depencency
                $output_dir = ABSPATH . PLUGINDIR;
                $delete_dir = $output_dir.'/'.$plugin;
                if($update_method != 'install' && file_exists($delete_dir)){
                    $skip_dots = new RecursiveDirectoryIterator($delete_dir, RecursiveDirectoryIterator::SKIP_DOTS);
                    $delete_files = new RecursiveIteratorIterator($skip_dots, RecursiveIteratorIterator::CHILD_FIRST);
                    foreach($delete_files as $file) {
                        if ($file->isDir()){
                            rmdir($file->getRealPath());
                        } else {
                            unlink($file->getRealPath());
                        }
                    }
                    rmdir($delete_dir);
                }
                if($update_method == 'ensure'){
                    $url .= '/'.$version;
                }
                $contents = file_get_contents($url);
                if (!$contents) {
                    return;
                }
                $z = new ZipArchive();
                $n = tempnam(sys_get_temp_dir(), 'archive');
                file_put_contents($n, $contents);
                $z->open($n, ZIPARCHIVE::CHECKCONS);
                $z->extractTo($output_dir);
                unlink($n);
                wp_cache_delete('plugins', 'plugins');
            }
        }
        $new_plugin_files = array_keys(get_plugins());
        $redirect = false;
        foreach ($new_plugin_files as $plugin_file) {
            if (strpos($plugin_file, "{$plugin}/") !== false) {
                add_filter('plugin_action_links', function($action, $file, $data, $context) use ($plugin_file) {
                    if ($file == $plugin_file) {
                        unset($action['deactivate']);
                        unset($action['delete']);
                        unset($action['edit']);
                    }
                    return $action;
                }, 10, 4);
                if($update_method != 'install'){
                    add_filter('site_transient_update_plugins', function($value) use ($plugin_file) {
                        unset($value->response[$plugin_file]);
                        return $value;
                    }, 10, 1);
                }
                if (!is_plugin_active($plugin_file)) {
                    activate_plugin($plugin_file);
                    $redirect = true;
                }
            }
        }
        if ($redirect) {
            $proto = 'http' . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 's' : '') . '://';
            wp_redirect($proto . @$_SERVER["HTTP_HOST"] . @$_SERVER["REQUEST_URI"]);
            exit;
        }
    }

}