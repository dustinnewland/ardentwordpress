<?php
$options = get_option('salient_redux');
if (isset($options['text_replacement']) && $options['text_replacement']) {
    add_filter('gettext', function( $translated_text, $text, $domain ) {
        $options = get_nectar_theme_options();
        $csv_rows = str_getcsv($options['text_replacement'], "\n");
        foreach ($csv_rows as $row) {
            $col = str_getcsv($row);
            if ($text == $col[0] && isset($col[1])) {
                $translated_text = __($col[1], NECTAR_THEME_NAME);
            }
        }
        return $translated_text;
    }, 20, 3);
}