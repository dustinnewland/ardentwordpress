<?php

require_once(dirname(__FILE__) . "/deps/text_replacement.php");
add_action('after_setup_theme', function() {
	$dir = dirname(__FILE__);
	$parent_version = '7.6';
	$framework_version = '0.6.8';
	require_once("{$dir}/deps/ardent_install_theme.php");
	require_once("{$dir}/deps/ardent_install_plugin.php");
	ardent_install_theme('ac', 'http://stage.ardentcreative.com/updates/theme/ac/download', $parent_version, 'ensure');
	ardent_install_plugin('ardent_framework', 'http://dev.ardentcreative.com/updates/plugin/ardent_framework/download', $framework_version, 'ensure');
	\Ardent\Autoloader::addPath(dirname(__FILE__) . '/classes/');
	foreach (array('functions', 'ajax', 'settings', 'tasks', 'post_types', 'widgets', 'shortcodes') as $file_dir) {
		foreach (glob("{$dir}/{$file_dir}/*.php") as $file) {
			require_once($file);
		}
	}
});
