<?php

namespace Ardent;

if (!class_exists('\Ardent\Helper')) {
    final class Helper {
        static function search_keys($ary, $field, $value){
            foreach($ary as $key => $item){
               if ( $item[$field] === $value ){ return $key; }
            }
            return false;
        }
        static function image_sizes_array(){
            global $_wp_additional_image_sizes;
            $sizes = get_intermediate_image_sizes();
            $return = array('' => 'Full Size');
            foreach($sizes as $size){
                $width = isset($_wp_additional_image_sizes[$size])?$_wp_additional_image_sizes[$size]['width']:'';
                $height = isset($_wp_additional_image_sizes[$size])?$_wp_additional_image_sizes[$size]['height']:'';
                $crop = isset($_wp_additional_image_sizes[$size])?$_wp_additional_image_sizes[$size]['crop']:'';
                if($size == 'thumbnail' || $size == 'medium' || $size == 'large' || $size == 'medium_large'){
                    $width = get_option($size.'_size_w');
                    $height = get_option($size.'_size_h');
                }
                if($size == 'thumbnail'){
                    $crop = (int)get_option($size.'_crop')==1?true:false;
                }
                $return[$size] = $width.'x'.$height.($crop?' Cropped':'').' ('.$size.')';
            }
            return $return;
        }
        static function image_id($url) {
            global $wpdb;
            $att = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $url )); 
            return $att[0]; 
        }
    }
}