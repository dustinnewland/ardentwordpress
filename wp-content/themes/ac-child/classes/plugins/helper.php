<?php

namespace Ardent\Plugins;

if (!class_exists('\Ardent\Plugins\Helper')) {

    final class Helper {
        static function isActive($name){
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
            return is_plugin_active("$name/$name.php");
        }
    }
    
}