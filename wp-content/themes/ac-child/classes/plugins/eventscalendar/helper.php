<?php

namespace Ardent\Plugins\Eventscalendar;

if (!class_exists('\Ardent\Plugins\Eventscalendar\Helper')) {

    final class Helper {
        static function isCalendar(){
            return \Ardent\Plugins\Helper::isActive('the-events-calendar') && (is_post_type_archive('tribe_events') || is_singular('tribe_events') || tribe_is_venue() || tribe_is_organizer() || tribe_is_event_category() || get_post_type == "tribe_events");
        }
    }
    
}