<?php

namespace Ardent\Salient;

if (!class_exists('\Ardent\Salient\Section')) {
    final class Section {
	static $_instances = array();
        static $_optname = 'salient_redux';
	private $_section_id = null;
	private $_section_name = null;
	private $_section_data = array();
        /**
	 * This is the general construct of the Salient Section Class.
	 *
         * This class is used to modify Saleint's Redux framework options. In doing so, it allows you to manipulate fields and add new ones along with new sections.
         * 
         * Please refer to the getInstance function's documentation for more detailed information.
         * 
	 * The preferred way of accessing the Salient Section Class is like this:<br />
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$config = \Ardent\Salient\Section::getInstance($name, $data);
	 *
	 * @param string $name
	 * @param string $data
	 */
        public function __construct($name, $data = array()){
            $this->_section_id = sanitize_title_with_dashes($data['id']?:$name);
            $this->_section_name = $data['title']?:$name;
            $this->_section_data = $data;
            $this->_alter_keys();
            if(\Redux::getSection(self::$_optname, $this->_section_id)){
                add_filter("redux/options/".self::$_optname."/sections", array($this, 'save'), 100, 1);
            }else{
                $this->_set_section();
            }
        }
        /**
	 * This is the getInstance of the Salient Section Class.
	 *
         * This class is used to modify Saleint's Redux framework options. In doing so, it allows you to manipulate fields and add new ones along with new sections.
         * 
         * $name gives you the ability to supply a name that gets converted into a Redux ID. Sometimes if there already is an area created, supplying the ID will have to be done manually within the $data variable.
         * 
         * $data gives you the ability to add a multi-dimentional array that will allow you to either override an existing section or create a new one. Please refer to the redux function setSection on https://docs.reduxframework.com/core/redux-api/ to get a more detailed idea as to what can be done here. The following section keys can be used for compatibility with the ardent framework and are interchangable:<br />
         * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'title' => 'name'<br />
         * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'desc' => 'description'
         * 
         * Inside of the $data array, you can also add the ability to modify existing fields or allow for the addition of new fields to existing sections or when creating a new section. The key for this is 'fields' and it takes a value of an array. Inside the array you can have muliple arrays with fields. Please refer to the redux fields documentation https://docs.reduxframework.com/category/core/fields/ for more detailed information as I will only go through what changes take place within this class. Field types from the ardent framework are converted the their redux framework equivalent. The following keys can be used for compatibility with the ardent framework and are interchangable:<br />
         * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'title' => 'label'<br />
         * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'id' => 'name' // If name or id is not set, it will create it from it's title/label.<br />
         * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'desc' => 'description'
         * 
         * The 'fields' array mentioned previously also has the ability for a field to be placed within an existing section. So the following can be used to move the field around within a seciton. This only works with existing sections, using this on new sections will not work.<br />
         * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'before' => 'id of the field before it'<br />
         * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'after' => 'id of the field after it'
         * 
         * One other thing to note about fields is that ids/names have to be unique across all new and exsiting secitons. So try and name them something that dosn't exist. You can refer to the file nectar/redux-framework/options-config.php in the parent theme to see if your option name exists already.
         * 
	 * The preferred way of accessing the Salient Section Class is like this:<br />
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$config = \Ardent\Salient\Section::getInstance($name, $data);
	 *
	 * @param string $name
	 * @param string $data
	 */
        public static function getInstance($name, $data = array()) {
            $d = array_merge(array('id' => sanitize_title_with_dashes($name), 'title' => __( $name, NECTAR_THEME_NAME )), $data);
            $key = md5($name);
            if (!isset(self::$_instances[$key])) {
                    self::$_instances[$key] = new Section($name, $d);
            }
            return self::$_instances[$key];
	}
        /**
	 * This is a way of getting options from the Salient Section Class.
	 *
         * If you did not provide an id or name when creating the field you are looking for then you can use the name. Using a name allows you to not have to worry about creating IDs for everything. Just know that you cannot use the same id/name twice. So use the ID/Name when necessary but the title if an ID/Name was not specified.
         * 
	 * The preferred way of accessing options within the Salient Section Class is like this:<br />
	 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$value = $config->{'my-option'};
	 *
	 * @param string $name
	 */
        public function __get($name) {
            $options = get_nectar_theme_options();
            $new_name = sanitize_title_with_dashes($name);
            if (array_key_exists($new_name, $options)) {
                return $options[$new_name];
            } else {
                return null;
            }
	}

	/**
	 * @internal
	 */
        public function save($sections){
            $key = \Ardent\Helper::search_keys($sections, 'id', $this->_section_id);
            $section = $sections[$key];
            foreach($this->_section_data as $k => $v){
                if($k != 'fields'){
                    $section[$k] = $v;
                }else{
                    $section[$k] = $this->_edit_fields($section[$k], $v);
                    $section[$k] = $this->_add_fields($section[$k], $v);
                }
            }
            $sections[$key] = $section;
            return $sections;
        }
        private function _edit_fields($old_fields, $new_fields){
            $fields = array();
            foreach($old_fields as $field){
                $key = \Ardent\Helper::search_keys($new_fields, 'id', $field['id']);
                if($key === 0){
                    $fields[] = $this->_edit_field($field, $new_fields[$key]);
                }else{
                    $fields[] = $field;
                }
            }
            return $fields;
        }
        private function _edit_field($field, $new){
            $new_field = $field;
            foreach($new as $k => $v){
                if($k == 'options'){
                    $new_field[$k] = array_replace($new_field[$k],$new[$k]);
                }else{
                    $new_field[$k] = $v;
                }
            }
            return $new_field;
        }
        private function _add_fields($old_fields, $new_fields){
            $fields = $old_fields;
            foreach($new_fields as $field){
                if(!isset($field['before']) && !isset($field['after'])){
                    $fields[] = $field;
                }else{
                    $fields = $this->_insert_field($fields, $field);
                }
            }
            return $fields;
        }
        private function _insert_field($fields, $field){
            $new_fields = $fields;
            if(isset($field['before'])){
                $new_fields = $this->_splice_fields($new_fields, \Ardent\Helper::search_keys($new_fields, 'id', $field['before']), $field);
            }else if(isset($field['after'])){
                $new_fields = $this->_splice_fields($new_fields, \Ardent\Helper::search_keys($new_fields, 'id', $field['after'])+1, $field);
            }
            return $new_fields;
        }
        private function _splice_fields($fields, $key, $field){
            $start = array_slice($fields, 0, $key);
            $end = array_slice($fields, $key);
            $start[] = $field;
            return array_merge($start, $end);
        }
        private function _set_section(){
            foreach($this->_section_data['fields'] as $key => $field){
                if(!$this->_section_data['fields'][$key]['id']){
                    $this->_section_data['fields'][$key]['id'] = sanitize_title_with_dashes($field['title']);
                }
            }
            \Redux::setSection(self::$_optname, $this->_section_data);
        }
        private function _alter_keys(){
            $this->_alter_section_key('name', 'title');
            $this->_alter_section_key('description', 'desc');
            foreach($this->_section_data['fields'] as $key => $field){
                $this->_alter_field_key($field, $key, 'label', 'title');
                $this->_alter_field_key($field, $key, 'name', 'id');
                $this->_alter_field_key($field, $key, 'description', 'desc');
                $this->_alter_field_value($field, $key, 'type', 'input.text', 'text');
                $this->_alter_field_value($field, $key, 'type', 'input.password', 'password');
                $this->_alter_field_value($field, $key, 'type', 'input.checkbox', 'checkbox');
                $this->_alter_field_value($field, $key, 'type', 'input.radio', 'radio');
                $this->_alter_field_value($field, $key, 'type', 'input.radiogroup', 'switch');
                $this->_alter_field_value($field, $key, 'type', 'input.slider', 'slider');
                $this->_alter_field_value($field, $key, 'type', 'input.wp.color', 'color');
                $this->_alter_field_value($field, $key, 'type', 'input.wp.editor', 'editor');
                $this->_alter_field_value($field, $key, 'type', 'input.wp.media', 'media');
                $this->_alter_field_value($field, $key, 'type', 'input.jquery.datepicker', 'date');
            }
        }
        private function _alter_section_key($original, $new){
            if(isset($this->_section_data[$original])){
                $this->_section_data[$new] = isset($this->_section_data[$new])?$this->_section_data[$new]:$this->_section_data[$original];
                unset($this->_section_data[$original]);
            }
        }
        private function _alter_field_key($field, $key, $original, $new){
            if(isset($field[$original])){
                $this->_section_data['fields'][$key][$new] = isset($field[$new])?$field[$new]:$field[$original];
                unset($this->_section_data['fields'][$key][$original]);
            }
        }
        private function _alter_field_value($field, $key, $option, $original, $new){
            if(isset($field[$option]) && $field[$option] == $original){
                $this->_section_data['fields'][$key][$option] = $new;
            }
        }
    }
}