<?php

namespace Ardent\Salient\Nectar\Slider;

if (!class_exists('\Ardent\Salient\Nectar\Slider\Display')) {

    final class Display {
        
        static function shortcodeProcessing($atts, $content = null) {
	
                extract(shortcode_atts(array("arrow_navigation" => 'false', "autorotate"=> '', "tablet_header_font_size" => "auto", "tablet_caption_font_size" => "auto", "phone_header_font_size" => "auto", "phone_caption_font_size" => "auto", "button_sizing"=> 'regular', "slider_button_styling"=> 'btn_with_count', "overall_style" => 'classic', "slider_transition"=> 'swipe', "flexible_slider_height"=> '', "min_slider_height"=> '', "loop" => 'false', 'fullscreen' => 'false', "bullet_navigation" => 'false', "bullet_navigation_style" => 'see_through', "parallax" => 'false', "full_width" => '', "slider_height" => '650', "desktop_swipe" => 'false', "location" => ''), $atts));   

            if($overall_style == 'directional') $desktop_swipe = 'false';

                $slider_config = array(
                  'slider_height' => $slider_height,
                  'full_width' => $full_width,
                  'flexible_slider_height' => $flexible_slider_height,
                  'min_slider_height' => $min_slider_height,
                  'autorotate' => $autorotate,
                  'arrow_navigation' => $arrow_navigation,
                  'bullet_navigation' => $bullet_navigation,
                  'bullet_navigation_style' => $bullet_navigation_style,
                  'desktop_swipe' => $desktop_swipe,
                  'parallax' => $parallax,
                  'slider_transition' => $slider_transition,
                  'overall_style' => $overall_style,
                  'slider_button_styling' => $slider_button_styling,
                  'loop' => $loop,
                  'fullscreen' => $fullscreen,
                  'button_sizing' => $button_sizing,
                  'location' => $location,
                  "tablet_header_font_size" => $tablet_header_font_size,
                  "tablet_caption_font_size" => $tablet_caption_font_size,
                  "phone_header_font_size" => $phone_header_font_size,
                  "phone_caption_font_size" => $phone_caption_font_size
                );
                
                return do_shortcode(\Ardent\Salient\Nectar\Slider\Display::sliderDisplay($slider_config));
        }
        
        static function sliderDisplay($config_arr){
		 
		 global $post;
		 global $options;
		 global $real_fs;
		 
		 $midnight_parallax = null;
	     $midnight_regular = null;

	     //disable parallax for full page
	     $page_full_screen_rows = (isset($post->ID)) ? get_post_meta($post->ID, '_nectar_full_screen_rows', true) : '';
	     if($page_full_screen_rows == "on")
	     	$config_arr['parallax'] = 'false';

	     $animate_in_effect = (!empty($options['header-animate-in-effect'])) ? $options['header-animate-in-effect'] : 'none';

		 //adding parallax wrapper if selected
		 if($config_arr['parallax'] == 'true') {
	
			 if( stripos( $post->post_content, '[nectar_slider') !== FALSE  && stripos( $post->post_content, '[nectar_slider') === 0  &&  $real_fs == 0) { $first_section = 'first-section';  $real_fs = 1; } else { $first_section = ''; }
	   		 
	   		 $midnight_parallax = 'data-midnight="nectar-slider"';
			 $midnight_regular = null;

			 $slider = '<div style="height: '.$config_arr['slider_height'].'px" '.$midnight_parallax.' class="parallax_slider_outer '.$first_section.'">'; 
		 
		 } else { $slider = ''; }
		 
		 $boxed = (!empty($options['boxed_layout']) && $options['boxed_layout'] == '1') ? '1' : '0';
		if($config_arr['full_width'] == 'true' && $boxed != '1') {  $fullwidth = 'true'; }
		else if($config_arr['full_width'] == 'true' && $boxed == '1') { $fullwidth = 'boxed-full-width'; }
		else { $fullwidth = 'false'; }
		
		if($config_arr['parallax'] != 'true') {
		 	 if( stripos( $post->post_content, '[nectar_slider') !== FALSE  && stripos( $post->post_content, '[nectar_slider') === 0 && $real_fs == 0) { $first_section =  'first-section'; $real_fs = 1; $midnight_parallax = null; $midnight_regular = 'data-midnight="nectar-slider"'; } else { $first_section = ''; }  
		 } else {
		 	 $midnight_parallax = null;
	    	 $midnight_regular = null;
		 	 $first_section = ''; 
		 }
			
			$text_overrides = null;
			if(!empty($config_arr['tablet_header_font_size'])) {
				$text_overrides .= ' data-tho="'.$config_arr['tablet_header_font_size'].'"';
			}
			if(!empty($config_arr['tablet_caption_font_size'])) {
				$text_overrides .= ' data-tco="'.$config_arr['tablet_caption_font_size'].'"';
			}
			if(!empty($config_arr['phone_header_font_size'])) {
				$text_overrides .= ' data-pho="'.$config_arr['phone_header_font_size'].'"';
			}
			if(!empty($config_arr['phone_caption_font_size'])) {
				$text_overrides .= ' data-pco="'.$config_arr['phone_caption_font_size'].'"';
			}

			$slider .= '<div style="height: '.$config_arr['slider_height'].'px" '.$midnight_regular.' data-transition="'.$config_arr['slider_transition'].'" data-overall_style="'.$config_arr['overall_style'].'" data-flexible-height="'.$config_arr['flexible_slider_height'].'" data-animate-in-effect="'.$animate_in_effect.'" data-fullscreen="'.$config_arr['fullscreen'].'" data-button-sizing="'.$config_arr['button_sizing'].'" data-button-styling="'.$config_arr['slider_button_styling'].'" data-autorotate="'.$config_arr['autorotate'].'" data-parallax="'.$config_arr['parallax'].'" data-full-width="'.$fullwidth.'" class="nectar-slider-wrap '.$first_section.'" id="ns-id-'.uniqid().'">
			<div style="height: '.$config_arr['slider_height'].'px" class="swiper-container" '.$text_overrides.' data-loop="'.$config_arr['loop'].'" data-height="'. $config_arr["slider_height"] .'" data-min-height="'.$config_arr['min_slider_height'].'" data-arrows="' . $config_arr["arrow_navigation"].'" data-bullets="'.$config_arr["bullet_navigation"].'" data-bullet_style="'.$config_arr["bullet_navigation_style"].'" data-desktop-swipe="'. $config_arr["desktop_swipe"].'" data-settings="">
				    <div class="swiper-wrapper">';
				     
				      $slide_count = 0;
					  
					  //get slider location by slug instead of raw name
					  $slider_terms = get_term_by('name',$config_arr['location'],'slider-locations');
			
				      //loop through and get all the slides in selected location
				      $slides = new \WP_Query( array( 'post_type' => 'nectar_slider', 'tax_query' => array( array( 'taxonomy' => 'slider-locations', 'field' => 'slug', 'terms' => $slider_terms->slug ) ), 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'menu_order' ) ); 
					 
					  if( $slides->have_posts() ) :  while( $slides->have_posts() ) : $slides->the_post(); 
					  
						  global $post;
					  	  
						  $background_type = get_post_meta($post->ID, '_nectar_slider_bg_type', true);
						  $background_alignment = get_post_meta($post->ID, '_nectar_slider_slide_bg_alignment', true); 
						  
						  $slide_title = get_post_meta($post->ID, '_nectar_slider_heading', true);
						  
					      $slide_description = get_post_meta($post->ID, '_nectar_slider_caption', true);
					      $slide_content_editor = get_post_meta($post->ID, '_nectar_slider_editor', true);
						  $slide_description_wrapped = '<span>'.$slide_description.'</span>';
						  $slide_description_bg = get_post_meta($post->ID, '_nectar_slider_caption_background', true);
						  $caption_bg = ( $slide_description_bg == 'on') ? 'class="transparent-bg"' : '';
						  
						  $down_arrow = get_post_meta($post->ID, '_nectar_slider_down_arrow', true);
						   
						  $poster = get_post_meta($post->ID, '_nectar_slider_preview_image', true);
						  $poster_markup = (!empty($poster)) ? 'poster="'.$poster.'"' : null ;
						  
						  $x_pos = get_post_meta($post->ID, '_nectar_slide_xpos_alignment', true); 
					  	  $y_pos = get_post_meta($post->ID, '_nectar_slide_ypos_alignment', true);
						  
						  $link_type = get_post_meta($post->ID, '_nectar_slider_link_type', true);  
						  
						  $full_slide_link = get_post_meta($post->ID, '_nectar_slider_entire_link', true);
						  
						  $button_1_text = get_post_meta($post->ID, '_nectar_slider_button', true); 
						  $button_1_link = get_post_meta($post->ID, '_nectar_slider_button_url', true); 
						  $button_1_target = get_post_meta($post->ID, '_nectar_slider_button_target', true); 
						  $button_1_style = get_post_meta($post->ID, '_nectar_slider_button_style', true); 
						  $button_1_color = get_post_meta($post->ID, '_nectar_slider_button_color', true); 
					  	  
						  $button_2_text = get_post_meta($post->ID, '_nectar_slider_button_2', true); 
						  $button_2_link = get_post_meta($post->ID, '_nectar_slider_button_url_2', true); 
						  $button_2_target = get_post_meta($post->ID, '_nectar_slider_button_target_2', true); 
						  $button_2_style = get_post_meta($post->ID, '_nectar_slider_button_style_2', true); 
						  $button_2_color = get_post_meta($post->ID, '_nectar_slider_button_color_2', true); 
						  
					  	  $video_mp4 = get_post_meta($post->ID, '_nectar_media_upload_mp4', true);
						  $video_webm = get_post_meta($post->ID, '_nectar_media_upload_webm', true);
						  $video_ogv = get_post_meta($post->ID, '_nectar_media_upload_ogv', true); 
						  $video_texture = get_post_meta($post->ID, '_nectar_slider_video_texture', true);  
						  
						  $slide_image = get_post_meta($post->ID, '_nectar_slider_image', true); 
						  $img_bg = null; 
						  
						  $slide_color = get_post_meta($post->ID, '_nectar_slider_slide_font_color', true); 
						  
						  $custom_class = get_post_meta($post->ID, '_nectar_slider_slide_custom_class', true); 
						  $custom_css_class = (!empty($custom_class)) ? ' '.$custom_class : null;
	
						  if($background_type == 'image_bg') { $bg_img_markup = 'style="background-image: url('. nectar_ssl_check($slide_image).');"'; } else { $bg_img_markup = null;}	
						  
						  (!empty($x_pos)) ? $x_pos_markup = $x_pos : $x_pos_markup = 'center';
						  (!empty($y_pos)) ? $y_pos_markup = $y_pos : $y_pos_markup = 'middle';
						  
						                         		                                             
					      $slider .= '<div class="swiper-slide'.$custom_css_class.'" data-bg-alignment="'.$background_alignment.'" data-color-scheme="'. $slide_color .'" data-x-pos="'.$x_pos_markup.'" data-y-pos="'.$y_pos_markup.'"> 
								';
								
								if($background_type == 'image_bg') $slider .='<div class="image-bg" '.$bg_img_markup.'> &nbsp; </div>';

								 if(!empty($slide_title) || !empty($slide_description) || !empty($slide_content_editor) || !empty($button_1_text) || !empty($button_2_text)) {
								 	
									 $slider .= '<div class="container"><style type="text/css" scoped="scoped">.editor_content img{margin:0;}.editor_content *{opacity:1 !important;padding-top:0 !important;}</style>
									<div class="content">';
                                                                                 if(!empty($slide_content_editor)){
                                                                                     $slider .= '<div class="editor_content" style="opacity:0;padding-top:55px;">'.apply_filters('the_content', $slide_content_editor).'</div>';
                                                                                 }else{
                                                                                    if(!empty($slide_title)) { $slider .=  '<h2>'.$slide_title.'</h2>'; } 
                                                                                    if(!empty($slide_description)) { $slider .=  '<p '. $caption_bg.' >'. $slide_description_wrapped.'</p>'; } 
                                                                                 }
										
	
										   if($link_type == 'button_links' && !empty($button_1_text) || $link_type == 'button_links' && !empty($button_2_text)) { 
											$slider .= '<div class="buttons">';
												
												 if(!empty($button_1_text)) {
												 		
												 	$button_1_link = !empty($button_1_link) ? $button_1_link : '#';
													
													//check button link to see if it's a video or googlemap
													$link_extra = null;
													
													if(strpos($button_1_link, 'youtube.com/watch') !== false) $link_extra = 'pp '; 
													if(strpos($button_1_link, 'vimeo.com/') !== false) $link_extra = 'pp '; 
													if(strpos($button_1_link, 'maps.google.com/maps') !== false) $link_extra = 'map-popup '; 
													
													//wrapper for tilt button
													$button_wrap_begin = ($button_1_style == 'solid_color_2') ? "<div class='button-wrap'>": null; 
													$button_wrap_end = ($button_1_style == 'solid_color_2') ? "</div>": null; 

												 	$slider .= 
												 	'<div class="button '.$button_1_style.'">
												 		'.$button_wrap_begin .' <a class="'.$link_extra .$button_1_color .'" href="'.$button_1_link.'" target="'.$button_1_target.'">'.$button_1_text.'</a>'. $button_wrap_end.'
												 	 </div>';
												 } 
												 
												
												 if(!empty($button_2_text)) {
												 		
												 	$button_2_link = !empty($button_2_link) ? $button_2_link : '#';
													
													//check button link to see if it's a video or googlemap
													$link_extra = null;
													
													if(strpos($button_2_link, 'youtube.com/watch') !== false) $link_extra = 'pp '; 
													if(strpos($button_2_link, 'vimeo.com/') !== false) $link_extra = 'pp '; 
													if(strpos($button_2_link, 'maps.google.com/maps') !== false) $link_extra = 'map-popup '; 
													
												 	$slider .= 
												 	'<div class="button '.$button_2_style.'">
												 		 <a class="'.$link_extra . $button_2_color .'" href="'.$button_2_link.'" target="'.$button_2_target.'">'.$button_2_text.'</a>
												 	 </div>';
												 }
												 
											$slider .= '</div>';
										 } 
	
									$slider .= '</div>
								</div><!--/container-->';
								
								}
								
								if(!empty($down_arrow) && $down_arrow == 'on') { 

									 $header_down_arrow_style = (!empty($options['header-down-arrow-style'])) ? $options['header-down-arrow-style'] : 'default'; 
			 	 
								 	 if($header_down_arrow_style == 'scroll-animation') {
								 	 	$slider .=  '<a href="#" class="slider-down-arrow no-border"><svg class="nectar-scroll-icon" viewBox="0 0 30 45" enable-background="new 0 0 30 45">
					                			<path class="nectar-scroll-icon-path" fill="none" stroke="#ffffff" stroke-width="2" stroke-miterlimit="10" d="M15,1.118c12.352,0,13.967,12.88,13.967,12.88v18.76  c0,0-1.514,11.204-13.967,11.204S0.931,32.966,0.931,32.966V14.05C0.931,14.05,2.648,1.118,15,1.118z"></path>
					            			  </svg></a>';
								 	 } else {

										$slider .=  '<a href="#" class="slider-down-arrow"><i class="icon-salient-down-arrow icon-default-style"> <span class="ie-fix"></span> </i></a>';
									} 
								}  
								

								$active_texture = ($video_texture == 'on') ? 'active_texture' : '';
								$slider .=  '<div class="video-texture '.$active_texture.'"> <span class="ie-fix"></span> </div>';
								
								 if($background_type == 'video_bg') {
								 	 
									$slider .= '
									
									<div class="mobile-video-image" style="background-image: url('.$poster.')"> <span class="ie-fix"></span>  </div>
									<div class="video-wrap">
										
										
										<video class="slider-video" width="1800" height="700" preload="auto" loop>';
	
										    if(!empty($video_webm)) { $slider .= '<source type="video/webm" src="'.$video_webm.'">'; }
										    if(!empty($video_mp4)) { $slider .= '<source type="video/mp4" src="'.$video_mp4.'">'; }
										    if(!empty($video_ogv)) { $slider .= '<source type="video/ogg" src="'. $video_ogv.'">'; }
										  
									$slider .='</video>
										
										
									</div>';
									
								} 
							
							if($link_type == 'full_slide_link' && !empty($full_slide_link)) {
									$slider .= '<a href="'. $full_slide_link.'" class="entire-slide-link"> <span class="ie-fix"></span> </a>';
							}
							
					     $slider .= '</div><!--/swiper-slide-->';
						
						 $slide_count ++; 
						 
				    endwhile; endif;
					
					wp_reset_postdata();
				    
				   $slider .= '</div>';
	
				     if($config_arr['arrow_navigation'] == 'true' && $slide_count > 1 && $config_arr['slider_button_styling'] != 'btn_with_preview' && $config_arr['overall_style'] != 'directional') {

					     $slider .= '<a href="" class="slider-prev"><i class="icon-salient-left-arrow"></i> <div class="slide-count"> <span class="slide-current">1</span> <i class="icon-salient-right-line"></i> <span class="slide-total"></span> </div> </a>
				     		<a href="" class="slider-next"><i class="icon-salient-right-arrow"></i> <div class="slide-count"> <span class="slide-current">1</span> <i class="icon-salient-right-line"></i> <span class="slide-total"></span> </div> </a>';
				      } 

					 else if($config_arr['arrow_navigation'] == 'true' && $slide_count > 1 && $config_arr['slider_button_styling'] == 'btn_with_preview' || $config_arr['overall_style'] == 'directional') {
					     $slider .= '<a href="" class="slider-prev"><i class="icon-angle-left"></i> </a>
				     		<a href="" class="slider-next"><i class="icon-angle-right"></i> </a>';
				      } 

					 if($config_arr['bullet_navigation'] == 'true' && $slide_count > 1){ 
				     	$slider .= '<div class="slider-pagination"></div>';
				     }
				    
				$loading_animation = (!empty($options['loading-image-animation']) && !empty($options['loading-image'])) ? $options['loading-image-animation'] : null; 
				$default_loader = (empty($options['loading-image']) && !empty($options['theme-skin']) && $options['theme-skin'] == 'ascend') ? '<span class="default-loading-icon spin"></span>' : null;
				$default_loader_class = (empty($options['loading-image']) && !empty($options['theme-skin']) && $options['theme-skin'] == 'ascend') ? 'default-loader' : null;
				$slider .= '<div class="nectar-slider-loading '.$default_loader_class.'"> <span class="loading-icon '.$loading_animation.'"> '.$default_loader.'  </span> </div> </div> 
				
			</div>';
		
		if($config_arr['parallax'] == 'true') { $slider .= '</div>'; }
		
		return $slider;
	
	}
        
    }
    
}