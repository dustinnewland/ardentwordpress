<?php

namespace Ardent\Salient\Nectar;

if (!class_exists('\Ardent\Salient\Nectar\Vcmaps')) {

    final class Vcmaps {
        
        static function vc_row() {
            vc_map( array(
                    'name' => __( 'Row', 'js_composer' ),
                    'base' => 'vc_row',
                    'is_container' => true,
                    'icon' => 'icon-wpb-row',
                    'show_settings_on_create' => false,
                    'category' => __( 'Structure', 'js_composer' ),
                    'description' => __( 'Place content elements inside the row', 'js_composer' ),
                    'params' => array(
                             array(
                                    "type" => "dropdown",
                                    "class" => "",
                                    "heading" => "Type",
                                    "param_name" => "type",
                                    'save_always' => true,
                                    "value" => array(
                                            "In Container" => "in_container",
                                            "Full Width Background" => "full_width_background",
                                            "Full Width Content" => "full_width_content"		
                                            )
                            ),

                             array(
                                    "type" => "dropdown",
                                    "class" => "",
                                    "heading" => "Fullscreen Row Position",
                                    "param_name" => "full_screen_row_position",
                                    'save_always' => true,
                                    'description' => __( 'Select how your content will be aligned in the fullscreen row - if full height is selected, columns will be 100% of the screen height as well.', 'js_composer' ),
                                    "value" => array(
                                            "Middle" => "middle",
                                            "Top" => "top",
                                            "Bottom" => "bottom",
                                            "Full Height" => 'full_height'		
                                            )
                            ),

                             array(
                                    'type' => 'checkbox',
                                    'heading' => __( 'Equal height', 'js_composer' ),
                                    'param_name' => 'equal_height',
                                    'description' => __( 'If checked columns will be set to equal height.', 'js_composer' ),
                                    'value' => array( __( 'Yes', 'js_composer' ) => 'yes' )
                            ),
                            array(
                                    'type' => 'dropdown',
                                    'heading' => __( 'Column Content position', 'js_composer' ),
                                    'param_name' => 'content_placement',
                                    'value' => array(
                                            __( 'Default', 'js_composer' ) => '',
                                            __( 'Top', 'js_composer' ) => 'top',
                                            __( 'Middle', 'js_composer' ) => 'middle',
                                            __( 'Bottom', 'js_composer' ) => 'bottom',
                                    ),
                                    'description' => __( 'Select content position within columns.', 'js_composer' ),
                                    "dependency" => Array('element' => "equal_height", 'not_empty' => true)
                            ),
                            array(
                                    "type" => "checkbox",
                                    "class" => "",
                                    "heading" => "Vertically Center Columns",
                                    "value" => array("Make all columns in this row vertically centered?" => "true" ),
                                    "param_name" => "vertically_center_columns",
                                    "description" => "",
                                    "dependency" => Array('element' => "type", 'value' => array('full_width_content'))
                            ),

                            array(
                                    "type" => "fws_images",
                                    "class" => "",
                                    "heading" => "Background Images",
                                    "param_name" => "bg_image",
                                    "value" => "",
                                    "description" => "When selecting  multiple, the images will randomly rotate on page reload.",
                                    "dependency" => Array('element' => "mouse_based_parallax_bg", 'is_empty' => true)
                            ),

                            array(
                                    "type" => "checkbox",
                                    "class" => "",
                                    "heading" => "Background Image Mobile Hidden",
                                    "param_name" => "background_image_mobile_hidden",
                                    "value" => array("Hide Background Image on Mobile Views?" => "true" ),
                                    "description" => "Use this to remove your row BG image from displaying on mobile devices",
                                    "dependency" => Array('element' => "bg_image", 'not_empty' => true)
                            ),

                            array(
                                    "type" => "dropdown",
                                    "class" => "",
                                    'save_always' => true,
                                    "heading" => "Background Position",
                                    "param_name" => "bg_position",
                                    "value" => array(
                                             "Left Top" => "left top",
                                             "Left Center" => "left center",
                                             "Left Bottom" => "left bottom",
                                             "Center Top" => "center top",
                                             "Center Center" => "center center",
                                             "Center Bottom" => "center bottom",
                                             "Right Top" => "right top",
                                             "Right Center" => "right center",
                                             "Right Bottom" => "right bottom"
                                    ),
                                    "dependency" => Array('element' => "bg_image", 'not_empty' => true)
                            ),


                            array(
                                    "type" => "dropdown",
                                    "class" => "",
                                    "heading" => "Background Repeat",
                                    "param_name" => "bg_repeat",
                                    'save_always' => true,
                                    "value" => array(
                                            "No Repeat" => "no-repeat",
                                            "Repeat" => "repeat"
                                    ),
                                    "dependency" => Array('element' => "bg_image", 'not_empty' => true)
                            ),

                            array(
                                    "type" => "checkbox",
                                    "class" => "",
                                    "heading" => "Parallax Background",
                                    "value" => array("Enable Parallax Background?" => "true" ),
                                    "param_name" => "parallax_bg",
                                    "description" => "",
                                    "dependency" => Array('element' => "bg_image", 'not_empty' => true)
                            ),

                            array(
                                    "type" => "dropdown",
                                    "class" => "",
                                    "description" => "The faster you choose, the closer your BG will match the users scroll speed",
                                    "heading" => "Parallax Background Speed",
                                    "param_name" => "parallax_bg_speed",
                                    'save_always' => true,
                                    "value" => array(
                                             "Slow" => "slow",
                                             "Medium" => "medium",
                                             "Fast" => "fast",
                                             "Fixed" => "fixed"
                                    ),
                                    "dependency" => Array('element' => "parallax_bg", 'not_empty' => true)
                            ),

                            array(
                                    "type" => "colorpicker",
                                    "class" => "",
                                    "heading" => "Background Color",
                                    "param_name" => "bg_color",
                                    "value" => "",
                                    "description" => ""
                            ),
                            array(
                                    "type" => "checkbox",
                                    "class" => "",
                                    "heading" => "Mouse Based Parallax Scene",
                                    "value" => array("Enable Mouse Based Parallax BG?" => "true" ),
                                    "param_name" => "mouse_based_parallax_bg",
                                    "description" => ""
                            ),

                             array(
                                      "type" => "dropdown",
                                      "heading" => __("Scene Positioning", "js_composer"),
                                      "param_name" => "scene_position",
                                      'save_always' => true,
                                      "value" => array(
                                             "Center" => "center",
                                             "Top" => "top",
                                             "Bottom" => "bottom"
                                            ),
                                      "description" => __("Select your desired scene alignment within your row", "js_composer")
                            ),

                             array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Scene Parallax Overall Strength",
                                    "value" => "",
                                    "param_name" => "mouse_sensitivity",
                                    "description" => "Enter a number between 1 and 25 that will effect the overall strength of the parallax movement within the entire scene - the default is 10."
                            ),


                            array(
                                    "type" => "fws_image",
                                    "class" => "",
                                    "heading" => "Scene Layer One",
                                    "param_name" => "layer_one_image",
                                    "value" => "",
                                    "description" => "Please upload all of your layers at the same dimensions to ensure accurate placement."
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Layer One Strength",
                                    "value" => "",
                                    "param_name" => "layer_one_strength",
                                    "description" => "Enter a number <strong>between 0 and 1</strong> that will determine the strength this layer responds to mouse movement. <br/><br/>By default each layer will increment by .2"
                            ),

                            array(
                                    "type" => "fws_image",
                                    "class" => "",
                                    "heading" => "Scene Layer Two",
                                    "param_name" => "layer_two_image",
                                    "value" => "",
                                    "description" => ""
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Layer Two Strength",
                                    "value" => "",
                                    "param_name" => "layer_two_strength",
                                    "description" => "See the description on \"Layer One Strength\" for guidelines on this property."
                            ),

                            array(
                                    "type" => "fws_image",
                                    "class" => "",
                                    "heading" => "Scene Layer Three",
                                    "param_name" => "layer_three_image",
                                    "value" => "",
                                    "description" => ""
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Layer Three Strength",
                                    "value" => "",
                                    "param_name" => "layer_three_strength",
                                    "description" => "See the description on \"Layer One Strength\" for guidelines on this property."
                            ),

                            array(
                                    "type" => "fws_image",
                                    "class" => "",
                                    "heading" => "Scene Layer Four",
                                    "param_name" => "layer_four_image",
                                    "value" => "",
                                    "description" => ""
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Layer Four Strength",
                                    "value" => "",
                                    "param_name" => "layer_four_strength",
                                    "description" => "See the description on \"Layer One Strength\" for guidelines on this property."
                            ),

                            array(
                                    "type" => "fws_image",
                                    "class" => "",
                                    "heading" => "Scene Layer Five",
                                    "param_name" => "layer_five_image",
                                    "value" => "",
                                    "description" => ""
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Layer Five Strength",
                                    "value" => "",
                                    "param_name" => "layer_five_strength",
                                    "description" => "See the description on \"Layer One Strength\" for guidelines on this property."
                            ),

                            array(
                                    "type" => "checkbox",
                                    "class" => "",
                                    "heading" => "Video Background",
                                    "value" => array("Enable Video Background?" => "use_video" ),
                                    "param_name" => "video_bg",
                                    "description" => ""
                            ),

                            array(
                                    "type" => "checkbox",
                                    "class" => "",
                                    "heading" => "Video Color Overlay",
                                    "value" => array("Enable a color overlay ontop of your video?" => "true" ),
                                    "param_name" => "enable_video_color_overlay",
                                    "description" => "",
                                    "dependency" => Array('element' => "video_bg", 'value' => array('use_video'))
                            ),

                            array(
                                    "type" => "colorpicker",
                                    "class" => "",
                                    "heading" => "Overlay Color",
                                    "param_name" => "video_overlay_color",
                                    "value" => "",
                                    "description" => "",
                                    "dependency" => Array('element' => "enable_video_color_overlay", 'value' => array('true'))
                            ),

                            array(
                                    "type" => "checkbox",
                                    "class" => "",
                                    "heading" => "Mute Video",
                                    "value" => array("Do you want to mute the video (recommended)" => "true" ),
                                    "param_name" => "video_mute",
                                    "description" => "",
                                    "dependency" => Array('element' => "video_bg", 'value' => array('use_video'))
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Youtube Video URL",
                                    "value" => "",
                                    "param_name" => "video_external",
                                    "description" => "Can be used as an alternative to self hosted videos. Enter full video URL e.g. https://www.youtube.com/watch?v=6oTurM7gESE",
                                    "dependency" => Array('element' => "video_bg", 'value' => array('use_video'))
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "WebM File URL",
                                    "value" => "",
                                    "param_name" => "video_webm",
                                    "description" => "You must include this format & the mp4 format to render your video with cross browser compatibility. OGV is optional.
                            Video must be in a 16:9 aspect ratio.",
                                    "dependency" => Array('element' => "video_bg", 'value' => array('use_video'))
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "MP4 File URL",
                                    "value" => "",
                                    "param_name" => "video_mp4",
                                    "description" => "Enter the URL for your mp4 video file here",
                                    "dependency" => Array('element' => "video_bg", 'value' => array('use_video'))
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "OGV File URL",
                                    "value" => "",
                                    "param_name" => "video_ogv",
                                    "description" => "Enter the URL for your ogv video file here",
                                    "dependency" => Array('element' => "video_bg", 'value' => array('use_video'))
                            ),

                            array(
                                    "type" => "attach_image",
                                    "class" => "",
                                    "heading" => "Video Preview Image",
                                    "value" => "",
                                    "param_name" => "video_image",
                                    "description" => "",
                                    "dependency" => Array('element' => "video_bg", 'value' => array('use_video'))
                            ),

                            array(
                                    "type" => "dropdown",
                                    "class" => "",
                                    "heading" => "Text Color",
                                    "param_name" => "text_color",
                                    "value" => array(
                                            "Dark" => "dark",
                                            "Light" => "light",
                                            "Custom" => "custom"
                                    ),
                                    'save_always' => true
                            ),

                            array(
                                    "type" => "colorpicker",
                                    "class" => "",
                                    "heading" => "Custom Text Color",
                                    "param_name" => "custom_text_color",
                                    "value" => "",
                                    "description" => "",
                                    "dependency" => Array('element' => "text_color", 'value' => array('custom'))
                            ),

                            array(
                                    "type" => "dropdown",
                                    "class" => "",
                                    'save_always' => true,
                                    "heading" => "Text Alignment",
                                    "param_name" => "text_align",
                                    "value" => array(
                                            "Left" => "left",
                                            "Center" => "center",
                                            "Right" => "right"
                                    )
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Padding Top",
                                    "value" => "",
                                    "param_name" => "top_padding",
                                    "description" => "Don't include \"px\" in your string. e.g \"40\" - However you can also use a percent value in which case a \"%\" would be needed at the end e.g. \"10%\""
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Padding Bottom",
                                    "value" => "",
                                    "param_name" => "bottom_padding",
                                    "description" => "Don't include \"px\" in your string. e.g \"40\" - However you can also use a percent value in which case a \"%\" would be needed at the end e.g. \"10%\""
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Extra Class Name",
                                    "param_name" => "class",
                                    "value" => ""
                            ),

                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Row ID",
                                    "param_name" => "id",
                                    "value" => "",
                                    "description" => "Use this to option to add an ID onto your row. This can then be used to target the row with CSS or as an anchor point to scroll to when the relevant link is clicked."
                            ),
                            array(
                                    "type" => "textfield",
                                    "class" => "",
                                    "heading" => "Row Name",
                                    "param_name" => "row_name",
                                    "value" => "",
                                    "description" => "This will be shown in your dot navigation when using the Fullscreen Row option"
                            ),
                            array(
                                    'type' => 'checkbox',
                                    'heading' => __( 'Disable Ken Burns BG effect', 'js_composer' ),
                                    'param_name' => 'disable_ken_burns', // Inner param name.
                                    'description' => __( 'If checked the ken burns background zoom effect will not occur on this row.', 'js_composer' ),
                                    'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
                            ),
                            array(
                                    'type' => 'checkbox',
                                    'heading' => __( 'Disable row', 'js_composer' ),
                                    'param_name' => 'disable_element', // Inner param name.
                                    'description' => __( 'If checked the row won\'t be visible on the public side of your website. You can switch it back any time.', 'js_composer' ),
                                    'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
                            ),
                            array(
                                    "type" => "checkbox",
                                    "class" => "",
                                    "group" => "Color Overlay",
                                    "heading" => "Enable Gradient?",
                                    "value" => array("Yes, please" => "true" ),
                                    "param_name" => "enable_gradient",
                                    "description" => ""
                            ),
                            array(
                                    "type" => "colorpicker",
                                    "class" => "",
                                    "heading" => "Color Overlay",
                                    "param_name" => "color_overlay",
                                    "value" => "",
                                    "group" => "Color Overlay",
                                    "description" => ""
                            ),
                            array(
                                    "type" => "colorpicker",
                                    "class" => "",
                                    "heading" => "Color Overlay 2",
                                    "param_name" => "color_overlay_2",
                                    "value" => "",
                                    "group" => "Color Overlay",
                                    "description" => "",
                                    "dependency" => Array('element' => "enable_gradient", 'not_empty' => true)
                            ),
                            array(
                                    "type" => "dropdown",
                                    "class" => "",
                                    'save_always' => true,
                                    "heading" => "Gradient Direction",
                                    "param_name" => "gradient_direction",
                                    "group" => "Color Overlay",
                                    "value" => array(
                                            "Left to Right" => "left_to_right",
                                            "Left Top to Right Bottom" => "left_t_to_right_b",
                                            "Left Bottom to Right Top" => "left_b_to_right_t",
                                            "Bottom to Top" => 'top_to_bottom'
                                    ),
                                    "dependency" => Array('element' => "enable_gradient", 'not_empty' => true)
                            ),
                            array(
                                    "type" => "dropdown",
                                    "class" => "",
                                    'save_always' => true,
                                    "group" => "Color Overlay",
                                    "heading" => "Overlay Strength",
                                    "param_name" => "overlay_strength",
                                    "value" => array(
                                            "Light" => "0.3",
                                            "Medium" => "0.5",
                                            "Heavy" => "0.8",
                                            "Very Heavy" => "0.95",
                                            "Solid" => '1'
                                    )
                            )

                    ),
                    'js_view' => 'VcRowView'
            ));
        }
        
        static function vc_column(){
            global $vc_column_width_list;
            vc_map( array(
		'name' => __( 'Column', 'js_composer' ),
		'base' => 'vc_column',
		'is_container' => true,
		'content_element' => false,
		'params' => array(
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => "Enable Animation",
				"value" => array("Enable Column Animation?" => "true" ),
				"param_name" => "enable_animation",
				"description" => ""
			),

			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => "Animation",
				"param_name" => "animation",
				'save_always' => true,
				"value" => array(
					 "None" => "none",
				     "Fade In" => "fade-in",
			  		 "Fade In From Left" => "fade-in-from-left",
			  		 "Fade In Right" => "fade-in-from-right",
			  		 "Fade In From Bottom" => "fade-in-from-bottom",
			  		 "Grow In" => "grow-in",
			  		 "Flip In Horizontal" => "flip-in",
			  		 "Flip In Vertical" => "flip-in-vertical",
			  		 "Reveal From Right" => "reveal-from-right",
			  		 "Reveal From Bottom" => "reveal-from-bottom",
			  		 "Reveal From Left" => "reveal-from-left",
			  		 "Reveal From Top" => "reveal-from-top"
				),
				"dependency" => Array('element' => "enable_animation", 'not_empty' => true)
			),

			array(
				"type" => "textfield",
				"class" => "",
				"heading" => "Animation Delay",
				"param_name" => "delay",
				"admin_label" => false,
				"description" => __("Enter delay (in milliseconds) if needed e.g. 150. This parameter comes in handy when creating the animate in \"one by one\" effect.", "js_composer"),
				"dependency" => Array('element' => "enable_animation", 'not_empty' => true)
			),

			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => "Boxed Column",
				"value" => array("Boxed Style" => "true" ),
				"param_name" => "boxed",
				"description" => ""
			),

			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => "Centered Content",
				"value" => array("Centered Content Alignment" => "true" ),
				"param_name" => "centered_text",
				"description" => ""
			),

			array(
				"type" => "dropdown",
				"class" => "",
				'save_always' => true,
				"heading" => "Column Padding",
				"param_name" => "column_padding",
				"value" => array(
					"None" => "no-extra-padding",
					"1%" => "padding-1-percent",
					"2%" => "padding-2-percent",
					"3%" => "padding-3-percent",
					"4%" => "padding-4-percent",
					"5%" => "padding-5-percent",
					"6%" => "padding-6-percent",
					"7%" => "padding-7-percent",
					"8%" => "padding-8-percent",
					"9%" => "padding-9-percent",
					"10%" => "padding-10-percent",
					"11%" => "padding-11-percent",
					"12%" => "padding-12-percent",
					"13%" => "padding-13-percent",
					"14%" => "padding-14-percent",
					"15%" => "padding-15-percent"
				),
				"description" => "When using the full width content row type or providing a background color/image for the column, you have the option to define the amount of padding your column will receive."
			),

			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => "Column Padding Position",
				"param_name" => "column_padding_position",
				'save_always' => true,
				"value" => array(
					"All Sides" => 'all',
					'Top' => "top",
					'Right' => 'right',
					'Left' => 'left',
					'Bottom' => 'bottom',
					'Left & Right' => 'left-right',
					'Top & Right' => 'top-right',
					'Top & Left' => 'top-left',
					'Top & Bottom' => 'top-bottom',
					'Bottom & Right' => 'bottom-right',
					'Bottom & Left' => 'bottom-left'
				),
				"description" => "Use this to fine tune where the column padding will take effect"
			),

			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => "Background Color",
				"param_name" => "background_color",
				"value" => "",
				"description" => "",
			),

			array(
				"type" => "dropdown",
				"class" => "",
				'save_always' => true,
				"heading" => "Background Color Opacity",
				"param_name" => "background_color_opacity",
				"value" => array(
					"1" => "1",
					"0.9" => "0.9",
					"0.8" => "0.8",
					"0.7" => "0.7",
					"0.6" => "0.6",
					"0.5" => "0.5",
					"0.4" => "0.4",
					"0.3" => "0.3",
					"0.2" => "0.2",
					"0.1" => "0.1",
				)
				
			),

			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => "Background Color Hover",
				"param_name" => "background_color_hover",
				"value" => "",
				"description" => "",
			),

			array(
				"type" => "dropdown",
				"class" => "",
				'save_always' => true,
				"heading" => "Background Hover Color Opacity",
				"param_name" => "background_hover_color_opacity",
				"value" => array(
					"1" => "1",
					"0.9" => "0.9",
					"0.8" => "0.8",
					"0.7" => "0.7",
					"0.6" => "0.6",
					"0.5" => "0.5",
					"0.4" => "0.4",
					"0.3" => "0.3",
					"0.2" => "0.2",
					"0.1" => "0.1",
				)
				
			),


			array(
				"type" => "fws_images",
				"class" => "",
				"heading" => "Background Images",
				"param_name" => "background_image",
				"value" => "",
				"description" => "When selecting  multiple, the images will randomly rotate on page reload.",
			),

			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => "Scale Background Image To Column",
				"value" => array("Enable" => "true" ),
				"param_name" => "enable_bg_scale",
				"description" => "",
				"dependency" => Array('element' => "background_image", 'not_empty' => true)
			),

			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => "Font Color",
				"param_name" => "font_color",
				"value" => "",
				"description" => ""
			),

			array(
				"type" => "textfield",
				"class" => "",
				"heading" => "Column Link",
				"param_name" => "column_link",
				"admin_label" => false,
				"description" => "If you wish for this column to link somewhere, enter the URL in here",
			),

			array(
				"type" => "textfield",
				"class" => "",
				"heading" => "Margin Top",
				"value" => "",
				"param_name" => "top_margin",
				"description" => "Don't include \"px\" in your string. e.g \"40\" - However you can also use a percent value in which case a \"%\" would be needed at the end e.g. \"10%\". Negative Values are also accepted."
			),

			array(
				"type" => "textfield",
				"class" => "",
				"heading" => "Margin Bottom",
				"value" => "",
				"param_name" => "bottom_margin",
				"description" => "Don't include \"px\" in your string. e.g \"40\" - However you can also use a percent value in which case a \"%\" would be needed at the end e.g. \"10%\". Negative Values are also accepted."
			),

			array(
				"type" => "textfield",
				"class" => "",
				"heading" => "Extra Class Name",
				"param_name" => "el_class",
				"value" => ""
			),
			array(
				'type' => 'dropdown',
				'save_always' => true,
				'heading' => __( 'Width', 'js_composer' ),
				'param_name' => 'width',
				'value' => $vc_column_width_list,
				'group' => __( 'Responsive Options', 'js_composer' ),
				'description' => __( 'Select column width.', 'js_composer' ),
				'std' => '1/1'
			),
			array(
				'type' => 'column_offset',
				'heading' => __( 'Responsiveness', 'js_composer' ),
				'param_name' => 'offset',
				'group' => __( 'Responsive Options', 'js_composer' ),
				'description' => __( 'Adjust column for different screen sizes. Control width, offset and visibility settings.', 'js_composer' )
			),

			array(
				"type" => "dropdown",
				"class" => "",
				'group' => __( 'Responsive Options', 'js_composer' ),
				'save_always' => true,
				"heading" => "Tablet Text Alignment",
				"param_name" => "tablet_text_alignment",
				"value" => array(
					"Default" => "default",
					"Left" => "left",
					"Center" => "center",
					"Right" => "right",
				),
				"description" => "Text alignment that will be used on tablet devices"
			),

			array(
				"type" => "dropdown",
				"class" => "",
				'group' => __( 'Responsive Options', 'js_composer' ),
				'save_always' => true,
				"heading" => "Smartphone Text Alignment",
				"param_name" => "phone_text_alignment",
				"value" => array(
					"Default" => "default",
					"Left" => "left",
					"Center" => "center",
					"Right" => "right",
				),
				"description" => "Text alignment that will be used on smartphones"
			)

		),
		'js_view' => 'VcColumnView'
            ));
        }
        
        static function vc_column_inner(){
            global $vc_column_width_list;
            vc_map( array(
		"name" => __( "Column", "js_composer" ),
		"base" => "vc_column_inner",
		"class" => "",
		"icon" => "",
		"wrapper_class" => "",
		"controls" => "full",
		"allowed_container_element" => false,
		"content_element" => false,
		"is_container" => true,
		"params" => array(
			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => "Enable Animation",
				"value" => array("Enable Column Animation?" => "true" ),
				"param_name" => "enable_animation",
				"description" => ""
			),

			array(
				"type" => "dropdown",
				"class" => "",
				'save_always' => true,
				"heading" => "Animation",
				"param_name" => "animation",
				"value" => array(
					 "None" => "none",
				     "Fade In" => "fade-in",
			  		 "Fade In From Left" => "fade-in-from-left",
			  		 "Fade In Right" => "fade-in-from-right",
			  		 "Fade In From Bottom" => "fade-in-from-bottom",
			  		 "Grow In" => "grow-in",
			  		 "Flip In Horizontal" => "flip-in",
			  		 "Flip In Vertical" => "flip-in-vertical",
			  		 "Reveal From Right" => "reveal-from-right",
			  		 "Reveal From Bottom" => "reveal-from-bottom",
			  		 "Reveal From Left" => "reveal-from-left",
			  		 "Reveal From Top" => "reveal-from-top"		
				),
				"dependency" => Array('element' => "enable_animation", 'not_empty' => true)
			),

			array(
				"type" => "textfield",
				"class" => "",
				"heading" => "Animation Delay",
				"param_name" => "delay",
				"admin_label" => false,
				"description" => __("Enter delay (in milliseconds) if needed e.g. 150. This parameter comes in handy when creating the animate in \"one by one\" effect.", "js_composer"),
				"dependency" => Array('element' => "enable_animation", 'not_empty' => true)
			),

			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => "Boxed Column",
				"value" => array("Boxed Style" => "true" ),
				"param_name" => "boxed",
				"description" => ""
			),

			array(
				"type" => "fws_images",
				"class" => "",
				"heading" => "Background Images",
				"param_name" => "background_image",
				"value" => "",
				"description" => "When selecting  multiple, the images will randomly rotate on page reload.",
			),

			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => "Scale Background Image To Column",
				"value" => array("Enable" => "true" ),
				"param_name" => "enable_bg_scale",
				"description" => "",
				"dependency" => array('element' => "background_image", 'not_empty' => true)
			),

			array(
				"type" => "dropdown",
				"class" => "",
				'save_always' => true,
				"heading" => "Column Padding",
				"param_name" => "column_padding",
				"value" => array(
					"None" => "no-extra-padding",
					"1%" => "padding-1-percent",
					"2%" => "padding-2-percent",
					"3%" => "padding-3-percent",
					"4%" => "padding-4-percent",
					"5%" => "padding-5-percent",
					"6%" => "padding-6-percent",
					"7%" => "padding-7-percent",
					"8%" => "padding-8-percent",
					"9%" => "padding-9-percent",
					"10%" => "padding-10-percent",
					"11%" => "padding-11-percent",
					"12%" => "padding-12-percent",
					"13%" => "padding-13-percent",
					"14%" => "padding-14-percent",
					"15%" => "padding-15-percent"
				),
				"description" => "When using the full width content row type or providing a background color/image for the column, you have the option to define the amount of padding your column will receive."
			),

			array(
				"type" => "dropdown",
				"class" => "",
				'save_always' => true,
				"heading" => "Column Padding Position",
				"param_name" => "column_padding_position",
				"value" => array(
					"All Sides" => 'all',
					'Top' => "top",
					'Right' => 'right',
					'Left' => 'left',
					'Bottom' => 'bottom',
					'Left & Right' => 'left-right',
					'Top & Right' => 'top-right',
					'Top & Left' => 'top-left',
					'Top & Bottom' => 'top-bottom',
					'Bottom & Right' => 'bottom-right',
					'Bottom & Left' => 'bottom-left',
				),
				"description" => "Use this to fine tune where the column padding will take effect"
			),

			array(
				"type" => "colorpicker",
				"class" => "",
				"heading" => "Background Color",
				"param_name" => "background_color",
				"value" => "",
				"description" => "",
			),

			array(
				"type" => "dropdown",
				"class" => "",
				'save_always' => true,
				"heading" => "Background Color Opacity",
				"param_name" => "background_color_opacity",
				"value" => array(
					"1" => "1",
					"0.9" => "0.9",
					"0.8" => "0.8",
					"0.7" => "0.7",
					"0.6" => "0.6",
					"0.5" => "0.5",
					"0.4" => "0.4",
					"0.3" => "0.3",
					"0.2" => "0.2",
					"0.1" => "0.1",
				)
				
			),


			array(
				"type" => "checkbox",
				"class" => "",
				"heading" => "Centered Content",
				"value" => array("Centered Content Alignment" => "true" ),
				"param_name" => "centered_text",
				"description" => ""
			),

			array(
				"type" => "textfield",
				"class" => "",
				"heading" => "Column Link",
				"param_name" => "column_link",
				"admin_label" => false,
				"description" => "If you wish for this column to link somewhere, enter the URL in here",
			),

			array(
				"type" => "textfield",
				"class" => "",
				"heading" => "Extra Class Name",
				"param_name" => "el_class",
				"value" => ""
			),

			array(
				'type' => 'dropdown',
				'save_always' => true,
				'heading' => __( 'Width', 'js_composer' ),
				'param_name' => 'width',
				'value' => $vc_column_width_list,
				'group' => __( 'Responsive Options', 'js_composer' ),
				'description' => __( 'Select column width.', 'js_composer' ),
				'std' => '1/1'
			),
			array(
				'type' => 'column_offset',
				'heading' => __( 'Responsiveness', 'js_composer' ),
				'param_name' => 'offset',
				'group' => __( 'Responsive Options', 'js_composer' ),
				'description' => __( 'Adjust column for different screen sizes. Control width, offset and visibility settings.', 'js_composer' )
			)
		),
		"js_view" => 'VcColumnView'
            ));
        }
    }
}