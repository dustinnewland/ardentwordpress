<?php

namespace Ardent\Salient\Nectar;

if (!class_exists('\Ardent\Salient\Nectar\Vcparams')) {

    final class Vcparams {
        
        static function fws_images($param, $value) {
            $param_line = '';
            $param_line .= '<input type="hidden" class="wpb_vc_param_value gallery_widget_attached_images_ids '.esc_attr($param['param_name']).' '.esc_attr($param['type']).'" name="'.esc_attr($param['param_name']).'" value="'.esc_attr($value).'"/>';
            //$param_line .= '<a class="button gallery_widget_add_images" href="#" use-single="true" title="'.__('Add image', "js_composer").'">'.__('Add image', "js_composer").'</a>';
            $param_line .= '<div class="gallery_widget_attached_images">';
            $param_line .= '<ul class="gallery_widget_attached_images_list">';
            $use_single = '';

                    if(strpos($value, "http://") !== false || strpos($value, "https://") !== false) {
                            //$param_value = fjarrett_get_attachment_id_by_url($param_value);
                            $param_line .= '<li class="added">
                                    <img src="'. esc_attr($value) .'" />
                                    <a href="#" class="icon-remove"></a>
                            </li>';
                            $use_single = ' use-single="true"';
                    } else {
                            $param_line .= ($value != '') ? fieldAttachedImages(explode(",", esc_attr($value))) : '';
                    }


            $param_line .= '</ul>';
            $param_line .= '</div>';
            $param_line .= '<div class="gallery_widget_site_images">';
            // $param_line .= siteAttachedImages(explode(",", $param_value));
            $param_line .= '</div>';
            $param_line .= '<a class="gallery_widget_add_images"'.$use_single.' href="#" title="'.__('Add images', "js_composer").'">'.__('Add images', "js_composer").'</a>';//class: button
            //$param_line .= '<div class="wpb_clear"></div>';

            return $param_line;
        }
    }
}