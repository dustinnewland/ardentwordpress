<?php

namespace Ardent\Salient\Nectar;

if (!class_exists('\Ardent\Salient\Nectar\Header')) {

    final class Header {
        
        private $cpt;
        
        static function setFields($cpt){
            new \Ardent\Salient\Nectar\Header($cpt);
        }
        public function nectar_logo_output($activate_transparency = false, $off_canvas_style = 'slide-out-from-right'){
            global $options;
		if(!empty($options['use-logo'])) {
                    $default_logo_class = ( !empty($options['retina-logo']['id']) || !empty($options['retina-logo']['url']) ) ? 'default-logo' : null;
                    $dark_default_class = ( empty($options['header-starting-logo-dark']['id']) && empty($options['header-starting-logo-dark']['url']) ) ? ' dark-version': null; 

                    if(!empty($options['mobile-logo']['url'])) echo '<img class="mobile-logo '.$dark_default_class.'" alt="'. get_bloginfo('name') .'" src="' . nectar_options_img($options['mobile-logo']) . '" />';

                    echo '<img class="stnd '.$default_logo_class. $dark_default_class.'" alt="'. get_bloginfo('name') .'" src="' . nectar_options_img($options['logo']) . '" />';

                    if( !empty($options['retina-logo']['id']) || !empty($options['retina-logo']['url']) ) echo '<img class="retina-logo '.$dark_default_class.'" alt="'. get_bloginfo('name') .'" src="' . nectar_options_img($options['retina-logo']) . '" />';

                    if($activate_transparency == 'true' || $off_canvas_style == 'fullscreen-alt'){
                        if( !empty($options['header-starting-logo']['id']) || !empty($options['header-starting-logo']['url']) ) echo '<img class="starting-logo '.$default_logo_class.'"  alt="'. get_bloginfo('name') .'" src="' . nectar_options_img($options['header-starting-logo']) . '" />';
                        if( !empty($options['header-starting-retina-logo']['id']) || !empty($options['header-starting-retina-logo']['url']) ) echo '<img class="retina-logo starting-logo" alt="'. get_bloginfo('name') .'" src="' . nectar_options_img($options['header-starting-retina-logo']) . '" />';

                        if( !empty($options['header-starting-logo-dark']['id']) || !empty($options['header-starting-logo-dark']['url']) ) echo '<img class="starting-logo dark-version '.$default_logo_class.'"  alt="'. get_bloginfo('name') .'" src="' . nectar_options_img($options['header-starting-logo-dark']) . '" />';
                        if( !empty($options['header-starting-retina-logo-dark']['id']) || !empty($options['header-starting-retina-logo-dark']['url']) ) echo '<img class="retina-logo starting-logo dark-version " alt="'. get_bloginfo('name') .'" src="' . nectar_options_img($options['header-starting-retina-logo-dark']) . '" />';
                    }
		} else { echo get_bloginfo('name'); }
        }
        public function __construct($cpt){
            $this->cpt = $cpt; // Get data in var
            add_action( 'add_meta_boxes', array(&$this, '_reg_meta') );
        }
        public function _reg_meta() {
            $options = get_nectar_theme_options(); 
            if(!empty($options['transparent-header']) && $options['transparent-header'] == '1') {
                    $disable_transparent_header = array( 
                                            'name' =>  __('Disable Transparency From Navigation', NECTAR_THEME_NAME),
                                            'desc' => __('You can use this option to force your navigation header to stay a solid color even if it qualifies to trigger the <a target="_blank" href="'. admin_url('?page=redux_options&tab=4#header-padding') .'"> transparent effect</a> you have activated in the Salient options panel.', NECTAR_THEME_NAME),
                                            'id' => '_disable_transparent_header',
                                            'type' => 'checkbox',
                            'std' => ''
                                    );
                    $force_transparent_header = array( 
                                            'name' =>  __('Force Transparency On Navigation', NECTAR_THEME_NAME),
                                            'desc' => __('You can use this option to force your navigation header to start transparent even if it does not qualify to trigger the <a target="_blank" href="'. admin_url('?page=redux_options&tab=4#header-padding') .'"> transparent effect</a> you have activated in the Salient options panel.', NECTAR_THEME_NAME),
                                            'id' => '_force_transparent_header',
                                            'type' => 'checkbox',
                            'std' => ''
                                    );
            } else {
                    $disable_transparent_header = null;
                    $force_transparent_header = null;
            }
            
            #-----------------------------------------------------------------#
            # Fullscreen rows
            #-----------------------------------------------------------------#
            $meta_box = array(
                    'id' => 'nectar-metabox-fullscreen-rows',
                    'title' => __('Page Full Screen Rows', NECTAR_THEME_NAME),
                    'description' => __('Here you can configure your page fullscreen rows', NECTAR_THEME_NAME),
                    'post_type' => $this->cpt,
                    'context' => 'normal',
                    'priority' => 'high',
                    'fields' => array(

                                    array( 
                                            'name' => __('Activate Fullscreen Rows', NECTAR_THEME_NAME),
                                            'desc' => __('This will cause all rows to be fullscreen. Some functionality and options within visual composer will be changed when this is active.', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows',
                                            'type' => 'choice_below',
                                            'options' => array(
                                                    'off' => 'Off',
                                                    'on' => 'On'
                                            ),
                                            'std' => 'off'
                                    ),
                                    array( 
                                            'name' => __('Animation Bewteen Rows', NECTAR_THEME_NAME),
                                            'desc' => __('Select your desired animation between rows', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows_animation',
                                            'type' => 'select',
                                            'std' => 'none',
                                            'options' => array(
                                                    "none" => "Default Scroll",
                                                     "zoom-out-parallax" => "Zoom Out + Parallax",
                                                     "parallax" => "Parallax"
                                            )
                                    ),
                                    array( 
                                            'name' => __('Animation Speed', NECTAR_THEME_NAME),
                                            'desc' => __('Selection your desired animation speed', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows_animation_speed',
                                            'type' => 'select',
                                            'std' => 'medium',
                                            'options' => array(
                                                    "slow" => "Slow",
                                                     "medium" => "Medium",
                                                     "fast" => "Fast"
                                            )
                                    ),
                                    array( 
                                            'name' => __('Overall BG Color', NECTAR_THEME_NAME),
                                            'desc' => __('Set your desired background color which will be seen when transitioning through rows. Defaults to #333333', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows_overall_bg_color',
                                            'type' => 'color',
                                            'std' => ''
                                    ),
                                    array(
                                            'name' =>  __('Add Row Anchors to URL', NECTAR_THEME_NAME),
                                            'desc' => __('Enable this to add anchors into your URL for each row.', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows_anchors',
                                            'type' => 'checkbox',
                            'std' => '0'
                                    ),
                                    array(
                                            'name' =>  __('Disable On Mobile', NECTAR_THEME_NAME),
                                            'desc' => __('Check this to disable the page full screen rows when viewing on a mobile device.', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows_mobile_disable',
                                            'type' => 'checkbox',
                            'std' => '0'
                                    ),
                                    array( 
                                            'name' => __('Row BG Image Animation', NECTAR_THEME_NAME),
                                            'desc' => __('Select your desired row BG image animation', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows_row_bg_animation',
                                            'type' => 'select',
                                            'std' => 'none',
                                            'options' => array(
                                                    "none" => "None",
                                                     "ken_burns" => "Ken Burns Zoom"
                                            )
                                    ),
                                    array( 
                                            'name' => __('Dot Navigation', NECTAR_THEME_NAME),
                                            'desc' => __('Select your desired dot navigation style', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows_dot_navigation',
                                            'type' => 'select',
                                            'std' => 'tooltip',
                                            'options' => array(
                                                    "transparent" => "Transparent",
                                                     "tooltip" => "Tooltip",
                                                     "tooltip_alt" => "Tooltip Alt",
                                                     "hidden" => "None (Hidden)"
                                            )
                                    ),
                                    array( 
                                            'name' => __('Row Overflow', NECTAR_THEME_NAME),
                                            'desc' => __('Select how you would like rows to be handled that have content taller than the users window height. This only applies to desktop (mobile will automatically get scrollbars)', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows_content_overflow',
                                            'type' => 'select',
                                            'std' => 'tooltip',
                                            'options' => array(
                                                    "scrollbar" => "Provide Scrollbar",
                                                    "hidden" => "Hide Extra Content",
                                            )
                                    ),
                                    array( 
                                            'name' => __('Page Footer', NECTAR_THEME_NAME),
                                            'desc' => __('This option allows you to define what will be used for the footer after your fullscreen rows', NECTAR_THEME_NAME),
                                            'id' => '_nectar_full_screen_rows_footer',
                                            'type' => 'select',
                                            'std' => 'none',
                                            'options' => array(
                                                    "default" => "Default Footer",
                                                    "last_row" => "Last Row",
                                                    "none" => "None"
                                            )
                                    ),
                    )
            );
            $callback = create_function( '$post,$meta_box', 'nectar_create_meta_box( $post, $meta_box["args"] );' );
            add_meta_box( $meta_box['id'], $meta_box['title'], $callback, $meta_box['post_type'], $meta_box['context'], $meta_box['priority'], $meta_box );

            #-----------------------------------------------------------------#
            # Header Settings
            #-----------------------------------------------------------------#
            $meta_box = array(
                'id' => 'nectar-metabox-page-header',
                'title' => __('Page Header Settings', NECTAR_THEME_NAME),
                'description' => __('Here you can configure how your page header will appear. <br/> For a full width background image behind your header text, simply upload the image below. To have a standard header just fill out the fields below and don\'t upload an image.', NECTAR_THEME_NAME),
                'post_type' => $this->cpt,
                'context' => 'advanced',
                'priority' => 'high',
                'fields' => array(
                        array( 
                                        'name' => __('Background Type', NECTAR_THEME_NAME),
                                        'desc' => __('Please select the background type you would like to use for your slide.', NECTAR_THEME_NAME),
                                        'id' => '_nectar_slider_bg_type',
                                        'type' => 'choice_below',
                                        'options' => array(
                                                'image_bg' => 'Image Background',
                                                'video_bg' => 'Video Background',
                                                'particle_bg' => 'HTML5 Canvas Background'
                                        ),
                                        'std' => 'image_bg'
                                ),

                        array( 
                                        'name' => __('Particle Images', NECTAR_THEME_NAME),
                                        'desc' => 'Add images here that will be used to create the particle shapes.',
                                        'id' => '_nectar_canvas_shapes',
                                        'type' => 'canvas_shape_group',
                                        'class' => 'nectar_slider_canvas_shape',
                                        'std' => ''
                                ),


                        array( 
                                        'name' => __('Video WebM Upload', NECTAR_THEME_NAME),
                                        'desc' => __('Browse for your WebM video file here.<br/> This will be automatically played on load so make sure to use this responsibly for enhancing your design, rather than annoy your user. e.g. A video loop with no sound.<br/><strong>You must include this format & the mp4 format to render your video with cross browser compatibility. OGV is optional.</strong> <br/><strong>Video must be in a 16:9 aspect ratio.</strong>', NECTAR_THEME_NAME),
                                        'id' => '_nectar_media_upload_webm',
                                        'type' => 'media',
                                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Video MP4 Upload', NECTAR_THEME_NAME),
                                        'desc' => __('Browse for your mp4 video file here.<br/> See the note above for recommendations on how to properly use your video background.', NECTAR_THEME_NAME),
                                        'id' => '_nectar_media_upload_mp4',
                                        'type' => 'media',
                                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Video OGV Upload', NECTAR_THEME_NAME),
                                        'desc' => __('Browse for your OGV video file here.<br/>  See the note above for recommendations on how to properly use your video background.', NECTAR_THEME_NAME),
                                        'id' => '_nectar_media_upload_ogv',
                                        'type' => 'media',
                                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Preview Image', NECTAR_THEME_NAME),
                                        'desc' => __('This is the image that will be seen in place of your video on mobile devices & older browsers before your video is played (browsers like IE8 don\'t allow autoplaying).', NECTAR_THEME_NAME),
                                        'id' => '_nectar_slider_preview_image',
                                        'type' => 'file',
                                        'std' => ''
                                ),	


                        array( 
                                        'name' => __('Page Header Image', NECTAR_THEME_NAME),
                                        'desc' => __('The image should be between 1600px - 2000px wide and have a minimum height of 475px for best results. Click "Add" to upload and then "Choose Image". To add more images do the same. When multiple images are added it will choose a random image from the set of images to be displayed on the page.', NECTAR_THEME_NAME),
                                        'id' => '_nectar_header_bg',
                                        'type' => 'file',
                                        'multiple' => true,
                                        'std' => ''
                                ),
                        array(
                                        'name' =>  __('Parallax Header', NECTAR_THEME_NAME),
                                        'desc' => __('This will cause your header to have a parallax scroll effect.', NECTAR_THEME_NAME),
                                        'id' => '_nectar_header_parallax',
                                        'type' => 'checkbox',
                                        'extra' => 'first2',
                        'std' => 1
                                ),	
                        array(
                                        'name' =>  __('Box Roll Header', NECTAR_THEME_NAME),
                                        'desc' => __('This will cause your header to have a 3d box roll on scroll. (deactivated for boxed layouts)', NECTAR_THEME_NAME),
                                        'id' => '_nectar_header_box_roll',
                                        'type' => 'checkbox',
                                        'extra' => 'last',
                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Page Header Height', NECTAR_THEME_NAME),
                                        'desc' => __('How tall do you want your header? <br/>Don\'t include "px" in the string. e.g. 350 <br/><strong>This only applies when you are using an image/bg color.</strong>', NECTAR_THEME_NAME),
                                        'id' => '_nectar_header_bg_height',
                                        'type' => 'text',
                                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Fullscreen Height', NECTAR_THEME_NAME),
                                        'desc' => __('Chooseing this option will allow your header to always remain fullscreen on all devices/screen sizes.', NECTAR_THEME_NAME),
                                        'id' => '_nectar_header_fullscreen',
                                        'type' => 'checkbox',
                                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Page Header Title', NECTAR_THEME_NAME),
                                        'desc' => __('Enter in the page header title', NECTAR_THEME_NAME),
                                        'id' => '_nectar_header_title',
                                        'type' => 'text',
                                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Page Header Subtitle', NECTAR_THEME_NAME),
                                        'desc' => __('Enter in the page header subtitle', NECTAR_THEME_NAME),
                                        'id' => '_nectar_header_subtitle',
                                        'type' => 'text',
                                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Text Effect', NECTAR_THEME_NAME),
                                        'desc' => __('Please select your desired text effect', NECTAR_THEME_NAME),
                                        'id' => '_nectar_page_header_text-effect',
                                        'type' => 'select',
                                        'std' => 'none',
                                        'options' => array(
                                                "none" => "None",
                                                 "rotate_in" => "Rotate In"
                                        )
                                ),
                        array( 
                                        'name' => __('Shape Autorotate Timing', NECTAR_THEME_NAME),
                                        'desc' => __('Enter your desired autorotation time in milliseconds e.g. "5000". Leaving this blank will disable the functionality.', NECTAR_THEME_NAME),
                                        'id' => '_nectar_particle_rotation_timing',
                                        'type' => 'text',
                                        'std' => ''
                                ),
                        array(
                                        'name' =>  __('Disable Chance For Particle Explosion', NECTAR_THEME_NAME),
                                        'desc' => __('By default there\'s a 50% chance on autorotation that your particles will explode. Checking this box disables that.', NECTAR_THEME_NAME),
                                        'id' => '_nectar_particle_disable_explosion',
                                        'type' => 'checkbox',
                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Content Alignment', NECTAR_THEME_NAME),
                                        'desc' => __('Horizontal Alignment', NECTAR_THEME_NAME),
                                        'id' => '_nectar_page_header_alignment',
                                        'type' => 'caption_pos',
                                        'options' => array(
                                                'left' => 'Left',
                                                'center' => 'Centered',
                                                'right' => 'Right',
                                        ),
                                        'std' => 'left',
                                        'extra' => 'first2'
                                ),

                        array( 
                                        'name' => __('Content Alignment', NECTAR_THEME_NAME),
                                        'desc' => __('Vertical Alignment', NECTAR_THEME_NAME),
                                        'id' => '_nectar_page_header_alignment_v',
                                        'type' => 'caption_pos',
                                        'options' => array(
                                                'top' => 'Top',
                                                'middle' => 'Middle',
                                                'bottom' => 'Bottom',
                                        ),
                                        'std' => 'middle',
                                        'extra' => 'last'
                                ),
                        array( 
                                        'name' => __('Background Alignment', NECTAR_THEME_NAME),
                                        'desc' => __('Please choose how you would like your slides background to be aligned', NECTAR_THEME_NAME),
                                        'id' => '_nectar_page_header_bg_alignment',
                                        'type' => 'select',
                                        'std' => 'center',
                                        'options' => array(
                                                "top" => "Top",
                                                 "center" => "Center",
                                                 "bottom" => "Bottom"
                                        )
                                ),
                        array( 
                                        'name' => __('Page Header Background Color', NECTAR_THEME_NAME),
                                        'desc' => __('Set your desired page header background color if not using an image', NECTAR_THEME_NAME),
                                        'id' => '_nectar_header_bg_color',
                                        'type' => 'color',
                                        'std' => ''
                                ),
                        array( 
                                        'name' => __('Page Header Font Color', NECTAR_THEME_NAME),
                                        'desc' => __('Set your desired page header font color', NECTAR_THEME_NAME),
                                        'id' => '_nectar_header_font_color',
                                        'type' => 'color',
                                        'std' => ''
                                ),
                    $disable_transparent_header,
                    $force_transparent_header
                )
            );
            $callback = create_function( '$post,$meta_box', '\Ardent\Salient\Nectar\Header::nectar_create_meta_box( $post, $meta_box["args"] );' );
            add_meta_box( $meta_box['id'], $meta_box['title'], $callback, $meta_box['post_type'], $meta_box['context'], $meta_box['priority'], $meta_box );

        }
        public function nectar_header_social_icons($location) {
                global $options;

                $social_networks = array(
                        'twitter' => 'fa fa-twitter',
                        'facebook' => 'fa fa-facebook',
                        'vimeo' => 'fa fa-vimeo',
                        'pinterest' => 'fa fa-pinterest',
                        'linkedin' => 'fa fa-linkedin',
                        'youtube' => 'fa fa-youtube-play',
                        'tumblr' => 'fa fa-tumblr',
                        'dribbble' => 'fa fa-dribbble',
                        'rss' => 'fa fa-rss',
                        'github' => 'fa fa-github-alt',
                        'behance' => 'fa fa-behance',
                        'google-plus' => 'fa fa-google-plus',
                        'instagram' => 'fa fa-instagram',
                        'stackexchange' => 'fa fa-stackexchange',
                        'soundcloud' => 'fa fa-soundcloud',
                        'flickr' => 'fa fa-flickr',
                        'spotify' => 'icon-salient-spotify',
                        'vk' => 'fa fa-vk',
                        'vine' => 'fa fa-vine'
                );
                $social_output_html = '';

                if($location == 'main-nav') {
                        $social_link_before = '';
                        $social_link_after = '';
                } else {
                        $social_link_before = '<li>';
                        $social_link_after = '</li>';
                }

                if($location == 'secondary-nav')
                        $social_output_html .= '<ul id="social">';

                        foreach($social_networks as $network_name => $icon_class) {

                                if($network_name == 'rss') {
                                        if(!empty($options['use-'.$network_name.'-icon-header']) && $options['use-'.$network_name.'-icon-header'] == 1) {
                                                $nectar_rss_url_link = (!empty($options['rss-url'])) ? $options['rss-url'] : get_bloginfo('rss_url');
                                                $social_output_html .= $social_link_before.'<a target="_blank" href="'.$nectar_rss_url_link.'"><i class="'.$icon_class.'"></i> </a>'.$social_link_after;
                                        }
                                }
                                else {
                                        if(!empty($options['use-'.$network_name.'-icon-header']) && $options['use-'.$network_name.'-icon-header'] == 1) 
                                                $social_output_html .= $social_link_before.'<a target="_blank" href="'.$options[$network_name."-url"].'"><i class="'.$icon_class.'"></i> </a>'.$social_link_after;
                                }
                        }

                if($location == 'secondary-nav')	
                        $social_output_html .= '</ul>';

                echo $social_output_html;
        }
        
        static function nectar_create_meta_box( $post, $meta_box ){

            if( !is_array($meta_box) ) return false;

            if( isset($meta_box['description']) && $meta_box['description'] != '' ){
                echo '<p>'. $meta_box['description'] .'</p>';
            }
            
                wp_nonce_field( basename(get_template_directory().'/nectar/meta/meta-config.php'), 'nectar_meta_box_nonce' );
                echo '<table class="form-table nectar-metabox-table">';

                $count = 0;

                foreach( $meta_box['fields'] as $field ){

                        $meta = get_post_meta( $post->ID, $field['id'], true );

                        $inline = null;
                        if(isset($field['extra'])) { $inline = true; }

                        if($inline == null) {

                        echo '<tr><th><label for="'. $field['id'] .'"><strong>'. $field['name'] .'</strong>
                                  <span>'. $field['desc'] .'</span></label></th>';
                        }


                        switch( $field['type'] ){	
                                case 'text': 
                                        echo '<td><input type="text" name="nectar_meta['. $field['id'] .']" id="'. $field['id'] .'" value="'. ($meta ? $meta : $field['std']) .'" size="30" /></td>';
                                        break;	

                                case 'textarea':
                                        echo '<td><textarea name="nectar_meta['. $field['id'] .']" id="'. $field['id'] .'" rows="8" cols="5">'. ($meta ? $meta : $field['std']) .'</textarea></td>';
                                        break;
                                case 'media_textarea':
                                        echo '<td><div style="display:none;" class="attr_placeholder" data-poster="" data-media-mp4="" data-media-ogv=""></div><textarea name="nectar_meta['. $field['id'] .']" id="'. $field['id'] .'" rows="8" cols="5">'. ($meta ? $meta : $field['std']) .'</textarea></td>';
                                        break;

                                case 'editor' :
                                        $settings = array(
                                    'textarea_name' => 'nectar_meta['. $field['id'] .']',
                                    'editor_class' => '',
                                    'wpautop' => true
                                );
                                wp_editor($meta, $field['id'], $settings );

                                        break;

                                case 'slim_editor' :
                                        $settings = array(
                                    'textarea_name' => 'nectar_meta['. $field['id'] .']',
                                    'editor_class' => 'slim',
                                    'wpautop' => true
                                );
                                echo'<td>';
                                        wp_editor($meta, $field['id'], $settings );
                                        echo '</td>';
                                        break;
                                case 'file':
                                        echo '<td>';
                                    echo '<input type="hidden" id="' . $field['id']. '" ' . 'name="nectar_meta[' . $field['id'] . ']" value="' . ($meta ? $meta : $field['std']) . '" />';
                                    if($field['multiple']){ 
                                        $multi_val = ($meta ? $meta : $field['std']);
                                        if(strpos($multi_val, 'http') !== false){
                                            $multi_val = \Ardent\Helper::image_id($multi_val);
                                        }
                                        ?>
                                            <script type="text/javascript" defer="defer">
                                                jQuery(function($){
                                                    function headerimagechange(e){
                                                        $('#<?php echo $field['id']; ?>').val($('#<?php echo $field['id'].'_multiple'; ?> input.ardent-html-input-hidden').val());
                                                        if(!$(e.target).is('tr')){
                                                            $('#<?php echo $field['id'].'_multiple'; ?>').parents('tr').trigger('click');
                                                        }
                                                    }
                                                    $('body').on('click', '.media-modal', function(e){
                                                        headerimagechange(e);
                                                    });
                                                    $('#nectar-metabox-page-header').on('click', function(e){
                                                        headerimagechange(e);
                                                    });
                                                });
                                            </script>
                                        <?php
                                        echo \Ardent\Html::get(array('type' => 'input.wp.media', 'id' => $field['id'].'_multiple', 'multiple' => true, 'value' => $multi_val));
                                        echo '<div style="display:none;">';
                                    }
                                echo '<img class="redux-opts-screenshot" id="redux-opts-screenshot-' . $field['id'] . '" src="' . ($meta ? $meta : $field['std']) . '" />';
                                if( ($meta ? $meta : $field['std']) == '') {$remove = ' style="display:none;"'; $upload = ''; } else {$remove = ''; $upload = ' style="display:none;"'; }
                                echo ' <a data-update="Select File" data-choose="Choose a File" href="javascript:void(0);"class="redux-opts-upload button-secondary"' . $upload . ' rel-id="' . $field['id'] . '">' . __('Upload', NECTAR_THEME_NAME) . '</a>';
                                echo ' <a href="javascript:void(0);" class="redux-opts-upload-remove"' . $remove . ' rel-id="' . $field['id'] . '">' . __('Remove Upload', NECTAR_THEME_NAME) . '</a>';
                                    if($field['multiple']){
                                        echo '</div>';
                                    }
                                    echo '</td>';

                                        break;

                                case 'color':

                                         if(get_bloginfo('version') >= '3.5') {
                                    wp_enqueue_style('wp-color-picker');
                                    wp_enqueue_script(
                                        'redux-opts-field-color-js',
                                        NECTAR_FRAMEWORK_DIRECTORY . 'options/fields/color/field_color.js',
                                        array('wp-color-picker'),
                                        time(),
                                        true
                                    );
                                } else {
                                    wp_enqueue_script(
                                        'redux-opts-field-color-js', 
                                        NECTAR_FRAMEWORK_DIRECTORY . 'options/fields/color/field_color_farb.js', 
                                        array('jquery', 'farbtastic'),
                                        time(),
                                        true
                                    );
                                }

                                        if(get_bloginfo('version') >= '3.5') {
                                    echo '<td><input type="text" id="' . $field['id'] . '" name="nectar_meta[' . $field['id'] . ']" value="' . ($meta ? $meta : $field['std']) . '" class=" popup-colorpicker" style="width: 70px;" data-default-color="' . ($meta ? $meta : $field['std']) . '"/></td>';
                                } else {
                                    echo '<div class="farb-popup-wrapper">';
                                    echo '<input type="text" id="' . $field['id'] . '" name="nectar_meta[' . $field['id'] . ']" value="' . ($meta ? $meta : $field['std']) . '" class=" popup-colorpicker" style="width:70px;"/>';
                                    echo '<div class="farb-popup"><div class="farb-popup-inside"><div id="' . $field['id'] . 'picker" class="color-picker"></div></div></div>';
                                    echo '</div>';
                                }


                                        break;

                                case 'media':

                                        echo '<td><input type="text" class="file_display_text" id="' . $field['id'] . '" name="nectar_meta[' . $field['id'] . ']" value="' . ($meta ? $meta : $field['std']) . '" />';
                                if( ($meta ? $meta : $field['std']) == '') {$remove = ' style="display:none;"'; $upload = ''; } else {$remove = ''; $upload = ' style="display:none;"'; }
                                echo ' <a data-update="Select File" data-choose="Choose a File" href="javascript:void(0);"class="redux-opts-media-upload button-secondary"' . $upload . ' rel-id="' . $field['id'] . '">' . __('Add Media', NECTAR_THEME_NAME) . '</a>';
                                echo ' <a href="javascript:void(0);" class="redux-opts-upload-media-remove"' . $remove . ' rel-id="' . $field['id'] . '">' . __('Remove Media', NECTAR_THEME_NAME) . '</a></td>';

                                        break;

                                case 'images':
                                    echo '<td><input type="button" class="button" name="' . $field['id'] . '" id="nectar_images_upload" value="' . $field['std'] .'" /></td>';
                                    break;

                                case 'select':
                                        echo'<td><select name="nectar_meta['. $field['id'] .']" id="'. $field['id'] .'">';
                                        foreach( $field['options'] as $key => $option ){
                                                echo '<option value="' . $key . '"';
                                                if( $meta ){ 
                                                        if( $meta == $key ) echo ' selected="selected"'; 
                                                } else {
                                                        if( $field['std'] == $key ) echo ' selected="selected"'; 
                                                }
                                                echo'>'. $option .'</option>';
                                        }
                                        echo'</select></td>';
                                        break;
                                case 'choice_below' :

                                        wp_register_style(
                                'redux-opts-jquery-ui-custom-css',
                                apply_filters('redux-opts-ui-theme',  NECTAR_FRAMEWORK_DIRECTORY . 'options/css/custom-theme/jquery-ui-1.10.0.custom.css'),
                                '',
                                time(),
                                'all'
                            );
                                         wp_enqueue_style('redux-opts-jquery-ui-custom-css');
                                 wp_enqueue_script(
                                    'redux-opts-field-button_set-js', 
                                    NECTAR_FRAMEWORK_DIRECTORY . 'options/fields/button_set/field_button_set.js', 
                                    array('jquery', 'jquery-ui-core', 'jquery-ui-dialog'),
                                    time(),
                                    true
                                );
                                        echo '<td colspan="8">';
                                            echo '<fieldset class="buttonset '.$field['id'].'" >';
                                                        foreach( $field['options'] as $key => $option ){

                                                                echo '<input type="radio" id="nectar_meta_'. $key .'" name="nectar_meta['. $field["id"] .']" value="'. $key .'" ';
                                                                if( $meta ){ 
                                                                        if( $meta == $key ) echo ' checked="checked"'; 
                                                                } else {
                                                                        if( $field['std'] == $key ) echo ' checked="checked"';
                                                                }
                                                                echo ' /> ';
                                                                echo '<label for="nectar_meta_'. $key .'"> '.$option.'</label>';

                                                        }
                                                echo '</fieldset>';
                                        echo '</td>';
                                        break;
                                case 'multi-select':
                                        echo'<td><select multiple="multiple" name="nectar_meta['. $field['id'] .'][]" id="'. $field['id'] .'">';
                                        foreach( $field['options'] as $key => $option ){
                                                echo '<option value="' . $key . '"';
                                                if( $meta ){

                                                        echo (is_array($meta) && in_array($key, $meta)) ? ' selected="selected"' : '';

                                                        if( $meta == $key ) echo ' selected="selected"'; 
                                                } else {
                                                        if( $field['std'] == $key ) echo ' selected="selected"'; 
                                                }
                                                echo'>'. $option .'</option>';
                                        }
                                        echo'</select></td>';
                                        break;

                                case 'slide_alignment' :

                                        wp_register_style(
                                'redux-opts-jquery-ui-custom-css',
                                apply_filters('redux-opts-ui-theme',  NECTAR_FRAMEWORK_DIRECTORY . 'options/css/custom-theme/jquery-ui-1.10.0.custom.css'),
                                '',
                                time(),
                                'all'
                            );
                                         wp_enqueue_style('redux-opts-jquery-ui-custom-css');
                                 wp_enqueue_script(
                                    'redux-opts-field-button_set-js', 
                                    NECTAR_FRAMEWORK_DIRECTORY . 'options/fields/button_set/field_button_set.js', 
                                    array('jquery', 'jquery-ui-core', 'jquery-ui-dialog'),
                                    time(),
                                    true
                                );
                                        echo '<td>';
                                            echo '<fieldset class="buttonset">';
                                                        foreach( $field['options'] as $key => $option ){

                                                                echo '<input type="radio" id="nectar_meta_'. $key .'" name="nectar_meta['. $field["id"] .']" value="'. $key .'" ';
                                                                if( $meta ){ 
                                                                        if( $meta == $key ) echo ' checked="checked"'; 
                                                                } else {
                                                                        if( $field['std'] == $key ) echo ' checked="checked"';
                                                                }
                                                                echo ' /> ';
                                                                echo '<label for="nectar_meta_'. $key .'"> '.$option.'</label>';

                                                        }
                                                echo '</fieldset>';
                                        echo '</td>';
                                        break;
                                case 'radio':
                                        echo '<td>';
                                        foreach( $field['options'] as $key => $option ){
                                                echo '<label class="radio-label"><input type="radio" name="nectar_meta['. $field['id'] .']" value="'. $key .'" class="radio"';
                                                if( $meta ){ 
                                                        if( $meta == $key ) echo ' checked="checked"'; 
                                                } else {
                                                        if( $field['std'] == $key ) echo ' checked="checked"';
                                                }
                                                echo ' /> '. $option .'</label> ';
                                        }
                                        echo '</td>';
                                        break;
                                case 'slider_button_text':
                                        if($field['extra'] == 'first'){
                                                $count++;
                                                echo '<tr><td><label><strong>Button #'.$count.'</strong> <span>Configure your button here.</span> </label></td>';
                                        }
                                        echo '<td class="inline">';
                                        if($inline != null) {
                                                echo '<label for="'. $field['id'] .'"><strong>'. $field['name'] .'</strong>
                                                 <span>'. $field['desc'] .'</span></label>';
                                        }
                                        echo '<input type="text" name="nectar_meta['. $field['id'] .']" id="'. $field['id'] .'" value="'. ($meta ? $meta : $field['std']) .'" size="30"  />';
                                        echo '</td>';
                                        break;
                                case 'slider_button_textarea':
                                        if($field['extra'] == 'first'){
                                                $count++;
                                                echo '<tr><td><label><strong>Button #'.$count.'</strong> <span>Configure your button here.</span> </label></td>';
                                        }
                                        echo '<td class="inline">';
                                        if($inline != null) {
                                                echo '<label for="'. $field['id'] .'"><strong>'. $field['name'] .'</strong>
                                                 <span>'. $field['desc'] .'</span></label>';
                                        }
                                        echo '<textarea name="nectar_meta['. $field['id'] .']" id="'. $field['id'] .'">'.($meta ? $meta : $field['std']) .'</textarea>';
                                        echo '</td>';
                                        break;

                                case 'slider_button_select':
                                        echo '<td class="inline">';
                                        if($inline != null) {
                                                echo '<label for="'. $field['id'] .'"><strong>'. $field['name'] .'</strong>
                                                 <span>'. $field['desc'] .'</span></label>';
                                        }
                                        echo'<select name="nectar_meta['. $field['id'] .']" id="'. $field['id'] .'">';
                                        foreach( $field['options'] as $key => $option ){
                                                echo '<option value="' . $key . '"';
                                                if( $meta ){ 
                                                        if( $meta == $key ) echo ' selected="selected"'; 
                                                } else {
                                                        if( $field['std'] == $key ) echo ' selected="selected"'; 
                                                }
                                                echo'>'. $option .'</option>';
                                        }
                                        echo'</select></td>';
                                        if($field['extra'] == 'last'){
                                                echo '</tr>';
                                        }
                                        break;
                                case 'checkbox':
                                        if(!empty($field['extra']) && $field['extra'] == 'first2'){
                                                echo '<tr><th><label><strong>Scroll Effect</strong> <span>Choose your desired scroll effect here.</span> </label></th>';
                                        }

                                    echo '<td>';		 
                                    $val = '';
                        if( $meta ) {
                            if( $meta == 'on' ) $val = ' checked="checked"';
                        } else {
                            if( $field['std'] == 'on' ) $val = ' checked="checked"';
                        }

                        echo '<input type="hidden" name="nectar_meta['. $field['id'] .']" value="off" />
                        <input type="checkbox" id="'. $field['id'] .'" name="nectar_meta['. $field['id'] .']" value="on"'. $val .' /> ';

                         if(!empty($field['extra']) && $field['extra'] == 'first2' || !empty($field['extra']) && $field['extra'] == 'last'){
                           echo '<br/><br/><label for="'. $field['id'] .'"><strong>'. $field['name'] .'</strong><span>'. $field['desc'] .'</span></label>'; 
                        }

                                    echo '</td>';
                                    if(!empty($field['extra']) && $field['extra'] == 'last'){
                                                echo '</tr>';
                                        }
                                    break;
                                case 'caption_pos' :

                                        wp_register_style(
                                'redux-opts-jquery-ui-custom-css',
                                apply_filters('redux-opts-ui-theme',  NECTAR_FRAMEWORK_DIRECTORY . 'options/css/custom-theme/jquery-ui-1.10.0.custom.css'),
                                '',
                                time(),
                                'all'
                            );
                                         wp_enqueue_style('redux-opts-jquery-ui-custom-css');
                                 wp_enqueue_script(
                                    'redux-opts-field-button_set-js', 
                                    NECTAR_FRAMEWORK_DIRECTORY . 'options/fields/button_set/field_button_set.js', 
                                    array('jquery', 'jquery-ui-core', 'jquery-ui-dialog'),
                                    time(),
                                    true
                                );
                                        if($field['extra'] == 'first'){
                                                echo '<tr><td><label><strong>Slide Content Alignment</strong> <span>Configure the position for your slides content</span> </label></td>';
                                        }
                                        if($field['extra'] == 'first2'){
                                                echo '<tr><th><label><strong>Header Content Alignment</strong> <span>Configure the position for your slides content</span> </label></th>';
                                        }
                                        echo '<td class="content-alignment"> <label><strong>'.$field['desc'].'</strong><span>Select Your Alignment</span></label>';
                                            echo '<fieldset class="buttonset">';
                                                        foreach( $field['options'] as $key => $option ){

                                                                echo '<input type="radio" id="nectar_meta_'. $key .'" name="nectar_meta['. $field["id"] .']" value="'. $key .'" ';
                                                                if( $meta ){ 
                                                                        if( $meta == $key ) echo ' checked="checked"'; 
                                                                } else {
                                                                        if( $field['std'] == $key ) echo ' checked="checked"';
                                                                }
                                                                echo ' /> ';
                                                                echo '<label for="nectar_meta_'. $key .'"> '.$option.'</label>';

                                                        }
                                                echo '</fieldset>';
                                        echo '</td>';
                                        if($field['extra'] == 'last'){
                                                echo '</tr>';
                                        }

                                        break;

                                case 'canvas_shape_group':

                                        //static or animated shape
                                /*
                                        $field['options'] = array('static' => 'Static', 'sequenced' => 'Sequenced');

                                        echo'<td>
                                        <h4>Type</h4>
                                        <select name="nectar_meta[nectar_slider_canvas_shape_type_1]" id="nectar_slider_canvas_shape_type_1">';

                                        foreach( $field['options'] as $key => $option ){
                                                echo '<option value="' . $key . '"';
                                                if( $meta ){ 
                                                        if( $meta == $key ) echo ' selected="selected"'; 
                                                } else {
                                                        if( $field['std'] == $key ) echo ' selected="selected"'; 
                                                }
                                                echo'>'. $option .'</option>';
                                        }
                                        echo'</select></td>';
        */

                                        if ( function_exists( 'wp_enqueue_media' ) ) {


                            } else {
                                wp_enqueue_script( 'media-upload' );
                                wp_enqueue_script( 'thickbox' );
                                wp_enqueue_style( 'thickbox' );
                            }

                             wp_enqueue_script(
                                'redux-field-gallery-js',
                                 NECTAR_FRAMEWORK_DIRECTORY . 'options/fields/upload/gallery.js',
                                array( 'jquery' ),
                                time(),
                                true
                            );


                                    echo '<td>
                                        <fieldset id="'. $field['class'].'" class="redux-field-container redux-field redux-container-gallery" data-id="opt-gallery" data-type="gallery">
                                    <div class="screenshot">';

                            if ( ! empty( $meta) ) {
                                $ids = explode( ',', $meta);

                                foreach ( $ids as $attachment_id ) {
                                    $img = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
                                    echo '<img class="redux-option-image" id="image_' . $field['id'] . '_' . $attachment_id . '" src="' . $img[0] . '" alt="" target="_blank" rel="external" />';
                                }
                            }

                            echo '</div>';
                            echo '<a href="#" onclick="return false;" id="edit-gallery" class="gallery-attachments button button-primary">' . __( 'Add/Edit Images', 'redux-framework' ) . '</a> ';
                            echo '<a href="#" onclick="return false;" id="clear-gallery" class="gallery-attachments button">' . __( 'Clear Images', 'redux-framework' ) . '</a>';
                            echo '<input type="hidden" class="gallery_values " value="' . esc_attr( $meta ) . '" name="nectar_meta['. $field["id"] .']" />
                            </fieldset></td>';


                                        break;
                        }



                        if($inline == null) {
                                echo '</tr>';
                        }
                }

                echo '</table>';
        }

    }

}