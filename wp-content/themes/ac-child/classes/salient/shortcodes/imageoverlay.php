<?php

namespace Ardent\Salient\Shortcodes;

if (!class_exists('\Ardent\Salient\Shortcodes\Imageoverlay')) {
    final class Imageoverlay {

        static function Fields() {
            $imagesizes = \Ardent\Salient\Options::ImageSizes();
            $imagescales = \Ardent\Salient\Options::ImageScales();
            $imagerepeat = \Ardent\Salient\Options::ImageRepeat();
            $textalign = array('flex-start' => 'Top', 'center' => 'Middle', 'flex-end' => 'Bottom');
            $imagealigncontain = array('' => 'Center', 'left top' => 'Left Top', 'left center' => 'Left Center', 'left bottom' => 'Left Bottom', 'right top' => 'Right Top', 'right center' => 'Right Center', 'right bottom' => 'Right Bottom');
            $imagealigncustom = array('' => 'Center', 'center top' => 'Center Top', 'center bottom' => 'Center Bottom', 'left top' => 'Left Top', 'left center' => 'Left Center', 'left bottom' => 'Left Bottom', 'right top' => 'Right Top', 'right center' => 'Right Center', 'right bottom' => 'Right Bottom');
            $easings = \Ardent\Salient\Options::Easings();
            return array(
                'General' => array(
                    array('type' => 'input.wp.media', 'name' => 'image', 'label' => 'Image'),
                    array('type' => 'select', 'name' => 'image_scale', 'label' => 'Image Scale - <span style="font-size:small;font-weight:normal;"><em>Used to select what the image needs to scale to. ex: cover entire box, contain within the box, or default image size.</em></span>', 'options' => $imagescales),
                    array('type' => 'input.text', 'name' => 'area_height', 'label' => 'Area Height - <span style="font-size:small;font-weight:normal;"><em>The height of the block area. (Add px or %, ex: 100px)</em></span>', 'dependency' => array('element' => 'image_scale', 'not_empty' => true)),
                    array('type' => 'input.text', 'name' => 'image_width', 'label' => 'Image Width - <span style="font-size:small;font-weight:normal;"><em>The width of the image. (Add px or %, ex: 100px)</em></span>', 'dependency' => array('element' => 'image_scale', 'value' => 'custom')),
                    array('type' => 'input.text', 'name' => 'image_height', 'label' => 'Image Height - <span style="font-size:small;font-weight:normal;"><em>The height of the image. (Add px or %, ex: 100px)</em></span>', 'dependency' => array('element' => 'image_scale', 'value' => 'custom')),
                    array('type' => 'select', 'name' => 'image_repeat', 'label' => 'Image Repeat - <span style="font-size:small;font-weight:normal;"><em>Used to select how the image needs to repeat.</em></span>', 'options' => $imagerepeat, 'dependency' => array('element' => 'image_scale', 'value' => array('contain', 'custom'))),
                    array('type' => 'select', 'name' => 'image_align_contain', 'label' => 'Image Align - <span style="font-size:small;font-weight:normal;"><em>Used to select where the image needs to align.</em></span>', 'options' => $imagealigncontain, 'dependency' => array('element' => 'image_scale', 'value' => 'contain')),
                    array('type' => 'select', 'name' => 'image_align_custom', 'label' => 'Image Align - <span style="font-size:small;font-weight:normal;"><em>Used to select where the image needs to align.</em></span>', 'options' => $imagealigncustom, 'dependency' => array('element' => 'image_scale', 'value' => 'custom')),
                    array('type' => 'select', 'name' => 'image_size', 'label' => 'Image Size - <span style="font-size:small;font-weight:normal;"><em>Used to select the physical size of the image based on the images sizes available within wordpress.</em></span>', 'options' => $imagesizes),
                    array('type' => 'select', 'name' => 'image_link', 'label' => 'Link to', 'options' => array('' => 'No Link', 'self' => 'Image', 'custom' => 'Custom Link', 'lightbox' => 'Lightbox Tag')),
                    array('type' => 'input.text', 'name' => 'custom_link', 'label' => 'URL - <span style="font-size:small;font-weight:normal;"><em>If this is a lightbox tag, you do not need to include the hash(#) symbol.</em></span>', 'dependency' => array('element' => 'image_link', 'value' => array('custom', 'lightbox'))),
                    array('type' => 'select', 'name' => 'custom_link_target', 'label' => 'Target - <span style="font-size:small;font-weight:normal;"><em>Select the target where you want the link to point to.</em></span>', 'options' => array('_self' => 'Same window', '_blank' => 'New window'), 'dependency' => array('element' => 'image_link', 'value' => array('custom'))),
                    array('type' => 'input.checkbox', 'name' => 'mobile_state', 'label' => 'Show default state on mobile'),
                    array('type' => 'input.wp.editor', 'name' => 'content', 'label' => 'Content - <span style="font-size:small;font-weight:normal;"><em>In order to split the content into default and hover states, select the "Insert Read More tag" in the wysiwyg editor options.</em></span>', 'holder' => 'div'),
                ),
                'Default State' => array(
                    array('type' => 'input.wp.color', 'name' => 'overlay_text_color', 'label' => 'Text Color', 'description' => 'This is the text color for the default overlay state.'),
                    array('type' => 'input.checkbox', 'name' => 'overlay_text_force', 'label' => 'Force Text Color?', 'description' => 'This will force the text color for all elements within the overlay. If not selected, elements like links will inherit their color from the theme itself.'),
                    array('type' => 'input.text', 'name' => 'overlay_text_opacity', 'label' => 'Text Opacity', 'description' => 'Enter the opacity in decimal places from 0 to 1.'),
                    array('type' => 'select', 'name' => 'overlay_vertical_align', 'label' => 'Vertical Alignment', 'description' => 'Align text vertically based on the height of the element.', 'options' => $textalign),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'CSS box', 'js_composer' ),
                        'param_name' => 'default_css',
                        'group' => __( 'Default State', 'js_composer' )
                    )
                ),
                'Hover State' => array(
                    array('type' => 'input.wp.media', 'name' => 'hover_image', 'label' => 'Image'),
                    array('type' => 'select', 'name' => 'hover_image_scale', 'label' => 'Image Scale - <span style="font-size:small;font-weight:normal;"><em>Used to select what the image needs to scale to. ex: cover entire box, contain within the box, or default image size.</em></span>', 'options' => $imagescales),
                    array('type' => 'input.text', 'name' => 'hover_image_width', 'label' => 'Image Width - <span style="font-size:small;font-weight:normal;"><em>The width of the image. (Add px or %, ex: 100px)</em></span>', 'dependency' => array('element' => 'hover_image_scale', 'value' => 'custom')),
                    array('type' => 'input.text', 'name' => 'hover_image_height', 'label' => 'Image Height - <span style="font-size:small;font-weight:normal;"><em>The height of the image. (Add px or %, ex: 100px)</em></span>', 'dependency' => array('element' => 'hover_image_scale', 'value' => 'custom')),
                    array('type' => 'select', 'name' => 'hover_image_repeat', 'label' => 'Image Repeat - <span style="font-size:small;font-weight:normal;"><em>Used to select how the image needs to repeat.</em></span>', 'options' => $imagerepeat, 'dependency' => array('element' => 'hover_image_scale', 'value' => array('contain', 'custom'))),
                    array('type' => 'select', 'name' => 'hover_image_align_contain', 'label' => 'Image Align - <span style="font-size:small;font-weight:normal;"><em>Used to select where the image needs to align.</em></span>', 'options' => $imagealigncontain, 'dependency' => array('element' => 'hover_image_scale', 'value' => 'contain')),
                    array('type' => 'select', 'name' => 'hover_image_align_custom', 'label' => 'Image Align - <span style="font-size:small;font-weight:normal;"><em>Used to select where the image needs to align.</em></span>', 'options' => $imagealigncustom, 'dependency' => array('element' => 'hover_image_scale', 'value' => 'custom')),
                    array('type' => 'select', 'name' => 'hover_image_size', 'label' => 'Image Size - <span style="font-size:small;font-weight:normal;"><em>Used to select the physical size of the image based on the images sizes available within wordpress.</em></span>', 'options' => $imagesizes),
                    array('type' => 'input.wp.color', 'name' => 'overlay_text_color_hover', 'label' => 'Text Color', 'description' => 'This is the text color for the hover state.'),
                    array('type' => 'input.checkbox', 'name' => 'overlay_text_force_hover', 'label' => 'Force Text Color?', 'description' => 'This will force the text color for all elements within the overlay. If not selected, elements like links will inherit their color from the theme itself.'),
                    array('type' => 'input.text', 'name' => 'overlay_text_opacity_hover', 'label' => 'Text Opacity', 'description' => 'Enter the opacity in decimal places from 0 to 1.'),
                    array('type' => 'select', 'name' => 'overlay_vertical_align_hover', 'label' => 'Vertical Alignment', 'description' => 'Align text vertically based on the height of the element. This is only applicable if the content in the General tab is split with a read more tag.', 'options' => $textalign),
                    array('type' => 'input.checkbox', 'name' => 'overlay_transition_animation_on_hover', 'label' => 'Allow for transition animation?', 'description' => 'This option will only work when there the content is not split on the general tab.'),
                    array(
                        'type' => 'css_editor',
                        'heading' => __( 'CSS box', 'js_composer' ),
                        'param_name' => 'hover_css',
                        'group' => __( 'Hover State', 'js_composer' )
                    )
                ),
                'Image Options' => array(
                    array('type' => 'select', 'name' => 'image_align', 'label' => 'Image Alignment', 'options' => array('' => 'Left', 'right' => 'Right', 'center' => 'Center')),
                    array('type' => 'select', 'name' => 'css_animation', 'label' => 'CSS Animation', 'description' => 'Select animation type if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'options' => array('' => 'None', 'top-to-bottom' => 'Top to bottom', 'bottom-to-top' => 'Bottom to top', 'left-to-right' => 'Left to Right', 'right-to-left' => 'Right to Left', 'appear' => 'Appear from center')),
                    array('type' => 'input.text', 'name' => 'animation_delay', 'label' => 'Animation Delay', 'description' => 'Enter delay (in milliseconds) if needed e.g. 150. This parameter comes in handy when creating the animate in "one by one" effect in horizontal columns.'),
                    array('type' => 'input.text', 'name' => 'extra_class', 'label' => 'Extra class name', 'description' => 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.'),
                ),
                'Transition Options' => array(
                    array('type' => 'input.text', 'name' => 'overlay_transition_in_speed', 'label' => 'Transition In Speed', 'description' => 'Enter speed (in seconds) if needed e.g. 0.25. This parameter comes in handy when wanting a hard or soft animation. A hard animation would be something like 0 or 0.05 to not allow it to animate or animate super quick.', 'default_value' => '0.25'),
                    array('type' => 'select', 'name' => 'overlay_transition_in_easing', 'label' => 'Transition In Easing', 'description' => 'Select the animation easing you would like. Easing is a beizer curve to say how fast an element animates at the beginning or end. You can see examples at <a href="http://easings.net/">http://easings.net/</a>.', 'options' => $easings),
                    array('type' => 'input.text', 'name' => 'overlay_transition_out_speed', 'label' => 'Transition Out Speed', 'description' => 'Enter speed (in seconds) if needed e.g. 0.25. This parameter comes in handy when wanting a hard or soft animation. A hard animation would be something like 0 or 0.05 to not allow it to animate or animate super quick.', 'default_value' => '0.25'),
                    array('type' => 'select', 'name' => 'overlay_transition_out_easing', 'label' => 'Transition Out Easing', 'description' => 'Select the animation easing you would like. Easing is a beizer curve to say how fast an element animates at the beginning or end. You can see examples at <a href="http://easings.net/">http://easings.net/</a>.', 'options' => $easings),
                )
            );
        }

    }
}