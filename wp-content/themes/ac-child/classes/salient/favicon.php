<?php

namespace Ardent\Salient;

if (!class_exists('\Ardent\Salient\Favicon')) {
    final class Favicon {
        static function display(){
            $options = get_nectar_theme_options();
            echo '<link rel="shortcut icon" href="'.nectar_options_img($options['favicon']).'" />';
        }
    }
}