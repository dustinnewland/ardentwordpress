<?php

namespace Ardent\Salient;

if (!class_exists('\Ardent\Salient\Options')) {
    final class Options {
        static function Easings(){
            return array(
                '' => 'ease', 'linear' => 'linear', 'ease-out' => 'easeOut', 'ease-in' => 'easeIn', 'ease-in-out' => 'easeInOut',
                'cubic-bezier(0.47, 0, 0.745, 0.715)' => 'easeInSine', 'cubic-bezier(0.39, 0.575, 0.565, 1)' => 'easeOutSine', 'cubic-bezier(0.445, 0.05, 0.55, 0.95)' => 'easeInOutSine',
                'cubic-bezier(0.55, 0.085, 0.68, 0.53)' => 'easeInQuad', 'cubic-bezier(0.25, 0.46, 0.45, 0.94)' => 'easeOutQuad', 'cubic-bezier(0.455, 0.03, 0.515, 0.955)' => 'easeInOutQuad',
                'cubic-bezier(0.55, 0.055, 0.675, 0.19)' => 'easeInCubic', 'cubic-bezier(0.215, 0.61, 0.355, 1)' => 'easeOutCubic', 'cubic-bezier(0.645, 0.045, 0.355, 1)' => 'easeInOutCubic',
                'cubic-bezier(0.895, 0.03, 0.685, 0.22)' => 'easeInQuart', 'cubic-bezier(0.165, 0.84, 0.44, 1)' => 'easeOutQuart', 'cubic-bezier(0.77, 0, 0.175, 1)' => 'easeInOutQuart',
                'cubic-bezier(0.755, 0.05, 0.855, 0.06)' => 'easeInQuint', 'cubic-bezier(0.23, 1, 0.32, 1)' => 'easeOutQuint', 'cubic-bezier(0.86, 0, 0.07, 1)' => 'easeInOutQuint',
                'cubic-bezier(0.95, 0.05, 0.795, 0.035)' => 'easeInExpo', 'cubic-bezier(0.19, 1, 0.22, 1)' => 'easeOutExpo', 'cubic-bezier(1, 0, 0, 1)' => 'easeInOutExpo',
                'cubic-bezier(0.6, 0.04, 0.98, 0.335)' => 'easeInCirc', 'cubic-bezier(0.075, 0.82, 0.165, 1)' => 'easeOutCirc', 'cubic-bezier(0.785, 0.135, 0.15, 0.86)' => 'easeInOutCirc',
                'cubic-bezier(0.6, -0.28, 0.735, 0.045)' => 'easeInBack', 'cubic-bezier(0.175, 0.885, 0.32, 1.275)' => 'easeOutBack', 'cubic-bezier(0.68, -0.55, 0.265, 1.55)' => 'easeInOutBack'
            );
        }
        static function ImageSizes(){
            $sizes = array('full')+get_intermediate_image_sizes();
            return array_combine($sizes,$sizes);
        }
        static function ImageScales(){
            return array('' => 'Auto', 'original' => 'Original', 'contain' => 'Contained Background', 'cover' => 'Covered Background', 'custom' => 'Custom');
        }
        static function ImageRepeat(){
            return array('' => 'No Repeat', 'repeat' => 'Repeat', 'repeat-x' => 'Repeat Horizontally', 'repeat-y' => 'Repeat Vertically');
        }
    }
}