<?php

namespace Ardent\Ajax;

if (!class_exists('\Ardent\Ajax\Password')) {
    final class Password {
        static function checkPassword() {
            header("Content-type:application/json");
            require_once(ABSPATH.WPINC.'/class-phpass.php');
            $wp_hasher = new \PasswordHash(8, true);
            setcookie( 'wp-postpass_' . COOKIEHASH, $wp_hasher->HashPassword( stripslashes( $_POST['pass'] ) ), time() + 864000, COOKIEPATH );
            $_COOKIE['wp-postpass_' . COOKIEHASH] = $wp_hasher->HashPassword( stripslashes( $_POST['pass'] ) );
            $message = 'You have entered an incorrect password. Please try again.';
            $error = true;
            if(isset($_POST['pid'])){
                $p = get_post((int)$_POST['pid']);
                if (!post_password_required($p->ID)) {
                        $error = false;
                }
                $meta = new \Ardent\Wp\Meta($p->ID);
                $message = $meta->password_error_message?:$message;
                $redirect = $meta->password_redirect?:  get_permalink($p->ID);
            }
            $return = array( 'status' => $error?'error':'success', 'message' => $error?$message:'', 'redirect' => $error?'':($redirect?:''));
            echo json_encode( $return );
            wp_die();
        }
    }
}