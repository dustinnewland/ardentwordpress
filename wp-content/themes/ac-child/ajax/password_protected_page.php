<?php
add_action( 'wp_ajax_ardent_pw_check', array('\\Ardent\\Ajax\\Password', 'checkPassword') );
add_action( 'wp_ajax_nopriv_ardent_pw_check', array('\\Ardent\\Ajax\\Password', 'checkPassword') );