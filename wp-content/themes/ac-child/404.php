<?php
    $options = get_nectar_theme_options();
    $pid = $options['page-not-found-page'];
    $content = null;
    get_header();
    if($pid){
        nectar_page_header($pid);
        $content = get_post((int)$pid);
    }
    $fp_options = nectar_get_full_page_options();
    extract($fp_options);
    ?>
        <div class="container-wrap">
            <div class="<?php if($page_full_screen_rows != 'on') echo 'container'; ?> main-content">
                <div class="row">
                    <?php
                        if($pid){
                            if($page_full_screen_rows == 'on') echo '<div id="nectar_fullscreen_rows" data-animation="'.$page_full_screen_rows_animation.'" data-row-bg-animation="'.$page_full_screen_rows_bg_img_animation.'" data-animation-speed="'.$page_full_screen_rows_animation_speed.'" data-content-overflow="'.$page_full_screen_rows_content_overflow.'" data-mobile-disable="'.$page_full_screen_rows_mobile_disable.'" data-dot-navigation="'.$page_full_screen_rows_dot_navigation.'" data-footer="'.$page_full_screen_rows_footer.'" data-anchors="'.$page_full_screen_rows_anchors.'">';
                            echo apply_filters('the_content', $content->post_content);
                            if($page_full_screen_rows == 'on') echo '</div>';
                        }else{
                    ?>
                            <div class="col span_12">
                                <div id="error-404">
                                    <h1>404</h1>
                                    <h2><?php echo __('Not Found', NECTAR_THEME_NAME); ?></h2>
                                </div>
                            </div><!--/span_12-->
                    <?php } ?>
                </div><!--/row-->
            </div><!--/container-->
        </div>
    <?php
    get_footer();