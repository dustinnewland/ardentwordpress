<?php 
/*template name: Sidebar - Left*/
$post_password = $post->post_password;
$logged_in = post_password_required($post->ID);
$enable_header_pw = $post_password?get_post_meta($post->ID, 'enable_header_pw_only', true):false;
$is_event = \Ardent\Plugins\Eventscalendar\Helper::isCalendar();
get_header(); ?>

<?php if(!($enable_header_pw && !$logged_in)){ nectar_page_header($is_event?get_nectar_theme_options()['the-events-calendar-page']:$post->ID); } ?>

<div class="container-wrap">
	
	<div class="container main-content">
		
		<div class="row">
			
			<div id="post-area" class="col span_9 col_last">
				<?php 
				
				if(have_posts()) : while(have_posts()) : the_post(); ?>
					
					<?php the_content(); ?>
	
				<?php endwhile; endif; ?>
				
			</div><!--/span_9-->
			
			<div id="sidebar" class="col span_3 left-sidebar">
				<?php get_sidebar(); ?>
			</div><!--/span_9-->
			
			
		</div><!--/row-->
		
	</div><!--/container-->

</div><!--/container-wrap-->

<?php get_footer(); ?>

