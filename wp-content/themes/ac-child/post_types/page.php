<?php
$metaboxes = array(
    'Search' => array(
        array('type' => 'input.checkbox', 'name' => 'exclude_from_search', 'label' => 'Exclude From Search'),
    )
);
if(is_admin() && (get_post((int)$_REQUEST['post'])->post_password || isset($_REQUEST['post_password']))){
    $metaboxes['Password Protected Page'] = array(
        array('type' => 'input.checkbox', 'name' => 'enable_header_pw_only', 'label' => 'Enable Header On Login Form Only'),
        array('type' => 'input.text', 'name' => 'password_redirect', 'label' => 'Redirect After Login'),
        array('type' => 'textarea', 'name' => 'password_error_message', 'label' => 'Password Error Message', 'default_value' => 'You have entered an incorrect password. Please try again.'),
    );
}
foreach ($metaboxes as $name => $fields) {
    $mb = \Ardent\Wp\Metabox::getInstance('page', $name, 'side');
    foreach ($fields as $field) {
        $mb->add_field($field);
    }
}