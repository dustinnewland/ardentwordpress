<?php
$sc = new \Ardent\Wp\Shortcode('Typer', array(
    'base' => 'ardent_typer',
    'category' => 'Ardent Plugins',
    'description' => 'Allows for having text that swaps in and out with a typing effect.',
    'fields' => array(
        'General' => array(
            array('type' => 'select', 'name' => 'tag', 'label' => 'Outter Tag', 'options' => array('' => 'Default - span', 'h1' => 'H1', 'h2' => 'H2', 'h3' => 'H3', 'h4' => 'H4', 'h5' => 'H5', 'h6' => 'H6', 'p' => 'P', 'div' => 'DIV'), 'admin_label' => true),
            array('type' => 'select', 'name' => 'alignment', 'label' => 'Text Align', 'options' => array('' => 'Left', 'center' => 'Center', 'right' => 'Right'), 'dependency' => array('element' => 'tag', 'not_empty' => true)),
            array('type' => 'input.text', 'name' => 'before_effect', 'label' => 'Text Before Effect', 'admin_label' => true),
            array('type' => 'textarea', 'name' => 'effect_text', 'label' => 'Effect Text', 'description' => 'This option takes multiple values. Separate values with a comma. Example: Terrific,Wonderful,Completely Amazing', 'admin_label' => true),
            array('type' => 'input.text', 'name' => 'after_effect', 'label' => 'Text After Effect', 'admin_label' => true),
            array('type' => 'input.text', 'name' => 'custom_class', 'label' => 'Custom Class'),
        ),
        'Advanced' => array(
            array('type' => 'input.text', 'name' => 'highlight_speed', 'label' => 'Highlight Speed', 'description' => 'Defaults to 20'),
            array('type' => 'input.text', 'name' => 'type_speed', 'label' => 'Type Speed', 'description' => 'Defaults to 100'),
            array('type' => 'input.text', 'name' => 'clear_delay', 'label' => 'Clear Delay', 'description' => 'Defaults to 500'),
            array('type' => 'input.text', 'name' => 'type_delay', 'label' => 'Type Delay', 'description' => 'Defaults to 200'),
            array('type' => 'select', 'name' => 'clear_on_highlight', 'label' => 'Clear On Highlight', 'options' => array('Yes', 'No')),
            array('type' => 'input.text', 'name' => 'typer_interval', 'label' => 'Typer Interval', 'description' => 'Defaults to 2000'),
        )
    ),
    'function' => function($args, $content = '') {
        $id = str_replace('.','',uniqid('acid_', true));
        
        $options = '';
        if(@$args['highlight_speed']) $options .= 'highlightSpeed:'.@$args['highlight_speed'].',';
        if(@$args['type_speed']) $options .= 'typeSpeed:'.@$args['type_speed'].',';
        if(@$args['clear_delay']) $options .= 'clearDelay:'.@$args['clear_delay'].',';
        if(@$args['type_delay']) $options .= 'typeDelay:'.@$args['type_delay'].',';
        if(@$args['clear_on_highlight']) $options .= 'clearOnHighlight:false,';
        if(@$args['typer_interval']) $options .= 'typerInterval:'.@$args['typer_interval'].',';
        $options = rtrim($options, ',');
        
        ob_start();
        ?><script type="text/javascript" defer="defer">
            jQuery(function($){
                $('#<?php echo $id; ?> .typer_text_effect[data-typer-targets]').typer(<?php if($options){ echo '{'.$options.'}'; } ?>);
            });
        </script><<?php echo @$args['tag']?:'span'; ?> id="<?php echo $id; ?>" class="typer_text <?php echo @$args['custom_class']; ?>" style="<?php echo @$args['tag']?'text-align:'.(@$args['alignment']?:'left').';':''; ?>"><span class="typer_text_before"><?php echo @$args['before_effect']; ?></span><span class="typer_text_effect" data-typer-targets="<?php echo @$args['effect_text']; ?>"></span><span class="typer_text_after"><?php echo @$args['after_effect']; ?></span></<?php echo @$args['tag']?:'span'; ?>><?php
        $return .= ob_get_clean();
        return $return;
    },
));