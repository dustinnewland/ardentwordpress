<?php
$sc = new \Ardent\Wp\Shortcode('Lightbox', array(
    'base' => 'ardent_lightbox',
    'category' => 'Ardent Plugins',
    'description' => 'Create a targetable lightbox',
    'fields' => array(
        'General' => array(
            array('type' => 'input.text', 'name' => 'lightbox_id', 'label' => 'Lightbox ID', 'description' => '&lt;a href="#id"&gt;CLICK&lt;/a&gt; to target this lightbox', 'admin_label' => true),
            array('type' => 'select', 'name' => 'type', 'label' => 'Type', 'options' => array('' => 'Content', 'url' => 'URL', 'youtube_id' => 'YouTube ID', 'vimeo_id' => 'Vimeo ID'), 'admin_label' => true),
            array('type' => 'input.text', 'name' => 'title', 'label' => 'Title', 'dependency' => array('element' => 'type', 'is_empty' => true), 'admin_label' => true),
            array('type' => 'input.text', 'name' => 'url', 'label' => 'URL', 'description' => 'Do not use this for Vimeo or YouTube videos. You may use this to load video files, image files, or websites.', 'dependency' => array('element' => 'type', 'value' => 'url'), 'admin_label' => true),
            array('type' => 'input.text', 'name' => 'youtube_id', 'label' => 'YouTube ID', 'dependency' => array('element' => 'type', 'value' => 'youtube_id'), 'admin_label' => true),
            array('type' => 'input.text', 'name' => 'vimeo_id', 'label' => 'Vimeo ID', 'dependency' => array('element' => 'type', 'value' => 'vimeo_id'), 'admin_label' => true),
            array('type' => 'input.wp.editor', 'name' => 'content', 'label' => 'Content', 'description' => 'Do not use this for Videos.', 'dependency' => array('element' => 'type', 'is_empty' => true), 'admin_label' => true),
        ),
        'Settings' => array(
            array('type' => 'input.text', 'name' => 'lightbox_width', 'label' => 'Width (i.e. 500px or 75%)'),
            array('type' => 'input.text', 'name' => 'lightbox_height', 'label' => 'Height (i.e. 500px or 75%)'),
            array('type' => 'input.text', 'name' => 'lightbox_speed', 'label' => 'Open Speed (ms)', 'value' => '350'),
            array('type' => 'input.checkbox', 'name' => 'open_on_enter', 'label' => 'Open on load'),
            array('type' => 'input.text', 'name' => 'open_delay', 'label' => 'Delay before opening (ms)', 'dependency' => array('element' => 'open_on_enter', 'not_empty' => true)),
        )
    ),
    'function' => function($args, $content = '') {
ob_start();

$type = @$args['type']?:'';
$url = $type?(@$args['url']?:''):'';
$youtube_id = $type?(@$args['youtube_id']?:''):'';
$vimeo_id = $type?(@$args['vimeo_id']?:''):'';

$lb_data = array(
    'id' => @$args['lightbox_id'] ? : uniqid("alb_"),
    'class' => 'lightcase-inline',
    'data-lightcase-speed' => @$args['lightbox_speed'] ? : '350',
    'data-lightcase-width' => @$args['lightbox_width'] ? : '50%',
    'data-lightcase-height' => @$args['lightbox_height'] ? : '50%',
    'data-lightcase-auto' => @$args['open_on_enter'],
    'data-lightcase-delay' => @$args['open_on_enter'] ? (@$args['open_delay'] ? : '0') : null,
    'data-lightcase-type' => $type?:'content',
);
if($url){ $lb_data['data-lightcase-url'] = $url; }
if($youtube_id){ $lb_data['data-lightcase-url'] = '//www.youtube.com/embed/'.$youtube_id.'?rel=0&autoplay=1'; }
if($vimeo_id){ $lb_data['data-lightcase-url'] = '//player.vimeo.com/video/'.$vimeo_id.'?autoplay=1'; }
$params = array_filter($lb_data);

$attribs = \Ardent\Html::build_attributes($params);
?>
<div style="display: none;">
    <div<?php echo \Ardent\Html::build_attributes($params); ?>>
        <?php if($type == ''){ ?>
            <?php echo @$args['title'] ? "<h2>{$args['title']}</h2>" : null; ?>
            <p><?php echo apply_filters('the_content', $content) ?></p>
        <?php } ?>
    </div>
</div>
<?php
return ob_get_clean();
},
        ));
