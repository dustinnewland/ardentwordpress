<?php
if(function_exists('vc_update_shortcode_param')){
    vc_update_shortcode_param('vc_column_inner', array(
        'type' => 'checkbox',
        'heading' => __( 'Disable Column', 'js_composer' ),
        'param_name' => 'disable_element',
        'description' => __( 'If checked the column won\'t be visible on the public side of your website. You can switch it back any time.', 'js_composer' ),
        'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
    ));
    vc_update_shortcode_param('vc_column_inner', array(
        "type" => "dropdown",
        "class" => "",
        "heading" => "Disable Column Type",
        "param_name" => "disable_element_type",
        'save_always' => true,
        "value" => array('Completely Hide' => '', 'Hide from logged in user' => 'hide_logged_in', 'Hide from logged out user' => 'hide_logged_out'),
        "dependency" => array('element' => 'disable_element', 'value' => 'yes')
    ));
    vc_update_shortcode_param('vc_column_inner', array(
        "type" => "dropdown",
        "class" => "",
        "heading" => "Background Image Quality",
        "param_name" => "bg_image_quality",
        'save_always' => true,
        "group" => "Advanced",
        "value" => array_flip(\Ardent\Helper::image_sizes_array())
    ));
    vc_update_shortcode_param('vc_column_inner', array(
        "type" => "dropdown",
        "class" => "",
        "heading" => "Background Image Size",
        "param_name" => "bg_image_size",
        'save_always' => true,
        "group" => "Advanced",
        "value" => array('Original' => '', 'Cover' => 'cover', 'Contain' => 'contain')
    ));
    vc_update_shortcode_param('vc_column_inner', array(
        "type" => "dropdown",
        "class" => "",
        "heading" => "Background Image Position",
        "param_name" => "bg_image_position",
        'save_always' => true,
        "group" => "Advanced",
        "value" => array(
            'Absolute Center' => '', 
            'Top Left' => 'left top', 
            'Top Center' => 'center top', 
            'Top Right' => 'right top',
            'Middle Left' => 'left center',
            'Middle Right' => 'right center',
            'Bottom Left' => 'left bottom',
            'Bottom Center' => 'center bottom',
            'Bottom Right' => 'right bottom',
        )
    ));
    vc_update_shortcode_param('vc_column_inner', array(
        "type" => "dropdown",
        "class" => "",
        "heading" => "Background Image Repeat",
        "param_name" => "bg_image_repeat",
        'save_always' => true,
        "group" => "Advanced",
        "value" => array('Repeat' => '', 'No Repeat' => 'no-repeat', 'Repeat X' => 'repeat-x', 'Repeat Y' => 'repeat-y')
    ));
}