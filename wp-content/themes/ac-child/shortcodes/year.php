<?php

$sc = new \Ardent\Wp\Shortcode('year', array(
    'base' => 'year',
    'category' => 'Ardent Plugins',
    'description' => 'Displays the current year according to the server.',
    'function' => function($args, $content = '') {
        return current_time('Y');
    },
));
