<?php
$imagesizes = \Ardent\Salient\Options::ImageSizes();
$sc = new \Ardent\Wp\Shortcode('Slide Reveal', array(
    'base' => 'ardent_slide_reveal',
    'category' => 'Ardent Plugins',
    'description' => 'Adds the ability to have an image with a slide out reveal functionality.',
    'fields' => array(
        'General' => array(
            array('type' => 'select', 'name' => 'direction', 'label' => 'Direction', 'description' => 'The direction you want the image to slide.', 'options' => array('' => 'Right', 'left' => 'Left', 'up' => 'Up', 'down' => 'Down'), 'admin_label' => true),
            array('type' => 'select', 'name' => 'effect', 'label' => 'Effect', 'options' => array('' => 'Slide', 'wipe' => 'Wipe')),
            array('type' => 'select', 'name' => 'mouse_event', 'label' => 'Mouse Event', 'options' => array('' => 'Mouse Move', 'click mousemove' => 'Drag', 'hover' => 'Hover', 'click' => 'Click'), 'admin_label' => true),
            array('type' => 'input.wp.media', 'name' => 'drag_icon', 'label' => 'Drag Icon', 'dependency' => array('element' => 'mouse_event', 'value' => 'click mousemove')),
            array('type' => 'input.text', 'name' => 'icon_width', 'label' => 'Icon Width', 'description' => 'Do not use px or %. Example: 50', 'dependency' => array('element' => 'drag_icon', 'not_empty' => true)),
            array('type' => 'input.text', 'name' => 'icon_height', 'label' => 'Icon Height', 'description' => 'Do not use px or %. Example: 50', 'dependency' => array('element' => 'drag_icon', 'not_empty' => true)),
            array('type' => 'select', 'name' => 'icon_location', 'label' => 'Icon Location', 'description' => 'Corner location varies based on the direction the slider slides.', 'options' => array('' => 'Middle', 'corner_tl' => 'Corner Top or Left', 'corner_rb' => 'Corner Right or Bottom'), 'dependency' => array('element' => 'drag_icon', 'not_empty' => true)),
            array('type' => 'input.wp.media', 'name' => 'slide_image', 'label' => 'Slide Image'),
            array('type' => 'select', 'name' => 'slide_image_size', 'label' => 'Slide Image Compression', 'description' => 'This is for decreasing the amount of time it takes to load the image. Used to select the physical size of the image based on the images sizes available within wordpress.', 'options' => $imagesizes),
            array('type' => 'input.wp.media', 'name' => 'reveal_image', 'label' => 'Reveal Image'),
            array('type' => 'select', 'name' => 'reveal_image_size', 'label' => 'Reveal Image Compression', 'description' => 'This is for decreasing the amount of time it takes to load the image. Used to select the physical size of the image based on the images sizes available within wordpress.', 'options' => $imagesizes),
        ),
        'Advanced' => array(
            array('type' => 'select', 'name' => 'easing', 'label' => 'Slide Easing', 'options' => \Ardent\Salient\Options::Easings()),
            array('type' => 'input.text', 'name' => 'delay', 'label' => 'Slide Delay', 'descripton' => 'Defaults to 250 when the mouse event is set to mouse move. Defaults to 1000 on the hover and click mouse events.')
        )
    ),
    'function' => function($args, $content = '') {
        $id = str_replace('.','',uniqid('acid_', true));
        $direction = @$args['direction']? :'right';
        $effect = @$args['effect']? :'slide';
        $mouse_event = @$args['mouse_event']? :'mousemove';
        $drag_icon = @$args['drag_icon']?wp_get_attachment_image_src(@$args['drag_icon'], 'medium'):'';
        $icon_width = @$args['icon_width']? :'50';
        $icon_height = @$args['icon_height']? :$icon_width;
        $icon_location = @$args['icon_location']? :'middle';
        $slide_image = @$args['slide_image']? :'';
        $slide_image_size = @$args['slide_image_size']? :'full';
        $reveal_image = @$args['reveal_image']? :'';
        $reveal_image_size = @$args['reveal_image_size']? :'full';
        $easing = @$args['easing']? :'';
        $delay = ((int)(@$args['delay']? :(strstr($mouse_event,'mousemove')?'0':'1000')))/1000;
        $css_rules = array();
        $drag_icon_classes = '';
        $styles = '';
        $return = '';
        if($reveal_image && $slide_image){
            $rimg = wp_get_attachment_image_src($reveal_image, $reveal_image_size);
            $simg = wp_get_attachment_image_src($slide_image, $slide_image_size);
            $css_rules['#'.$id.'.reveal_image']['background'] = 'url('.$rimg[0].') center no-repeat';
            $css_rules['#'.$id.' .slide_image']['background'] = 'url('.$simg[0].') center no-repeat';
            if((int)$delay){
                $css_rules['#'.$id.' .slide_image']['-webkit-transition'] = 'left '.$delay.'s '.$easing.', top '.$delay.'s '.$easing.', width '.$delay.'s '.$easing.', top '.$delay.'s '.$easing.', bottom '.$delay.'s '.$easing;
                $css_rules['#'.$id.' .slide_image']['-moz-transition'] = 'left '.$delay.'s '.$easing.', top '.$delay.'s '.$easing.', width '.$delay.'s '.$easing.', top '.$delay.'s '.$easing.', bottom '.$delay.'s '.$easing;
                $css_rules['#'.$id.' .slide_image']['-o-transition'] = 'left '.$delay.'s '.$easing.', top '.$delay.'s '.$easing.', width '.$delay.'s '.$easing.', top '.$delay.'s '.$easing.', bottom '.$delay.'s '.$easing;
                $css_rules['#'.$id.' .slide_image']['transition'] = 'left '.$delay.'s '.$easing.', top '.$delay.'s '.$easing.', width '.$delay.'s '.$easing.', top '.$delay.'s '.$easing.', bottom '.$delay.'s '.$easing;
            }
            if($drag_icon){
                $css_rules['#'.$id.' .drag_icon > .inner_drag_icon']['background'] = 'url('.$drag_icon[0].') center no-repeat';
                $css_rules['#'.$id.' .drag_icon > .inner_drag_icon']['background-size'] = 'contain';
                $css_rules['#'.$id.' .drag_icon']['width'] = $icon_width.'px';
                $css_rules['#'.$id.' .drag_icon']['height'] = $icon_height.'px';
            }
            $css_rules['#'.$id.'.reveal_image,#'.$id.' .slide_image']['background-size'] = 'cover';
            foreach ($css_rules as $selector => $rules) {
                    $rule = array();
                    foreach ($rules as $key => $val) {
                            $rule[] = "{$key}: {$val};";
                    }

                    $styles .= $selector . '{' . implode('', $rule) . '}';
            }
            $img = \Ardent\Html::get(array(
                'type' => 'wp.image',
                'itemid' => $slide_image,
                'size' => $slide_image_size,
            ));
            $slide_classes = array('slide_image', $direction);
            if($mouse_event == 'hover'){
                $slide_classes[] = 'hover';
            }
            if($drag_icon){
                if($direction === 'right' && $icon_location === 'middle'){
                    $drag_icon_classes = 'left middle';
                }else if($direction === 'right' && $icon_location === 'corner_tl'){
                    $drag_icon_classes = 'left top';
                }else if($direction === 'right' && $icon_location === 'corner_rb'){
                    $drag_icon_classes = 'left bottom';
                }else if($direction === 'left' && $icon_location === 'middle'){
                    $drag_icon_classes = 'right middle';
                }else if($direction === 'left' && $icon_location === 'corner_tl'){
                    $drag_icon_classes = 'right top';
                }else if($direction === 'left' && $icon_location === 'corner_rb'){
                    $drag_icon_classes = 'right bottom';
                }else if($direction === 'up' && $icon_location === 'middle'){
                    $drag_icon_classes = 'bottom middle';
                }else if($direction === 'up' && $icon_location === 'corner_tl'){
                    $drag_icon_classes = 'bottom left';
                }else if($direction === 'up' && $icon_location === 'corner_rb'){
                    $drag_icon_classes = 'bottom right';
                }else if($direction === 'down' && $icon_location === 'middle'){
                    $drag_icon_classes = 'top middle';
                }else if($direction === 'down' && $icon_location === 'corner_tl'){
                    $drag_icon_classes = 'top left';
                }else if($direction === 'down' && $icon_location === 'corner_rb'){
                    $drag_icon_classes = 'top right';
                }
            }
            $return .= '<style type="text/css">'.$styles.'</style>';
            ob_start(); ?>
                <script type="text/javascript" defer="defer">
                    jQuery(function($){
                        <?php if($mouse_event == 'click'){ ?>
                                var slideclick = function(){
                                    var slide_image = $('<?php echo '#'.$id; ?>').find('.slide_image');
                                    slide_image.toggleClass('slide');
                                    <?php if($effect === 'slide'){ ?>
                                        if(slide_image.hasClass('slide')){
                                            if(slide_image.hasClass('up')){
                                                slide_image.css('top','-'+slide_image.height()+'px');
                                            }else if(slide_image.hasClass('down')){
                                                slide_image.css('top',slide_image.height()+'px');
                                            }
                                        }else{
                                            slide_image.css('top','');
                                        }
                                    <?php } ?>
                                };
                                $('<?php echo '#'.$id; ?>').on('click', slideclick);
                                $(window).on('resize', slideclick);
                        <?php }else if(strstr($mouse_event, 'mousemove')){ ?>
                            <?php $is_click = strstr($mouse_event, 'click'); ?>;
                            function mouseMoveFunc(e){
                                var main = $('<?php echo '#'.$id; ?>');
                                var slide_image = main.find('.slide_image');
                                var drag_icon = main.find('.drag_icon');
                                var parentOffset = main.parent().offset();
                                var relX = e.pageX - parentOffset.left;
                                var relY = e.pageY - parentOffset.top;
                                if(slide_image.hasClass('right')){
                                    var newrelX = Math.max(0,Math.min(main.width(),relX));
                                    slide_image.css('left',newrelX+'px');
                                    <?php if($drag_icon){ ?>drag_icon.css('left',newrelX+'px');<?php } ?>
                                }else if(slide_image.hasClass('left')){
                                    <?php if($effect === 'slide'){ ?>
                                        slide_image.css('left',Math.max(-main.width(),Math.min(0,(relX-main.width())))+'px');
                                    <?php }else{ ?>
                                        slide_image.css('right',Math.max(0,Math.min(main.width(),(main.width()-relX)))+'px');
                                    <?php } ?>
                                    <?php if($drag_icon){ ?>drag_icon.css('right',Math.max(0,Math.min(main.width(),(main.width()-relX)))+'px');<?php } ?>
                                }else if(slide_image.hasClass('up')){
                                    <?php if($effect === 'slide'){ ?>
                                        slide_image.css('top',Math.max(-main.height(),Math.min(0,(relY-main.height())))+'px');
                                    <?php }else{ ?>
                                        slide_image.css('bottom',Math.max(0,Math.min(main.height(),(main.height()-relY)))+'px');
                                    <?php } ?>
                                    <?php if($drag_icon){ ?>drag_icon.css('bottom',Math.max(0,Math.min(main.height(),(main.height()-relY)))+'px');<?php } ?>
                                }else if(slide_image.hasClass('down')){
                                    var newrelY = Math.max(0,Math.min(main.height(),relY));
                                    slide_image.css('top',newrelY+'px');
                                    <?php if($drag_icon){ ?>drag_icon.css('top',newrelY+'px');<?php } ?>
                                }
                            }
                            <?php if($is_click){ ?>
                                $('<?php echo '#'.$id.($drag_icon?' > .drag_icon > .inner_drag_icon':''); ?>').on('mousedown', function(e){
                                    e.preventDefault();
                                    if(e.which === 1){
                            <?php } ?>
                                $('<?php echo $is_click?'body':'#'.$id; ?>').on('mousemove', mouseMoveFunc);
                            <?php if($is_click){ ?>
                                    }
                                });
                                $('body').on('mouseup', function(){
                                    $('body').unbind('mousemove', mouseMoveFunc);
                                });
                            <?php } ?>
                        <?php }else if($mouse_event == 'hover'){ ?>
                                var slidehover = function(){
                                    var slide_image = $('<?php echo '#'.$id; ?>').find('.slide_image');
                                    if(slide_image.hasClass('hovering')){
                                        if(slide_image.hasClass('up')){
                                            slide_image.css('top','-'+slide_image.height()+'px');
                                        }else if(slide_image.hasClass('down')){
                                            slide_image.css('top',slide_image.height()+'px');
                                        }
                                    }
                                };
                                $('<?php echo '#'.$id; ?>').hover(function(){
                                    var slide_image = $(this).find('.slide_image');
                                    slide_image.addClass('hovering');
                                    <?php if($effect === 'slide'){ ?>slidehover();<?php } ?>
                                },function(){
                                    var slide_image = $(this).find('.slide_image');
                                    slide_image.removeClass('hovering');
                                    <?php if($effect === 'slide'){ ?>$(this).find('.slide_image').css('top','');<?php } ?>
                                });
                                $(window).on('resize', slidehover);
                        <?php } ?>
                    });
                </script>
                <div id="<?php echo $id; ?>" class="reveal_image <?php echo $effect.(strstr($mouse_event, 'mousemove')?' mousemove':''); ?>">
                    <?php if($drag_icon){ ?><div class="drag_icon <?php echo $drag_icon_classes; ?>"><div class="inner_drag_icon"></div></div><?php } ?>
                    <div class="slide_container">
                        <div class="<?php echo implode(' ',$slide_classes); ?>"><?php echo $img; ?></div>
                        <?php echo $img; ?>
                    </div>
                </div>
            <?php
            $return .= ob_get_clean();
        }
        return $return;
    },
));