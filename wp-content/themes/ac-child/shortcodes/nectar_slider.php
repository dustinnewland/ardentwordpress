<?php
add_action('init', function(){
    remove_shortcode('nectar_slider', 'nectar_slider_processing');
    add_shortcode('nectar_slider', array('\Ardent\Salient\Nectar\Slider\Display', 'shortcodeProcessing'));
}, 20);