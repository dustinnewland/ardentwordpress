<?php
$posts = new WP_Query(array(
    'post_type' => 'page',
    'depth' => 0,
    'numberposts' => -1,
    'orderby' => 'title',
    'order' => 'ASC',
    'post_status' => 'publish',
    'child_of' => 0,
    'has_password' => true
));
$passposts = array();
if($posts->have_posts()){
    while($posts->have_posts()){
        $posts->the_post();
        $passposts[] = (object) array('value' => get_the_ID(), 'title' => get_the_title());
    }
}
wp_reset_postdata();
$sc = new \Ardent\Wp\Shortcode('Page Password', array(
    'base' => 'ardent_page_password',
    'category' => 'Ardent Plugins',
    'description' => 'Shows password form for a page and redirects based on selected options.',
    'fields' => array(
        'General' => array(
            array('type' => 'select', 'name' => 'pw_page', 'label' => 'Password Page', 'description' => 'Select the page that the password is setup on.', 'options' => $passposts),
        )
    ),
    'function' => function($args, $content = '') {
        $post = get_post((int)@$args['pw_page']);
	$label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
	$return = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post"><div class="post_password_form">
	<p>' . __( 'This content is password protected. To view it please enter your password below:' ) . '</p>
	<p><label for="' . $label . '">' . __( 'Password:' ) . '</label> <input name="post_password" id="' . $label . '" type="password" size="20" /> <input type="submit" name="Submit" value="' . esc_attr_x( 'Enter', 'post password form' ) . '" /></p></div></form>';
        return $return;
    },
));
