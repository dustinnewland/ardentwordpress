<?php
$sc = new \Ardent\Wp\Shortcode('Colorbox', array(
    'base' => 'ardent_colorbox',
    'category' => 'Ardent Plugins',
    'description' => 'Create a targetable lightbox',
    'fields' => array(
        'General' => array(
            array('type' => 'input.text', 'name' => 'lightbox_id', 'label' => 'Lightbox ID', 'description' => '&lt;a href="#id"&gt;CLICK&lt;/a&gt; to target this lightbox', 'admin_label' => true),
            array('type' => 'input.text', 'name' => 'title', 'label' => 'Title', 'holder' => 'h4'),
            array('type' => 'input.wp.editor', 'name' => 'content', 'label' => 'Content', 'holder' => 'div'),
        ),
        'Settings' => array(
            array('type' => 'input.text', 'name' => 'lightbox_width', 'label' => 'Width (i.e. 500px or 75%)'),
            array('type' => 'input.text', 'name' => 'lightbox_height', 'label' => 'Height (i.e. 500px or 75%)'),
            array('type' => 'input.text', 'name' => 'lightbox_speed', 'label' => 'Open Speed (ms)', 'value' => '350'),
            array('type' => 'input.checkbox', 'name' => 'open_on_enter', 'label' => 'Open on load'),
            array('type' => 'input.text', 'name' => 'open_delay', 'label' => 'Delay before opening (ms)', 'dependency' => array('element' => 'open_on_enter', 'not_empty' => true)),
        )
    ),
    'function' => function($args, $content = '') {
ob_start();
$params = array_filter(array(
    'id' => @$args['lightbox_id'] ? : uniqid("alb_"),
    'class' => 'colorbox-inline',
    'data-colorbox-speed' => @$args['lightbox_speed'] ? : '350',
    'data-colorbox-width' => @$args['lightbox_width'] ? : '50%',
    'data-colorbox-height' => @$args['lightbox_height'] ? : '50%',
    'data-colorbox-auto' => @$args['open_on_enter'],
    'data-colorbox-delay' => @$args['open_on_enter'] ? (@$args['open_delay'] ? : '0') : null,
        ));

$attribs = \Ardent\Html::build_attributes($params);
?>
<div style="display: none;">
    <div<?php echo \Ardent\Html::build_attributes($params); ?>>
        <?php echo @$args['title'] ? "<h2>{$args['title']}</h2>" : null; ?>
        <p><?php echo apply_filters('the_content', $content) ?></p>
    </div>
</div>
<?php
return ob_get_clean();
},
        ));
