<?php
if(function_exists('vc_update_shortcode_param')){
    vc_update_shortcode_param('vc_row_inner', array(
        'type' => 'checkbox',
        'heading' => __( 'Disable Row', 'js_composer' ),
        'param_name' => 'disable_element',
        'description' => __( 'If checked the row won\'t be visible on the public side of your website. You can switch it back any time.', 'js_composer' ),
        'value' => array( __( 'Yes', 'js_composer' ) => 'yes' ),
    ));
    vc_update_shortcode_param('vc_row_inner', array(
        "type" => "dropdown",
        "class" => "",
        "heading" => "Disable Row Type",
        "param_name" => "disable_element_type",
        'save_always' => true,
        "value" => array('Completely Hide' => '', 'Hide from logged in user' => 'hide_logged_in', 'Hide from logged out user' => 'hide_logged_out'),
        "dependency" => array('element' => 'disable_element', 'value' => 'yes')
    ));
}