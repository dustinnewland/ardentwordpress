<?php
if(function_exists('vc_update_shortcode_param')){
    vc_update_shortcode_param('vc_row', array(
        "type" => "dropdown",
        "class" => "",
        "heading" => "Disable Row Type",
        "param_name" => "disable_element_type",
        'save_always' => true,
        "value" => array('Completely Hide' => '', 'Hide from logged in user' => 'hide_logged_in', 'Hide from logged out user' => 'hide_logged_out'),
        "dependency" => array('element' => 'disable_element', 'value' => 'yes')
    ));
    vc_update_shortcode_param('vc_row', array(
        "type" => "dropdown",
        "class" => "",
        "heading" => "Background Image Quality",
        "param_name" => "bg_image_quality",
        'save_always' => true,
        "group" => "Advanced",
        "value" => array_flip(\Ardent\Helper::image_sizes_array())
    ));
    vc_update_shortcode_param('vc_row', array(
        "type" => "dropdown",
        "class" => "",
        "heading" => "Show Video On Mobile",
        "param_name" => "video_on_mobile",
        'save_always' => true,
        "group" => "Advanced",
        "value" => array("No", "Yes")
    ));
}