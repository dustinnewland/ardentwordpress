<?php
$sc = new \Ardent\Wp\Shortcode('Image with Overlay', array(
    'base' => 'ardent_image_overlay',
    'category' => 'Ardent Plugins',
    'description' => 'Create an image with overlay',
    'fields' => \Ardent\Salient\Shortcodes\Imageoverlay::Fields(),
    'function' => function($args, $content = '') {
        $id = str_replace('.','',uniqid('acid_', true));
        $image_scale = @$args['image_scale']? : '';
        $area_height = @$args['area_height']? : '250px';
        $image_width = @$args['image_width']? : '';
        $image_height = @$args['image_height']? : '';
        $image_repeat = @$args['image_repeat']? : 'no-repeat';
        $image_align_contain = @$args['image_align_contain']? : 'center';
        $image_align_custom = @$args['image_align_custom']? : 'center';
        $image_size = @$args['image_size']? : 'full';
        $hover_image_scale = @$args['hover_image_scale']? : '';
        $hover_image_width = @$args['hover_image_width']? : '100px';
        $hover_image_height = @$args['hover_image_height']? : 'auto';
        $hover_image_repeat = @$args['hover_image_repeat']? : 'no-repeat';
        $hover_image_align_contain = @$args['hover_image_align_contain']? : 'center';
        $hover_image_align_custom = @$args['hover_image_align_custom']? : 'center';
        $hover_image_size = @$args['hover_image_size']? : 'full';
        $image_link = @$args['image_link']? : '';
        $image_src = wp_get_attachment_image_src(@$args['image'], 'full');
        $image_span_src = wp_get_attachment_image_src(@$args['image'], $image_size);
        $hover_image_src = wp_get_attachment_image_src(@$args['hover_image'], $hover_image_size);
        $src = html_entity_decode($image_link?(($image_link=='self')?$image_src[0]:(@$args['custom_link'] ? : '')):'');
        $target = @$args['custom_link_target'] ? : '_self';
        $text_color = @$args['overlay_text_color']? :'';
        $text_force = @$args['overlay_text_force']? :0;
        $text_opacity = @$args['overlay_text_opacity']? :'1';
        $text_color_hover = @$args['overlay_text_color_hover']? :'';
        $text_force_hover = @$args['overlay_text_force_hover']? :0;
        $text_opacity_hover = @$args['overlay_text_opacity_hover']? :'1';
        $mobile_state = @$args['mobile_state']? :0;
        $vertical_align = @$args['overlay_vertical_align']? :'flex-start';
        $vertical_align_hover = @$args['overlay_vertical_align_hover']? :'top';
        $transition_animation_on_hover = @$args['overlay_transition_animation_on_hover']? :'';
        $transition_in_speed = @$args['overlay_transition_in_speed']? :'';
        $transition_in_easing = @$args['overlay_transition_in_easing']? :'';
        $transition_out_speed = @$args['overlay_transition_out_speed']? :$transition_in_speed;
        $transition_out_easing = @$args['overlay_transition_out_easing']? :$transition_in_easing;
        $default_css = @$args['default_css']? :'';
        $hover_css = @$args['hover_css']? :'';
        $split_content = preg_split('/(((<.*?>)|)<span id="more.*?"><\/span>((<\/.*?>)|))|(((<.*?>)|)<!--more-->((<\/.*?>)|))/i', $content);
        
        $root_classes = array(
            'ac-overlay-wrap',
            (count($split_content) > 1)?'ac-has-split-content':'',
            $image_link?(($image_link=="self")?'pp ':''):'img-with-aniamtion-wrap',
            @$args['image_align'] ? : '',
            $mobile_state?'':'mobile-force-hover'
        );
        
        $params = array(
            'id' => $id,
            'class' => implode(' ',$root_classes)
        );
        if($image_link=='lightbox'){
            $params['data-lightbox-id'] = $src;
        }
        
        $img = \Ardent\Html::get(array(
            'type' => 'wp.image',
            'itemid' => @$args['image'],
            'class' => 'img-with-animation '.(@$args['extra_class'] ? : ''),
            'data-delay' => @$args['animation_delay'] ? : '0',
            'height' => '100%',
            'width' => '100%',
            'size' => $image_size,
            'data-animation' => strtolower(str_replace(" ", "-", (@$args['css_animation'] ? : 'Fade In'))),
        ));
        $img_params = array(
            'class' => 'img-default-span img-with-animation '.(@$args['extra_class'] ? : ''),
            'data-delay' => @$args['animation_delay'] ? : '0',
            'data-animation' => strtolower(str_replace(" ", "-", (@$args['css_animation'] ? : 'Fade In'))),
        );
        $img_container_params = array(
            'class' => 'ac-overlay-image'.($image_scale?' image-scale':'').($args['hover_image']?' ac-overlay-image-fade':''),
        );
        
        $backgroundsize = $image_scale?:'cover';
        $backgroundsize = $image_scale=='original'?$image_span_src[1].'px '.$image_span_src[2].'px':$backgroundsize;
        $backgroundsize = $image_scale=='custom'?$image_width.' '.$image_height:$backgroundsize;
        $hover_backgroundsize = $hover_image_scale?:'cover';
        $hover_backgroundsize = $hover_image_scale=='original'?$hover_image_src[1].'px '.$hover_image_src[2].'px':$hover_backgroundsize;
        $hover_backgroundsize = $hover_image_scale=='custom'?$hover_image_width.' '.$hover_image_height:$hover_backgroundsize;
        
        $atts = \Ardent\Html::build_attributes($params);
        $img_atts = \Ardent\Html::build_attributes($img_params);
        $img_container_atts = \Ardent\Html::build_attributes($img_container_params);
        $c = 0;
        ob_start();
        ?>
            <?php if($image_link == 'lightbox' && $src){ ?>
                <a href="#<?php echo $src; ?>" style="display:none;"></a>
            <?php } ?>
            <div class="ac-overlay-container">
                <?php foreach($split_content as $cnt){
                    if($c > 1) break;
                    $augment = !$c?'default':'hover';
                    ?>
                    <?php $css_box = vc_shortcode_custom_css_class( $args[$augment.'_css'], ' ' ); ?>
                    <div class="ac-overlay-content-wrapper<?php if(count($split_content) > 1){ ?> split-content-<?php echo $augment; ?><?php } ?><?php echo $css_box; ?>">
                        <div class="ac-overlay-content-table">
                            <div class="ac-overlay-content">
                                <div class="ac-overlay-content-inner-wrapper">
                                    <?php
                                        echo apply_filters('the_content',$cnt);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php $c++; } ?>
            </div>
        <?php
        $overlay = ob_get_clean();
        
        ob_start();
        ?><script type="text/javascript">
            jQuery(function($){
                <?php if($image_link && $src){ ?>
                    $("#<?php echo $id; ?>").on('click', function(e){
                        e.preventDefault(); 
                        <?php if ($image_link == 'lightbox'){ ?>
                            $('#<?php echo $id; ?> > a[href^="#"]').trigger('click');
                        <?php }else{ ?>
                            if (e.ctrlKey){
                                window.open('<?php echo $src; ?>','_blank');
                            }else{
                                window.open('<?php echo $src; ?>','<?php echo $target; ?>');
                            }
                        <?php } ?>
                    });
                <?php } ?>
                <?php if(count($split_content) == 1){ ?>
                    $(window).on("resize", function(){
                        if($('body').hasClass('mobile') && $('#<?php echo $id; ?>').hasClass('mobile-force-hover')){
                            $("#<?php echo $id; ?>").find('.ac-overlay-content-wrapper').addClass('<?php echo vc_shortcode_custom_css_class( $hover_css, ' ' ); ?>');
                            $("#<?php echo $id; ?>").find('.ac-overlay-content-wrapper').removeClass('<?php echo vc_shortcode_custom_css_class( $default_css, ' ' ); ?>');
                        }else{
                            $("#<?php echo $id; ?>").find('.ac-overlay-content-wrapper').addClass('<?php echo vc_shortcode_custom_css_class( $default_css, ' ' ); ?>');
                            $("#<?php echo $id; ?>").find('.ac-overlay-content-wrapper').removeClass('<?php echo vc_shortcode_custom_css_class( $hover_css, ' ' ); ?>');
                        }
                    });
                    $("#<?php echo $id; ?>").on("mouseover", function(){
                        if($('body').hasClass('mobile') && $('#<?php echo $id; ?>').hasClass('mobile-force-hover')){
                            $(this).find('.ac-overlay-content-wrapper').addClass('<?php echo vc_shortcode_custom_css_class( $default_css, ' ' ); ?>');
                            $(this).find('.ac-overlay-content-wrapper').removeClass('<?php echo vc_shortcode_custom_css_class( $hover_css, ' ' ); ?>');
                        }else{
                            $(this).find('.ac-overlay-content-wrapper').addClass('<?php echo vc_shortcode_custom_css_class( $hover_css, ' ' ); ?>');
                            $(this).find('.ac-overlay-content-wrapper').removeClass('<?php echo vc_shortcode_custom_css_class( $default_css, ' ' ); ?>');
                        }
                    });
                    $("#<?php echo $id; ?>").on("mouseout", function(){
                        if($('body').hasClass('mobile') && $('#<?php echo $id; ?>').hasClass('mobile-force-hover')){
                            $(this).find('.ac-overlay-content-wrapper').addClass('<?php echo vc_shortcode_custom_css_class( $hover_css, ' ' ); ?>');
                            $(this).find('.ac-overlay-content-wrapper').removeClass('<?php echo vc_shortcode_custom_css_class( $default_css, ' ' ); ?>');
                        }else{
                            $(this).find('.ac-overlay-content-wrapper').addClass('<?php echo vc_shortcode_custom_css_class( $default_css, ' ' ); ?>');
                            $(this).find('.ac-overlay-content-wrapper').removeClass('<?php echo vc_shortcode_custom_css_class( $hover_css, ' ' ); ?>');
                        }
                    });
                <?php } ?>
            })
        </script><style type="text/css" scoped="true">
            <?php if($image_link && $src){ ?>
                #<?php echo $id; ?> > .ac-overlay-container *{
                    pointer-events: none;
                    cursor:pointer;
                }
                #<?php echo $id; ?> > .ac-overlay-container{
                        cursor:pointer;
                }
            <?php } ?>
            <?php if(count($split_content) == 1 && $transition_animation_on_hover){ ?>
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper{
                    -webkit-transition: all .25s ease-out;
                    -moz-transition: all .25s ease-out;
                    -o-transition: all .25s ease-out;
                    transition: all .25s ease-out;
                }
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper.<?php echo vc_shortcode_custom_css_class( $default_css, ' ' ); ?>{
                    <?php if($transition_out_speed){ ?>
                        -webkit-transition-duration:<?php echo $transition_out_speed; ?>s;
                        -moz-transition-duration:<?php echo $transition_out_speed; ?>s;
                        -o-transition-duration:<?php echo $transition_out_speed; ?>s;
                        transition-duration:<?php echo $transition_out_speed; ?>s;
                    <?php } if($transition_out_easing){ ?>
                        -webkit-transition-timing-function:<?php echo $transition_out_easing; ?>;
                        -moz-transition-timing-function:<?php echo $transition_out_easing; ?>;
                        -o-transition-timing-function:<?php echo $transition_out_easing; ?>;
                        transition-timing-function:<?php echo $transition_out_easing; ?>;
                    <?php } ?>
                }
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper.<?php echo vc_shortcode_custom_css_class( $hover_css, ' ' ); ?>{
                    <?php if($transition_out_speed){ ?>
                        -webkit-transition-duration:<?php echo $transition_in_speed; ?>s;
                        -moz-transition-duration:<?php echo $transition_in_speed; ?>s;
                        -o-transition-duration:<?php echo $transition_in_speed; ?>s;
                        transition-duration:<?php echo $transition_in_speed; ?>s;
                    <?php } if($transition_out_easing){ ?>
                        -webkit-transition-timing-function:<?php echo $transition_in_easing; ?>;
                        -moz-transition-timing-function:<?php echo $transition_in_easing; ?>;
                        -o-transition-timing-function:<?php echo $transition_in_easing; ?>;
                        transition-timing-function:<?php echo $transition_in_easing; ?>;
                    <?php } ?>
                }
            <?php } ?>
            #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table{
                align-items:<?php echo $vertical_align; ?>;
            }
            #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content{
                opacity:<?php echo $text_opacity; ?>;
            }
            #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper.split-content-hover > .ac-overlay-content-table{
                align-items:<?php echo $vertical_align_hover; ?>;
            }
            #<?php echo $id; ?>:hover > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content{
                opacity:<?php echo $text_opacity_hover; ?>;
            }
            <?php if($text_color){ ?>
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content<?php echo $text_force?' *':'' ?>{
                    <?php echo $text_color?('color:'.$text_color.($text_force_hover?' !important':'').';'):'' ?>
                }
            <?php } ?>
            <?php if($text_color_hover){ ?>
                #<?php echo $id; ?>:hover > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content<?php echo $text_force_hover?' *':'' ?>{
                    <?php echo $text_color_hover?('color:'.$text_color_hover.($text_force_hover?' !important':'').';'):'' ?>
                }
                @media only screen and (max-width:1000px){
                    #<?php echo $id; ?>.mobile-force-hover > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content<?php echo $text_force_hover?' *':'' ?>{
                        <?php echo $text_color_hover?('color:'.$text_color_hover.($text_force_hover?' !important':'').';'):'' ?>
                    }
                }
                <?php } ?>
            <?php if($transition_out_speed){ ?>
                #<?php echo $id; ?> > .ac-overlay-container,
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper,
                #<?php echo $id; ?> > .ac-overlay-image-fade > .ac-overlay-image-container,
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content,
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content *{
                    -webkit-transition-duration:<?php echo $transition_out_speed; ?>s;
                    -moz-transition-duration:<?php echo $transition_out_speed; ?>s;
                    -o-transition-duration:<?php echo $transition_out_speed; ?>s;
                    transition-duration:<?php echo $transition_out_speed; ?>s;
                }
            <?php } ?>
            <?php if($transition_out_easing){ ?>
                #<?php echo $id; ?> > .ac-overlay-container,
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper,
                #<?php echo $id; ?> > .ac-overlay-image-fade > .ac-overlay-image-container,
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content,
                #<?php echo $id; ?> > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content *{
                    -webkit-transition-timing-function:<?php echo $transition_out_easing; ?>;
                    -moz-transition-timing-function:<?php echo $transition_out_easing; ?>;
                    -o-transition-timing-function:<?php echo $transition_out_easing; ?>;
                    transition-timing-function:<?php echo $transition_out_easing; ?>;
                }
            <?php } ?>
            <?php if($transition_in_speed){ ?>
                #<?php echo $id; ?>:hover > .ac-overlay-container,
                #<?php echo $id; ?>:hover > .ac-overlay-container > .ac-overlay-content-wrapper,
                #<?php echo $id; ?>:hover > .ac-overlay-image-fade > .ac-overlay-image-container,
                #<?php echo $id; ?>:hover > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content,
                #<?php echo $id; ?>:hover > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content *{
                    -webkit-transition-duration:<?php echo $transition_in_speed; ?>s;
                    -moz-transition-duration:<?php echo $transition_in_speed; ?>s;
                    -o-transition-duration:<?php echo $transition_in_speed; ?>s;
                    transition-duration:<?php echo $transition_in_speed; ?>s;
                }
            <?php if($transition_in_easing){ ?>
                #<?php echo $id; ?>:hover > .ac-overlay-container,
                #<?php echo $id; ?>:hover > .ac-overlay-container > .ac-overlay-content-wrapper,
                #<?php echo $id; ?>:hover > .ac-overlay-image-fade > .ac-overlay-image-container,
                #<?php echo $id; ?>:hover > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content,
                #<?php echo $id; ?>:hover > .ac-overlay-container > .ac-overlay-content-wrapper > .ac-overlay-content-table > .ac-overlay-content *{
                    -webkit-transition-timing-function:<?php echo $transition_in_easing; ?>;
                    -moz-transition-timing-function:<?php echo $transition_in_easing; ?>;
                    -o-transition-timing-function:<?php echo $transition_in_easing; ?>;
                    transition-timing-function:<?php echo $transition_in_easing; ?>;
                }
            <?php } ?>
            <?php } ?>
            #<?php echo $id; ?> > .ac-overlay-image{
                background:url('<?php echo $hover_image_src[0]; ?>') center no-repeat;
                background-size:<?php echo $hover_backgroundsize; ?>;
                <?php if($hover_image_scale){ ?>
                    background-repeat:<?php echo $hover_image_repeat; ?>;
                    background-position:<?php echo $hover_image_scale=='contain'?$hover_image_align_contain:$hover_image_align_custom; ?>;
                <?php } ?>
            }
            #<?php echo $id; ?>:hover > .ac-overlay-image-fade > .ac-overlay-image-container{
                opacity:0;
            }
            <?php if($image_scale){ ?>
                #<?php echo $id; ?>{
                    height:<?php echo $area_height?:'100px'; ?>;
                }
                #<?php echo $id; ?> .img-default-span{
                    background:url(<?php echo $image_span_src[0]; ?>);
                    background-size:<?php echo $backgroundsize; ?>;
                    background-repeat:<?php echo $hover_image_repeat; ?>;
                    background-position:<?php echo $image_scale=='contain'?$image_align_contain:$image_align_custom; ?>;
                }
            <?php } ?>
        </style><?php
        $imgspan = '<span'.$img_atts.'></span>';
        echo '<div'.$atts.'>'.$overlay.'<span'.$img_container_atts.'><span class="ac-overlay-image-container">'.($image_scale?$imgspan:$img).'</span></span></div>';
        return ob_get_clean();
    }
));
