jQuery(function($){
    $('body').on('DOMNodeInserted', '.pp_content_container', function(){
        $('.pp_content_container').swipe({
            swipeRight:function(e){ 
                $('.pp_pic_holder .pp_arrow_previous').trigger('click');
            },
            swipeLeft:function(e){ 
                $('.pp_pic_holder .pp_arrow_next').trigger('click');
            }
        });
    });
});