var colorbox_is_mobile, colorbox_was_mobile, current_colorbox;

(function ($) {
    $(function () {
        colorbox_is_mobile = ($(window).width() <= 1000) ? true : false;
        colorbox_was_mobile = colorbox_is_mobile;
        $('.colorbox-inline').each(function () {
            var $cb_el = $(this);
            var target_id = $(this).attr('id');
            var links = jQuery('a[href="#' + target_id + '"]');

            var opts = {
                inline: true,
                width: $(this).data('colorbox-width'),
                height: $(this).data('colorbox-height'),
                speed: $(this).data('colorbox-speed'),
                open: true,
                onOpen: function () {
                    $('body').css('overflow', 'visible');
                },
                onClosed: function () {
                    $('body').css('overflow', '');
                    current_colorbox = null;
                }
            }
            var iframeopts = {
                iframe: true,
                width: $(this).data('colorbox-width'),
                height: $(this).data('colorbox-height'),
                speed: $(this).data('colorbox-speed'),
                onOpen: function () {
                    $('body').css('overflow', 'visible');
                },
                onComplete: function(){
                    if(colorbox_is_mobile){
                        $.colorbox.resize({width: '85%', height: '85%'});
                    }
                },
                onClosed: function () {
                    $('body').css('overflow', '');
                    current_colorbox = null;
                }
            }

            if ($(this).data('colorbox-auto')) {
                var delay = +$(this).data('colorbox-delay');
                if (delay = 'NaN') {
                    delay = 0;
                }
                window.setTimeout(function () {
                    opts.href = '#' + target_id;
                    opts.width = colorbox_is_mobile ? '85%' : $cb_el.data('colorbox-width');
                    opts.height = colorbox_is_mobile ? '85%' : $cb_el.data('colorbox-height');
                    current_colorbox = $(this).colorbox(opts);
                }, delay);
            }
            links.each(function () {
                $(this).on('click', function (ev, ui) {
                    opts.width = colorbox_is_mobile ? '85%' : $cb_el.data('colorbox-width');
                    opts.height = colorbox_is_mobile ? '85%' : $cb_el.data('colorbox-height');
                    ev.preventDefault();
                    current_colorbox = $(this).colorbox(opts);
                    ev.preventDefault();
                    return false;
                });
            });
        });
        $(window).on('resize', function () {
            colorbox_is_mobile = ($(window).width() <= 1000) ? true : false;
            if (current_colorbox) {
                colorbox_was_mobile = colorbox_is_mobile;
                if (colorbox_is_mobile) {
                    $.colorbox.resize({width: '85%', height: '85%'});
                } else {
                    var current_colorbox_id = $.colorbox.element().data('colorbox').href;
                    var el = jQuery(current_colorbox_id);
                    var o = {width: el.data('colorbox-width'), height: el.data('colorbox-height'), speed: 0};
                    $.colorbox.resize(o);
                }
            }
        });
    });
})(jQuery);
