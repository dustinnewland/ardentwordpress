(function ($) {
    $(function () {
        $('.lightcase-inline').each(function () {
            var target_id = $(this).attr('id');
            var links = jQuery('a[href="#' + target_id + '"]');
            var that = this;

            var opts = {
                speedIn: $(this).data('lightcase-speed'),
                speedOut: $(this).data('lightcase-speed')
            }
            function lbClick(obj){
                var width = (String($(that).data('lightcase-width')).indexOf('%')>-1)?($(window).width()*(parseInt($(that).data('lightcase-width'))/100)):parseInt($(that).data('lightcase-width'));
                var height = (String($(that).data('lightcase-height')).indexOf('%')>-1)?($(window).height()*(parseInt($(that).data('lightcase-height'))/100)):parseInt($(that).data('lightcase-height'));
                opts.maxWidth = width;
                opts.maxHeight = height;
                opts.inline = {width:width,height:height};
                opts.iframe = {width:width,height:height};
                opts.video = {width: width,height: height,poster: '',preload: 'auto',controls: true,autobuffer: true,autoplay: true,loop: false};
                if($(that).data('lightcase-url')){
                    opts.href = $(that).data('lightcase-url');
                }
                $(obj).lightcase('start', opts);
            }
            if ($(this).data('lightcase-auto')) {
                var delay = $(this).data('lightcase-delay');
                if (delay == 'NaN') {
                    delay = 0;
                }
                window.setTimeout(function () {
                    opts.href = '#' + target_id;
                    lbClick(that);
                }, delay);
            }
            links.data('lightcase-width', $(this).data('lightcase-width')).data('lightcase-height', $(this).data('lightcase-height'));
            $("a[href*='wp-admin/admin-ajax.php?action=frm_forms_preview']").data('lightcase-width', $(this).data('lightcase-width')).data('lightcase-height', $(this).data('lightcase-height'));
            links.each(function () {
                $(this).on('click', function(ev){ lbClick(this); ev.preventDefault(); return false; });
            });
            $("a[href*='wp-admin/admin-ajax.php?action=frm_forms_preview']").on('click', function(ev){ lbClick(this); ev.preventDefault(); return false; });
        });
    });
})(jQuery);
