(function($){
    $(function(){
        $('form.post-password-form[action$="postpass"]').on('submit' ,function(ev){
            ev.preventDefault();
            var t = $(this);
            t.find('.notice').remove();
            var id = t.find('label').attr('for').replace('pwbox-', '');
            t.find('input[type="submit"]').attr('disabled','disabled');
            t.prepend('<div class="custom-loader-css"><div class="uil-ring-css" style="transform:scale(0.33);"><div></div></div></div>');
            $.ajax({url:acPostPW.ajaxurl, async: false, dataType: 'text', method:'POST', data:{
                action: 'ardent_pw_check',
                pass: t.find('input[name="post_password"]').val(),
                pid: id
            }, success:function( text ) {
                var response = $.parseJSON(text);
                if ( response.status === 'success' ) {
                    window.location.href = response.redirect;
                } else {
                    t.find('.post_password_form').prepend('<div class="notice notice-error notice-box" style="display:none;">'+response.message+'</div>');
                    t.find('.notice').slideDown();
                    t.find('.custom-loader-css').fadeOut(function(){ $(this).remove(); });
                    t.find('input[type="submit"]').removeAttr('disabled');
                }
            },error:function(){
                t.find('.post_password_form').prepend('<div class="notice notice-error notice-box" style="display:none;">There was an error submitting the password. Please try again later.</div>');
                t.find('.notice').slideDown();
                t.find('.custom-loader-css').fadeOut(function(){ $(this).remove(); });
                t.find('input[type="submit"]').removeAttr('disabled');
            }});
        });
    });
}(jQuery));