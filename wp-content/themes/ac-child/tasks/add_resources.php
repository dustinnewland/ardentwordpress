<?php
add_action('wp_enqueue_scripts', function() {
    $tdata = wp_get_theme();
    wp_deregister_script( 'nectarFrontend' );
    wp_deregister_script( 'nectarSlider' );
    if(WP_DEBUG){
        wp_register_script('nectarFrontend', get_stylesheet_directory_uri() . '/js/init.js', array('jquery', 'superfish'), $tdata['Version'], TRUE);
        wp_register_script('nectarSlider', get_stylesheet_directory_uri() . '/js/nectar-slider.js', array('jquery'), $tdata['Version'], TRUE);
    }else{
        wp_register_script('nectarFrontend', get_stylesheet_directory_uri() . '/js/min/init.min.js', array('jquery', 'superfish'), $tdata['Version'], TRUE);
        wp_register_script('nectarSlider', get_stylesheet_directory_uri() . '/js/min/nectar-slider.min.js', array('jquery'), $tdata['Version'], TRUE);
    }
}, 5);
add_action('wp_enqueue_scripts', function() {
    $tdata = wp_get_theme();
    $options = get_nectar_theme_options();
    $development = false; // change to true to bypass minification
    wp_deregister_style( 'main-styles' );
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));
    wp_enqueue_style('main-styles', get_stylesheet_uri(), array('parent-style'), $tdata['Version']);
    wp_enqueue_style('colorbox', get_stylesheet_directory_uri() . '/css/colorbox.css', array(), $tdata['Version']);
    wp_enqueue_style('lightcase', get_stylesheet_directory_uri() . '/css/lightcase.css', array(), $tdata['Version']);
    wp_enqueue_script( 'touchswipe' );
    if($development || WP_DEBUG){
        wp_enqueue_script('colorbox', get_stylesheet_directory_uri() . '/js/jquery.colorbox.min.js', array('jquery'), $tdata['Version']);
        wp_enqueue_script('ardent-colorbox', get_stylesheet_directory_uri() . '/js/colorbox.js', array('colorbox'), $tdata['Version']);
        wp_enqueue_script('lightcase', get_stylesheet_directory_uri() . '/js/jquery.lightcase.js', array('jquery'), $tdata['Version']);
        wp_enqueue_script('ardent-lightcase', get_stylesheet_directory_uri() . '/js/lightcase.js', array('jquery', 'lightcase'), $tdata['Version']);
        wp_enqueue_script('ardent-typer', get_stylesheet_directory_uri() . '/js/typer.min.js', array('jquery'), $tdata['Version']);
        wp_enqueue_script('ardent-functions', get_stylesheet_directory_uri() . '/js/functions.js', array('jquery'), $tdata['Version']);
        wp_enqueue_script('pretty-photo-swipe', get_stylesheet_directory_uri() . '/js/pretty-photo-swipe.js', array('jquery', 'touchswipe'), $tdata['Version']);
        wp_register_script('ardent-post-password', get_stylesheet_directory_uri() . '/js/post-pw-ajax.js', array('jquery'), $tdata['Version']);
        wp_localize_script( 'ardent-post-password', 'acPostPW', array(
                'ajaxurl' => admin_url('admin-ajax.php'),
        ));
        wp_enqueue_script('ardent-post-password');
    }else{
        wp_register_script('ardent-minified', get_stylesheet_directory_uri() . '/js/min/ac-child.min.js', array('jquery', 'touchswipe'), $tdata['Version']);
        wp_localize_script( 'ardent-minified', 'acPostPW', array(
                'ajaxurl' => admin_url('admin-ajax.php'),
        ));
        wp_enqueue_script('ardent-minified');
    }
}, 50);

add_action('admin_enqueue_scripts', function() {
    $screen = get_current_screen();
    $tdata = wp_get_theme();
    $user_meta = new \Ardent\Wp\User\Meta;
    wp_enqueue_style('ac-child-admin-css', get_stylesheet_directory_uri() . '/css/admin.css', array(), $tdata['Version']);
    if($screen->post_type == 'nectar_slider'){
        wp_enqueue_script('ac-child-nectar_slider', get_stylesheet_directory_uri() . '/js/admin-nectar_slider.js', array(), $tdata['Version']);
    }
});
