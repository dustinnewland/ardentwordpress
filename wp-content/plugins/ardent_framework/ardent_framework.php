<?php

/**
 * Plugin Name: Ardent Framework
 * Plugin URI: http://www.ardentcreative.com/
 * Description: Core functionality for Ardent Plugins and Themes.
 * Version: 0.6.8
 * Author: Ardent Creative
 * Author URI: http://www.ardentcreative.com
 * Text Domain: ardent_framework
 * Domain Path: Optional. Plugin's relative directory path to .mo files. Example: /locale/
 * Network: Optional. Whether the plugin can only be activated network wide. Example: true
 * License: A short license name. Example: GPL2
 */
if (!defined('ARDENT_FRAMEWORK')) {
	define('ARDENT_FRAMEWORK', true);
	define('ARDENT_FRAMEWORK_PATH', dirname(__FILE__));
	define('ARDENT_FRAMEWORK_URL', site_url(substr(dirname(__FILE__), strlen(ABSPATH))));
	define('ARDENT_FRAMEWORK_CLASSPATH', constant('ARDENT_FRAMEWORK_PATH') . '/classes');
}

require_once(constant('ARDENT_FRAMEWORK_PATH') . "/autoload.php");
require_once(constant('ARDENT_FRAMEWORK_PATH') . "/functions/ardent_get_url_from_file.php");
require_once(constant('ARDENT_FRAMEWORK_PATH') . "/tasks/add_resources.php");
require_once(constant('ARDENT_FRAMEWORK_PATH') . "/tasks/ajax.ardent_image_info.php");
require_once(constant('ARDENT_FRAMEWORK_PATH') . "/tasks/ajax.ardent_postsearch.php");
require_once(constant('ARDENT_FRAMEWORK_PATH') . "/tasks/allow_update_server.php");
require_once(constant('ARDENT_FRAMEWORK_PATH') . "/tasks/store_post_types.php");
require_once(constant('ARDENT_FRAMEWORK_PATH') . "/tasks/update_plugins.php");
require_once(constant('ARDENT_FRAMEWORK_PATH') . "/tasks/update_themes.php");

