<?php

if (!function_exists('ardent_get_url_from_file')) {

    function ardent_get_url_from_file($file) {
        return site_url(substr($file, strlen(ABSPATH)));
    }

}
