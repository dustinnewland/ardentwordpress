(function ($) {
	$(function () {
		// autocomplete widget
		$('.ardent-html-input-wp-post').each(function () {
			var pt = $(this).data('post-type');
			$(this).autocomplete({
				source: ajaxurl + '?action=ardent_postsearch&post_type=' + pt,
				minLength: 2,
				select: function (event, ui) {
					$(this).val(ui.item.value);
				}
			}).addClass('ardent-framework-ui');
		});
		$('.ardent-html-input-jquery-datepicker').each(function () {
			var dateformat = $(this).data('jqui-dateformat');
			if (!dateformat) {
				dateformat = "mm/dd/yy";
			}
			$(this).datepicker({
				dateFormat: dateformat
			});
		});
		if ($('.ardent-html-input-jquery-datepicker').length)
			$("#ui-datepicker-div").addClass('ardent-framework-ui');

		// input[type=range] needs to show values!
		$('input.ardent-html-input-range').on('input', function (ev, ui) {
			$(this).parent().find('.value_container').html(this.value);
		});
		$('input.ardent-html-input-range-time').on('input', function (ev, ui) {
			$(this).parent().find('.value_container').html(minToTime(this.value));
		});

		$('.ardent-html-input-xlist').XList();
		$(".ardent-html-input-radiogroup").buttonset().addClass('ardent-framework-ui');

		$('.ardent-html-input-wp-media').each(function () {
			$(this).CustomMediaManager();
		});
		$('.ardent-html-input-wp-media').CustomMediaManager();
		$('.ardent-html-input-wp-color-inner > input').iris({
			mode: 'hsl',
			palettes: true,
			change: function (ev, ui) {
				$(this).parent().find('.ardent-color-preview').css('background-color', ui.color.toString());
			}
		}).addClass('ardent-framework-ui');
		$('.ardent-html-input-wp-color-inner > input').on('blur', function () {
			if (!$(this).val()) {
				$(this).parent().find('.ardent-color-preview').css('background-color', '');
			}
		});
		$('.ardent-tab-widget').not('.ardent-config').each(function () {
			$(this).tabs();
		});
		$('.ardent-tab-widget.ardent-config').each(function () {
			var expose_tabs = $(this).hasClass('expose-tabs');
			var is_config = $(this).hasClass('ardent-config');
			var cookie_name = "tab-" + $(this).attr('id');
			var url_vars = window.location.search.substring(1).split("-");
//			// Lets find all our tab titles
			var tab = -1;
			var tab_index = 0;
			var wp_admin_page = null;
			var form_action = $(this).closest('form').attr('action');
			var urlvar_pairs = form_action.substring(form_action.indexOf('?') + 1).split('&');
			for (var i in urlvar_pairs) {
				var data = urlvar_pairs[i].split('=');
				if (data[0] == 'page') {
					wp_admin_page = data[1];
				}
			}
			$(this).children('ul').children('li').each(function () {
				var tab_url_data = $(this).text().toLowerCase().split(' ');
				var var_slice = url_vars.slice(url_vars.length - tab_url_data.length);
				if (var_slice.length == tab_url_data.length) {	// catches cases where the tab has more spaces than the url has dashes!
					var match = true;
					for (var i in var_slice) {
						if (var_slice[i] != tab_url_data[i]) {
							match = false;
						}
					}
					if (match) {
						tab = tab_index;
					}
				}
				tab_index++;
			});
			if (tab == -1 && !expose_tabs) {
				tab = $.cookie(cookie_name);
			}

			$(this).tabs({
				active: tab,
				activate: function (event, ui) {
					$.cookie(cookie_name, ui.newTab.index());
					var menu_href = "admin.php?page=" + wp_admin_page;
					if (ui.newTab.index()) {
						menu_href += "-" + ui.newTab.text().toLowerCase().replace(' ', '-');
					}
					var $active_menu_item = $('a[href="' + menu_href + '"]');
					if ($active_menu_item.length) {
						$active_menu_item.parent().siblings().removeClass('current');
						$active_menu_item.parent().addClass('current');
						$(this).closest('form.ardent-config').attr('action', menu_href);
						window.history.pushState('zzz', ui.newTab.text(), menu_href);
					}
				},
			}).addClass('ardent-framework-ui');
		});
		$(document).on('click', function (ev, ui) {
			if (!$(ev.target).is(".ardent-html-input-wp-color-inner, .iris-picker, .iris-picker-inner")) {
				var $shown_pickers = $('.iris-picker:visible');
				if ($shown_pickers.length) {
					$('.ardent-html-input-wp-color-inner > input').iris('hide');
				}
			}
		});
		$('.ardent-html-input-wp-color-inner').on('click', function (ev, ui) {
			$('.ardent-html-input-wp-color-inner > input').iris('hide');
			$(this).find('input').iris('show');
			return false;
		});


		$('.ardent-html-input-slider').each(function () {
			var $parent = $(this).parent();
			var $input = $parent.find('input[type=hidden]');
			var $label = $('<label>').html($input.val());
			var $widget = $('<div>');
			$widget.slider({
				min: $(this).data('slider-min'),
				max: $(this).data('slider-max'),
				step: $(this).data('slider-step'),
				value: $input.val(),
				slide: $.proxy(function (ev, ui) {
					$label.html(ui.value);
				}, this),
				change: $.proxy(function (ev, ui) {
					$input.val(ui.value);
				}, this)


			});
			$parent.append($label);
			$parent.append($widget);
		});
		$('.ardent-html-input-time').each(function () {
			var $parent = $(this).parent();
			var $input = $parent.find('input[type=hidden]');
			var $label = $('<label>').html(minToTime($input.val()));
			var $widget = $('<div>');
			$widget.slider({
				min: $(this).data('slider-min'),
				max: $(this).data('slider-max'),
				step: $(this).data('slider-step'),
				value: $input.val(),
				slide: $.proxy(function (ev, ui) {
					$label.html(minToTime(ui.value));

				}, this),
				change: $.proxy(function (ev, ui) {
					$input.val(ui.value);
				}, this)


			});
			$parent.append($label);
			$parent.append($widget);
		});


		// GEOCODE LOOKUPS
		$(document).on('click', 'button.ardent-coordinates-lookup', function (ev) {
			ev.preventDefault();
			var target = $(this).siblings('input[type=text]').attr('name');
			$('<div class="coordinates-lookup-popup" data-target="' + target + '"><form class="ardent-coordinates-address">Address<input type="text" /><input type="submit"></form><div class="results"><div>').dialog({dialogClass: "ardent-framework-ui", modal: true});
		});
		$(document).on('click', 'div.coordinates-lookup-popup div.result', function (ev) {
			var lat = $(this).data('lat');
			var lng = $(this).data('lng');
			var $popup = $(this).closest('.coordinates-lookup-popup')
			$('input[type=text][name="' + $popup.data('target') + '"]').val(lat + ',' + lng);
			$popup.dialog('close');
		});
		$(document).on('submit', 'form.ardent-coordinates-address', function (ev) {
			ev.preventDefault();
			var address = $(this).find('input[type=text]').val();
			var $results = $(this).parent().find('div.results');
			var url = '//maps.googleapis.com/maps/api/geocode/json?sensor=false&address=' + encodeURIComponent(address);
			$.get(url, function (data, txtStatus) {
				$results.html('');
				for (i in data.results) {
					var r = data.results[i];
					$results.append(
							$('<div class="result">').data('lat', r.geometry.location.lat).data('lng', r.geometry.location.lng).data('address', r.formatted_address)
							.append($('<div class="address">').html(r.formatted_address))
							);
				}
			});
		});
	});
})(jQuery);

function minToTime(selectedMin) {
	var min = selectedMin % 60;
	var hrs = Math.floor(selectedMin / 60);
	var suf = hrs > 11 && hrs != 24 ? "P.M." : "A.M.";

	min = min === 0 ? '00' : min; // correct formatting of 0 minutes
	hrs = hrs > 12 ? hrs - 12 : hrs; // convert military time to normal
	hrs = hrs === 0 ? 12 : hrs; // correct display of midnight
	return "" + hrs + ':' + min + ' ' + suf;
}
