(function ($) {
    $(function () {
        $('[data-ardent-conditions]').each(function () {
            var $f = $(this).closest('.field');
            var c = $(this).data('ardent-conditions');
            var $t = $('[name=' + c[0] + ']');
            var name = $(this).attr('name');
            if (!name) {
                name = $(this).data('name');
            }
            var val = null;
            if ($t.filter(':radio').length) {
                var $checked = $t.filter(':radio:checked');
                if (!$checked.length) {
                    $checked = $t.filter(':radio');
                }
                val = $checked.eq(0).val();
            } else if ($t.filter(':checkbox').length) {
                var $checked = $t.filter(':checkbox:checked');
                val = $checked.length ? 1 : 0;
            } else {
                val = $t.val();
            }
            if (val == c[1]) {
                $f.show();
            } else {
                $f.hide();
            }
            $('[name=' + c[0] + ']').on('change', function (ev, ui) {
                var val = null;
                if ($(this).attr('type') == 'radio') {
                    val = $('input[type=radio][name=' + $(this).attr('name') + ']:checked').val();
                
                } else if ($(this).attr('type') == 'checkbox'){
                    val = $(this).filter(':checked').length ? 1 : 0;
                } else {
                    val = $(this).val();
                }
                if (val == c[1]) {
                    $f.show();
                } else {
                    $f.hide();
                }
            });
        });
    });
})(jQuery);