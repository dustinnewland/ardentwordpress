function xlist_filter_block($el) {
    var display = $el.data('display');
    if (!display) {
        display = 'text';
    }
    var params = {};
    if (display && display.substring(0, 8) == 'truncate') {
        params.len = display.substring(9, display.length - 1);
        display = 'truncate';
    }
    var $target = $el.find('.xlist-value');
    var value = $el.data('xlist-value');
    var name = $el.data('name');
    $el.show();
    switch (display) {
        case "truncate":
            var html_new = value.substring(0, params.len);
            if (value != html_new) {
                html_new += ' ...';
            }
            $target.html(html_new);
            break;
        case "bool":
            $target.html(value ? 'true' : 'false')
            break;
        case "none":
            $el.hide();
            break;
        case "select.label":
            var select_target = jQuery('.ui-dialog').length?jQuery('.ui-dialog'):$target.parents('.ardent-html-input-xlist');
            $target.html(select_target.find('select[name="' + name + '"]').find('[value="' + value + '"]').first().text());
            break;
        case "youtube.image":
            var content = null;
            if (value) {
                content = jQuery('<img>').addClass('ardent-input-wp-media-preview size-thumbnail ardent-html-wp-image')
                        .data('itemid', value)
                        .attr('alt', 'Preview')
                        .attr('src', '//img.youtube.com/vi/' + value + '/default.jpg');
            } else {
                content = '-----';
            }
            $target.html(content);
            break;
        case "wp.image":
            jQuery.post(ajaxurl, {action: 'ardent_image_info', ids: value}, jQuery.proxy(function (data) {
                var $images = jQuery('<div class="ardent-xlist-images"></div>');
                for (var id in data) {
                    var img = data[id];
                    var $image = jQuery('<img>').addClass('ardent-input-wp-media-preview size-thumbnail ardent-html-wp-image')
                            .data('itemid', id)
                            .attr('alt', img.title)
                            .attr('src', img.sizes.thumbnail.url)
                            .width(img.sizes.thumbnail.width)
                            .height(img.sizes.thumbnail.height);
                    $images.append($image);
                }
                $target.html($images);
            }, this), 'json');
            break;
        case "text":
        default:
            break;
    }
}

(function ($) {
    $(function () {
        $('.xlist-property').each(function () {
            var $this = $(this);
            xlist_filter_block($this);
            var name = $this.data('name');
            var $c = $('[data-ardent-conditions][data-name=' + name + ']').add($('[data-ardent-conditions][name=' + name + ']'));
            if ($c.length) {
                var c = $c.data('ardent-conditions');
                // got to find the target item and get it's value
                var other_value = $this.closest('.xlist-item').find('.xlist-property[data-name=' + c[0] + ']').data('xlist-value');
                if (other_value != c[1]) {  // hide this
                    $this.hide();
                }
            }

        });
    });
    $(document).on('focusin', function (e) {
        if ($(e.target).closest(".mce-window").length) {
            e.stopImmediatePropagation();
        }
        if ($(e.target).closest("#wp-link").length) {
            e.stopImmediatePropagation();
        }
    });
$.widget('ardent.XList', {
        _edit: null,
        options: {
            target: null,
            selectors: {
                list: 'ul.ardent-html-input-droplist-list',
                values: 'div.values',
                dragform: '.ardent-html-input-dragform',
                add_button: '.dashicons.dashicons-plus-alt',
            }
        },
        value: function (v) {
            if (typeof v === 'string') {
                this._list.html('');
                if (v) {
                    var data = JSON.parse(v);
                    for (var i = 0; i < data.length; i++) {
                        this.add(data[i]);
                    }
                }
            }
            var ret = [];
            this._list.children().each(function () {
                var o = {};
                $(this).children('div.xlist-item').children('div.xlist-property').each(function () {
                    var name = $(this).data('name');
                    if (name) {
                        o[name] = $(this).data('xlist-value');
                    }
                });
                ret.push(o);
            });
            return ret;
        },
        name: null,
        _list: null,
        _add_button: null,
        _input: null,
        add: function (value) {
            var $li = $('<li>');
            var $drag_btn = $('<span class="handle dashicons dashicons-menu ardent-html-span"></span>');
            var $edit_btn = $('<span class="dashicons dashicons-edit ardent-html-span"></span>');
            var $delete_btn = $('<span class="dashicons dashicons-trash ardent-html-span"></span>');
            var $item_div = $('<div class="xlist-item ardent-html-div"></div>');
            for (var name in value) {
                var $form_field = this._form.find('[name=' + name + ']');
                var id = $form_field.attr('id');
                var $label = null;
                var display = 'text';
                if ($form_field.attr('type') == 'radio') {
                    $label = this._form.find('label[for=' + name + ']');
                    display = $form_field.parent().data('xlist-display');
                } else {
                    $label = this._form.find('label[for=' + id + ']');
                    display = $form_field.data('xlist-display');
                }
                if ($label && $label.length != 1) {
                    $label = this._form.find('label[for=' + name + ']');
                }

                var lbl = $label.clone().children().remove().end().text();
                var $label_div = $label.length ? $('<div class="xlist-label ardent-html-div">' + lbl + '</div>') : null;
                var $property_div = $('<div class="xlist-property ardent-html-div"></div>');
                var $value_div = $('<div class="xlist-value ardent-html-div"></div>');

                var v = value[name];
                if (typeof (v) === 'object') {
                    v = JSON.stringify(v);
                } else if (typeof (v) === 'boolean') {
                    v = +v;
                }
                var $prop = $('<div>');

                var parts = name.split('_');
                $.each(parts, function (index) {
                    $prop.addClass(parts[index]);
                })
                $property_div.data('name', name);
                $property_div.data('xlist-value', v);
                $property_div.data('display', display);
                $value_div.html(v);
                $property_div.append($label_div).append($value_div);
                xlist_filter_block($property_div);
                var $c = $('[data-ardent-conditions][data-name=' + name + ']').add($('[data-ardent-conditions][name=' + name + ']'));
                if ($c.length) {
                    var c = $c.data('ardent-conditions');
                    if (value[c[0]] != c[1]) { // hide
                        $property_div.hide();
                    }
                }
                $item_div.append($property_div);
            }
            $li.append($drag_btn).append($edit_btn).append($delete_btn).append($item_div);
            if (this._edit !== null) {
                this._list.children().eq(this._edit).replaceWith($li);
                this._edit = null;
            } else {
                this._list.append($li);
            }
            this._change();

        },
        _change: function () {
            var v = this.value();
            this._input.val(JSON.stringify(v));
            this._trigger('change');
        },
        _form: null,
        _create: function () {
            var $widget = this;
            var $element = this.element;
            this.name = this.element.data('name');
            this._input = this.element.children('input[name=' + this.name + ']');
            this._form = this.element.children('div.form').XForm({
                save: $.proxy(function () {
                    this.add(this._form.XForm('value'));
                }, this)
            });
            this._add_button = this.element.children(this.options.selectors.add_button);
            this._list = this.element.children('ul');
            this._list.on('click', ' > li > .dashicons.dashicons-trash', $.proxy(function (ev) {
                $(ev.target).parent().remove();
                this._change();
            }, this));
            this._list.on('click', ' > li > .dashicons.dashicons-edit', $.proxy(function (ev) {
                var $li = $(ev.target).closest('li');
                this._edit = index = this._list.children().index($li);
                var d = {};
                $li.find('.xlist-property').each(function () {
                    var name = $(this).data('name');
                    if (name) {
                        d[name] = $(this).data('xlist-value');
                    }
                });
                this._form.XForm("open", d);
            }, this));
            this._list.sortable({
                handle: '.handle',
                revert: true,
                update: $.proxy(function (ev, d) {
                    this._change();
                }, this)
            }).addClass('ardent-framework-ui');
            this._add_button.on('click', $.proxy(function () {
                this._form.XForm("open", {});
            }, this));
        },
        _setOption: function (key, value) {
            this.options[ key ] = value;
        },
    });

    $.widget('ardent.XForm', {
        options: {
            target: null,
            yay: null,
            save: null,
            cancel: null,
            selectors: {
                fields: 'input,select,textarea',
            }
        },
        value: function () {
            var ret = {};
            if (typeof (tinymce) != 'undefined') {
                tinymce.triggerSave();
            }
            this._fields.each(function () {
                var name = $(this).attr('name') || $(this).data('name');
                var tag = $(this).prop('tagName') + $(this).prop('type');
                var val;
                switch (tag.toUpperCase()) {
                    case 'INPUTCHECKBOX':
                        val = Boolean($(this).attr('checked'));
                        break;
                    case 'DIVUNDEFINED':
                        if ($(this).hasClass('ardent-html-input-wp-editor')) {
                            var ta = $(this).find('textarea');
                            var ed_id = ta.attr('id');
                            name = ta.attr('name');
                            var ed = tinyMCE.get(ed_id);
                            val = ed ? ed.save() : '';
                            break;
                        } else if ($(this).hasClass('ardent-html-input-radiogroup')) {
                            var $checked = $(this).children('input[type=radio]:checked');
                            var name = $checked.attr('name');
                            val = $checked.val();
                            break;
                        } else if ($(this).hasClass('ardent-html-input-wp-media')) {
                            val = $(this).CustomMediaManager('value');
                            break;
                        } else if ($(this).hasClass('ardent-html-input-xlist')) {
                            val = $(this).data('ardentXList').value();
                            break;
                        }
                    default:
                        val = $(this).val();
                        break;
                }
                ret[name] = val;
            });
            return ret;
        },
        cancel: function () {
            this.element.dialog("close");
            this._trigger('cancel');
        },
        save: function () {
            this.element.dialog("close");
            this._trigger('save');
        },
        open: function (v) {
            var xform = this;
            this.element.dialog({
                dialogClass: "ardent-framework-ui",
                modal: true,
//                stack: true,
                minWidth: 640,
                minHeight: 480,
                buttons: [
                    {
                        text: "Ok",
                        click: $.proxy(function () {
                            this.save();
                            this.element.dialog('close');
                        }, this)
                    },
                    {
                        text: "Cancel",
                        click: $.proxy(function () {
                            this.cancel();
                            this.element.dialog('close');
                        }, this)}
                ],
                close: function () {
                    xform.element.find('.ardent-mce-me').each(function () {
                        tinymce.get($(this).attr('id')).remove();
                    });
                },
                open: function () {
                    xform._fields.each(function () {
                        var name = $(this).attr('name') || $(this).data('name');
                        var tag = $(this).prop('tagName') + $(this).prop('type');
                        switch (tag.toUpperCase()) {
                            case 'INPUTCHECKBOX':
                                if (v[name]) {
                                    $(this).attr('checked', 'checked');
                                } else {
                                    if (jQuery.isEmptyObject(v)) {
                                        $(this).attr('checked', $(this).data('default') ? 'checked' : null);
                                    } else {
                                        $(this).attr('checked', null);
                                    }
                                }
                                $(this).change();
                                break;
                            case 'DIVUNDEFINED':
                                if ($(this).hasClass('ardent-html-input-radiogroup')) {
                                    var $checked = $(this).children('input[type=radio]:checked');
                                    name = $checked.attr('name');
                                    if (v[name]) {
                                        var $radios = $(this).children('input[type=radio]');
                                        $radios.attr('checked', null);
                                        $radios.filter('[value=' + v[name] + ']').attr('checked', 'checked');
                                        $radios.change();
                                    }
                                } else if ($(this).hasClass('ardent-html-input-wp-media')) {
                                    var name = $(this).data('name');
                                    var val = v[name];
                                    if (typeof val === 'undefined') {
                                        val = '';
                                    }
                                    $(this).CustomMediaManager('value', val);
                                } else if ($(this).hasClass('ardent-html-input-xlist')) {
                                    var name = $(this).data('name');
                                    var val = v[name];
                                    if (typeof val === 'undefined') {
                                        val = '';
                                    } else if (typeof val === 'object') {
                                        val = JSON.stringify(val);
                                    }
                                    $(this).XList('value', val);
                                }
                                break;
                            default:
                                if (typeof (v[name]) == 'object') {
                                    $(this).val(JSON.stringify(v[name]));
                                } else {
                                    $(this).val(v[name]);
                                }
                                $(this).change();
                                break;
                        }
                    });
                    $(this).find('.ardent-mce-me').each(function () {
                        var editor_id = $(this).attr('id');
                        if (!$(this).parent().find('div.addmediabutton').length) {
                            $(this).before('<div class="addmediabutton"><a href="#" id="insert-media-button" class="button insert-media add_media" data-editor="' + editor_id + '" title="Add Media"><span class="wp-media-buttons-icon"></span> Add Media</a></div>');
                        }
                        var baseurl = ajaxurl.replace('wp-admin/admin-ajax.php', '');
                        var opts = tinyMCEPreInit.mceInit.placeholder_editor;
                        opts.body_class = opts.body_class.replace('placeholder_editor', editor_id);
                        opts.selector = opts.selector.replace('placeholder_editor', editor_id);
                        opts.setup = function (ed) {
                            ed.onBeforeSetContent.add(function (ed, o) {
                                if (o.initial) {
                                    o.content = o.content.replace(/>\s+</g, '><');
                                    o.content = o.content.replace(/\r?\n/g, '<br />');
                                }
                            });
                        };
                        ed = tinymce.init(opts);
                    });
                }
            });

        },
        _fields: null,
        _create: function () {
            this._fields = this.element.find('.xform-input').not(this.element.find('.xform-input .xform-input'));

        },
        _setOption: function (key, value) {
            this.options[ key ] = value;
        }
    });
})(jQuery);
