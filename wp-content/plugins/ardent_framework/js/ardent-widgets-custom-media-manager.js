(function ($) {
    $.widget('ardent.CustomMediaManager', {
        custom_uploader: null,
        options: {
            multiple: false,
            types: null,
            input_selector: 'input.ardent-input-wp-media-value',
            add_button_selector: '.ardent-button.add-media',
            remove_button_selector: '.ardent-button.remove-media'
        },
        media_options: function () {
            return {
                title: 'Choose Media',
                frame: 'select',
                button: {text: 'Choose Media'},
                multiple: this.options.multiple,
                library: {type: this.options.types},
                value: this.$input.val()
            }
        },
        value: function (v) {
            if (v === undefined) {
                return this.$input.val();
            } else {
                // clear out our image list and create new previews
                this.$input.val(v);
                $.post(ajaxurl, {action: 'ardent_image_info', ids: v}, $.proxy(function (data) {
                    var $list = this.element.find('ul.imagelist');
                    $list.html('');
                    var remove_btn = '<span class="dashicons dashicons-trash"></span>';
                    for (var id in data) {
                        var img = data[id];

                        var $image = $('<img>').addClass('ardent-input-wp-media-preview size-thumbnail ardent-html-wp-image')
                                .data('itemid', id)
                                .attr('alt', img.title)
                                .attr('src', img.sizes.thumbnail.url)
                                .width(img.sizes.thumbnail.width)
                                .height(img.sizes.thumbnail.height);
                        $list.append($('<li>').append(remove_btn).append($image));
                    }
                }, this), 'json');
            }
        },
        $add_button: null,
        $remove_button: null,
        $input: null,
        _create: function () {
            this.$input = this.element.find(this.options.input_selector);
            this.$add_button = this.element.find(this.options.add_button_selector);
            this.$remove_button = this.element.find(this.options.remove_button_selector);
            this.options.multiple = this.element.data('multiple');
            this.options.types = this.element.data('types');

            if (this.$input.val()) {
                this.$remove_button.show();
            } else {
                this.$remove_button.hide();
            }
            var $widget = this;

            this.element.on('click', 'ul.imagelist > li > span.dashicons-trash', $.proxy(function (ev, ui) {
                var $li = $(ev.target).parent();
                var id = $li.find('img').data('itemid');
                var val = this.$input.val().split(',');
                var index = val.indexOf("" + id);
                if (index !== -1) {
                    val.splice(index, 1);
                }
                this.$input.val(val);
                $li.remove();
            }, this));

            this.$add_button.on('click', $.proxy(function (ev) {
                var $element = $widget.element;
                if (this.$input.filter('[disabled]').length) {
                    return;
                }
                ev.preventDefault();
                this.custom_uploader = wp.media.frames.file_frame = wp.media(this.media_options());
                this.custom_uploader.off('select');
                this.custom_uploader.on('select', $.proxy(function () {
                    var value;
                    value = [];
                    if (this.options.multiple) {
                        value = $.grep(this.$input.val().split(','), function (n, i) {
                            return (n !== "" && n != null);
                        });
                    } else {
                        this.element.find('img.ardent-input-wp-media-preview').remove();
                    }
                    this.custom_uploader.state().get('selection').each(function (item) {
                        if ($.inArray("" + item.id, value) == -1) {
                            var $li = $('<li>').append('<span class="dashicons dashicons-trash"></span>');
                            var $image = $('<img />').addClass('ardent-input-wp-media-preview size-thumbnail ardent-html-wp-image');
                            $image.data('itemid', item.id);
                            if (item.attributes.title) {
                                $image.attr('title', item.attributes.title);
                                $image.attr('alt', item.attributes.title);
                            }
                            var src = null;
                            var title = null;
                            switch (item.attributes.type) {
                                case 'image':
                                    if (item.attributes.sizes) {
                                        if (item.attributes.sizes.thumbnail) {
                                            src = item.attributes.sizes.thumbnail.url
                                        } else if (item.attributes.sizes.full) {
                                            src = item.attributes.sizes.full.url
                                        }
                                    }
                                    if (!src) {
                                        src = item.attributes.link;
                                    }
                                    break;
                                default:
                                    src = item.attributes.icon;
                                    break;
                            }
                            $image.attr('src', src);
                            $element.find('ul.imagelist').append($li.append($image));
                            value.push(item.id + '');
                        }

                    });
                    this.$input.val(value);
                    this.$remove_button.show();
                }, this));
                this.custom_uploader.open();
            }, this));
            this.$remove_button.on('click', $.proxy(function (ev) {
                ev.preventDefault();
                if (this.$input.filter('[disabled]').length) {
                    return;
                }
                this.$input.val('');
                this.element.find('img.ardent-input-wp-media-preview').remove();
                this.$remove_button.hide();
            }, this));

        },
        _setOption: function (key, value) {
            this.options[ key ] = value;
        }
    });
})(jQuery);

