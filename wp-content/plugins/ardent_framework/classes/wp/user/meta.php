<?php

/**
 * @file classes/wp/user/meta.php Contains a wrapper for Wordpress Custom Meta functions
 */

namespace Ardent\Wp\User;

class Meta implements \ArrayAccess {

	private $_id = null;
	private $_meta = null;
	public $keys = array();

	public function __construct($id = null) {
		if (!$id) {
			$user = wp_get_current_user();
			if (get_class($user) === 'WP_User' && $user->ID) {
				$id = $user->ID;
			}
		}
		if (!get_user_by('ID', $id)) {
			return;
		}
		$this->_id = $id;
//		$this->_meta = get_post_meta($this->_id);
		$this->_meta = get_user_meta($this->_id);
		$this->keys = array_keys((array) $this->_meta);
	}

	/**
	 * Access meta keys by value
	 * @code
	 * $meta->some_key
	 * @endcode
	 * @param string $name
	 * @return mixed
	 */
	public function __get($name) {
		$value = $this[$name];
		if (is_serialized($value)) {
			$value = unserialize($value);
		}
		return $value;
	}

	/**
	 * Setter function for meta parameters
	 * @code
	 * $meta->some_key = 'a value';
	 * @endcode
	 * @param string $name
	 * @param mixed $value
	 * @return mixed
	 */
	public function __set($name, $value) {
		return $this[$name] = $value;
	}

	public function offsetExists($offset) {
		return array_key_exists($offset, $this->_meta);
	}

	public function offsetGet($offset) {
		if (!isset($this->_meta[$offset])) {
			return null;
		}
		$item = $this->_meta[$offset];
		if (is_array($item)) {
			if (count($item) == 1) {
				$item = array_shift($item);
			}
		}
		return $item;
	}

	public function offsetSet($offset, $value) {
		$this->_meta[$offset] = array($value);
		update_user_meta($this->_id, $offset, $value);
//		update_metadata('post', $this->_id, $offset, $value);
	}

	public function offsetUnset($offset) {
		unset($this->_data[$offset]);
	}

	/**
	 * Convert all meta values for the current post_id to an array
	 * @return array
	 */
	public function toArray() {
		$ret = array();
		foreach ($this->keys as $key) {
			$ret[$key] = $this->$key;
		}
		return $ret;
	}

	public function public_keys() {
		$return = array();
		foreach ($this->keys as $key) {
			if (substr($key, 0, 1) == '_') {
				continue;
			}
			$return[] = $key;
		}
		return $return;
	}

	public function public_values() {
		$return = array();
		foreach ($this->keys as $key) {
			if (substr($key, 0, 1) == '_') {
				continue;
			}
			$return[$key] = $this->{$key};
		}
		return $return;
	}

}

// %% incomplete %%
