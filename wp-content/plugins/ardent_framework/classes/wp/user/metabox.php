<?php

/**
 * @file classes/wp/metabox.php
 * This file contains the Metabox class.
 * Metaboxes allow you to manage different types of data for different situations within WordPress. Metaboxes can be applied specifically to post types and data within that metabox is stored to the specific post. Certain manipulation is needed when using metaboxes. This class makes that manipulation easier and more efficient.
 */

namespace Ardent\Wp\User;

/**
 * Wraps Wordpress functionality to generate / manage / save Post type metaboxes
 */
class Metabox {

	private static $_instances = null; // Used for getting and setting the instance of a new or existing metabox.
	private $_fields = array();   // Used for setting the fields of a metabox instance you are using.
	private $_nonce = null; // Used for validating that the contents came from the current site when saving data in the admin for specific instances of metaboxes.
	private $_functions = array();   // %% incomplete %% Don't know what this does or how it gets set.
	private $_name = ''; // Used for naming an instance of a metabox.
	public $id = '';  // Used for giving a unique name to an instance of a metabox.
	private $_draw_meta_function = null;

	/**
	 * This is the general construct of the Metabox class.
	 *
	 * Upon invoking the class, you have the ablility of doing 2 things:
	 * 1. Create a non-existent metabox.
	 * 2. Grab an instance of an existing metabox.
	 *
	 * The best way to do these items involve using the exact same syntax. No special function is needed when specifying whether or not you want to create or get a metabox. They use the same function to do both.
	 *
	 * The conventional means used by PHP to use a class is not what needs to be used when using this class. Please reference the getInstance function of this class if you want to create or get a metabox.
	 *
	 * The preferred way of accessing the metabox class is like this:
	 * @code
	 * $metabox = \Ardent\Wp\Metabox::getInstance($posttype, $name, $location);
	 * @endcode
	 *
	 * @param string $name
	 */
	public function __construct($name, $meta_func = null) {
		$this->_name = $name;
		$this->id = sanitize_title_with_dashes(strtolower($name));
		$this->_draw_meta_function = $meta_func;
		$draw_function = $this->_draw_meta_function ? : array(&$this, 'draw_meta');

		add_action('show_user_profile', $draw_function);
		add_action('edit_user_profile', $draw_function);

		add_action('personal_options_update', array($this, 'save_meta'));
		add_action('edit_user_profile_update', array($this, 'save_meta'));
	}

	public function reset_fields() {
		$this->_fields = array();
	}

	/**
	 * This function is used to get the fields of the metabox selected by the getInstance function.
	 *
	 * @return Array An array of the fields used.
	 */
	public function fields() {
		return $this->_fields;
	}

	/**
	 * This is the function for creating a metabox or for getting an existing metabox instance.
	 *
	 * Here is an example of this function:
	 * @code
	 * $metabox = \Ardent\Wp\Metabox::getInstance($posttype, $name, $location);
	 * @endcode
	 *
	 * @param string $posttype
	 * @param string $name
	 * @param string $location = normal | advanced | side
	 * @return object Instance of the metabox.
	 */
	public static function getInstance($name, $meta_func = null) {
		$key = md5($name);
		if (!isset(self::$_instances[$key])) {
			self::$_instances[$key] = new Metabox($name, $meta_func);
		}
		return self::$_instances[$key];
	}

	public function add_field($field) {
		$this->_fields[] = $field;
	}

	/** Not for public consumption
	 * @internal
	 *
	 * This function is specifically for WordPress to display metaboxes in the WordPress administration system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function draw_meta($user) {
		$meta = new \Ardent\Wp\User\Meta($user->ID);
//		$meta = new \Ardent\Wp\Meta($post->ID);
		$group = $this->id;
		$metabox_class = "ardent-html-metabox " . sanitize_title_with_dashes("ardent-html-metabox-{$group}");
		if (!$this->_nonce) {
			wp_nonce_field(plugin_basename(__FILE__), $this->id);
			$this->_nonce = true;
		}
		$mb = \Ardent\Html::get(array('type' => 'ul'));
		foreach ($this->_fields as $field) {
			if (isset($field['is_admin'])) {
				unset($field['is_admin']);
				if (!current_user_can('administrator')) {
					continue;
				}
			}
			if (is_object(@$field['value']) && get_class(@$field['value']) === 'Closure') {
				$f = $field['value'];
				$field['value'] = $f($post);
			} elseif (is_object(@$field['value'])) {
				$field['value'] = json_encode($field['value']);
			} elseif (@$field['name']) {
				$v = $meta->{$field['name']};
				if ($v === null) {
					$v = @$field['default_value'] ? $field['default_value'] : null;
				}
				$field['value'] = $v;
			}

			$field_class = preg_replace('/[^a-z]+/', '_', @$field['type']);
			$li = \Ardent\Html::get(array('type' => 'li', 'text' => \Ardent\Html::get($field), 'class' => array($field_class, 'field')));
			if (@$field['name']) {
				$li->class = $field['name'];
			}
			$mb->text .= $li;
		}
		echo "<div class=\"$metabox_class\"><h2>$this->_name</h2>".$mb."</div>";
	}

	/** @internal
	 *
	 * This function is specifically for WordPress to save metaboxes in the WordPress administration system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function save_meta($user_id) {
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}
		if (!wp_verify_nonce($_POST[$this->id], plugin_basename(__FILE__))) {
			return;
		}
		$meta = new \Ardent\Wp\User\Meta($user_id);
		foreach ($this->_fields as $field) {
			if (isset($field['is_admin'])) {
				$is_admin = $field['is_admin'];
				unset($field['is_admin']);
				if ($is_admin && !current_user_can('administrator')) {
					continue;
				}
			}
			$name = @$field['name'];
			$v = @$_POST[$name];
			if (isset($field['validate'])) {
				switch ($field['validate']) {
					case 'numeric':
						$v = preg_replace('/[^0-9]/', '', $v);
						break;
					case 'email':
						$v = filter_var($v, FILTER_SANITIZE_EMAIL);
						if (!filter_var($v, FILTER_VALIDATE_EMAIL)) {
							$v = '';
						}
						break;
					default:
						break;
				}
			}

			if ($name) {
				if ($meta[$name] !== $v) {
					$meta[$name] = $v;
				} else {
					if ($meta[$name] === null) {
						$meta[$name] = '';
					}
				}
			}
		}
		foreach ($this->_functions as $func) {
			$func($user_id, $post);
		}
	}

	/** @internal
	 *
	 * This function is specifically for WordPress to save metaboxes in the WordPress administration system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function onSave($func) {
		if (get_class($func) === 'Closure') {
			$this->_functions[] = $func;
		}
	}

}

// %% incomplete %%
