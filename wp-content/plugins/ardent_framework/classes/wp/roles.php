<?php
namespace Ardent\Wp;

class Roles {

	public static function ManagerRoles() {
		return array(
			'subscriber' => 'Subscriber',
			'author' => 'Author',
			'editor' => 'Editor',
			'manager' => 'Manager'
		);
	}
}