<?php

// %% incomplete %%  

/**
 * @file acframework/classes/wp/shortcode.php
 * This file contains shortcode helper functionality
 */

namespace Ardent\Wp;

class Shortcode extends \Ardent\Object {

    protected $_config = array(
        'js_composer' => false,
        'function' => null,
        'custom_markup' => null,
        'js' => array(),
        'show_settings_on_create' => null
    );
    private $_fields = array();

    /**
     * 
     * @param string $name
     * @param array $config
     */
    public function __construct($name, $config = array()) {
        if (isset($config['fields'])) {
            $this->_fields = $config['fields'];
            unset($config['fields']);
        }
        parent::__construct($config);
        $this->name = $name;
        if (did_action('plugins_loaded')) {
            $this->plugins_loaded();
        } else {
            add_action('plugins_loaded', array(&$this, 'plugins_loaded'));
        }
        if (did_action('wp_loaded')) {
            $this->init();
        } else {
            add_action('wp_loaded', array(&$this, 'init'));
        }
    }

    public function plugins_loaded() {
        $this->js_composer = function_exists('add_shortcode_param');
    }

    public function init() {

        if (!$this->base) {
            $this->base = sanitize_title_with_dashes($this->name);
        }
        $this->_add_shortcode();
        if ($this->js_composer) {
            $this->_add_js_composer();
        }
    }

    private function _add_shortcode() {
        add_shortcode($this->base, array(&$this, 'do_shortcode'));
    }

    private function _add_js_composer() {
        if (function_exists('vc_map')) {
            $registered = array();
            $defaults = array(
                "name" => null, // required
                "base" => null, // required
                "class" => '',
                "description" => '',
                "icon" => 'icon-wpb-layer-shape-text', // wp-content/plugins/js_composer/asssets/css/js_composer.css
                "wrapper_class" => "clearfix",
                "category" => 'Ardent Creative',
            );
            $args = array_merge($defaults, $this->_args);

            if ($this->show_settings_on_create === null) {
                if (count($this->_fields)) {
                    $args['params'] = array();
                    $this->show_settings_on_create = true;
                } else {
                    $this->show_settings_on_create = false;
                }
            } elseif ($this->show_settings_on_create) {
                $args['params'] = array();
            }

            foreach ($this->_fields as $group => $fields) {
                if (is_numeric($group)) {   // old style
                    $args['params'][] = $this->_convert_field_to_param($fields);
                } else {
                    foreach ($fields as $field) {
                        $field['group'] = @$field['group'] ? : $group;
                        $args['params'][] = $this->_convert_field_to_param($field);
                    }
                }
            }
            vc_map($args);
        }
    }

    public function do_shortcode($args, $content = '') {
        $func = $this->function;
        return $func($args, $content);
    }

    public function add_field($field) {
        $this->_fields[] = $field;
    }

    private function _convert_field_to_param($field) {
        static $registered = array();
        if ($field['type'] == 'input.wp.editor') {
            $param = array(
                'type' => 'textarea_html',
                'field' => $field
            );
        } elseif ($field['type'] == 'input.wp.media') {
            $param = array(
                'type' => 'attach_image',
            );
        } elseif ($field['type'] == 'input.wp.color') {
            $param = array(
                'type' => 'colorpicker',
            );
        } elseif ($field['type'] == 'input.checkbox') {
            $param = array(
                'type' => 'checkbox',
                'value' => isset($field['value'])?$field['value']:array("Yes" => 'yes')
            );
        } elseif (!in_array($field['type'], array('textarea_html', 'textfield', 'textarea', 'dropdown', 'attach_images', 'attach_image', 'posttypes', 'colorpicker', 'exploded_textarea', 'widgetised_sidebars', 'textarea_raw_html', 'vc_link', 'checkbox', 'css_editor'))) {
            $type = 'ardent-' . $field['type'];
            if (!in_array($type, $registered)) {
                $registered[] = $type;
                add_shortcode_param($type, function($settings, $value) {
                    $dependency = vc_generate_dependencies_attributes($settings);
                    $field = $settings['field'];
                    $field['value'] = $value;
                    $field['class'] = 'wpb_vc_param_value';
                    unset($field['label']);
                    return \Ardent\Html::get($field);
                });
            }
            $param = array(
                'type' => 'ardent-' . $field['type'],
                'field' => $field
            );
        } else {
            $param = $field;
        }
        if (isset($field['name'])) {
            $param['param_name'] = $field['name'];
        }
        if (isset($field['label']) || isset($field['placeholder'])) {
            $param['heading'] = @$field['label'] ? $field['label'] : @$field['placeholder'];
        }
        if (isset($field['group'])) {
            $param['group'] = $field['group'];
            unset($field['group']);
        }
        if (isset($field['admin_label'])) {
            $param['admin_label'] = $field['admin_label'];
            unset($field['admin_label']);
        }
        if (isset($field['holder'])) {
            $param['holder'] = $field['holder'];
            unset($field['holder']);
        }
        if (isset($field['dependency'])) {
            $param['dependency'] = $field['dependency'];
            unset($field['dependency']);
        }
        if (isset($field['description'])) {
            $param['description'] = $field['description'];
            unset($field['description']);
        }

        return $param;
    }

}
