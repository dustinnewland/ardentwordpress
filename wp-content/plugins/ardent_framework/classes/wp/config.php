<?php

namespace Ardent\Wp;

/**
 * Config class is used to easily create and manage configuration pages.
 *
 * You can use this class to retrieve / set configuration settings
 * as well as add new user-input settings to a config page.
 */
class Config {

	static $_instances = array();
	static $_cached_settings = array();
	private $_settings = array();
	private $_subpage = false;
	private $_values = null;
	private $_title = null;
	private $_icon = null;
	private $_parent_id = null;
	private $_id = null;
	private $_callbacks = array();
	private $_expose_tabs = null;

	/**
	 * This function will retrieve the requested instance of the Config class.
	 * This object can then be used to retrieve / set settings
	 * or add new user inputs to the config page.
	 *
	 * @param string $title = 'Ardent Creative'
	 * @param bool $subpage = true
	 * @return \Ardent\Config
	 */
	public static function getInstance($page = 'Settings', $subpage = null, $icon = null, $expose_tabs = false, $settings_slug = null) {
		$key = md5($page . $subpage);
		if (!isset(self::$_instances[$key])) {
			self::$_instances[$key] = new Config($page, $subpage, $icon, $expose_tabs, $settings_slug);
		}
		return self::$_instances[$key];
	}

	public function __construct($title = 'Settings', $subpage = '', $icon = null, $expose_tabs = false, $settings_slug = null) {
		$this->_id = $settings_slug ?: sanitize_title_with_dashes(implode('-', array_filter(array($title, $subpage))));
		$this->_icon = $icon ?: 'dashicons-icon-ardent-bug';
		$this->_title = $title;
		$this->_subpage = $subpage;
		$this->_parent_id = sanitize_title_with_dashes($title);
		$this->_expose_tabs = $expose_tabs;
		add_action('admin_menu', array($this, 'init_menu'), 1, 9);
		add_action("admin_init", array($this, 'save'));
	}

	public function getUrls() {
		$slug = "ardent_options-{$this->_id}";
		$urls = array(
			get_admin_url(null, "admin.php?page={$slug}"),
			get_admin_url(null, "options-general.php?page={$slug}")
		);

		return $urls;
	}

	/**
	 * This function will retrieve the selected setting. Use it when a full
	 * config object is not necessary.
	 * @code
	 * $image_id = \Ardent\Config::getSetting('TCU', 'Branding & Design', 'favicon_ico');
	 * @endcode
	 *
	 * @param string $page Name of the top level config menu
	 * @param string $subpage Name of the second level config menu
	 * @param string $setting Name of the setting
	 * @return mixed
	 */
	public static function getSetting($page, $subpage = null, $setting = null) {
		$slug = implode('-', array_filter(array($page, $subpage)));
		$option = sanitize_title_with_dashes("ardent_options_{$slug}");
		if (!@self::$_cached_settings[$option]) {
			self::$_cached_settings[$option] = get_option($option);
		}
		if ($setting === null) {
			return self::$_cached_settings[$option];
		}
		return @self::$_cached_settings[$option][$setting];
	}

	/**
	 * @internal
	 */
	public function init_menu() {
		if (empty($this->_settings)) {
			return;
		}
		$filter_prefix = str_replace('-', '_', $this->_id);

		$slug = "ardent_options-{$this->_id}";
		$title = apply_filters(strtolower("ardent_config_menu-{$filter_prefix}"), $this->_title);
		$subpage = apply_filters(strtolower("ardent_config_submenu-{$filter_prefix}"), $this->_subpage);
		if ($subpage) {
			if ($title == 'Settings') {
				add_options_page($subpage, $subpage, 'manage_options', $slug, array($this, 'options_page'));
			} else {
				global $menu;
				if ($matches = wp_list_filter($menu, array(0 => $title))) {
					$match = array_shift($matches);
					$parent_slug = $match[2];
				} else {
					$parent_slug = "ardent_options-{$this->_parent_id}";
				}
				add_submenu_page($parent_slug, $title, $subpage, 'manage_options', $slug, array($this, 'options_page'));
			}
		} else {
			add_menu_page($title, $title, 'manage_options', $slug, array($this, 'options_page'), $this->_icon);
			if ($this->_expose_tabs) {
				foreach (array_keys($this->_settings) as $i => $tab) {
					$t = sanitize_title_with_dashes(strtolower($tab));
					$subpageslug = "{$slug}-{$t}";
					add_submenu_page($slug, $title, $tab, 'manage_options', $i ? $subpageslug : $slug, array($this, 'options_page'));
				}
			}
		}
	}

	public function add_setting($group, $field) {
		if (!isset($this->_settings[$group])) {
			$this->_settings[$group] = array();
		}
		$this->_settings[$group][] = $field;
	}

	public function save() {
		if (@$_REQUEST['task'] !== $this->_id . '.save') {
			return;
		}
		$data = stripslashes_deep($_REQUEST);
		$this->_save_section($this->_settings, $data);
		if (!defined('DOING_AJAX')) {
			wp_redirect(\Ardent\Url::Current());
		}
		foreach ($this->_callbacks as $cb) {
			if (is_callable($cb)) {
				$cb($data);
			}
		}
		exit;
	}

	public function onSave($func) {
		$this->_callbacks[] = $func;
	}

	public function get_settings() {
		return $this->_settings;
	}

	public function set_settings($settings) {
		$this->_settings = $settings;
	}

	private function _save_section($section, $data) {
		foreach ($section as $k => $v) {
			if (is_int($k)) {
				if (isset($v['type']) && isset($v['name'])) {
					$name = preg_replace('/[^a-zA-Z0-9_]/', '', $v['name']);
					$this->{$name} = $data[$name];
				}
			} else {
				$this->_save_section($v, $data);
			}
		}
	}

	private function _render_section($title, $section, $lvl) {
		$safetitle = sanitize_title_with_dashes($title);
		$ret = "<div class='section level_{$lvl} group-{$safetitle} ardent-group'><div class='title'>" . ($lvl == '1' ? $this->_title . ' &gt; ' : '') . "{$title}</div>";
		$container_count = 0;
		foreach ($section as $k => $v) {
			if (is_int($k)) {
				$is_hidden = isset($v['type']) && $v['type'] == 'input.hidden';
				$next_is_hidden = isset($section[$k + 1]['type']) && $section[$k + 1]['type'] == 'input.hidden';
				if (isset($v['type']) && isset($v['name'])) {
					if (!isset($v['id'])) {
						$v['id'] = uniqid(preg_replace('/[^a-zA-Z0-9_]/', '', $v['name']));
					}
					$name = preg_replace('/[^a-zA-Z0-9_]/', '', $v['name']);
					if (@$v['type'] !== 'button') {
						$v['value'] = $this->{$name};
					}
				}
				if (!$is_hidden) {
					$container_classes = implode(' ', array_filter(array(
						'field',
						'ardent-block',
						isset($v['container_class']) ? $v['container_class'] : null,
						!$container_count ? 'first' : null,
						($next_is_hidden && $k + 1 == count($section) - 1 || $k == count($section) - 1) ? 'last' : null
					)));
					$subcontainer_class = isset($v['subcontainer_class']) ? $v['subcontainer_class'] : null;

					$ret .= "<div class=\"{$container_classes}\">";
					if ($subcontainer_class) {
						unset($v['container_class']);
						$ret .= "<div class='{$subcontainer_class}'>";
					}
					// label and description
					if (isset($v['label'])) {
						if (isset($v['id'])) {
							$ret .= "<div class=\"label-container\"><label class=\"ardent-html-label\" for=\"{$v['id']}\">{$v['label']}</label>";
						} else {
							$ret .= "<div class=\"label-container\"><label class=\"ardent-html-label\">{$v['label']}</label>";
						}
						if (isset($v['description'])) {
							$ret .= "<div class=\"label-description\">{$v['description']}</div>";
							unset($v['description']);
						}
						$ret .= '</div>';
						unset($v['label']);
					}
					$ret .= '<div class="input-container">' . \Ardent\Html::get($v) . '</div>';

					if ($subcontainer_class) {
						$ret .= "</div>";
					}
					$ret .= '</div>';
					$container_count++;
				} else {
					$ret .= \Ardent\Html::get($v);
				}
			} else {
				$ret .= $this->_render_section($k, $v, $lvl + 1);
			}
		}
		$ret .= '</div>';
		return $ret;
	}

	public function options_page() {
		$action = $this->get_redirect_url();
		$content = '';
		$tabs = array();
		// go through _settings, each key is a tab.
		foreach ($this->_settings as $key => $val) {
			$tabs[$key] = $this->_render_section($key, $val, 1);
		}
		$pd = get_plugin_data(plugin_dir_path(__FILE__) . '../../ardent_framework.php');
		$tab_widget_id = 'atw_' . md5($this->_id);
		?>
		<form action="<?php echo $action ?>" method="post" enctype="multipart/form-data" class='ardent-config'>
			<div class="ardent-framework-title">
				<input type="submit" value="Save Changes">
				<div class="wp-menu-image dashicons-before dashicons-icon-ardent-bug"></div>
				<h1>AC Framework v<?php echo $pd['Version']; ?></h1>
			</div>
			<div class="ardent-table">
				<?php
				if (count($tabs) > 1) {
					?>
					<div class="ui-tabs-vertical ardent-tab-widget ardent-config<?php echo $this->_expose_tabs ? ' expose-tabs' : ''; ?>" id="ardent-tab-widget-<?php echo $this->_id ?>">
						<ul>
							<?php
							foreach (array_keys($tabs) as $tab_title) {
								?>
								<li><a href="#tab-<?php echo sanitize_title_with_dashes($tab_title) ?>"><?php echo $tab_title ?></a></li>
							<?php } ?>
						</ul>
						<?php
						foreach ($tabs as $tab_title => $tab_content) {
							?>
							<div id="tab-<?php echo sanitize_title_with_dashes($tab_title) ?>">
								<?php echo $tab_content ?>
							</div>
						<?php } ?>
						<div class="clear"></div>
					</div>
					<?php
				} else {
					?>
					<?php
					foreach ($tabs as $title => $content) {
						?>
						<div class="ui-tabs-vertical ardent-tab-widget ui-tabs ui-widget ui-widget-content ui-corner-all no-menu ardent-config">
							<ul style="display:none;"></ul>
							<div class="ui-tabs-panel ui-widget-content ui-corner-bottom">
								<?php echo $content ?>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<?php echo \Ardent\Html::get(array('type' => 'input.hidden', 'name' => 'task', 'value' => "{$this->_id}.save")) ?>
			<?php wp_nonce_field('save', str_replace('-', '_', $this->_id), false); ?>
			<input type="submit" value="Save Changes">
		</form>
		<?php
		return;
	}

	public function __get($key) {
		if ($this->_values === null) {
			$this->_values = get_option("ardent_options_{$this->_id}");
		}
		return isset($this->_values[$key]) ? $this->_values[$key] : null;
	}

	public function __set($key, $value) {
		if ($this->_values === null) {
			$this->_values = get_option("ardent_options_{$this->_id}");
		}
		$old = @$this->_values[$key];
		if ($old !== $value) {
			if ($value === null) {
				unset($this->_values[$key]);
			} else {
				$this->_values[$key] = $value;
			}
			update_option("ardent_options_{$this->_id}", $this->_values);
		}
	}

	private function get_redirect_url() {
		return get_admin_url(null, "admin.php?page=ardent_options-{$this->_id}");
	}

	public function onConfigPage() {
		$current_url = \Ardent\Url::Current();
		foreach ($this->getUrls() as $config_page_url) {
			if (strlen($current_url) >= strlen($config_page_url)) {
				if ($config_page_url == substr($current_url, 0, strlen($config_page_url))) {
					return true;
				}
			}
		}
		return false;
	}

}
