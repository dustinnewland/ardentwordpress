<?php

namespace Ardent\Wp;

class Taxonomy extends \Ardent\Object {

	protected $_config = array(
		'name' => null,
		'id' => null,
		'posttype' => null,
		'metaboxes' => array(),
	);

	public function __construct($config = array()) {
		if (empty($config['id'])) {
			$config['id'] = sanitize_title_with_dashes($config['name']);
		}
		if (empty($config['label'])) {
			$config['label'] = $config['name'];
		}

		parent::__construct($config);
		add_action('init', array(&$this, 'init'));
		add_action("{$this->id}_add_form_fields", array(&$this, 'add_form_fields'), 10, 2);
		add_action("{$this->id}_edit_form_fields", array(&$this, 'edit_form_fields'), 10, 2);

		add_action("edited_{$this->id}", array(&$this, 'save_taxonomy_custom_meta'), 10, 2);
		add_action("create_{$this->id}", array(&$this, 'save_taxonomy_custom_meta'), 10, 2);
	}

	public function add_form_fields() {
		foreach ((array) @$this->metaboxes as $group => $fields) {
			foreach ($fields as $field) {
				?>
				<div class="form-field">
					<?php echo \Ardent\Html::get($field) ?>
				</div>
				<?php
			}
		}
	}

	public function edit_form_fields($term) {
		$t_id = $term->term_id;
		// retrieve the existing value(s) for this meta field. This returns an array
		$term_meta = get_option("taxonomy_$t_id");
		foreach ($this->metaboxes as $group => $fields) {
			foreach ($fields as $field) {
				if (isset($field['name']) && isset($term_meta[$field['name']])) {
					$field['value'] = $term_meta[$field['name']];
				}
				?>
				<tr class="form-field">
					<th scope="row" valign="top"><?php echo new \Ardent\Html\Label(array('for' => @$field['name'], 'text' => @$field['label'])) ?></th>
					<td>
						<?php
						unset($field['label']);
						echo \Ardent\Html::get($field)
						?>
						<p class="description"><?php _e('Enter a value for this field', 'pippin'); ?></p>
					</td>
				</tr>
				<?php
			}
		}
	}

	public function save_taxonomy_custom_meta($term_id) {
		$t_id = $term_id;
		$term_meta = (array) array_filter((array) get_option("taxonomy_{$t_id}"));
		$data = stripslashes_deep($_POST);
		foreach ($this->metaboxes as $group => $fields) {
			foreach ($fields as $field) {
				$term_meta[$field['name']] = $data[$field['name']];
			}
		}
		update_option("taxonomy_$t_id", $term_meta);
	}

	public static function getSettings($term_id) {
		$data = (array) array_filter((array) \get_option("taxonomy_{$term_id}"));
		$term_meta = new \Ardent\Object($data);
		return $term_meta;
	}

	private function _update_labels() {
		$defaults = array(
			'name' => $this->label,
			'singular_name' => $this->label,
			'search_items' => "Search {$this->label}",
			'popular_items' => "Popular {$this->label}",
			'all_items' => "All {$this->label}",
			'parent_item' => "Parent {$this->label}",
			'parent_item_colon' => "Parent {$this->label}:",
			'edit_item' => "Edit {$this->label}",
			'view_item' => "View {$this->label}",
			'update_item' => "Update {$this->label}",
			'add_new_item' => "Add New {$this->label}",
			'new_item_name' => "New {$this->label} Name",
			'separate_items_with_commas' => "Separate {$this->label} with commas",
			'add_or_remove_items' => "Add or remove {$this->label}",
			'choose_from_most_used' => "Choose from the most used {$this->label}",
			'not_found' => "No {$this->label} found.",
		);
		$this->labels = array_merge($defaults, (array) $this->labels);
	}

	public function init() {
		$this->_update_labels();
		register_taxonomy($this->id, $this->posttype, $this->_args);
	}

}
