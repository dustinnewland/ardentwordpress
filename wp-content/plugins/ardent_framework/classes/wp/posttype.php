<?php

// %% incomplete %%      

/**
 * @file classes/wp/posttype.php
 * This file contains the Posttype class definition
 */

namespace Ardent\Wp;

/**
 * This is the primary class for creating new custom post types.
 */
class Posttype {

    protected $_fields = array();
    protected $_config = array(
        'name' => null,
        'id' => null,
        'archive' => null,
        'taxonomies' => array(),
        'fields' => array(),
        'custom_caps' => false,
        'singular' => null,
        'plural' => null,
        'settings_page' => null,
    );
    private $_nonce = false;
    protected $_args = array(
        'public' => true,
        'menu_icon' => 'dashicons-icon-ardent-bug',
    );

    public function __get($name) {
        if (array_key_exists($name, $this->_config)) {
            return $this->_config[$name];
        } elseif (array_key_exists($name, $this->_args)) {
            return $this->_args[$name];
        } else {
            return null;
        }
    }

    public function __set($name, $value) {
        if (array_key_exists($name, $this->_config)) {
            $this->_config[$name] = $value;
        } else {
            $this->_args[$name] = $value;
        }
    }

    /**
     * 
     * @param array $config This is the primary way to configure the custom post type
     * @code
     *  $config = array(
     *      'name' => '',   // This is the friendly name of your post type, like Staff, or Home Slide
     *      'plural' => '', // This is used for automatic labeling purposes. Fine grained control can be obtained by using the lables attribute
     *      'settings_page' => array('Top level menu', 'submenu'), // Use this to specify a page for the post type to put it's settings
     *      'metaboxes' => array(   // Use this to add metaboxes to the post type
     *          'General' => array(
     *              array('type' => 'input.text', 'name' => 'first_name', 'label' => 'First Name', 'placeholder' => 'First Name')
     *          )
     *      )
     *  )
     * @endcode
     */
    public function __construct($config = array()) {
        if (empty($config['id'])) {
            $config['id'] = sanitize_title_with_dashes($config['name']);
        }
        if (isset($config['metaboxes'])) {
            foreach ($config['metaboxes'] as $metabox_name => $fields) {
                $metabox = \Ardent\Wp\Metabox::getInstance($config['id'], $metabox_name);
                foreach ($fields as $field) {
                    $metabox->add_field($field);
                }
            }
            unset($config['metaboxes']);
        }
        if (empty($config['supports'])) {
            $config['supports'] = array('title', 'editor');
        }
        foreach ((array) $config as $key => $value) {
            $this->$key = $value;
        }

        $this->_update_labels();

        if ($this->settings_page) {
            $this->_update_settings();
        }

        $single = $this->id;
        $plural = $this->plural ? strtolower($this->plural) : $single . 's';
        $role = get_role('administrator');
        $caps_are_stored = $role->has_cap('edit_' . $plural);
        if ($this->custom_caps) {
            if (!isset($this->_args['capabilities'])) {
                $this->_args['capability_type'] = array($single, $plural);
                $this->_args['map_meta_cap'] = true;
            }
            // automatically generate missing caps
            if (!$caps_are_stored) {
                $role->add_cap('edit_' . $plural);
                $role->add_cap('edit_others_' . $plural);
                $role->add_cap('publish_' . $plural);
                $role->add_cap('read_private_' . $plural);
                $role->add_cap('delete_' . $plural);
                $role->add_cap('delete_private_' . $plural);
                $role->add_cap('delete_published_' . $plural);
                $role->add_cap('delete_others_' . $plural);
                $role->add_cap('edit_private_' . $plural);
                $role->add_cap('edit_published_' . $plural);
            }
        } else {
            if ($caps_are_stored) {
                global $wp_roles;
                foreach (array_keys($wp_roles->roles) as $r) {
                    $role = get_role($r);
                    $role->remove_cap('edit_' . $plural);
                    $role->remove_cap('edit_others_' . $plural);
                    $role->remove_cap('publish_' . $plural);
                    $role->remove_cap('read_private_' . $plural);
                    $role->remove_cap('delete_' . $plural);
                    $role->remove_cap('delete_private_' . $plural);
                    $role->remove_cap('delete_published_' . $plural);
                    $role->remove_cap('delete_others_' . $plural);
                    $role->remove_cap('edit_private_' . $plural);
                    $role->remove_cap('edit_published_' . $plural);
                }
            }
        }

        add_action('init', array(&$this, 'init'));
        add_action('admin_init', array(&$this, 'admin_init'));
        add_filter('rewrite_rules_array', array(&$this, 'rewrite_rules_array'));
//        add_filter('request', array(&$this, 'request'));
    }

    /**
     * 
     * @param type $rules
     * @return type
     */
    public function rewrite_rules_array($rules) {
        $newrules = array();
        if ($this->archive) {
            $baseurl = "index.php?post_type={$this->id}";
            $newrules["{$this->archive}/?$"] = "index.php?post_type={$this->id}";
            $newrules["{$this->archive}/category/([^/]+)/?$"] = "{$baseurl}&custom_cat=\$matches[1]";
            $newrules["{$this->archive}/category/([^/]+)/page/([\d+])/?$"] = "{$baseurl}&custom_cat=\$matches[1]&paged=\$matches[2]";
            $newrules["{$this->archive}/(\d{4})/(\d{2})/?$"] = "{$baseurl}&year=\$matches[1]&monthnum=\$matches[2]";
            $newrules["{$this->archive}/(\d{4})/(\d{2})/page/([\d+])/?$"] = "{$baseurl}&year=\$matches[1]&month=\$matches[2]&paged=\$matches[3]";
        }
        return $newrules + $rules;
    }

    private function _update_labels() {
        $singular = $this->singular ? : $this->name;
        $plural = $this->plural ? $this->plural : $singular . 's';
        $defaults = array(
            'name' => "{$plural}",
            'singular_name' => "{$singular}",
            'add_new' => "Add New",
            'add_new_item' => "Add New {$singular}",
            'edit_item' => "Edit {$singular}",
            'new_item' => "New $singular",
            'view_item' => "View {$singular}",
            'search_items' => "Search {$plural}",
            'not_found' => "No {$plural} found",
            'not_found_in_trash' => "{$plural} found in Trash.",
            'parent_item_colon' => "Parent {$singular}:",
            'all_items' => "All {$plural}",
        );
        $this->labels = array_merge($defaults, (array) $this->labels);
    }

    private function _update_settings() {
        $page = $this->settings_page[0];
        $subpage = @$this->settings_page[1];
        $config = \Ardent\Wp\Config::getInstance($page, $subpage);

// POST SETTINGS
        $settings = array(
            array('type' => 'h3', 'text' => 'URLs'),
            array('type' => 'input.text', 'name' => "{$this->id}_rewrite_single", 'default_value' => $this->id, 'label' => 'Single Item slug', 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_rewrite_archive", 'default_value' => $this->id, 'label' => 'Archive Url slug', 'class' => 'widefat'),
            array('type' => 'a', 'href' => get_site_url(null, $config->{"{$this->id}_rewrite_archive"}), 'text' => 'View Archive', 'new_window' => true),
            array('type' => 'h3', 'text' => 'Post Type Support'),
            array('type' => 'input.checkbox', 'name' => "{$this->id}_support_wysiwyg", 'label' => 'WYSIWYG Editor (post_content)'),
            array('type' => 'input.checkbox', 'name' => "{$this->id}_support_revisions", 'label' => 'Revisions'),
            array('type' => 'input.checkbox', 'name' => "{$this->id}_support_comments", 'label' => 'Comments'),
            array('type' => 'input.checkbox', 'name' => "{$this->id}_support_thumbnail", 'label' => 'Featured Thumbnail'),
            array('type' => 'input.checkbox', 'name' => "{$this->id}_support_attributes", 'label' => 'Page Attributes'),
            array('type' => 'h3', 'text' => 'Labels'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_name", 'label' => 'name', 'default_value' => $this->labels['name'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_singular_name", 'label' => 'singular_name', 'default_value' => $this->labels['singular_name'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_add_new", 'label' => 'add_new', 'default_value' => $this->labels['add_new'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_add_new_item", 'label' => 'add_new_item', 'default_value' => $this->labels['add_new_item'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_edit_item", 'label' => 'edit_item', 'default_value' => $this->labels['edit_item'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_new_item", 'label' => 'new_item', 'default_value' => $this->labels['new_item'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_view_item", 'label' => 'view_item', 'default_value' => $this->labels['view_item'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_search_items", 'label' => 'search_items', 'default_value' => $this->labels['search_items'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_not_found", 'label' => 'not_found', 'default_value' => $this->labels['not_found'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_not_found_in_trash", 'label' => 'not_found_in_trash', 'default_value' => $this->labels['not_found_in_trash'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_parent_item_colon", 'label' => 'parent_item_colon', 'default_value' => $this->labels['parent_item_colon'], 'class' => 'widefat'),
            array('type' => 'input.text', 'name' => "{$this->id}_label_all_items", 'label' => 'all_items', 'default_value' => $this->labels['all_items'], 'class' => 'widefat'),
            array('type' => 'hr'),
        );
        foreach ($settings as $setting) {
            $config->add_setting($this->id, $setting);
        }

// POST TYPE SUPPORT
        $this->_args['supports'] = array_filter(array(
            'title',
            $config->{"{$this->id}_support_wysiwyg"} ? 'editor' : '',
            $config->{"{$this->id}_support_revisions"} ? 'revisions' : '',
            $config->{"{$this->id}_support_comments"} ? 'comments' : '',
            $config->{"{$this->id}_support_thumbnail"} ? 'thumbnail' : '',
            $config->{"{$this->id}_support_attributes"} ? 'page-attributes' : '',
        ));

// LABELS
        foreach (array('name', 'singular_name', 'add_new', 'add_new_item', 'edit_item', 'new_item', 'view_item', 'search_items', 'not_found', 'not_found_in_trash', 'parent_item_colon', 'all_items') as $fieldname) {
            $this->_args['labels'][$fieldname] = $config->{"{$this->id}_label_{$fieldname}"} ? : $this->labels[$fieldname];
        }

// REWRITE
        $slug_single = $config->{"{$this->id}_rewrite_single"};
        $slug_archive = $config->{"{$this->id}_rewrite_archive"};
        $this->_args['rewrite'] = array('slug' => $slug_single);
        $this->_args['has_archive'] = $slug_archive;
        $posttype = $this->id;
        $args = &$this->_args;
        $config->onSave(function($d) use ($posttype, $args) {
            global $wp_post_types;
            $slug_single = @$_REQUEST["{$posttype}_rewrite_single"];
            $slug_archive = @$_REQUEST["{$posttype}_rewrite_archive"];
            $args['rewrite'] = array('slug' => $slug_single);
            $args['has_archive'] = $slug_archive;
            register_post_type($posttype, $args);
            flush_rewrite_rules();
            echo 'reload';
        });
    }

    public function init() {
        register_post_type($this->id, $this->_args);
        foreach (array_filter((array) $this->taxonomies) as $tax) {
            if (taxonomy_exists($tax)) {
                register_taxonomy_for_object_type($tax, $this->id);
            }
        }
    }

//    public function add_taxonomy($args) {
//        if (!array_key_exists('posttype', $args)) {
//            $args['posttype'] = $this->id ? $this->id : sanitize_title_with_dashes($this->name);
//        }
//        new Taxonomy($args);
//    }

    public function admin_init() {
        $id = $this->id;
        $post_type = isset($_REQUEST['post']) ? get_post_type($_REQUEST['post']) : @$_REQUEST['post_type'];
        if ($post_type !== $this->id) {
            return;
        }
    }

}
