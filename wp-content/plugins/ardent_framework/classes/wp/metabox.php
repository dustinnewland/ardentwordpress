<?php

/**
 * @file classes/wp/metabox.php
 * This file contains the Metabox class.
 * Metaboxes allow you to manage different types of data for different situations within WordPress. Metaboxes can be applied specifically to post types and data within that metabox is stored to the specific post. Certain manipulation is needed when using metaboxes. This class makes that manipulation easier and more efficient.
 */

namespace Ardent\Wp;

/**
 * Wraps Wordpress functionality to generate / manage / save Post type metaboxes
 */
class Metabox {

	private static $_instances = null; // Used for getting and setting the instance of a new or existing metabox.
	private $_fields = array();   // Used for setting the fields of a metabox instance you are using.
	private $_posttype = null; // Used for specifying the post type that a new metabox belongs to or getting the instance of a metabox.
	private $_nonce = null; // Used for validating that the contents came from the current site when saving data in the admin for specific instances of metaboxes.
	private $_functions = array();   // %% incomplete %% Don't know what this does or how it gets set.
	private $_name = ''; // Used for naming an instance of a metabox.
	public $id = '';  // Used for giving a unique name to an instance of a metabox.
	private $_location = 'normal';   // Used for specifying a location of a metabox. (normal | advanced | side)
	private $_priority = null; // Used for specifying the priority of a metabox. (high | core | default | low)
	private $_draw_meta_function = null;

	/**
	 * This is the general construct of the Metabox class.
	 *
	 * Upon invoking the class, you have the ablility of doing 2 things:
	 * 1. Create a non-existent metabox.
	 * 2. Grab an instance of an existing metabox.
	 *
	 * The best way to do these items involve using the exact same syntax. No special function is needed when specifying whether or not you want to create or get a metabox. They use the same function to do both.
	 *
	 * The conventional means used by PHP to use a class is not what needs to be used when using this class. Please reference the getInstance function of this class if you want to create or get a metabox.
	 *
	 * The preferred way of accessing the metabox class is like this:
	 * @code
	 * $metabox = \Ardent\Wp\Metabox::getInstance($posttype, $name, $location);
	 * @endcode
	 *
	 * @param string $posttype
	 * @param string $name
	 * @param string $location = normal | advanced | side
	 * @param string $priority = high | core | default | low
	 */
	public function __construct($posttype, $name, $location = null, $priority = 'default', $meta_func = null) {
		$this->_posttype = $posttype;
		$this->_priority = $priority;
		$this->_name = $name;
		$this->id = sanitize_title_with_dashes(strtolower($name));
		$this->_draw_meta_function = $meta_func;
		if ($location) {
			$this->_location = $location;
		}
		add_action('add_meta_boxes_' . $this->_posttype, array(&$this, 'add_meta_boxes'), 100, 1);
		add_action("save_post", array(&$this, 'save_post'), 10, 2);

		add_action('wp_restore_post_revision', array($this, 'wp_restore_post_revision'), 10, 2);
		add_action('_wp_post_revision_fields', array($this, 'wp_post_revision_fields'));
		add_filter('wp_save_post_revision_check_for_changes', array($this, 'wp_save_post_revision_check_for_changes'), 10, 3);
	}

	public function reset_fields() {
		$this->_fields = array();
	}

	/**
	 * This function is used to get the fields of the metabox selected by the getInstance function.
	 *
	 * @return Array An array of the fields used.
	 */
	public function fields() {
		return $this->_fields;
	}

	/** @internal
	 *
	 * This function is specifically for WordPress to save metabox revisions in the WordPress administration system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function wp_post_revision_fields($fields) {
		if (!post_type_supports($this->_posttype, 'revisions')) {
			return $fields;
		}

		foreach ($this->_fields as $field) {
			if (!isset($field['name'])) {
				continue;
			}
			$name = $field['name'];
			$label = @$field['placeholder'] ? : (@$field['label'] ? : $name);
			$fields[$name] = $label;
		}
		return $fields;
	}

	/** @internal
	 *
	 * This function is specifically for WordPress to save metabox revisions in the WordPress administration system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function wp_save_post_revision_check_for_changes($flag, $last_rev, $post) {
		if (!post_type_supports($this->_posttype, 'revisions')) {
			return true;
		}

		$meta_rev = new \Ardent\Wp\Meta($last_rev->ID);
		$meta_orig = new \Ardent\Wp\Meta($post->ID);
		$data_rev = $meta_rev->public_values();
		$data_post = $meta_orig->public_values();
		foreach ($data_post as $k => $v) {
			$new_v = @$_REQUEST[$k];
			$data_post[$k] = $new_v ? $new_v : $v;
		}
		if ($data_post != $data_rev) {
			return true;
		}
	}

	/** @internal
	 *
	 * This function is specifically for WordPress to save metabox revisions in the WordPress administration system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function wp_restore_post_revision($post_id, $rev_id = 0) {
		if (!post_type_supports($this->_posttype, 'revisions')) {
			return;
		}

		$meta_rev = new \ Ardent\Wp\Meta($rev_id);
		$meta_post = new \Ardent\Wp\Meta($post_id);

		foreach ($meta_rev->toArray() as $key => $value) {
			$meta_post->{$key} = $value;
		}
	}

	/**
	 * This is the function for creating a metabox or for getting an existing metabox instance.
	 *
	 * Here is an example of this function:
	 * @code
	 * $metabox = \Ardent\Wp\Metabox::getInstance($posttype, $name, $location);
	 * @endcode
	 *
	 * @param string $posttype
	 * @param string $name
	 * @param string $location = normal | advanced | side
	 * @return object Instance of the metabox.
	 */
	public static function getInstance($posttype = 'post', $name = null, $location = null, $priority = null, $meta_func = null) {
		$key = md5(implode(':', array($posttype, $name, $location)));
		if (!isset(self::$_instances[$posttype])) {
			self::$_instances[$posttype] = array();
		}
		if ($name) {
			if (!isset(self::$_instances[$posttype][$name])) {
				self::$_instances[$posttype][$name] = new Metabox($posttype, $name, $location, $priority, $meta_func);
			}
			return self::$_instances[$posttype][$name];
		} else {
			return self::$_instances[$posttype];
		}
	}

	public function add_field($field) {
		$this->_fields[] = $field;
	}

	/** @internal
	 *
	 * This function is specifically for WordPress to add the metaboxes to the system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function add_meta_boxes($p) {
		$func = $this->_draw_meta_function ? : array(&$this, 'draw_meta');
		add_meta_box($this->id, $this->_name, $func, $this->_posttype, $this->_location, $this->_priority, array($this->_name));
	}

	/** Not for public consumption
	 * @internal
	 *
	 * This function is specifically for WordPress to display metaboxes in the WordPress administration system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function draw_meta($post, $metabox) {
		$meta = new \Ardent\Wp\Meta($post->ID);
		$group = $metabox['id'];
		$metabox_class = "ardent-html-metabox " . sanitize_title_with_dashes("ardent-html-metabox-{$group}");
		if (!$this->_nonce) {
			wp_nonce_field(plugin_basename(__FILE__), $this->_posttype);
			$this->_nonce = true;
		}
		$mb = \Ardent\Html::get(array('type' => 'ul', 'class' => $metabox_class));
		foreach ($this->_fields as $field) {
			if (is_object(@$field['value']) && get_class(@$field['value']) === 'Closure') {
				$f = $field['value'];
				$field['value'] = $f($post);
			} elseif (is_object(@$field['value'])) {
				$field['value'] = json_encode($field['value']);
			} elseif (@$field['name']) {
				$v = $meta->{$field['name']};
				if ($v === null) {
					$v = @$field['default_value'] ? $field['default_value'] : null;
				}
				$field['value'] = $v;
			}

			$field_class = preg_replace('/[^a-z]+/', '_', @$field['type']);
			$li = \Ardent\Html::get(array('type' => 'li', 'text' => \Ardent\Html::get($field), 'class' => array($field_class, 'field')));
			if (@$field['name']) {
				$li->class = $field['name'];
			}
			$mb->text .= $li;
		}
		echo $mb;
	}

	/** @internal
	 *
	 * This function is specifically for WordPress to save metaboxes in the WordPress administration system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function save_post($post_id, $post) {
		$data = array(
			'post' => $post_id,
			'autosave' => defined('DOING_AUTOSAVE'),
			'posttype' => @$_POST [$this->_posttype], 'revision' => wp_is_post_revision($post_id));
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}
		if (!isset($_POST[$this->_posttype])) {
			return;
		}
		if (!wp_verify_nonce($_POST[$this->_posttype], plugin_basename(__FILE__))) {
			return;
		}
		$meta = new \Ardent\Wp\Meta($post_id);
		foreach ($this->_fields as $field) {
			$name = @$field['name'];
			$v = @$_POST[$name];
			if (isset($field['validate'])) {
				switch ($field['validate']) {
					case 'numeric':
						$v = preg_replace('/[^0-9]/', '', $v);
						break;
					case 'email':
						$v = filter_var($v, FILTER_SANITIZE_EMAIL);
						if (!filter_var($v, FILTER_VALIDATE_EMAIL)) {
							$v = '';
						}
						break;
					default:
						break;
				}
			}

			if ($name) {
				if ($meta[$name] !== $v) {
					$meta[$name] = $v;
				} else {
					if ($meta[$name] === null) {
						$meta[$name] = '';
					}
				}
			}
		}
		foreach ($this->_functions as $func) {
			$func($post_id, $post);
		}
	}

	/** @internal
	 *
	 * This function is specifically for WordPress to save metaboxes in the WordPress administration system.
	 *
	 * This function should not be used outsite of this class. It is a public class because of the way WordPress works.
	 *
	 * @endinternal
	 */
	public function onSave($func) {
		if (get_class($func) === 'Closure') {
			$this->_functions[] = $func;
		}
	}

}

// %% incomplete %%
