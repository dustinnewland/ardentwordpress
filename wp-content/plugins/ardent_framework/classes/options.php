<?php

namespace Ardent;

abstract class Options {

    static function Posts($post_type = 'post', $name = false) {
        $posts = get_posts(array(
            'post_type' => $post_type,
            'depth' => 0,
            'numberposts' => -1,
            'orderby' => 'title',
            'order' => 'ASC',
            'post_status' => 'publish',
            'child_of' => 0
        ));
        if (empty($posts)) {
            return array();
        }
        if ($name) {
            $return = array_map(function($p) {
                return (object) array(
                            'value' => $p->post_name,
                            'title' => $p->post_title
                );
            }, $posts);
        } else {
            $return = array_map(function($p) {
                return (object) array(
                            'value' => $p->ID,
                            'title' => $p->post_title
                );
            }, $posts);
        }
        return $return;
    }

    static function Taxonomies() {
	$taxes = array_merge(get_taxonomies(array('hierarchical' => true)), get_taxonomies(array('hierarchical' => false)));
	asort($taxes);
	$taxonomies = array();
	foreach ($taxes as $tax_slug) {
		$tax = get_taxonomy($tax_slug);
		$taxonomies[$tax_slug] = $tax->labels->name . " ({$tax_slug})";
	}
	return $taxonomies;
    }

    static function Terms($taxonomy = null) {
        if ($taxonomy === null) {
            $taxonomies = array_merge(get_taxonomies(array('hierarchical' => true)), get_taxonomies(array('hierarchical' => false)));
        } else {
            $taxonomies = array($taxonomy);
        }
        $terms = array();
        foreach ($taxonomies as $tax) {
            $set = array();
            foreach ((array) get_terms($tax, array('get' => 'all')) as $term) {
                $set["{$tax}:{$term->term_id}"] = $term->name;
            }
            $terms[$tax] = $set;
        }
        $terms = array_filter($terms);
        if ($taxonomy === null) {
            return $terms;
        } else {
            return $terms[$taxonomy];
        }
    }

    static function Menus($concat = false) {
        $menus = wp_get_nav_menus();

        if (empty($menus)) {
            return array();
        }
        $keys = wp_list_pluck($menus, 'term_id');
        $values = wp_list_pluck($menus, 'name');
        if ($concat) {
            $keys = array_map(function($a, $b) {
                return $a . ':' . $b;
            }, $keys, $values);
        }

        return array_combine($keys, $values);
    }

    static function States() {
        return array(
            "" => "Select...",
            "AL" => "Alabama",
            "AK" => "Alaska",
            "AZ" => "Arizona",
            "AR" => "Arkansas",
            "CA" => "California",
            "CO" => "Colorado",
            "CT" => "Connecticut",
            "DE" => "Delaware",
            "FL" => "Florida",
            "GA" => "Georgia",
            "HI" => "Hawaii",
            "ID" => "Idaho",
            "IL" => "Illinois",
            "IN" => "Indiana",
            "IA" => "Iowa",
            "KS" => "Kansas",
            "KY" => "Kentucky",
            "LA" => "Louisiana",
            "ME" => "Maine",
            "MD" => "Maryland",
            "MA" => "Massachusetts",
            "MI" => "Michigan",
            "MN" => "Minnesota",
            "MS" => "Mississippi",
            "MO" => "Missouri",
            "MT" => "Montana",
            "NE" => "Nebraska",
            "NV" => "Nevada",
            "NH" => "New Hampshire",
            "NJ" => "New Jersey",
            "NM" => "New Mexico",
            "NY" => "New York",
            "NC" => "North Carolina",
            "ND" => "North Dakota",
            "OH" => "Ohio",
            "OK" => "Oklahoma",
            "OR" => "Oregon",
            "PA" => "Pennsylvania",
            "RI" => "Rhode Island",
            "SC" => "South Carolina",
            "SD" => "South Dakota",
            "TN" => "Tennessee",
            "TX" => "Texas",
            "UT" => "Utah",
            "VT" => "Vermont",
            "VA" => "Virginia",
            "WA" => "Washington",
            "WV" => "West Virginia",
            "WI" => "Wisconsin",
            "WY" => "Wyoming"
        );
    }

    static function Years($start, $interval = null) {
        $years = array();

        if ($interval === null) {
            $stop = date("Y");
        } else {
            $str = "{$start} {$interval}";
            $stop = date("Y", strtotime($interval, strtotime("{$start}-01-01")));
        }
        $start = (int) $start;
        $stop = (int) $stop;
        $dir = $start > $stop ? -1 : 1;

        for ($i = $start; $i != $stop; $i += $dir) {
            $years[$i] = $i;
        }
        return $years;
    }

    static function Forms() {
        global $wpdb;
        $sql = "SELECT * FROM `{$wpdb->prefix}frm_forms` WHERE `is_template`='0' and `status`='published'";
        $forms = (array) array_filter((array) $wpdb->get_results($sql));
        $list = array_combine(wp_list_pluck($forms, 'id'), wp_list_pluck($forms, 'name'));
        asort($list, SORT_STRING | SORT_FLAG_CASE);
        return $list;
    }

    static function Users() {
        $users = get_users();
        $user_options = array();
        foreach ($users as $user) {
            $user_options[$user->ID] = $user->data->user_login;
        }
        return $user_options;
    }

    static function Roles() {
        $roles = wp_roles();
        $names = array_keys($roles->roles);
        return $names;
    }

}
