<?php

namespace Ardent;

/**
 * Html class contains a single static function designed to generate
 * html based on the configuration.
 */
abstract class Post {

    static function Excerpt($charlen = 275, $content = null) {
        if ($content === null) {

            $post = get_post();
            if (empty($post)) {
                return '';
            }
            if (post_password_required()) {
                return __('There is no excerpt because this is a protected post.');
            }
            $content = $post->post_excerpt ? : $post->post_content;
        }

        $content = strip_shortcodes($content);
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        $content = wp_strip_all_tags($content);
        $content = html_entity_decode($content, ENT_COMPAT | ENT_HTML401, 'UTF-8');
        if ($charlen < mb_strlen($content)) {
            $index = @mb_strpos($content, ' ', $charlen, 'UTF-8');
            if (!$index || $index > $charlen) {
                $index = mb_strrpos($content, ' ', $charlen - mb_strlen($content));
            }
        } else {
            $index = mb_strlen($content);
        }
        return htmlentities(mb_substr($content, 0, $index), ENT_COMPAT | ENT_HTML401 , 'UTF-8');
    }

}
