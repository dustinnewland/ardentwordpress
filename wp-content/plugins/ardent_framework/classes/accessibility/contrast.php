<?php

namespace Ardent\Accessibility;

class Contrast {

	public static function Calculate($c1, $c2) {
		$rl1 = self::rl($c1);
		$rl2 = self::rl($c2);
		return (max($rl1, $rl2) + 0.05) / (min($rl1, $rl2) + 0.05);
	}

	public static function rl($c) {
		$c = trim($c, '#');
		if (strlen($c) == 3) {
			$stuff = str_split($c);
			$c = "{$stuff[0]}{$stuff[0]}{$stuff[1]}{$stuff[1]}{$stuff[2]}{$stuff[2]}";
		}
		$color_parts = str_split($c, 2);
		$color_parts = array_map('hexdec', $color_parts);
		$rl = self::_rl($color_parts[0]) * 0.2126 +
				self::_rl($color_parts[1]) * 0.7152 +
				self::_rl($color_parts[2]) * 0.0722;
		return $rl;
	}

	private static function _rl($c) {
		$xsrgb = $c / 255;
		return $xsrgb <= 0.03928 ? $xsrgb / 12.92 : pow(($xsrgb + 0.055) / 1.055, 2.4);
	}

	public static function Analyze($text_color, $bg_color) {
		$cr = round(self::Calculate($text_color, $bg_color), 1);
		$demo_line = <<<EOHTML
			<span style='font-size:18px;background-color: {$bg_color}; color: {$text_color};'>Large Text</span>
			<span style='font-size:14px;background-color: {$bg_color}; color: {$text_color};'>Normal Text</span>
EOHTML;
		$messages = [
			$demo_line,
			"Contrast Ratio: {$cr}",
		];

		if ($cr >= 7) {
			$messages[] = "AAA: Pass!";
			$messages[] = "AA: Pass!";
		} elseif ($cr >= 4.5) {
			$messages[] = "AAA: Pass for Large Text only!";
			$messages[] = "AA: Pass!";
		} elseif ($cr >= 3) {
			$messages[] = "AAA: Fail :(";
			$messages[] = "AA: Pass for Large Text Only";
		} else {
			$messages[] = "Terrible :(";
		}
		return implode('<br>', $messages);
		;
	}

}
