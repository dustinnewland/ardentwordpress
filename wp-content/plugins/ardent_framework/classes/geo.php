<?php

namespace Ardent;

/**
 * Html class contains a single static function designed to generate
 * html based on the configuration.
 */
abstract class Geo {

    static function Distance($lat1, $lng1, $lat2, $lng2) {
        $r = 3958.761;
        $x1 = deg2rad((float) $lng1);
        $x2 = deg2rad((float) $lng2);
        $y1 = deg2rad((float) $lat1);
        $y2 = deg2rad((float) $lat2);
        return acos(sin($y1) * sin($y2) + cos($y1) * cos($y2) * cos($x2 - $x1)) * $r;
    }

}
