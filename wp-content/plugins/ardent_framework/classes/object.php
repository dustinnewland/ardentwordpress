<?php

namespace Ardent;

class Object implements \ArrayAccess {

	protected $_config = array();
	protected $_args = array();

	public function __construct($config = array()) {
		foreach ((array) $config as $key => $value) {
			$this->$key = $value;
		}
	}

	public function __get($name) {
		if (array_key_exists($name, $this->_config)) {
			return $this->_config[$name];
		} elseif (array_key_exists($name, $this->_args)) {
			return $this->_args[$name];
		} else {
			return null;
		}
	}

	public function __set($name, $value) {
		if (array_key_exists($name, $this->_config)) {
			$this->_config[$name] = $value;
		} else {
			$this->_args[$name] = $value;
		}
	}

	public function toArray() {
		return $this->_args;
	}

	public function offsetExists($offset) {
		return true;
	}

	public function offsetGet($offset) {
		return $this->__get($offset);
	}

	public function offsetSet($offset, $value) {
		return $this->__set($offset, $value);
	}

	public function offsetUnset($offset) {
		return $this->__set($offset, null);
	}

}
