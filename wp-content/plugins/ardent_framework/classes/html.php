<?php

/**
 * @file ardent/classes/html.php Contains abstract html generators
 */

namespace Ardent;

/**
 * Html class contains a single static function designed to generate
 * html based on the configuration.
 */
abstract class Html {

    static $defaults = array();

    /**
     * This function returns an @code \Ardent\Html\Tag @endcode based object with the
     * requested config.
     * @param array $config
     * @return Tag
     */
    public static function get($config) {
        if (isset($config['type'])) {
            $type = $config['type'];
            unset($config['type']);
        } else {
            return null;
        }

        $path = explode('.', strtolower($type));
        if (!isset($config['tag']))
            $config['tag'] = $path[0];

        $classname = 'Ardent\\Html';
        while ($fragment = array_shift($path)) {
            $classname .= '\\' . ucfirst(strtolower($fragment));
        }
        if (class_exists($classname)) {
            return new $classname($config);
        } else {
            return new \Ardent\Html\Tag($config);
        }
    }

    public static function build_attributes($attributes) {
        $attribs = array_map(function($first, $second) {
            return "$first='" . htmlEntities($second, ENT_QUOTES) . "'";
        }, array_keys($attributes), array_values($attributes));
        $attribs = count($attribs) ? ' ' . implode(' ', $attribs) : '';
        return $attribs;
    }

}
