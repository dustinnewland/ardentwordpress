<?php
/**
 * @file acframework/classes/formidable/field.php
 * This file contains formidable field code
 */

namespace Ardent\Formidable;

class Field extends \Ardent\Object {

    protected $_config = array(
        'id' => null,
        'name' => null,
        'fields' => false,
        'validate' => null,
        'display' => null,
    );

    /**
     * 
     * @param string $name
     * @param array $config
     */
    public function __construct($name, $config = array()) {
        parent::__construct($config);
        $this->name = $name;
        if (!$this->id) {
            $this->id = preg_replace('/[^a-z0-9_]/', '_', strtolower($name));
        }
        add_filter('frm_pro_available_fields', array($this, 'make_field_available'));
        add_filter('frm_update_field_options', array($this, 'update_options'), 10, 3);
        add_filter('frm_before_field_created', array($this, 'set_default_values'));
        add_action('frm_field_options_form', array($this, 'display_options_form'), 10, 3);
        if ($this->validate) {
            add_filter('frm_validate_field_entry', $this->validate, 20, 3);
        }
        if ($this->display) {
            add_action('frm_form_fields', $this->display);
        }
    }

    public function make_field_available($fields) {
        $fields[$this->id] = $this->name; // the key for the field and the label
        return $fields;
    }

    public function update_options($options, $field, $values) {
        if ($field->type == $this->id) {
            foreach ($this->fields as $f) {
                if (isset($f['name'])) {
                    $field_name = $this->id . '_' . $f['name'] . '_' . $field->id;
                    $options[$this->id][$f['name']] = $values[$field_name];
                }
            }
        }
        return $options;
    }

    public function set_default_values($field_data) {

        if (isset($field_data['type']) && $field_data['type'] == $this->id) {
            $field_data['name'] = $this->name;
            foreach ($this->fields as $f) {
                if (isset($f['name'])) {
                    $default_value = isset($f['default']) ? $f['default'] : '';
                    $field_name = $this->id . '_' . $f['name'];

                    $field_data[$this->id . '_' . $f['name']] = $default_value;
                }
            }
        }
        return $field_data;
    }

    public function display_options_form($field, $display, $values) {
        if ($field['type'] == $this->id) {
            ?>
            <tr>
                <td><label>Fields</label></td>
                <td>
                    <?php
                    foreach ($this->fields as $f) {
                        $display_field = $f;
                        $display_field['name'] = "{$this->id}_{$f['name']}_{$field['id']}";
                        $display_field['value'] = @$field[$this->id][$f['name']];
                        $display_field['id'] = "{$f['name']}_{$field['id']}";
                        ?>
                        <div class="field"><?php echo \Ardent\Html::get($display_field) ?></div>
                    <?php } ?>
                </td>
            </tr><?php
        }
    }

}
