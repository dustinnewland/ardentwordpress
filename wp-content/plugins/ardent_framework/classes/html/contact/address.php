<?php

/**
 * @file classes/html/contact/number.php
 * This file contains the phone number generator.
 * The phone number html generator is used to generate an HTML snippet for use with phone numbers.
 */

namespace Ardent\Html\Contact;

/**
 * Contact Number class
 *
 * This class returns the html for an input for phone numbers to format them based on a standardized format.
 *
 * The preferred way of accessing the phone number html generator class is like this:
 * @code
 * $number = \Ardent\Html::get(array(
 *      'type' => 'contact.number',
 *      'value' => '8175551111' // non-numeric chars will be stripped out.
 *      'format' => 'dot' | 'dash' | 'traditional'
 * ));
 * @endcode
 *
 * Another way of accessing the phone number html generator class is like this:
 * @code
 * $number = new \Ardent\Html\Contact\Number(array(
 *      'value' => '8175551111' // non-numeric chars will be stripped out.
 *      'format' => 'dot' | 'dash' | 'traditional'
 * ));
 * @endcode
 *
 * @ingroup html_generator
 * @ingroup format
 */
class Address extends \Ardent\Html\Tag {

	public function __construct($config = array()) {
		$this->_properties = array_merge($this->_properties, array(
			'value' => null,
			'label' => null,
			'address1' => null,
			'address2' => null,
			'city' => null,
			'state' => null,
			'zipcode' => null,
		));
		parent::__construct($config);
		$this->tag = 'div';
	}

	public function __toString() {
		// <meta itemprop="streetAddress" content="">
		$street_address_text = implode('<br>', array_filter(array($this->address1, $this->address2)));
		$street_address = $street_address_text ? new \Ardent\Html\Span(array('text' => $street_address_text)) : null;
		$city = $this->city ? new \Ardent\Html\Span(array('text' => $this->city)) : null;
		$state = $this->state ? new \Ardent\Html\Span(array('text' => $this->state)) : null;
		$zip = $this->zipcode ? new \Ardent\Html\Span(array('text' => $this->zipcode)) : null;

		$this->text = implode('<br>', array_filter(array(
			$street_address,
			implode(', ', array_filter(array(
				$city,
				implode(' ', array_filter(array(
					$state,
					$zip
				)))
			))),
		)));
		return parent::__toString();
	}

}
