<?php

/**
 * @file classes/html/contact/number.php
 * This file contains the phone number generator.
 * The phone number html generator is used to generate an HTML snippet for use with phone numbers.
 */

namespace Ardent\Html\Contact;

/**
 * Contact Number class
 *
 * This class returns the html for an input for phone numbers to format them based on a standardized format.
 *
 * The preferred way of accessing the phone number html generator class is like this:
 * @code
 * $number = \Ardent\Html::get(array(
 *      'type' => 'contact.number',
 *      'value' => '8175551111' // non-numeric chars will be stripped out.
 *      'format' => 'dot' | 'dash' | 'traditional'
 * ));
 * @endcode
 *
 * Another way of accessing the phone number html generator class is like this:
 * @code
 * $number = new \Ardent\Html\Contact\Number(array(
 *      'value' => '8175551111' // non-numeric chars will be stripped out.
 *      'format' => 'dot' | 'dash' | 'traditional'
 * ));
 * @endcode
 *
 * @ingroup html_generator
 * @ingroup format
 */
class Number extends \Ardent\Html\Tag {

	public static function format_options() {
		return array(
			'traditional' => '1 (800) 555-5555 x1234',
			'dot' => '1.800.555.5555 x1234',
			'dash' => '1-800-555-5555 x1234',
		);
	}

	public function __construct($config = array()) {
		$this->_properties = array_merge($this->_properties, array(
			'value' => null,
			'format' => null,
			'label' => null,
			'create_link' => false,
		));
		parent::__construct($config);
		$this->tag = 'span';
	}

	public function __toString() {
		// Turn letters into numbers
		$number = strtr(strtolower($this->value), "abcdefghijklmnopqrstuvwxyz", "22233344455566677778889999");

		$number = preg_replace('/[^0-9]/', '', $number);
		$v = sprintf("%010s", $number);

		if (substr($v, 0, 1) == 1) {
			$prefix = '1';
			$v = substr($v, 1);
		} else {
			$prefix = null;
		}
		$area_code = substr($v, 0, 3);
		$exchange = substr($v, 3, 3);
		$suffix = substr($v, 6, 4);
		$extension = substr($v, 10);
		$format = $this->format ? $this->format : 'traditional';

		if (is_null($this->text)) {
			switch ($format) {
				case 'dot':
					$this->text .= implode('.', array_filter(array(
						$prefix,
						$area_code,
						$exchange,
						$suffix,
					)));
					break;
				case 'dash':
					$this->text .= implode('-', array_filter(array(
						$prefix,
						$area_code,
						$exchange,
						$suffix,
					)));
					break;
				default: // traditional
					$this->text .= '';
					if ($prefix) {
						$this->text .= "{$prefix} ";
					}
					if ($area_code) {
						$this->text .= "({$area_code}) ";
					}
					$this->text .= "{$exchange}-{$suffix}";
					break;
			}
			if ($extension) {
				$this->text .= " x{$extension}";
			}
		}
		if ($this->create_link) {
			return (string) \Ardent\Html::get(array('type' => 'a', 'href' => "tel:{$number}", 'text' => $this->text));
		}
		return parent::__toString();
	}

}

// %% incomplete %%
?>
