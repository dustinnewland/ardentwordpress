<?php

namespace Ardent\Html\Input\Google;

class Font extends \Ardent\Html\Select {

    static $fonts = null;

    public function __construct($config = array()) {
        $config['tag'] = 'select';
        parent::__construct($config);
        if (self::$fonts === null) {
            $fonts = $font_string = file_get_contents('https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyAJ61UwC_A7twpwQe_YRc_UUt35rpkHjxI');
            $data = json_decode($font_string);
            $fonts = array();
            foreach ($data->items as $font) {
                $fonts[$font->family] = $font->family;
            }
            self::$fonts = $fonts;
        }
        $this->options = array('Select...') + self::$fonts;
    }

    public function __toString() {
        $return = parent::__toString();
        return $return;
    }

}

// %% incomplete %% 
