<?php // %% incomplete %% ?>
<?php

namespace Ardent\Html\Input;

class Hidden extends \Ardent\Html\Input
{

	public function __construct($config = array())
	{
		parent::__construct($config);
		$this->type = 'hidden';
		$this->self_closing = true;
	}

}
