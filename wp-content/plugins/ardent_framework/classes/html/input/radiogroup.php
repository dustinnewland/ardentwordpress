<?php // %% incomplete %%       ?>
<?php

namespace Ardent\Html\Input;

class Radiogroup extends \Ardent\Html\Div {

    private $_options = array();

    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'label' => null,
            'options' => null,
            'name' => null,
        ));
        parent::__construct($config);
    }

    public function __toString() {
        $output = '';
        $default_option = array(
            'type' => 'input.radio',
            'value' => null,
            'label' => null,
        );
        if ($this->label) {
//            $this->{'data-xlist-display'} = $this->display;
            $lbl = \Ardent\Html::get(array('type' => 'label', 'text' => $this->label));
            if ($this->description) {
                $lbl->text = "{$this->label}<br><div class='label-description'>{$this->description}</div>";
            } else {
                $lbl->text = $this->label;
            }
            $lbl->for = $this->name;
            $output .= $lbl;
        }
        foreach ($this->options as $value => $label) {
            if (is_object($label)) {
                $value = $label->value;
                $label = $label->title;
            }
            $option = array(
                'label' => $label,
                'value' => $value,
                'name' => $this->name,
            );
            $thevalue = $this->value !== NULL ? $this->value : $this->default_value;
            if ($thevalue == $value) {
                $option['checked'] = 'checked';
            }
            $output .= \Ardent\Html::get(array_merge($default_option, $option));
        }
        $div = new \Ardent\Html\Div(array(
            'class' => $this->class,
            'data-xlist-display' => $this->{'data-xlist-display'},
        ));
        $div->text = $output;
        return (string) $div;
    }

}
