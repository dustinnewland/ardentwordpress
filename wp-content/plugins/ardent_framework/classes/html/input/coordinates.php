<?php

namespace Ardent\Html\Input;

class Coordinates extends Text {

    public function __toString() {
        $container = new \Ardent\Html\Div;
        $button = \Ardent\Html::get(array('type' => 'button', 'text' => 'Find', 'class' => 'ardent-coordinates-lookup'));
        $container->text = parent::__toString() . $button;
        return (string) $container;
    }

}
