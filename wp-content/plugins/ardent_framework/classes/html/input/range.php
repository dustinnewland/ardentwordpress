<?php // %% incomplete %%                ?>
<?php

namespace Ardent\Html\Input;

class Range extends \Ardent\Html\Input\Text {

    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'show_value' => 'after',
            'display_value' => null,
        ));

        parent::__construct($config);
        $this->type = 'range';
    }

    public function __toString() {

        if ($this->value === null && $this->default_value !== null) {
            $this->value = $this->default_value;
        }

        if ($this->show_value !== 'none') {
            $value_container = \Ardent\Html::get(array('type' => 'span', 'class' => 'value_container', 'text' => $this->display_value ? : $this->value));
            if ($this->show_value == 'before') {
                $this->before = $value_container;
            } else {
                $this->after = $value_container;
            }
        } else {
            $value_container = '';
        }
        return parent::__toString();
    }

}
