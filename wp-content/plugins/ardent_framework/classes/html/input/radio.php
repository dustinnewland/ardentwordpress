<?php

namespace Ardent\Html\Input;

class Radio extends \Ardent\Html\Input {

    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'label_location' => 'after'
        ));
        parent::__construct($config);
        $this->type = 'radio';
        $this->self_closing = true;
    }
}
