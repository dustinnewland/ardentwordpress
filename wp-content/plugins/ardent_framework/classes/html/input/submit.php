<?php // %% incomplete %% ?>
<?php

/**
 * @file classes/html/input/submit.php
 */

namespace Ardent\Html\Input;

/**
 * Submit class useful for creating a submit button
 */
class Submit extends \Ardent\Html\Input {

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->type = 'submit';
        $this->self_closing = true;
    }

}
