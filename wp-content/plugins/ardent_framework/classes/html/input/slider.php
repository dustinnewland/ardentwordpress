<?php

namespace Ardent\Html\Input; // Used to group classes as Jquery

class Slider extends \Ardent\Html\Input\Hidden {

    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'min' => 0,
            'max' => 100,
            'step' => 1,
        ));
        $config['tag'] = 'input';
        parent::__construct($config);
        $this->{'data-slider-min'} = $this->min;
        $this->{'data-slider-max'} = $this->max;
        $this->{'data-slider-step'} = $this->step;
    }

}
