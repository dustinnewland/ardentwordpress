<?php // %% incomplete %% ?>
<?php

namespace Ardent\Html\Input;

class File extends \Ardent\Html\Input {

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->_properties = array_merge($this->_properties, array(
            'nosave' => null
        ));
        $this->type = 'file';
        $this->self_closing = true;
    }

    public function __toString() {
        $value = $this->value;
        $name = $this->name;
        $text = $this->text;
        
        $this->value = null;
        $this->name = "{$name}_upload";
        $this->text = null;
        $return = parent::__toString();
        if ($value) {
            $return .= $this->text = \Ardent\Html::get(array('type' => 'input.hidden', 'name' => $name, 'value' => $value)) . $text;
            $return .= " ({$value})";
        }
        $this->value = $value;
        $this->text = $text;
        $this->name = $name;
        return $return;
    }

}
