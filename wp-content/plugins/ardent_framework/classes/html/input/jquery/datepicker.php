<?php

namespace Ardent\Html\Input\Jquery;

class Datepicker extends \Ardent\Html\Input\Text {

    public function __construct($config = array()) {
        $config['tag'] = 'input';
        $config['type'] = 'text';
        parent::__construct($config);
    }

}
