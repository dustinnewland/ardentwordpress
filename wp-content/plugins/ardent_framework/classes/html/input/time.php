<?php

namespace Ardent\Html\Input; // Used to group classes as Jquery

class Time extends \Ardent\Html\Input\Slider {

    public function __construct($config = array()) {
        $config = array_merge(array(
            'min' => 0,
            'max' => 1440,
            'step' => 15,
        ), $config);
        parent::__construct($config);
        $this->{'data-slider-min'} = $this->min;
        $this->{'data-slider-max'} = $this->max;
        $this->{'data-slider-step'} = $this->step;
        $this->class = 'time-slider';
    }

}
