<?php // %% incomplete %%   ?>
<?php

namespace Ardent\Html\Input\Wp;

class Post extends \Ardent\Html\Input {

    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'post_type' => 'post',
        ));
        parent::__construct($config);
        $this->type = 'text';
        $this->self_closing = true;
    }

    public function __toString() {
        $this->{'data-post-type'} = $this->post_type ? : 'post';
        if ($this->value === null && $this->default_value !== null) {
            $this->value = $this->default_value;
        }
        return parent::__toString();
    }

}
