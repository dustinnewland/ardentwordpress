<?php

namespace Ardent\Html\Input\Wp;

class Editor extends \Ardent\Html\Tag
{

  public function __construct($config = array())
  {
    $config['tag'] = 'div';
    $this->_properties = array_merge($this->_properties, array(
        'label' => null,
        'name' => null,
        'id' => null,
        'maxlength' => null
    ));
    parent::__construct($config);
    if (!$this->id) {
      $this->id = preg_replace('/[^a-z]/', '', strtolower($this->name));
    }
  }

  public function __toString()
  {
    $args = array(
        'textarea_name' => $this->name,
        'textarea_rows' => '10',
        'wpautop' => true,
    );
    
    $this->text = '';
    if ($this->label) {
        $this->text .= \Ardent\Html::get(array('type' => 'label', 'text' => $this->label, 'for' => $this->name));
    }
    ob_start();
    wp_editor($this->value, $this->id . 'edit', $args);
    $this->text .= ob_get_contents();
    ob_end_clean();
    $v = $this->value;
    $this->value = null;
    $return = parent::__toString();
    $this->value = $v;

    return $return;
  }

}
