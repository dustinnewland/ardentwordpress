<?php

// %% incomplete %%

namespace Ardent\Html\Input\Wp;

class Color extends \Ardent\Html\Input\Text {

	public function __construct($config = array()) {
		parent::__construct($config);
		$this->_attributes['class'] = array('ardent-html-input');

		if (!$this->id) {
			$this->id = preg_replace('/[^a-z]/', '', strtolower($this->name));
		}
	}

	public function __toString() {
		$lbl = $this->label;
		$this->label = null;
		if (!$this->value) {
			$this->value = $this->default_value;
		}
		$input = parent::__toString();
		if (!$this->id && $lbl) {
			$this->id = uniqid(preg_replace('/[^a-z0-9\-]/', '', strtolower($this->name)));
		}
		$text = $lbl;
		if ($this->description) {
			$text = "{$lbl}<br><div class='label-description'>{$this->description}</div>";
		}
		$label = new \Ardent\Html\Label(array(
			'text' => $text,
			'for' => $this->id
		));

		$this->label = $lbl;
		$preview = \Ardent\Html::get(array('type' => 'div', 'class' => 'ardent-color-preview'));
		if ($this->value) {
			$preview->style = 'background-color: ' . $this->value . ';';
		}
		return (string) '<div class="ardent-html-input-wp-color">' . $label . '<div class="ardent-html-input-wp-color-inner">' . $preview . $input . '</div></div>';
	}

}
