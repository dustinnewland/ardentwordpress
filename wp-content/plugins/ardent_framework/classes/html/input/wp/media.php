<?php

// %% incomplete %%

namespace Ardent\Html\Input\Wp;

class Media extends \Ardent\Html\Tag {

    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'label' => null,
            'default_value' => null,
            'data-editable' => null,
            'data-xlist-display' => null,
            'preview_size' => 'thumbnail',
            'conditions' => null,
            'value' => null,
            'name' => null,
            'types' => array('image'),
        ));
        $config['tag'] = 'div';
        parent::__construct($config);
        $this->_attributes['class'] = array('ardent-html-input', 'ardent-html-input-wp-media');

        if (!$this->id) {
            $this->id = preg_replace('/[^a-z]/', '', strtolower($this->name));
        }
    }

    public function __toString() {
        if ($this->multiple !== null) {
            $this->{'data-multiple'} = $this->multiple;
            $this->multiple = null;
        }
        $t = $this->text;
        if ($this->conditions) {
            $this->{'data-ardent-conditions'} = json_encode($this->conditions);
        }
        $this->{'data-types'} = implode(',', (array) $this->types);
        $values = array_filter((array) explode(',', $this->value));
        $input = new \Ardent\Html\Input\Hidden(array(
            'name' => $this->name,
            'value' => $this->value,
            'class' => 'ardent-input-wp-media-value',
            'data-xlist-display' => $this->{'data-xlist-display'}
        ));
        if ($this->{'data-editable'}) {
            $input->{'data-editable'} = true;
        }
        $add_button = new \Ardent\Html\Div(array('text' => ($this->{'data-multiple'}?'Add':'Upload'), 'class' => 'ardent-button add-media'));
        $label = $this->label ? new \Ardent\Html\Label(array('text' => $this->label, 'for' => $this->name)) : '';

        $media = array();
        foreach ($values as $media_id) {
            $image_title = basename(get_attached_file($media_id));
            $image = '<span class="dashicons dashicons-trash"></span>';
            $image .= new \Ardent\Html\Wp\Image(array('itemid' => $media_id, 'size' => $this->preview_size, 'class' => "ardent-input-wp-media-preview size-{$this->preview_size}", 'autosize' => true, 'title' => $image_title));
            $media[] = $image;
        }
        $imagelist = new \Ardent\Html\Ul(array('items' => $media, 'class' => 'imagelist'));

        $this->text = $label . '<div class="ardent-media-container'.($this->{'data-multiple'}?' allow-multiple':'').'">' . $input . '<div class="ardent-imagelist-container">' . $imagelist . '</div>' . $add_button . '</div>'; //  . $remove_button;
        $this->{'data-name'} = $this->name;
        $return = parent::__toString();
        $this->text = $t;
        return $return;
    }

}
