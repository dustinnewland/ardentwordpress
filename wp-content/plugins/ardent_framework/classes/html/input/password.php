<?php

namespace Ardent\Html\Input;

class Password extends \Ardent\Html\Input {

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->type = 'password';
        $this->self_closing = true;
    }

}
