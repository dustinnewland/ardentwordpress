<?php

namespace Ardent\Html\Input;

class Xlist extends \Ardent\Html\Tag {

    public function __construct($config = array()) {
        $config['tag'] = 'div';
        $this->_properties = array_merge($this->_properties, array(
            'label' => null,
            'form' => null,
            'name' => null,
            'value' => null,
            'display' => null,
            'data-xlist-display' => null,
            'preview' => false
        ));
        parent::__construct($config);
        $this->{'data-name'} = $this->name;
    }

    public function __toString() {
        $this->class = $this->name;
        $form = \Ardent\Html::get(array('type' => 'div', 'class' => 'form'));
        $form->text .= \Ardent\Html::get(array('type' => 'h3', 'text' => 'Add/Edit ' . $this->label));
        $form->style = 'display: none';

        $form_keys = array_keys($this->form);
        $mykey = array_shift(array_pad($form_keys, 2, null));
        if (is_int($mykey)) { // This is a flat array, lets convert it to the new form
            $this->form = array('General' => $this->form);
        }

        if ($this->form) {
            $tabs = array();
            foreach ($this->form as $tab_title => $fields) {//$input) {
                $field_markup = '';
                $content = null;
                foreach ($fields as $field) {
                    if (isset($field['display'])) {
                        $field['data-xlist-display'] = $field['display'];
                        unset($field['display']);
                    }
                    $control = \Ardent\Html::get($field);
                    $control->class = 'xform-input';
                    $container = new \Ardent\Html\Div(array('text' => $control, 'class' => 'field'));
                    $field_markup .= $container;
                }
                $tabs[$tab_title] = $field_markup;
            }
            $form->text .= \Ardent\Html::get(array('type' => 'jquery.tabs', 'tabs' => $tabs));
            $add_button = \Ardent\Html::get(array('type' => 'span', 'class' => 'dashicons dashicons-plus-alt'));
        } else {
            $add_button = '';
        }
        if (is_callable($this->value)) {
            $this->value = call_user_func($this->value, $this);
        }
        $data = is_string($this->value) ? json_decode($this->value) : $this->value;
        $items = array();
        // Iterate through all list items
        foreach (array_filter((array) $data) as $item) {
            // Lets build a view for each Item's properties
            $drag_btn = $this->form ? \Ardent\Html::get(array('type' => 'span', 'class' => 'handle dashicons dashicons-menu')) : null;
            $edit_btn = $this->form ? \Ardent\Html::get(array('type' => 'span', 'class' => 'dashicons dashicons-edit')) : null;
            $delete_btn = $this->form ? \Ardent\Html::get(array('type' => 'span', 'class' => 'dashicons dashicons-trash')) : null;
            $parts = null;
            $item_div = new \Ardent\Html\Div(array('class' => 'xlist-item'));
            foreach (get_object_vars((object) $item) as $k => $v) {
                $label = null;
                $show = true;
                $display = 'text';
                $name = null;
                foreach ((array) array_filter((array) $this->form) as $tab_title => $fields) {
                    foreach ($fields as $element) {

                        if (isset($element['name']) && $element['name'] == $k && isset($element['type'])) {
                            if (isset($element['display'])) {
                                $display = $element['display'];
                                unset($element['display']);
                            }
                            if ($element['type'] == 'input.wp.editor') {
                                $v = apply_filters('the_content', $v);
                            }
                            if (isset($element['label']) && $element['label']) {
                                $label = $element['label'];
                            }
                            if (is_bool($v)) {
                                $v = (int) $v;
                            }
                        }
                    }
                }
                $classes = implode(' ', explode('_', $k));
                if (!$show) {
                    $classes .= ' hidden';
                }
                $label_div = new \Ardent\Html\Div(array('class' => 'xlist-label', 'text' => $label));
                $display_div = new \Ardent\Html\Div(array('class' => 'xlist-value', 'text' => $v));
                $property_div = new \Ardent\Html\Div(array(
                    'class' => 'xlist-property',
                    'text' => "{$label_div}{$display_div}",
                    'data-display' => $display,
                    'data-xlist-value' => $v,
                    'data-name' => $k
                ));
                $item_div->text .= $property_div;
            }
            $items[] = "{$drag_btn}{$edit_btn}{$delete_btn}{$item_div}";
        }
        $v = \Ardent\Html::get(array('type' => 'input.hidden', 'name' => $this->name, 'value' => $this->value));
        if ($this->{'data-xlist-display'}) {
            $v->{'data-xlist-display'} = $this->{'data-xlist-display'};
        }
        $list = \Ardent\Html::get(array('type' => 'ul', 'items' => $items));
        $label = ($this->label) ? \Ardent\Html::get(array('type' => 'label', 'for' => $this->id, 'text' => $this->label)) : '';
        $this->text = "{$label}{$form}{$add_button}{$list}{$v}";

        return parent::__toString();
    }

}

// %% incomplete %% 
