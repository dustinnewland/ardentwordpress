<?php // %% incomplete %%           ?>
<?php

namespace Ardent\Html\Input\Range;

class Time extends \Ardent\Html\Input\Range {

    public function __construct($config = array()) {
        $this->min = 0;
        $this->max = 1425;
        $this->step = 15;
        parent::__construct($config);
    }

    public function __toString() {
	if ($this->default && !$this->value) {
		$this->value = $this->default;
	}
        $min = $this->value % 60;
        $hrs = floor($this->value / 60);
        $suf = $hrs > 11 && $hrs != 24 ? "P.M." : "A.M.";

        $min = $min === 0 ? '00' : $min; // correct formatting of 0 minutes
        $hrs = $hrs > 12 ? $hrs - 12 : $hrs; // convert military time to normal
        $hrs = $hrs === 0 ? 12 : $hrs; // correct display of midnight
        $this->display_value = "{$hrs}:{$min} {$suf}";
        return parent::__toString();
    }

}
