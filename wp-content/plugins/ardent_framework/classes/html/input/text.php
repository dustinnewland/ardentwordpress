<?php // %% incomplete %% ?>
<?php

namespace Ardent\Html\Input;

class Text extends \Ardent\Html\Input {

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->type = 'text';
        $this->self_closing = true;
    }

    public function __toString() {

        if ($this->value === null && $this->default_value !== null) {
            $this->value = $this->default_value;
        }

        return parent::__toString();
    }

}
