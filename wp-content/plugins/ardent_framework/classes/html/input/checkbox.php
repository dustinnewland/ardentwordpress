<?php

namespace Ardent\Html\Input;

class Checkbox extends \Ardent\Html\Input {

    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'label_location' => 'after',
        ));
        parent::__construct($config);
        $this->self_closing = true;
        $this->type = 'checkbox';
    }

    public function __toString() {
        $v = $this->value;
        $c = $this->checked;
        if ($this->value || ($this->default_value && $this->value === null)) {
            $this->checked = 'checked';
        }
        if ($this->default_value) {
            $this->{'data-default'} = $this->default_value;
        }
        $this->value = '1';
        $return = parent::__toString();
        $this->value = $v;
        $this->checked = $c;
        return $return;
    }

}

// %% incomplete %%
