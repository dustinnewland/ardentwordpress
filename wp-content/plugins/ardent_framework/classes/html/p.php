<?php

namespace Ardent\Html;

class P extends Tag
{

	public function __construct($config = array())
	{
		$config['tag'] = 'p';
		parent::__construct($config);
	}

}
