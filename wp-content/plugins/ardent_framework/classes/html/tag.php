<?php

/**
 * @file classes/html/tag.php Base class for all html generator
 * classes
 */

namespace Ardent\Html;

/**
 * Tag Class is the basis for all html generators
 *
 * This class contains basic functionality.
 */
class Tag {

	protected $_properties = array(
		'tag' => null,
		'self_closing' => false,
		'text' => null
	);
	protected $_attributes = array(
		'class' => array()
	);
	protected $_innertext = null;

	/**
	 * This is a single paragraph.
	 *
	 * This is another paragraph. Class Constructor is the constructor for this class
	 * I'm not sure how multiple line thingies work
	 * @param array $config
	 */
	public function __construct($config = array()) {
		foreach ($config as $key => $value) {
			$this->{$key} = $value;
		}
		if ($this->tag === null) {
			$pieces = explode('\\', get_class($this));
			$this->tag = strtolower($pieces[2]);
		}
		$this->class = preg_replace('/[^a-z]+/', '-', strtolower(get_class($this)));
	}

	public function __get($name) {
		if (isset($this->_properties[$name])) {
			return $this->_properties[$name];
		} elseif (isset($this->_attributes[$name])) {
			return $this->_attributes[$name];
		} else {
			return null;
		}
	}

	public function __set($name, $value) {
		if (array_key_exists($name, $this->_properties)) {
			$oldvalue = $this->_properties[$name];
			$this->_properties[$name] = $value;
		} else {
			$oldvalue = array_key_exists($name, $this->_attributes) ? $this->_attributes[$name] : null;
			if (is_null($value)) {
				unset($this->_attributes[$name]);
			} else {
				if (is_array($oldvalue)) {
					$this->_attributes[$name] = array_merge($this->_attributes[$name], (array) $value);
				} else {
					$this->_attributes[$name] = $value;
				}
			}
		}
		return $oldvalue;
	}

	public function __toString() {
		if (isset($this->_attributes['disabled']) && !$this->disabled) {
			unset($this->_attributes['disabled']);
		}

		$attributes = $this->attributes();

		$string = "<{$this->tag}{$attributes}";
		if ($this->self_closing) {
			$string .= '/>';
		} else {
			$text = $this->text;
			if (is_object($text) && get_class($text) === 'Closure') {
				$text = $text();
			}
			$string .= ">{$text}</{$this->tag}>";
		}
		return $this->tag ? $string : $text;
	}

	private function attributes() {
		$return = array();
		foreach ($this->_attributes as $prop => $value) {
			if (is_array($value)) {
				$is_strings = true;
				foreach ($value as $t) {
					$is_strings = $is_strings && is_string($t);
				}
				if ($is_strings) {
					$v = implode(' ', $value);
				} else {
					$v = json_encode($value);
				}
			} else {
				$v = $value;
			}
			if ($v === false) {
				$return[] = $prop;
			} else {
				$v = htmlentities($v, ENT_QUOTES, 'UTF-8');
				$return[] = "{$prop}='{$v}'";
			}
		}
		return count($return) ? ' ' . implode(' ', $return) : '';
	}

}
