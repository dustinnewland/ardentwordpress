<?php // %% incomplete %%  ?>
<?php

namespace Ardent\Html;

class Textarea extends Input {

    public function __construct($config = array()) {
        $this->class = 'ardent-html-input';
        parent::__construct($config);
        $this->tag = 'textarea';
    }

    public function __toString() {
        $t = $this->text;
        $this->text = $this->value;
        $this->value = null;
        $return = parent::__toString();
        $this->value = $this->text;
        $this->text = $t;
        return $return;
    }

}
