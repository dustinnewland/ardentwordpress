<?php

/**
 * @file classes/html/button.php
 *
 * @brief This is the html generator for the button html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Button($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'button'));
 * @endcode
 */

namespace Ardent\Html;

class Button extends Tag {

    public function __construct($config = array()) {
        parent::__construct($config);
        if ($this->text)
            $this->class = 'ardent-html-button-' . preg_replace('/[^a-z]+/', '-', strtolower($this->text));
    }

}

// %% incomplete %%
