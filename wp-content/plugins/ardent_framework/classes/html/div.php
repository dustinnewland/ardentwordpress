<?php // %% incomplete %% ?>
<?php

/**
 * @file classes/html/div.php
 *
 * @brief This is the html generator for the div html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Div($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'div'));
 * @endcode
 */
namespace Ardent\Html;

class Div extends Tag
{

}
