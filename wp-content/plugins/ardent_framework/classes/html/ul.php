<?php

namespace Ardent\Html;

class Ul extends Tag {

    public function __construct($config = array()) {
        $config['tag'] = 'ul';
        $this->_properties = array_merge($this->_properties, array(
            'items' => array()
        ));
        parent::__construct($config);
    }

    public function __toString() {
        if (count($this->items) && !$this->text) {
            $this->text = '<li>' . implode('</li><li>', $this->items) . '</li>';
        }
        return parent::__toString();
    }

}