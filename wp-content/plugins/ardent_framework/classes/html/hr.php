<?php // %% incomplete %% ?>
<?php

/**
 * @file classes/html/hr.php
 *
 * @brief This is the html generator for the hr html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Hr($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'hr'));
 * @endcode
 */

namespace Ardent\Html;

class Hr extends Tag
{

	public function __construct($config = array())
	{
		parent::__construct($config);
		$this->_attributes = array();
		$this->_properties = array('tag' => 'hr', 'self_closing' => true);
	}

}