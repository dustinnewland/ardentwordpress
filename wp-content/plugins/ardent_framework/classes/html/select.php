<?php // %% incomplete %%                         ?>
<?php

namespace Ardent\Html;

class Select extends Input {

	private $_options = array();

	public function __construct($config = array()) {
		$this->_properties['value'] = null;
		$this->_properties['options'] = array();
		$this->_properties['empty'] = null;
		$this->class = 'ardent-html-input';
		parent::__construct($config);
	}

	public function __toString() {
		$this->text = '';
		$default_option = array(
			'type' => 'option',
			'value' => '0',
			'text' => null,
		);
		if ($this->empty) {
			$o = array_merge($default_option, array('text' => $this->empty, 'value' => ''));
			$this->text .= \Ardent\Html::get($o);
		}
		if (is_callable($this->options)) {
			$this->options = call_user_func($this->options);
		} elseif (is_string($this->options) && substr($this->options, 0, 7) == 'ardent/') {
			$parts = explode('/', $this->options);
			if (count($parts) > 1) {
				$func = '\Ardent\Options::' . ucfirst(strtolower($parts[1]));
				$params = array_slice($parts, 2);
				$this->options = call_user_func($func, $params);
			}
		}

		foreach ($this->options as $opt_val => $opt_lbl) {
			if (is_array($opt_lbl)) {
				$title = ucwords($opt_val);
				$this->text .= "<optgroup label='{$title}'>";
				foreach ($opt_lbl as $v => $l) {
					$option = array(
						'text' => $l,
						'value' => $v
					);
					if (in_array($v, (array) $this->value)) {
						$option['selected'] = 'selected';
					}
					$this->text .= \Ardent\Html::get(array_merge($default_option, $option));
				}
				$this->text .= "</optgroup>";
			} else {
				if (is_object($opt_lbl)) {
					$opt_val = $opt_lbl->value;
					$opt_lbl = $opt_lbl->title;
				}
				$option = array(
					'text' => $opt_lbl,
					'value' => $opt_val
				);
				if (in_array($opt_val, (array) $this->value)) {
					$option['selected'] = 'selected';
				}
				$this->text .= \Ardent\Html::get(array_merge($default_option, $option));
			}
		}
		$name = $this->name;
		if ($this->multiple) {
			$this->name .= '[]';
		}
		$ret = parent::__toString();
		$this->name = $name;
		return $ret;
	}

}
