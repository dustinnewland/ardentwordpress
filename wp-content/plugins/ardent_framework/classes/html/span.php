<?php

/**
 * @file classes/html/span.php
 *
 * @brief This is the html generator for the span html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Span($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'span'));
 * @endcode
 */
namespace Ardent\Html;

class Span extends Tag
{

}
