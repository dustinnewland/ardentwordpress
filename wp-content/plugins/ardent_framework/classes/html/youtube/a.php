<?php

/**
 * @file classes/html/youtube/a.php
 * This file contains the youtube a tag generator class.
 * The youtube a tag generator is used to generate an HTML snippet of a link to the youtube video when given a youtube ID.
 */

namespace Ardent\Html\Youtube;     // Used to group classes as Youtube.

/**
 * Youtube A class
 * 
 * This class returns html for a youtube link.
 * 
 * The preferred way of accessing the youtube a class is like this:
 * @code
 * $link = \Ardent\Html::get(array(
 *     'type' => 'youtube.a',
 *     'itemid' => 'youtubevideoid',
 * ));
 * @endcode
 * 
 * Another way of accessing the youtube a class is like this:
 * @code
 * $link = \Ardent\Html\Youtube\A(array('itemid' => 'youtubevideoid'));
 * @endcode
 * 
 * Here is an example of how to display the HTML:
 * @code
 * echo $link;
 * @endcode
 */

class A extends \Ardent\Html\A {

    /**
     * This is the general construct of the Youtube A class.
     * 
     * Upon invoking the class, you have the ablility to create a youtube link that returns html for an a link.
     * 
     * The preferred way of accessing the youtube a class is like this:
     * @code
     * $link = \Ardent\Html::get(array(
     *     'type' => 'youtube.a',
     *     'itemid' => 'youtubevideoid',
     * ));
     * @endcode
     * 
     * Another way of accessing the youtube a class is like this:
     * @code
     * $link = \Ardent\Html\Youtube\A(array('itemid' => 'youtubevideoid'));
     * @endcode
     * 
     * Here is an example of how to display the HTML:
     * @code
     * echo $link;
     * @endcode
     * 
     * @param array $config
     *        string ['itemid'] the video id of the video that you would like to link to.
     * @return object
     */
    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'itemid' => null,
        ));
        parent::__construct($config);
    }

    /** @internal
     * 
     * This function is used to generate the string that is created when running echo $link.
     * 
     * This function should not be used outsite of this class. It is what is considered a magic method and has no need of being called direcly unless this class is extended by another.
     * 
     * @endinternal
     */
    public function __toString() {
        if ($this->itemid) {
            $this->href = "http://www.youtube.com/watch?v={$this->itemid}";
        }
        return parent::__toString();
    }

}

// %% complete %% ?>