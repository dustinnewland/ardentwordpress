<?php

/**
 * @file classes/html/youtube/iframe.php
 * This file contains the youtube iframe generator class.
 * The youtube iframe generator is used to generate an HTML snippet of a youtube video iframe when given a youtube ID.
 */

namespace Ardent\Html\Youtube;     // Used to group classes as Youtube.

/**
 * Youtube IFrame class
 * 
 * This class returns the iframe html for a youtube video.
 * 
 * The preferred way of accessing the youtube iframe class is like this:
 * @code
 * $iframe = \Ardent\Html::get(array(
 *     'type' => 'youtube.iframe',
 *     'itemid' => 'youtubevideoid',
 * ));
 * @endcode
 * 
 * Another way of accessing the youtube iframe class is like this:
 * @code
 * $iframe = \Ardent\Html\Youtube\Iframe(array('itemid' => 'youtubevideoid'));
 * @endcode
 * 
 * Here is an example of how to display the HTML:
 * @code
 * echo $iframe;
 * @endcode
 */
class Iframe extends \Ardent\Html\Iframe {

    /**
     * This is the general construct of the Youtube Iframe class.
     * 
     * Upon invoking the class, you have the ablility to create a youtube video iframe that pulls all embed information from youtube in the iframe.
     * 
     * The preferred way of accessing the youtube iframe class is like this:
     * @code
     * $iframe = \Ardent\Html::get(array(
     *     'type' => 'youtube.iframe',
     *     'itemid' => 'youtubevideoid',
     * ));
     * @endcode
     * 
     * Another way of accessing the youtube iframe class is like this:
     * @code
     * $iframe = \Ardent\Html\Youtube\Iframe(array('itemid' => 'youtubevideoid'));
     * @endcode
     * 
     * Here is an example of how to display the HTML:
     * @code
     * echo $iframe;
     * @endcode
     * 
     * @param array $config
     *        string ['itemid'] the video id of the video that you would like to be displayed.
     * @return object
     */
    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'itemid' => null,
        ));
        $this->frameborder = 0;
        parent::__construct($config);
    }

    /** @internal
     * 
     * This function is used to generate the string that is created when running echo $iframe.
     * 
     * This function should not be used outsite of this class. It is what is considered a magic method and has no need of being called direcly unless this class is extended by another.
     * 
     * @endinternal
     */
    public function __toString() {
        if ($this->itemid) {
            if (!$this->src) {
                $this->src = "http://www.youtube.com/embed/{$this->itemid}?wmode=transparent&version=3&enablejsapi=1";
            }
        }
        return parent::__toString();
    }

}

// %% complete %% ?>