<?php

/**
 * @file classes/html/youtube/img.php
 * This file contains the youtube img tag generator class.
 * The youtube img tag generator is used to generate an HTML snippet of a preview image of the youtube video when given a youtube ID.
 */

namespace Ardent\Html\Youtube;     // Used to group classes as Youtube.

/**
 * Youtube Img class
 * 
 * This class returns html for a youtube video preview image.
 * 
 * The preferred way of accessing the youtube img class is like this:
 * @code
 * $img = \Ardent\Html::get(array(
 *     'type' => 'youtube.img',
 *     'itemid' => 'youtubevideoid',
 * ));
 * @endcode
 * 
 * Another way of accessing the youtube img class is like this:
 * @code
 * $img = \Ardent\Html\Youtube\Img(array('itemid' => 'youtubevideoid'));
 * @endcode
 * 
 * Here is an example of how to display the HTML:
 * @code
 * echo $img;
 * @endcode
 */

class Img extends \Ardent\Html\Img {

    /**
     * This is the general construct of the Youtube A class.
     * 
     * Upon invoking the class, you have the ablility to create a youtube link that returns html for an a link.
     * 
     * The preferred way of accessing the youtube img class is like this:
     * @code
     * $img = \Ardent\Html::get(array(
     *     'type' => 'youtube.img',
     *     'itemid' => 'youtubevideoid',
     * ));
     * @endcode
     * 
     * Another way of accessing the youtube img class is like this:
     * @code
     * $img = \Ardent\Html\Youtube\Img(array('itemid' => 'youtubevideoid'));
     * @endcode
     * 
     * Here is an example of how to display the HTML:
     * @code
     * echo $img;
     * @endcode
     * 
     * @param array $config
     *        string ['itemid'] the video id of the video that you would like to have an image of.
     * @return object
     */
    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'itemid' => null,
        ));
        parent::__construct($config);
    }

    /** @internal
     * 
     * This function is used to generate the string that is created when running echo $img.
     * 
     * This function should not be used outsite of this class. It is what is considered a magic method and has no need of being called direcly unless this class is extended by another.
     * 
     * @endinternal
     */
    public function __toString() {
        if ($this->itemid) {
            if (!$this->src) {
                $this->src = "http://img.youtube.com/vi/{$this->itemid}/hqdefault.jpg";
            }
        }
        return parent::__toString();
    }

}

// %% complete %% ?>