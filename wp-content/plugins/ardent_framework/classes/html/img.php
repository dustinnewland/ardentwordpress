<?php // %% incomplete %% ?>
<?php

/**
 * @file classes/html/img.php
 *
 * @brief This is the html generator for the img html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Img($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'img'));
 * @endcode
 */

namespace Ardent\Html;

class Img extends Tag
{

  public function __construct($config = array())
  {
    $config['tag'] = 'img';
    $config['self_closing'] = true;
    parent::__construct($config);
  }

}
