<?php

/**
 * @file classes/html/wp/image.php
 * This file contains the WordPress image size html generator.
 * The WordPress image size html generator is used to generate an HTML snippet for use with the specified wordpress image sizes.
 */

namespace Ardent\Html\Wp; // Used to group classes as Wp.

/**
 * Wp Image class
 * 
 * This class returns html for a resized wordpress image.
 * 
 * The preferred way of accessing the wp image class is like this:
 * @code
 * $wpimage = \Ardent\Html::get(array(
 *     'type' => 'wp.image',
 *     'size' => 'large',
 *     'itemid' => 1,
 *     'autosize' => false
 * ));
 * @endcode
 * 
 * Another way of accessing the wp image class is like this:
 * @code
 * $wpimage = \Ardent\Html\Youtube\A(array(
 *     'size' => 'large',
 *     'itemid' => 1,
 *     'autosize' => false
 * ));
 * @endcode
 * 
 * Here is an example of how to display the HTML:
 * @code
 * echo $wpimage;
 * @endcode
 */

class Image extends \Ardent\Html\Img {

    /**
     * Wp Image class
     * 
     * This class returns html for a resized wordpress image.
     * 
     * The preferred way of accessing the wp image class is like this:
     * @code
     * $wpimage = \Ardent\Html::get(array(
     *     'type' => 'wp.image',
     *     'size' => 'large',
     *     'itemid' => 1,
     *     'autosize' => false
     * ));
     * @endcode
     * 
     * Another way of accessing the wp image class is like this:
     * @code
     * $wpimage = \Ardent\Html\Youtube\A(array(
     *     'size' => 'large',
     *     'itemid' => 1,
     *     'autosize' => false
     * ));
     * @endcode
     * 
     * Here is an example of how to display the HTML:
     * @code
     * echo $wpimage;
     * @endcode
     * 
     * @param array $config
     *        string ['size'] the size you would like to make the image as determinted in the WordPress image sizes.
     *        int ['itemid'] the id of the image you would like to display.
     *        boolean ['autosize'] if true, sets the width/height attributes of the img tag, if false does nothing.
     * @return object
     */
    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'size' => null,
            'itemid' => null,
            'autosize' => false
        ));
        parent::__construct($config);
    }

    /** @internal
     * 
     * This function is used to generate the string that is created when running echo $wpimage.
     * 
     * This function should not be used outsite of this class. It is what is considered a magic method and has no need of being called direcly unless this class is extended by another.
     * 
     * @endinternal
     */
    public function __toString() {
        if ($this->itemid) {
            if ((int) $this->itemid) {
                $this->{'data-itemid'} = (int) $this->itemid;
                $post = get_post($this->itemid);
                if (!$this->alt) {
                    $this->alt = get_post_meta($this->itemid, '_wp_attachment_image_alt', true) ? : @$post->post_title;
                }
                $this->title = !is_null($this->title) ? $this->title : (get_post_meta($this->itemid, '_wp_attachment_image_alt', true) ? : @$post->post_title);
                $image = wp_get_attachment_image_src($this->itemid, $this->size, true);
                if (is_array($image)) {
                    $this->src = $image[0];
                    if ($this->autosize) {
                        $this->width = $image[1];
                        $this->height = $image[2];
                    }
                }
            } else {
                return \Ardent\Html::get(array('type' => 'span', 'text' => 'Image not found'));
            }
        }
        return parent::__toString();
    }

}

// %% complete %% 
