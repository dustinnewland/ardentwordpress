<?php

namespace Ardent\Html;

class Form extends Tag {

    public function __construct($config = array()) {
        $config = array_merge(array(
            'action' => '',
            'method' => 'POST',
            'tag' => 'form',
            'self_closing' => false
                ), $config);
        parent::__construct($config);
    }

//    public function __toString() {
//        $this->text = null;
//        return parent::__toString();
//    }
}

// %% incomplete %% 
