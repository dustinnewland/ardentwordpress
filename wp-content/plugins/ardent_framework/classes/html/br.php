<?php // %% incomplete %% ?>
<?php

/**
 * @file classes/html/br.php
 *
 * @brief This is the html generator for the br html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Br($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'br'));
 * @endcode
 */

namespace Ardent\Html;

/**
 * Br Tag
 * @ingroup html_generator
 */
class Br extends Tag {

    public function __construct($config = array()) {
        parent::__construct($config);
        $this->_attributes = array();
        $this->_properties = array('tag' => 'br', 'self_closing' => true);
    }

}