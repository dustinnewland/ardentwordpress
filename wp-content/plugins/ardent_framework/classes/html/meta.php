<?php

/**
 * @file classes/html/meta.php
 *
 * @brief This is the html generator for the meta html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Meta($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'meta'));
 * @endcode
 */

namespace Ardent\Html;

class Meta extends Tag
{

	public function __construct($config = array())
	{
		parent::__construct($config);
		$this->_properties = array('tag' => 'meta', 'self_closing' => true);
	}

}
