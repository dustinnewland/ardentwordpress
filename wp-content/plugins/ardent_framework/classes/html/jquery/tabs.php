<?php

/**
 * @file acframework/classes/html/jquery/tabs.php
 * This file contains the jqueryui tabs widget html generator.
 * The jqueryui tabs widget html generator is used to generate an HTML snippet for use with the jqueryui tabs widget javascript.
 */

namespace Ardent\Html\Jquery; // Used to group classes as Jquery

/**
 * Jquery Tabs class
 * 
 * This class returns the tabs widget html for the jqueryui tabs widget javascript.
 * 
 * The preferred way of accessing the jquery tabs class is like this:
 * @code
 * $tabs = \ACF\Html::get(array(
 *     'type' => 'jquery.tabs',
 *     'tabs' => array(
 *          'tab1' => 'content1',
 *          'tab2' => 'content2',
 *          'tab3' => 'content3'
 *     ),
 *     'itemid' => uniqid(),
 *     'before' => null,
 *     'after' => null
 * ));
 * @endcode
 * 
 * Another way of accessing the jquery tabs class is like this:
 * @code
 * $tabs = \ACF\Html\Jquery\Tabs(array(
 *     'tabs' => array(
 *         'tab1' => 'content1',
 *         'tab2' => 'content2',
 *         'tab3' => 'content3'
 *     ),
 *     'itemid' => uniqid(),
 *     'before' => null,
 *     'after' => null
 * ));
 * @endcode
 * 
 * Here is an example of how to display the HTML:
 * @code
 * echo $tabs;
 * @endcode
 */

class Tabs extends \Ardent\Html\Tag {

    /**
     * This is the general construct of the Jquery Tabs class.
     * 
     * Upon invoking the class, you have the ability to create HTML that is geared directly toward the jqueryui tabs widget javascript.
     * 
     * The preferred way of accessing the jquery tabs class is like this:
     * @code
     * $tabs = \ACF\Html::get(array(
     *     'type' => 'jquery.tabs',
     *     'tabs' => array(
     *          'tab1' => 'content1',
     *          'tab2' => 'content2',
     *          'tab3' => 'content3'
     *     ),
     *     'itemid' => uniqid(),
     *     'before' => null,
     *     'after' => null
     * ));
     * @endcode
     * 
     * Another way of accessing the jquery tabs class is like this:
     * @code
     * $tabs = \ACF\Html\Jquery\Tabs(array(
     *     'tabs' => array(
     *         'tab1' => 'content1',
     *         'tab2' => 'content2',
     *         'tab3' => 'content3'
     *     ),
     *     'itemid' => uniqid(),
     *     'before' => null,
     *     'after' => null
     * ));
     * @endcode
     * 
     * Here is an example of how to display the HTML:
     * @code
     * echo $tabs;
     * @endcode
     * 
     * @param array $config
     *        array ['tabs'] an associative array of data with the key being the tab text and the value being the content you would like to show up in the tab.
     *        string ['itemid'] the id you would like to append to the tabs to make them uniquely accessable. The default value will do for the majority of cases.
     *        string ['before'] html that you would like to show up before the tab widget.
     *        string ['after'] html that you would like to show up after the tab widget.
     * @return object
     */
    public function __construct($config = array()) {
        $config['tag'] = 'div';
        $config['class'] = 'ardent-tab-widget';
        $this->_properties = array_merge($this->_properties, array(
            'tabs' => null,
            'itemid' => uniqid(),
            'before' => null,
            'after' => null,
        ));
        $this->_properties['tabs'] = null;
        $this->_properties['itemid'] = uniqid();
        parent::__construct($config);
        if (!$this->id) {
            $this->id = 'atw_' . md5(json_encode($this->_properties));
        }
    }

    /** @internal
     * 
     * This function is used to generate the string that is created when running echo $tabs.
     * 
     * This function should not be used outsite of this class. It is what is considered a magic method and has no need of being called direcly unless this class is extended by another.
     * 
     * @endinternal
     */
    public function __toString() {
        $tabs = '<ul>';
        $content = '';
        $clearfix = "<div style='clear:both;'></div>";
        foreach ($this->tabs as $tab_title => $tab_content) {
            $id = sanitize_title_with_dashes($tab_title) . $this->itemid;
            $tabs .= "<li><a href='#{$id}'>{$tab_title}</a></li>";
            $content .= "<div id='{$id}'>{$tab_content}</div>";
        }
        $tabs .= '</ul>';
        $before = $this->before ? : '';
        $after = $this->after ? : '';
        $this->text = "{$before}<div class='acf-wrapper'>{$tabs}{$content}{$clearfix}</div>{$clearfix}{$after}";
        return parent::__toString();
    }

}

// %% complete %% 
?>
