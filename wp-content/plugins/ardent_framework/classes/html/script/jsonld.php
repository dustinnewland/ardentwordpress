<?php

/**
 * @file classes/html/script/jsonld.php
 *
 * @brief This is the html generator for the script html tag
 *
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Script\Jsonld($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'script.jsonld'));
 * @endcode
 */

namespace Ardent\Html\Script;

class Jsonld extends \Ardent\Html\Tag {

	public function __construct($config = array()) {
		$config['tag'] = 'script';
		$config['self_closing'] = false;
		$this->_attributes['type'] = 'application/ld+json';
		parent::__construct($config);
	}

	public function __toString() {
		$v = $this->value;
		$this->value = null;
		$this->text = json_encode($v);
		$ret = parent::__toString();
		$this->value = $v;
		return $ret;
	}

}
