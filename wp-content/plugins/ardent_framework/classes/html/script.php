<?php

/**
 * @file classes/html/script.php
 *
 * @brief This is the html generator for the script html tag
 *
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Script($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'script'));
 * @endcode
 */

namespace Ardent\Html;

class Script extends Tag {

	public function __construct($config = array()) {
		$config['tag'] = 'script';
		$config['self_closing'] = false;
		$this->_attributes['type'] = 'text/javascript';
		parent::__construct($config);
	}

}
