<?php

/**
 * @file classes/html/iframe.php
 *
 * @brief This is the html generator for the iframe html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Iframe($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'iframe'));
 * @endcode
 */

namespace Ardent\Html;

class Iframe extends Tag
{

  public function __construct($config = array())
  {
    $config['tag'] = 'iframe';
    $config['self_closing'] = false;
    parent::__construct($config);
  }

  public function __toString()
  {
    $this->text = null;
    return parent::__toString();
  }

}
// %% incomplete %% ?>