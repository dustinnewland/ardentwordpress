<?php

/**
 * @file classes/html/a.php
 *
 * @brief This is the html generator for the anchor html tag
 * 
 * This class can be instantiated in two ways:
 * use this like `new \Ardent\Html\A($config);`
 * @code
 * $anchor = new \Ardent\Html\A($config); // OR
 * $anchor = \Ardent\Html::get(array('type' => 'a'));
 * @endcode
 * and can contain *whatever* is necessary.
 */

namespace Ardent\Html;

/**
 * @brief A Tag
 * 
 * This class can contain the following parameters
 * - All standard html tag params
 * - (bool) new_window: Determines whether link will have target='_blank'
 * attached to it. This may also be accomplished by passing target => '_blank'
 * with the other config options.
 * 
 * @code
 * $anchor = \Ardent\Html::get(array(
 *     'type' => 'a',
 *     'href' => 'http://slashdot.org/recent/',
 *     'new_window' => true
 * ));
 * @endcode
 * @ingroup html_generator
 */
class A extends Tag {

    public function __construct($config = array()) {
        $config['tag'] = 'a';
        $this->_properties = array_merge($this->_properties, array(
            'new_window' => false
        ));
        parent::__construct($config);
    }

    public function __toString() {
        if (!$this->text) {
            $this->text = $this->href;
        }
        if ($this->new_window) {
            $this->target = '_blank';
        }
        return parent::__toString();
    }

}

// %% incomplete %% 
