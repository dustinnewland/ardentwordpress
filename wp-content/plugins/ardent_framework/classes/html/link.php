<?php // %% incomplete %% ?>
<?php

/**
 * @file classes/html/link.php
 *
 * @brief This is the html generator for the link html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Link($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'link'));
 * @endcode
 */

namespace Ardent\Html;

class Link extends Tag
{

  public function __construct($config = array())
  {
    $config['tag'] = 'link';
    $config['self_closing'] = true;
    parent::__construct($config);
  }

}
