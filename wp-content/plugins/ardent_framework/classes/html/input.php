<?php // %% incomplete %%       ?>
<?php

/**
 * @file classes/html/input.php
 *
 * @brief This is the html generator for the input html tag
 * 
 * This class can be instantiated in two ways:
 * @code
 * $tag = new \Ardent\Html\Input($config); // OR
 * $tag = \Ardent\Html::get(array('type' => 'input'));
 * @endcode
 */

namespace Ardent\Html;

class Input extends Tag {

    public function __construct($config = array()) {
        $this->_properties = array_merge($this->_properties, array(
            'label' => null,
            'description' => null,
            'default_value' => null,
            'validate' => null,
            'conditions' => null,
            'after' => '',
        ));
        $this->class = 'ardent-html-input';
        parent::__construct($config);
    }

    public function __toString() {
        $return = '';
        $label = '';
        if ($this->label) {
            if (!$this->id) {
                $this->id = uniqid(preg_replace('/[^a-z0-9\-]/', '', strtolower($this->name)));
            }
            $text = $this->label;
            if ($this->description) {
                $text = "{$this->label}<br><div class='label-description'>{$this->description}</div>";
            }
            $label = array(
                'type' => 'label',
                'text' => $text,
                'for' => $this->id
            );
            $label = \Ardent\Html::get($label);
        }
        if (is_object($this->value) && get_class($this->value) === 'Closure') {
            $func = $this->value;
            $this->value = $func($this);
        }
        if ($this->value === null && $this->default_value) {
            $this->value = $this->default_value;
        }
        if ($this->conditions) {
            $this->{'data-ardent-conditions'} = json_encode($this->conditions);
        }
        $before = $this->before ? : '';
        $after = $this->after ? : '';
        $input = parent::__toString();
        switch ($this->label_location) {
            case 'after':
                return $before . $input . $after . $label;
                break;
            default:
                return $label . $before . $input . $after;
                break;
        }
    }

}
