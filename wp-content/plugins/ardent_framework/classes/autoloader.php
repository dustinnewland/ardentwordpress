<?php

namespace Ardent;

class Autoloader {

    static function addPath($path) {
        global $ardent_autoloader_paths;
        $ardent_autoloader_paths[] = $path;
    }

}
