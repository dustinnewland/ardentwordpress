<?php

namespace Ardent;

/**
 * Html class contains a single static function designed to generate
 * html based on the configuration.
 */
abstract class Xlist {

    static function Data($string) {
        return (array) array_filter((array) json_decode((string) $string));
    }

}
