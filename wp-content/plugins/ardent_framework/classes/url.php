<?php

namespace Ardent;

/**
 * Html class contains a single static function designed to generate
 * html based on the configuration.
 */
abstract class Url {

    static function Current() {
        $proto = 'http' . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 's' : '') . '://';
        return $proto . @$_SERVER["HTTP_HOST"] . @$_SERVER["REQUEST_URI"];
    }

    static function FromFile($file) {
        return site_url(substr($file, strlen(ABSPATH)));
    }

    static function Parse($url) {
        if (strpos($url, 'http') !== 0) {
            $url = "http://{$url}";
        }
        $parts = parse_url($url);
        if (!isset($parts['path'])) {
            $parts['path'] = '/';
        }
        return $parts;
    }

    static function unParse($parts) {
        $scheme = isset($parts['scheme']) ? $parts['scheme'] . '://' : '';
        $host = isset($parts['host']) ? $parts['host'] : '';
        $port = isset($parts['port']) ? ':' . $parts['port'] : '';
        $user = isset($parts['user']) ? $parts['user'] : '';
        $pass = isset($parts['pass']) ? ':' . $parts['pass'] : '';
        $pass = ($user || $pass) ? "$pass@" : '';
        $path = isset($parts['path']) ? $parts['path'] : '';
        $query = isset($parts['query']) ? '?' . $parts['query'] : '';
        $fragment = isset($parts['fragment']) ? '#' . $parts['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }

}
