<?php

namespace Ardent;

/**
 * Html class contains a single static function designed to generate
 * html based on the configuration.
 */
abstract class Template {

    static function Current() {
        if (
                !isset($_REQUEST['action']) ||
                !(isset($_REQUEST['post']) || isset($_REQUEST['post_ID'])) ||
                !in_array($_REQUEST['action'], array('edit', 'editpost'))
        ) {
            return null;
        }
        $post_id = isset($_REQUEST['post']) ? $_REQUEST['post'] : $_REQUEST['post_ID'];
	$template_slug = get_page_template_slug($post_id);
        if (empty($template_slug) && get_option('show_on_front') == 'page' && (int) get_option('page_on_front') == $post_id) {
            return 'front-page';
        } else {
            return $template_slug;
        }
    }

}
