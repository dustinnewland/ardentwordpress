<?php

namespace Ardent;

class Pager extends \Ardent\Object {

	protected $_config = [
		'prev' => true,
		'next' => true,
		'page' => 1,
		'pages' => null,
		'format' => ""
	];

	private function _parse_link($link, $page) {
		return str_replace('{{page}}', $page, $link);
	}

	public function getLinks() {
		if ($pages <= 1) { return []; }
		$links = [
			(string) $this->_get_prev_link()
		];
		for ($p = 1; $p <= $this->pages; $p++) {
			$links[] = $this->_get_link($p, $p);
		}
		$links[] = $this->_get_next_link();
		return $links;
	}

	private function _get_prev_link() {
		if (!$this->prev) {
			return null;
		}
		return $this->_get_link($this->page - 1, 'Prev');
	}

	private function _get_next_link() {
		if (!$this->next || $this->page == $this->pages) {
			return null;
		}
		return $this->_get_link($this->page + 1, 'Next');
	}

	private function _get_link($page, $text) {

		$url = $this->_parse_link($this->format, $page);
		if ($page == $this->page) {
			$link = \Ardent\Html::get([
						'type' => 'span',
						'text' => $text ?: $url
			]);
		} else {
			$link = \Ardent\Html::get([
						'type' => 'a',
						'href' => $url,
						'text' => $text ?: $url
			]);
		}
		if ($page == $this->page) {
			$link->class = 'active';
		}
		return $link;
	}

}
