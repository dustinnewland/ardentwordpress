<?php

spl_autoload_register(function ($class) {
    global $ardent_autoloader_paths;
    if (substr($class, 0, 7) != 'Ardent\\')
        return;
    $paths = explode('\\', substr($class, 7));

    $tail = strtolower(implode('/', $paths)) . '.php';

    $include_paths = array(
        get_stylesheet_directory() . '/classes/',
        get_template_directory() . '/classes/',
        ARDENT_FRAMEWORK_CLASSPATH . '/'
    );
    if (is_array($ardent_autoloader_paths)) {
        $include_paths = array_merge($ardent_autoloader_paths, $include_paths);
    }
    foreach ($include_paths as $potential_file) {
        $real_file = $potential_file . $tail;
        if (is_file($real_file)) {
            require_once($real_file);
            return;
        }
    }
});
