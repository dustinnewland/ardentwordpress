<?php

function ardent_framework_register_js_cookie() {
	if (function_exists('get_plugin_data')) {
		$version = @get_plugin_data(ARDENT_FRAMEWORK_PATH . '/ardent_framework.php')['Version'] ? : false;
	} else {
		$version = null;
	}
	wp_register_script('js-cookie', ARDENT_FRAMEWORK_URL . '/js/cookie.js', array(), $version);
}

add_action('admin_enqueue_script', 'ardent_framework_register_js_cookie', 9);
add_action('wp_enqueue_scripts', 'ardent_framework_register_js_cookie', 9);

add_action('admin_enqueue_scripts', function() {
	$version = @get_plugin_data(ARDENT_FRAMEWORK_PATH . '/ardent_framework.php')['Version'] ? : false;
	add_thickbox();
	wp_enqueue_media();
	wp_enqueue_style('wp-color-picker', false, array(), $version);
	wp_enqueue_style('jquery-ui-stylesheet', ARDENT_FRAMEWORK_URL . '/css/jquery-ui.min.css', array(), $version);
	wp_enqueue_style('ardent-framework-admin', ARDENT_FRAMEWORK_URL . '/css/admin.css', array(), $version);

	wp_enqueue_script('ardent-field-conditionals', ARDENT_FRAMEWORK_URL . '/js/ardent-field-conditionals.js', array('jquery', 'jquery-effects-core'), $version);
	wp_enqueue_script('ardent-widgets-custom-media-manager', ARDENT_FRAMEWORK_URL . '/js/ardent-widgets-custom-media-manager.js', array(
		'jquery',
		'jquery-ui-widget'
			), $version);
	wp_enqueue_script('ardent-widgets-xlist', ARDENT_FRAMEWORK_URL . '/js/ardent-widgets-xlist.js', array(
		'jquery', 'jquery-ui-widget',
		'jquery-ui-dialog', 'ardent-field-conditionals'
			), $version);
	wp_enqueue_script('ardent-admin', ARDENT_FRAMEWORK_URL . '/js/admin.js', array(
		'iris',
		'jquery',
		'jquery-ui-autocomplete', 'jquery-ui-button', 'jquery-ui-tabs',
		'ardent-widgets-custom-media-manager',
		'jquery-ui-datepicker',
		'jquery-cookie',
		'ardent-field-conditionals',
		'ardent-widgets-xlist'
			), $version);
	wp_enqueue_script('jquery-cookie', ARDENT_FRAMEWORK_URL . '/js/jquery.cookie.js', array('jquery'), $version);
});
