<?php

add_filter('http_request_host_is_external', function($a, $host, $url) {
    if ($host == 'stage.ardentcreative.com' || $host == 'dev.ardentcreative.com') {
        return true;
    }
}, 10, 3);
