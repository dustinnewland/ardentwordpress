<?php

add_filter('pre_set_site_transient_update_themes', function($transient) {
    if (!property_exists($transient, 'checked')) {
        return $transient;
    }
    $slugs = array_filter(array_keys((array) $transient->checked));
    foreach ($slugs as $theme_slug) {
        $v = $transient->checked[$theme_slug];
        $body = file_get_contents("http://dev.ardentcreative.com/updates/theme/{$theme_slug}/check/{$v}");
        if ($data = json_decode($body)) {
            $transient->response[$theme_slug] = (array) json_decode($body);
        }
    }
    return $transient;
});
