<?php

add_action('wp_ajax_ardent_postsearch', function() {
    header('Content-type: text/javascript');
    $pt = @$_REQUEST['post_type'];
    if (!post_type_exists($pt)) {
        return;
    }
    $query = new WP_Query(array(
        'post_type' => $pt,
        's' => @$_REQUEST['term']
    ));
    $options = array();
    while ($query->have_posts()) {
        $query->the_post();
        $options[] = array(
            'label' => get_the_title(),
            'value' => get_the_ID()
        );
    }
    echo json_encode($options);
    exit;
    $ids = array_map('intval', array_filter((array) explode(',', (string) $_REQUEST['ids'])));
    $sizes = get_intermediate_image_sizes();
    $return = array();
    foreach ($ids as $id) {
        $post = get_post($id) ? : get_default_post_to_edit();
        $meta = new \Ardent\Wp\Meta($post->ID);
        $post->meta = $meta->toArray();
        $obj = (object) array(
                    'id' => $post->ID,
                    'title' => $post->post_title
        );
        $img_sizes = array();
        foreach ($sizes as $size) {
            $url = wp_get_attachment_image_src($id, $size);
            $img_sizes[$size] = (object) array(
                        'url' => $url[0],
                        'width' => $url[1],
                        'height' => $url[2],
            );
            $obj->sizes = $img_sizes;
        }
        $return[$id] = $obj;
    }
    header('Content-type: text/javascript');
    echo json_encode($return);
    exit;
});
