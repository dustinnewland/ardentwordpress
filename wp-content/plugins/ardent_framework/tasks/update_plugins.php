<?php

add_filter('pre_set_site_transient_update_plugins', function($transient) {
    if (!property_exists($transient, 'checked')) {
        return $transient;
    }
    $slugs = array_filter(array_keys((array) $transient->checked));
    foreach ($slugs as $slug) {
        $v = $transient->checked[$slug];
        if (substr($slug, 0, 7) == 'ardent_') {
            $name = dirname($slug);
            $body = file_get_contents("http://dev.ardentcreative.com/updates/plugin/{$name}/check/{$v}");
            if ($data = json_decode($body)) {
                $transient->response[$slug] = $data;
            }
        }
    }
    return $transient;
});
