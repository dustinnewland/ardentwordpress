<?php

/**
 * Plugin Name: Ardent Redirects
 * Plugin URI: http://www.ardentcreative.com/
 * Description: Provides Redirects for the site.
 * Version: 0.2.5
 * Author: Dustin Newland
 * Author URI: http://www.ardentcreative.com/
 */
add_action('plugins_loaded', function() {
    $dir = dirname(__FILE__);
    if (!defined('ARDENT_FRAMEWORK')) {
        require_once("{$dir}/deps/ardent_install_plugin.php");
        ardent_install_plugin('ardent_framework', 'http://stage.ardentcreative.com/updates/plugin/ardent_framework/download');
    }

    $config = \Ardent\Wp\Config::getInstance('Settings', 'Redirects');
    $config->add_setting('Global Redirects', array('type' => 'textarea', 'name' => 'redirect_input', 'label' => 'Redirect Entry', 'style' => 'white-space: pre; width: 100%; min-height: 300px'));

    $config->onSave(function($data) {
        $links = (array) array_filter((array) explode("\n", (string) @$data['redirect_input']));
        $new_redirects = array();
        foreach ($links as $link) {
            list($from, $to) = preg_split("/\s+/", $link);
            $new_redirects[] = (object) array(
                        'from' => $from,
                        'to' => $to,
                        'type' => 301
            );
        }
        update_option('ardent_redirects', $new_redirects);
        echo 'reload';
    });

    $redirects = get_option('ardent_redirects');
    $current = trim(\Ardent\Url::Current());
    $active = wp_list_filter($redirects, array('from' => $current));

    if (count($active)) {   // active redirect, let's do this
        $r = array_shift($active);
        header("Location: {$r->to}", true, $r->type ? : 301);
        exit;
    }
});
