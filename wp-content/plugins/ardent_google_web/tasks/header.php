<?php

add_action('wp_head', function() {
    $webmaster_code = ardent_google_web_get_setting('google_webmaster_code');
    $analytics_code = ardent_google_web_get_setting('google_analytics_code');

    if ($webmaster_code) {
        echo \Ardent\Html::get(array('type' => 'meta', 'name' => 'google-site-verification', 'content' => $webmaster_code));
    }
    if ($analytics_code) {
        echo $analytics_code;
    }
}, 999);
