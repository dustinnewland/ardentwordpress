<?php

function ardent_google_web_get_setting($setting) {
    $settings_page = apply_filters('ardent_settings_page', 'Settings');
    $settings_subpage = apply_filters('ardent_settings_subpage_google_web', 'Google Web');
    return \Ardent\Wp\Config::getSetting($settings_page, $settings_subpage, $setting);
}

$settings = array(
    'Google Web' => array(
        array('type' => 'textarea', 'name' => 'google_analytics_code', 'label' => 'Google Analytics Code', 'class' => 'full', 'rows' => '10'),
        array('type' => 'input.text', 'name' => 'google_webmaster_code', 'label' => 'Webmaster Verification Code', 'class' => 'full'),
        array('type' => 'input.file', 'name' => 'google_webmaster_file', 'label' => 'Google Webmaster File', 'nosave' => true),
    ),
);

$settings_page = apply_filters('ardent_settings_page', 'Settings');
$settings_subpage = apply_filters('ardent_settings_subpage_google_web', 'Google Web');
$config = \Ardent\Wp\Config::getInstance($settings_page, $settings_subpage);
foreach ($settings as $tab => $fields) {
    foreach ($fields as $field) {
        $config->add_setting($tab, $field);
    }
}
$config->onSave(function($o) {
    $defaults = array('name' => null, 'type' => null, 'tmp_name' => null, 'error' => null, 'size' => null);
    $data = isset($_FILES['google_webmaster_file_upload']) ? $_FILES['google_webmaster_file_upload'] : array();
    $files = array_merge($defaults, $data);
    if ($files['error'] === 0) {    // successful file upload!
        if (move_uploaded_file($files['tmp_name'], ABSPATH . $files['name'])) {
            $oldfile = ABSPATH . $o->google_webmaster_file;
            if (file_exists($oldfile) && is_file($oldfile)) {
                unlink($oldfile);
            }
            $o->google_webmaster_file = $files['name'];
        }
    }
});
