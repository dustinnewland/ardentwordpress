<?php

/**
 * Plugin Name: Ardent Google Web
 * Plugin URI: http://www.ardentcreative.com/
 * Description: Adds Google Web Tools functionality
 * Version: 0.2.1
 * Author: Dustin Newland
 * Author URI: http://www.ardentcreative.com/
 */

add_action('plugins_loaded', function() {
    $dir = dirname(__FILE__);
    if (!defined('ARDENT_FRAMEWORK')) {
        require_once("{$dir}/deps/ardent_install_plugin.php");
        ardent_install_plugin('ardent_framework', 'http://stage.ardentcreative.com/updates/plugin/ardent_framework/download');
    }
    require_once("{$dir}/tasks/settings.php");
    require_once("{$dir}/tasks/header.php");
});
