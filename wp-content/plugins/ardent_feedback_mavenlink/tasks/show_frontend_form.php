<?php

$config = \Ardent\Feedback::getConfig();
if (!$config->feedback_show_frontend)
{
	return;
}

switch ($config->feedback_show_role)
{
	case 'subscriber':
		if (!is_user_logged_in())
		{
			return;
		}
		break;
	default:
		if (!current_user_can('administrator'))
		{
			return;
		}
		break;
}
// Alright, we are configured to show the frontend form
// Load up our frontend form
//add_action('wp_footer', array('\\Ardent\\Feedback\\Markup', 'FeedbackForm'));
add_action('wp_footer', array('\\Ardent\\Feedback\\Form', 'Display'));

// Style is so it looks fancy, Load up any js we might need
add_action('wp_enqueue_scripts', array('\\Ardent\\Feedback\\Markup', 'EnqueueResources'));
add_action('init', array('\\Ardent\\Feedback\\Form', 'Process'));
