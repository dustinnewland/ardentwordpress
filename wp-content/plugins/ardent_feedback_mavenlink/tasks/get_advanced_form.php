<?php

if (!isset($_REQUEST['ardent_feedback_mavenlink_advanced_form']))
{
	return;
}

$user = wp_get_current_user();
$oauth_token = $user->ID ? get_user_meta($user->ID, 'mavenlink_oauth_token', true) : null;
$template_slug = 'form-advanced';
$template_override = locate_template(array("{$template_slug}.php"), false);
if ($template_override)
{
	get_template_part($template_slug);
}
else
{
	require(constant('ARDENT_FEEDBACK_DIR') . "/template/{$template_slug}.php");
}
exit;