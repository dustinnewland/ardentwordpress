<?php

if (!is_admin()) {
	return;
}
$settings = array(
	'Configure' => array(
		'Permissions' => array(
			array('type' => 'input.checkbox', 'name' => 'feedback_show_frontend', 'label' => 'Show Frontend Form'),
			array('type' => 'select', 'name' => 'feedback_show_role', 'label' => 'Show Feedback Form to', 'options' => \Ardent\Feedback\Options::Roles()),
			array('type' => 'input.checkbox', 'name' => 'feedback_show_client_advanced', 'label' => 'Show Advanced Form to clients')
		),
		'Form Fields' => array(
			array('type' => 'input.checkbox', 'name' => 'feedback_hide_task_mode', 'label' => 'Hide Task Entry Mode'),
			array('type' => 'input.checkbox', 'name' => 'feedback_hide_assignment', 'label' => 'Hide Task Assignment'),
			array('type' => 'input.checkbox', 'name' => 'feedback_hide_type', 'label' => 'Hide Task Type'),
			array('type' => 'input.checkbox', 'name' => 'feedback_hide_start', 'label' => 'Hide Start Date'),
			array('type' => 'input.checkbox', 'name' => 'feedback_hide_due', 'label' => 'Hide Due Date'),
		),
	),
	'Design' => array(
		'Customization' => array(
			array('type' => 'input.wp.media', 'name' => 'custom_icon', 'label' => 'Custom Icon'),
		),
		'Location' => array(
			array('type' => 'select', 'name' => 'feedback_corner', 'label' => 'Show Feedback Form at', 'options' => \Ardent\Feedback\Options::Corners()),
		),
		'Margins' => array(
			array('type' => 'input.text', 'name' => 'feedback_margin', 'label' => 'Form Margin (px)'),
			array('type' => 'input.text', 'name' => 'feedback_top_margin', 'label' => 'Additional Top Margin (px)'),
			array('type' => 'input.text', 'name' => 'feedback_left_margin', 'label' => 'Additional Left Margin (px)'),
			array('type' => 'input.text', 'name' => 'feedback_right_margin', 'label' => 'Additional Right Margin (px)'),
			array('type' => 'input.text', 'name' => 'feedback_bottom_margin', 'label' => 'Additional Bottom Margin (px)'),
		)
	),
);


$config = \Ardent\Feedback::getConfig();
if (in_array(\Ardent\Url::Current(), $config->getUrls())) {
	$oauth_token = \Ardent\Feedback\Mavenlink::getToken();
	$mavenlink_section_title = (!$oauth_token || !$config->default_workspace) ? '<span style="background-color: red; color: white;">Mavenlink</span>' : 'Mavenlink';
	if ($oauth_token) {
		$workspaces = \Ardent\Feedback\Mavenlink::getAllWorkspaces();
		uasort($workspaces, function($a, $b) {
			return strcmp($a->title, $b->title);
		});
		$workspace_options = array('' => 'Select...');
		foreach ($workspaces as $wsid => $data) {
			$workspace_options[$wsid] = $data->title . " ({$data->status->message})";
		}
		// Let's build the workspace list
		$settings[$mavenlink_section_title] = array(
			array(
				'type' => 'a',
				'href' => \Ardent\Feedback\Mavenlink::getDisconnectUrl(),
				'text' => 'Remove OAuth Token'
			),
			array(
				'type' => 'select',
				'name' => 'default_workspace',
				'options' => $workspace_options,
				'label' => 'Default Workspace'
			),
		);
		if ($config->default_workspace) {
		$settings[$mavenlink_section_title][] = [
			'type' => 'select',
			'name' => 'default_parent_task',
			'label' => 'Parent Task',
			'options' => \Ardent\Feedback\Mavenlink::getParentTasks()
		];
//			$settings['Mavenlink'][] = array(
//				'type' => 'div',
//				'text' => print_r(\Ardent\Feedback\Mavenlink::getAll("users.json?participant_in={$config->default_workspace}"))
//			);
		}
	} else {
		$settings[$mavenlink_section_title] = array(array(
				'type' => 'a',
				'href' => \Ardent\Feedback\Mavenlink::getAuthUrl(),
				'text' => 'Authorize'
		));
	}
}

$config->set_settings($settings);
