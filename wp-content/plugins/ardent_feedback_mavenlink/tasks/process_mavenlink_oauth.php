<?php

$code = isset($_REQUEST['ardent_feedback_mavenlink_token']) ? $_REQUEST['ardent_feedback_mavenlink_token'] : null;
if (empty($code))
{
	return;
}
$user = wp_get_current_user();

if (!$user->ID)
{
	return;
}
update_user_meta($user->ID, 'mavenlink_oauth_token', $code);
wp_redirect(remove_query_arg('ardent_feedback_mavenlink_token'));
exit;
