<?php

if (!isset($_REQUEST['ardent_feedback_mavenlink_disconnect']) || !$_REQUEST['ardent_feedback_mavenlink_disconnect'])
{
	return;
}
$user = wp_get_current_user();
if (!@$user->ID)
{
	return;
}
delete_user_meta($user->ID, 'mavenlink_oauth_token');
wp_redirect(remove_query_arg('ardent_feedback_mavenlink_disconnect'));
exit;
