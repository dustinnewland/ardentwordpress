(function ($) {
	$(function () {
		var $container = $('#feedback-form-container');
		var $form = $('#feedback-form-form');
		var $form_button = $('#feedback-form-button');
		var $advanced_button = $container.find('.feedback-form-advanced-button');
		var $advanced_form = $container.find('.feedback-form-advanced');
		$form_button.on('click', function (ev, ui) {
			$form.slideToggle();
		});
		$advanced_button.on('click', function (ev) {
			if (!$advanced_form.is(':visible')) {
				$advanced_button.addClass('loading');
				$advanced_form.load('?ardent_feedback_mavenlink_advanced_form=1', function () {
					$advanced_form.append('<hr />').stop().slideDown();
					$advanced_button.removeClass('loading');
				});
			} else {
				$advanced_form.stop().slideUp();
			}
		});
		var broken = $container.data('feedback-broken');
		var tem = Cookies.get('afeedback_task_entry_mode');
		if (tem || broken) {
			$form_button.click();
			if (tem) {
				$advanced_form.show();
			}
		}
	});
})(jQuery);