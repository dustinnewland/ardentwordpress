<?php

global $ardent_install_plugin_cache;

if (!function_exists('ardent_install_plugin')) {

    function ardent_install_plugin($plugin, $url) {
        if (defined('WP_INSTALLING')) {
            return;
        }
        if (isset($ardent_install_plugin_cache[$plugin])) {
            return true;
        }
        $ardent_install_plugin_cache[$plugin] = true;

        if (!function_exists('get_plugins')) {
            require_once(ABSPATH . 'wp-admin/includes/plugin.php');
        }
        $plugin_files = array_keys(get_plugins());
        $plugins = array_map(function($a) {
            $i = strpos($a, '/');
            if ($i === false) {
                return $a;
            }
            return substr($a, 0, $i);
        }, $plugin_files);
        if (!in_array($plugin, $plugins)) {
            // we need to install our depencency
            $output_dir = ABSPATH . PLUGINDIR;
            $contents = file_get_contents($url);
            if (!$contents) {
                return;
            }
            $z = new ZipArchive();
            $n = tempnam(sys_get_temp_dir(), 'archive');
            file_put_contents($n, $contents);
            $z->open($n, ZIPARCHIVE::CHECKCONS);
            $z->extractTo($output_dir);
            unlink($n);
            wp_cache_delete('plugins', 'plugins');
        }
        $new_plugin_files = array_keys(get_plugins());
        foreach ($new_plugin_files as $plugin_file) {
            if (strpos($plugin_file, "{$plugin}/") !== false) {
                add_filter('plugin_action_links', function($action, $file, $data, $context) use ($plugin_file) {
                    if ($file == $plugin_file) {
                        unset($action['deactivate']);
                        unset($action['delete']);
                        unset($action['edit']);
                    }
                    return $action;
                }, 10, 4);
                if (!is_plugin_active($plugin_file)) {
                    activate_plugin($plugin_file);
                }
            }
        }
    }

}
