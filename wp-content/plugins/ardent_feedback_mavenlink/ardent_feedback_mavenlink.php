<?php

/**
 * Plugin Name: Ardent Feedback - Mavenlink Edition
 * Plugin URI: http://www.ardentcreative.com/
 * Description: Allows clients to post issues to mavenlink
 * Version: 1.0
 * Author: Dustin Newland
 * Author URI: http://www.ardentcreative.com/
 */
define('ARDENT_FEEDBACK_MAVENLINK_URL', site_url(substr(__DIR__, strlen(ABSPATH))));
define('ARDENT_FEEDBACK_DIR', __DIR__);

add_action('plugins_loaded', function() {
	if (!defined('ARDENT_FRAMEWORK')) {
		require_once(constant('ARDENT_FEEDBACK_DIR') . "/deps/ardent_install_plugin.php");
		ardent_install_plugin('ardent_framework', 'http://stage.ardentcreative.com/updates/plugin/ardent_framework/download');
	}
	\Ardent\Autoloader::addPath(constant('ARDENT_FEEDBACK_DIR') . '/classes/');
	$config = \Ardent\Feedback::getConfig();
	require_once(constant('ARDENT_FEEDBACK_DIR') . "/tasks/settings.php");
	require_once(constant('ARDENT_FEEDBACK_DIR') . "/tasks/show_frontend_form.php");
	require_once(constant('ARDENT_FEEDBACK_DIR') . "/tasks/process_mavenlink_oauth.php");
	require_once(constant('ARDENT_FEEDBACK_DIR') . "/tasks/disconnect_mavenlink_oauth.php");
	require_once(constant('ARDENT_FEEDBACK_DIR') . "/tasks/get_advanced_form.php");
});
