<?php

$config = \Ardent\Feedback::getConfig();
if (!$config->default_workspace)
{
	return;
}
$ws_data = \Ardent\Feedback\Mavenlink::get("workspaces/{$config->default_workspace}.json");
if (!isset($ws_data->count) || !$ws_data->count)
{
	return; // No workspaces found :(
}
$permissions = $ws_data->{$ws_data->results[0]->key}->{$ws_data->results[0]->id}->permissions;
if (!is_object($permissions))
{
	return;
}
if ($permissions->user_is_client && !$config->feedback_show_client_advanced)
{
	return;
}
$mvn_users = \Ardent\Feedback\Mavenlink::getAll("users.json?participant_in={$config->default_workspace}");
$user_options = array();
foreach ($mvn_users as $mvnu)
{
	$user_options[] = (object) array('value' => $mvnu->id, 'title' => $mvnu->full_name);
}
$cookie_data = (object) json_decode(stripslashes(@$_COOKIE['afeedback_task_entry_mode']));

$form_fields = array_filter(array(
	$config->feedback_hide_task_mode ? '' : array('type' => 'input.checkbox', 'name' => 'afeedback_task_entry_mode', 'label' => 'Task Entry Mode', 'value' => @$_COOKIE['afeedback_task_entry_mode']),
	$config->feedback_hide_task_mode ? '' : array('type' => 'br'),
	$config->feedback_hide_assignment ? '' : array('type' => 'select', 'name' => 'afeedback_user', 'label' => 'Assign to', 'multiple' => true, 'options' => array('' => 'Ardent Creative Lead', 'unassigned' => 'Unassigned') + $user_options, 'value' => @$cookie_data->user),
	$config->feedback_hide_type ? '' : array('type' => 'select', 'name' => 'afeedback_type', 'label' => 'Task Type', 'options' => \Ardent\Feedback\Options::StoryTypes(), 'value' => @$cookie_data->type),
	$config->feedback_hide_start ? '' : array('type' => 'input.text', 'name' => 'afeedback_start', 'label' => 'Start Date', 'value' => @$cookie_data->start),
	$config->feedback_hide_due ? '' : array('type' => 'input.text', 'name' => 'afeedback_end', 'label' => 'Due Date', 'value' => @$cookie_data->end),
	array('type' => 'input.hidden', 'name' => 'afeedback_advanced', 'value' => 1),
		));

foreach ((array) $form_fields as $field)
{
	echo \Ardent\Html::get($field);
}
return;
