<?php
require_once(constant('ARDENT_FEEDBACK_DIR') . '/deps/mavenlink/mavenlink_api.php');
$current_url = urlencode(\Ardent\Url::Current());
$redirect_uri = urlencode("http://stage.ardentcreative.com/mavenlink_oauth/?site={$current_url}");
$auth_url = "https://app.mavenlink.com/oauth/authorize?response_type=code&client_id=bbb46741021a60dcff079491c041d65b69a63ef6374efbe9d363b2e73ac0e6e8&redirect_uri={$redirect_uri}";
?>
<form method="post" id="feedback-form-container">
	<div id="feedback-form-button"></div>
	<div id="feedback-form-form">
		<a href="<?php echo $auth_url ?>">Authorize</a>
	</div>
</form>
		<?php
