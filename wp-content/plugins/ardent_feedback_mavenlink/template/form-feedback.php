<?php
$config = \Ardent\Feedback::getConfig();
$user = wp_get_current_user();

$title_field = \Ardent\Html::get(array('type' => 'input.text', 'name' => 'afeedback_title', 'label' => 'Issue Title'));
$content_field = \Ardent\Html::get(array('type' => 'textarea', 'name' => 'afeedback_content', 'label' => 'Tell us more!'));
?><form method="post" id="feedback-form-container" data-feedback-broken="<?php echo get_user_meta($user->ID, 'afeedback_broken', true) ?>">
	<div id="feedback-form-button" class="ardent-feedback-form<?php echo $config->custom_icon ? ' noanimation' : '' ?>"></div>
	<div id="feedback-form-form">
		<?php if (!$config->default_workspace || !$config->default_parent_task) { ?>
			<div style="font-size: large; padding: 10px; background: red; color: white; font-weight: bolder;">
				Please configure a default workspace in the backend before trying to submit a task!<br />
				<a href="<?php echo admin_url('admin.php?page=ardent_options-settings-feedback') ?>">Visit Settings Page</a>
			</div>
		<?php } ?>
		<?php if (get_user_meta($user->ID, 'afeedback_broken', true)) { ?>
			<div style="font-size: large; padding: 10px; background: red; color: white; font-weight: bolder;">
				There is probably something wrong with this plugin :( Make sure you verify that your tasks are actually being created!
			</div>
		<?php } ?>
		I'm sorry to hear that you're having issues with your site :(
		Please fill out this form and we can talk about it!<br>
		<?php echo $title_field ?>
		<?php echo $content_field ?>
		<hr>
		<div class="feedback-form-advanced">
			<?php
			if (isset($_COOKIE['afeedback_task_entry_mode'])) {
				$template_slug = 'form-advanced';
				$template_override = locate_template(array("{$template_slug}.php"), false);
				if ($template_override) {
					get_template_part($template_slug);
				} else {
					require(constant('ARDENT_FEEDBACK_DIR') . "/template/{$template_slug}.php");
				}
			}
			?>
		</div>
		<div class="feedback-form-advanced-button">Advanced Settings</div>
		<input type="hidden" name="afeedback_process_form" value="1" />
		<input type="submit" />
	</div>
</form>
		<?php
