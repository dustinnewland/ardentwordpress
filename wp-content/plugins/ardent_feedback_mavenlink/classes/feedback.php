<?php

namespace Ardent;

final class Feedback
{

	public static function getConfig()
	{
		return \Ardent\Wp\Config::getInstance('Settings', 'Feedback');
	}

}
