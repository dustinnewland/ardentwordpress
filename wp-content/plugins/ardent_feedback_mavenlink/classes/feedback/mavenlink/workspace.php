<?php

namespace Ardent\Feedback\Mavenlink;

final class Workspace
{

	private $_id = null;
	private $_data = null;

	public function __construct($id, $data = null)
	{
		$this->_id = $id;
	}

	public static function getLead()
	{
		$project_users = \Ardent\Feedback\Mavenlink::getAll("participations.json?workspace_id={$this->_id}");
		if (!is_array($project_users) || !count($project_users))
		{
			return null;
		}
		$provider_leads = wp_list_filter($project_users, array('role' => 'maven', 'is_team_lead' => 1));
		if (!count($provider_leads))
		{
			return null;
		}
		$lead = array_shift($provider_leads);
		return $lead->user_id;
	}

}
