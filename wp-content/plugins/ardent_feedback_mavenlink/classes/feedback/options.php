<?php

namespace Ardent\Feedback;

final class Options
{

	static function StoryTypes()
	{
		return array(
			'task' => 'Task',
			'deliverable' => 'Deliverable',
			'milestone' => 'Milestone',
			'issue' => 'Issue'
		);
	}

	static function Corners()
	{
		return array(
			'tl' => 'Top Left',
			'tr' => 'Top Right',
			'bl' => 'Bottom Left',
			'br' => 'Bottom Right'
		);
	}

	static function Roles()
	{
		return array(
			'' => 'Administrators',
			'subscriber' => 'Subscribers',
		);
	}

}
