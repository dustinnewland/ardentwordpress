<?php

namespace Ardent\Feedback;

final class Markup {

	public static function EnqueueResources() {
		wp_enqueue_style('ardent-feedback', constant('ARDENT_FEEDBACK_MAVENLINK_URL') . '/css/ardent_feedback.css', array(), '1.2');
		wp_enqueue_script('js-cookie', constant('ARDENT_FEEDBACK_MAVENLINK_URL') . '/js/js.cookie.js');
		wp_enqueue_script('ardent-feedback', constant('ARDENT_FEEDBACK_MAVENLINK_URL') . '/js/ardent_feedback.js', array('jquery', 'js-cookie'));
		$config = \Ardent\Feedback::getConfig();
		$margin = $config->feedback_margin;
		$custom_icon_image_id = $config->custom_icon;
		$custom_image_url = $custom_icon_image_id ? wp_get_attachment_image_url($custom_icon_image_id, 'full') : null;
		?>
		<style type="text/css">
			#feedback-form-container {
		<?php
		if (in_array($config->feedback_corner, array('tl', 'tr'))) {
			?>
					top: <?php echo $margin + (is_admin_bar_showing() ? 32 : 0) + $config->feedback_top_margin ?>px;
				<?php } ?>
				<?php
				if (in_array($config->feedback_corner, array('tl', 'bl'))) {
					?>
					left: <?php echo $margin + $config->feedback_left_margin ?>px;
				<?php } ?>
				<?php
				if (in_array($config->feedback_corner, array('tr', 'br'))) {
					?>
					right: <?php echo $margin + $config->feedback_right_margin ?>px;
				<?php } ?>
				<?php
				if (in_array($config->feedback_corner, array('bl', 'br'))) {
					?>
					bottom: <?php echo $margin + $config->feedback_bottom_margin ?>px;
				<?php } ?>
			}
			#feedback-form-button {
				background: url('<?php echo $custom_image_url ?: constant('ARDENT_FEEDBACK_MAVENLINK_URL').'/images/icon_sequence.png?v=1.1'; ?>') left center no-repeat;
			}
		</style>
				<?php
			}

		}
