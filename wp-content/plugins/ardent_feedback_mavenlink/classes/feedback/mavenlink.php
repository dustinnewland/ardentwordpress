<?php

namespace Ardent\Feedback;

final class Mavenlink {

	private static $_oauth_base = 'https://app.mavenlink.com/oauth/';
	private static $_oauth_response_handler = 'http://stage.ardentcreative.com/mavenlink_oauth/';
	private static $_api = null;
	private static $_per_page = 200;

	public static function getAuthUrl() {
		$current_url = urlencode(\Ardent\Url::Current());
		$redirect_uri = urlencode(self::$_oauth_response_handler . "?site={$current_url}");

		$auth_url = self::$_oauth_base . "authorize?response_type=code&client_id=bbb46741021a60dcff079491c041d65b69a63ef6374efbe9d363b2e73ac0e6e8&redirect_uri={$redirect_uri}";
		return $auth_url;
	}

	public static function getDisconnectUrl() {
		return add_query_arg('ardent_feedback_mavenlink_disconnect', 1);
	}

	public static function getToken() {
		$user = wp_get_current_user();
		if (!$user || !@$user->ID) {
			return null;
		}
		$token = get_user_meta($user->ID, 'mavenlink_oauth_token', true);
		return $token;
	}

	public static function getApi() {
		if (self::$_api === null) {
			require_once(constant('ARDENT_FEEDBACK_DIR') . '/deps/mavenlink/mavenlink_api.php');
			$token = self::getToken();
			if ($token) {
				self::$_api = new \MavenlinkApi($token);
			}
		}
		return self::$_api;
	}

	public static function getAllWorkspaces() {
		return self::getAll('workspaces.json');
	}

	public static function get($url) {
		$api = self::getApi();
		$token = self::getToken();
		if (!$api) { // No need to check token value. It's checked by self::getApi() and reflected in the value of "$api"
			return array();
		}
		$curl = $api->getCurlHandle($api->getBaseUri() . $url, $token);
		$json = curl_exec($curl);

		return json_decode($json);
	}

	public static function getAll($url) {
		$count = self::$_per_page;
		$page = 1;
		$char = strpos($url, '?') === FALSE ? '?' : '&';
		$data = self::get("{$url}{$char}per_page={$count}");
		if (!count($data->results)) {
			return array();
		}
		$key = $data->results[0]->key;
		$remaining = $data->count - $count;
		$workspaces = (array) $data->{$key};
		while ($remaining > 0) {
			$page++;
			$data = self::get("{$url}{$char}per_page={$count}&page={$page}");
			$remaining -= $count;
			$workspaces = array_merge($workspaces, (array) $data->{$key});
		}
		return $workspaces;
	}

	public static function getParentTask() {
		$config = \Ardent\Feedback::getConfig();
		return $config->default_parent_task;
// We need to find a task with a specific name in mavenlink
//		$parent_task = get_option('ardent_feedback_mavenlink_parent_task_id');
		$parent_task = null;
		if (!$parent_task) {
			// Search mavenlink for the task
			$config = \Ardent\Feedback::getConfig();
			if (!$config->default_workspace) {
				return null;
			}
			$stories = self::getAll("stories.json?parents_only=1&workspace_id={$config->default_workspace}");
			foreach ($stories as $story) {
				if ($story->title !== 'Website Feedback') {
					continue;
				}
				$parent_task = $story->id;
			}
			if (!$parent_task) {
				// Lets create a new parent task in mavenlink
				$api = \Ardent\Feedback\Mavenlink::getApi();
				$task_data = json_decode($api->createStoryForWorkspace($config->default_workspace, array(
							'story[story_type]' => 'issue',
							'story[title]' => "Website Feedback",
				)));
				if (count($task_data->results)) {
					$parent_task = $task_data->results[0]->id;
				}
			}
		}
		return $parent_task;
	}

	public static function getParentTasks() {
		$config = \Ardent\Feedback::getConfig();
		$options = [];
		$top_tasks = \Ardent\Feedback\Mavenlink::getAll("stories.json?parents_only=1&workspace_id={$config->default_workspace}");
		foreach ($top_tasks as $top_task) {
			$options[$top_task->id] = $top_task->title;
			$sub_tasks = \Ardent\Feedback\Mavenlink::getAll("stories.json?workspace_id={$config->default_workspace}&with_parent_id={$top_task->id}");
			if (count($sub_tasks)) {
				$options[$top_task->title] = [];
				foreach ($sub_tasks as $subtask) {
					$options[$top_task->title][$subtask->id] = $subtask->title;
				}
			}
		}
		return $options;
	}

	public static function getWorkspaceLead($workspace_id) {

		$project_users = self::getAll("participations.json?workspace_id={$workspace_id}");
		if (!is_array($project_users) || !count($project_users)) {
			return null;
		}
		$provider_leads = wp_list_filter($project_users, array('role' => 'maven', 'is_team_lead' => 1));
		if (!count($provider_leads)) {
			return null;
		}
		$lead = array_shift($provider_leads);
		return $lead->user_id;
	}

	public function assignTask($task_id, $user_id) {

		$api = self::getApi();
		$token = self::getToken();
		if (!$api) { // No need to check token value. It's checked by self::getApi() and reflected in the value of "$api"
			return array();
		}
		$assignment_params = array(
			'assignment[story_id]' => $task_id,
			'assignment[assignee_id]' => $user_id,
		);
		$curl = $api->createPostRequest($api->getBaseUri() . 'assignments.json', $token, $assignment_params);
		$json = curl_exec($curl);

		return json_decode($json);
	}

}
